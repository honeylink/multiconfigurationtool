﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;

namespace M300Config
{
    public partial class SystemConfig : Form
    {
        bool AllSelect = true;//禁止选项卡
        int ReadType = 0;//响应类型
        int SendType = 0;
        public  HusbHelp HusbHelp;
        int sendNum = 0;
        int ReSendNum = 0; 
        string DviceType = "";
        private string KeyName = ""; 
        byte[] ConfigurationReadBuffer = new byte[872];
        int RelCount = 0;
        cmdModel sendModel;
        List<SendModel> listSendModel;
        public delegate void GdvAddItemCallback();
        public delegate void SetModelCallBack();
        bool IsPROGRAM = false;//配置模式
        TestForm test;//测试模式对象
        public SystemConfig(HusbHelp husbHelp )
        {
          
            HusbHelp = husbHelp; 
            InitializeComponent();
        }
       
        List<ConfigModel> list = new List<ConfigModel>();
        List<cmdModel> cmdlist=new List<cmdModel>();
        public Hashtable Ht = new Hashtable();
        SelectLanguage Language = new SelectLanguage();

        /// <summary>
        /// 读取参数
        /// </summary>
        private void Read()
        {
            try
            {
                AppCmd cmd = new AppCmd();
                //GetModel();
                //cmd.WriteConfig(Ht);
                ReadType = 3;
                cmdlist = cmd.ReadConfigCmd();
                packageSendcmd("R");
                HusbHelp.cmdType = 4;
                if (HusbHelp.IsOpen)
                {
                    progressBar1.Value = 30;
                    SendType = 1;
                    HusbHelp.SendData(listSendModel[0].Cmdbyte);
                }
                else
                {
                    MessagShow(Language.getMsg("CmdExceptionMsg"));
                }

                sendNum = 0;
                sendNum = sendNum + 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Read exception:" + ex);
                throw ex;
            }
        }
        private void bDataRead(byte[] data)
        {
            switch (ReadType)
            {
                case 3:
                    if (sendNum == listSendModel.Count)
                    {
                        Array.Copy(data, 0, ConfigurationReadBuffer, listSendModel[sendNum - 1].Sort - data.Length, data.Length);
                        SendType = 0;
                        ReadType = 0;

                        //数据读取完成

                        //读取设置文件，根据文件中的地址项解析数据，并更新hash table
                        //解析完成后根据名称赋值控件
                        ConfigurationReadBufferAnalyse();

                        try
                        {
                            if (InvokeRequired)
                            {
                                this.BeginInvoke(new SetModelCallBack(ReadConfigurationUpdateUI));//异步执行  
                            }
                            else
                            {
                                ReadConfigurationUpdateUI();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }
                    else
                    {
                        //读取到部分数据
                        Array.Copy(data, 0, ConfigurationReadBuffer, listSendModel[sendNum - 1].Sort - 32, data.Length);
                        HusbHelp.cmdType = 4;
                        if (HusbHelp.IsOpen)
                        {
                            if (sendNum == listSendModel.Count / 3)
                                this.BeginInvoke(new SetModelCallBack(SetprogressBar));//异步执行    
                            HusbHelp.SendData(listSendModel[sendNum].Cmdbyte);
                        }
                        else
                        {
                            MessagShow(Language.getMsg("CmdExceptionMsg"));
                        }
                        sendNum = sendNum + 1;
                    }
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 解析组合指令
        /// </summary>
        public void packageSendcmd(string type)
        {
            //读取所有的
            List<cmdModel> list = new List<cmdModel>();
            listSendModel = new List<SendModel>();
            int starAddress = 0;//起始地址
            AppCmd cmd = new AppCmd();
            int i = 0;
            switch (type)
            {
                case "R":
                    cmdModel model = cmd.ReadConfigCmd().Where(l => l.Type != "5" && l.Address == "0x6a0").OrderByDescending(l => l.Sort).FirstOrDefault();
                    //得到最后一个命令地址长度
                    int maxAddress = Convert.ToInt32(model.Address, 16);
                    //计算最后一个包的总长度（其中172字节长度属于自定义）
                    int maxLengths = maxAddress + 172;//计算包总长度
                        ConfigurationReadBuffer = new byte[maxLengths];
                    maxAddress = maxAddress + 1;
                    while (starAddress <= maxAddress)
                    {
                        SendModel sModel = new SendModel();
                        //组装数据从0x000->0x366    
                        int byteLength = 0;
                        byte[] address = intTobytenew(starAddress);
                        //每包递减
                        if (maxAddress - starAddress < 32)//获取小于16字节的
                        {
                            byteLength = maxAddress - starAddress;
                            maxAddress = 0;
                            starAddress = starAddress + byteLength;
                        }
                        else//获取大于16节的
                        {
                            byteLength = 32;
                            starAddress = starAddress + byteLength;
                        }
                        byte[] bLength = intTobytenew(byteLength);
                        int totalLength = 4;
                        byte[] data = new byte[totalLength];
                        data[0] = HusbHelp.HexStringToByte("R")[0];
                        data[1] = address[0];
                        data[2] = address[1];
                        data[3] = bLength[1];
                        sModel.Cmdbyte = data;
                        sModel.Sort = starAddress;
                        listSendModel.Add(sModel);
                        i++;
                    }
                    //组合
                    break;
                case "w":
                    string t = "";
                    int starIndex = 0;
                    int j = 0;
                    string name = "";
                    try
                    {
                        List<cmdModel> cmdList = cmd.ReadConfigCmd().OrderBy(l => l.Sort).ToList<cmdModel>();
                        cmdModel maxModel = cmdList[cmdList.Count - 1];   //获取最后一项，其地址值+4即为整个参数缓冲区大小
                        int maxLength = Convert.ToInt32(maxModel.Address, 16) + 100;//计算包总长度
                        ConfigurationReadBuffer = new byte[maxLength];
                        byte[] wData = new byte[maxLength];      //参数待写入缓冲区
                        string sel1 = "";
                        string sel = "";
                        foreach (cmdModel cModel in cmdList)
                        {
                            t = cModel.Type;
                            int Max = Convert.ToInt32(cModel.Max);
                            name = cModel.CmdName;
                            byte[] val;

                            //根据每个设置命令的名称进行分别的处理
                            //先判断是否为特殊名称
                            if (cModel.CmdName == "contactType_数据协议_42")
                            {
                                if (cModel.Val == "HRT100")
                                {
                                    sel = "1" + sel;
                                }
                                else
                                {
                                    sel = "0" + sel;
                                }
                                //待处理bit
                                continue;
                            }
                            else if (cModel.CmdName == "connectionType_网络协议_43")
                            {

                                if (cModel.Val == "UDP")
                                {
                                    sel = sel + "1";
                                }
                                else
                                {
                                    sel = sel + "0";
                                }
                                //待处理bit//在    cModel.CmdName == "isGPRS_启用GPRS_44"   处进行统一变更处理
                                continue;
                            }
                            else if (cModel.CmdName == "unit_温度单位_34")
                            {
                                byte[] vals = intTobytenew(int.Parse(cModel.Val));
                                val = new byte[1];
                                Array.Copy(vals, 1, val, 0, 1);
                            }
                            else if (cModel.CmdName == "everyOutTime_每日10时发送日报信息_2")
                            {
                                if (cModel.Val == "1")
                                {
                                    sel1 = sel1 + "1";
                                }
                                else
                                {
                                    sel1 = sel1 + "0";
                                }
                                continue;
                            }
                            else if (cModel.CmdName == "alarmPhone_报警前拨打电话_2.1")
                            {
                                if (cModel.Val == "1")
                                {
                                    sel1 = "1" + sel1;
                                }
                                else
                                {
                                    sel1 = "0" + sel1;
                                }
                                continue;
                            }
                            else if (cModel.CmdName == "allowClose_是否允许本地关机_2.2")
                            {
                                if (cModel.Val == "1")
                                {
                                    sel1 = "1" + sel1;
                                }
                                else
                                {
                                    sel1 = "0" + sel1;
                                }
                                byte[] vals = intTobytenew(Convert.ToInt32(sel1, 2));
                                val = new byte[1];
                                Array.Copy(vals, 1, val, 0, 1);
                            }
                            else if (cModel.CmdName == "isGPRS_启用GPRS_44")
                            {
                                if (cModel.Val == Language.getMsg("Y"))
                                {
                                    sel = "1" + sel;
                                }
                                else
                                {
                                    sel = "0" + sel;
                                }

                                byte[] vals = intTobytenew(Convert.ToInt32(sel, 2));
                                val = new byte[1];
                                Array.Copy(vals, 1, val, 0, 1);
                            }
                            else if (cModel.CmdName == "adress_安装位置_3")
                            {
                                if (Global.Language == "English")
                                {
                                    val = System.Text.Encoding.ASCII.GetBytes(cModel.Val);
                                }
                                else
                                {
                                    val = stringToUCS2(cModel.Val);
                                }
                            }
                            else if (cModel.CmdName == "Description")
                            {
                                if (Global.Language == "English")
                                {
                                    val = System.Text.Encoding.ASCII.GetBytes(cModel.Val);
                                }
                                else
                                {
                                    val = HusbHelp.HexStringToByte(cModel.Val);
                                }
                            }
                            else
                            {
                                //处理通用种类
                                if (cModel.Type == "1")//将整数字符串转换成byte字节序列
                                {
                                    val = new byte[Max];
                                    int num = 0;
                                    int.TryParse(cModel.Val, out num);
                                    byte[] vals = intTobyteold(num);
                                    Array.Copy(vals, 0, val, 0, Max);
                                }
                                else if (cModel.Type == "2")//将浮点数字符串转换成byte字节序列
                                {
                                    float num = 0;
                                    float.TryParse(cModel.Val, out num);
                                    val = floatTobyte(num);
                                }
                                else if (cModel.Type == "3") //将字符串按照语言转换成UCS2字节序列
                                {
                                    val = stringToUCS2(cModel.Val);
                                }
                                else if (cModel.Type == "5")//特殊种类
                                {
                                    val = new byte[Max];
                                    byte[] vals = cmd.strToToHexByte(cModel.Val);//cMode.Val必须要有指定的Max长
                                    Array.Copy(vals, 0, val, 0, Max);
                                }
                                else
                                {
                                    val = HusbHelp.HexStringToByte(cModel.Val);//直接将16进制字符串转换成byte
                                }
                            }

                            if (Max > val.Length)
                            {
                                Max = val.Length;
                            }

                            //将数据值赋值到wData
                            starIndex = Convert.ToInt32(cModel.Address, 16);//将地址字符串转换成数字

                            Array.Copy(val, 0, wData, starIndex, Max);//将此单项参数的值复制到缓冲区中的对应的偏移处   
                        }

                        //将整个缓冲区分包进行发送
                        starIndex = 0;
                        while (starIndex < maxLength)
                        {
                            SendModel sModel = new SendModel();
                            int byteLength = 0;//本包长度
                            int CheckCode = 0;//校验码

                            if (maxLength - starIndex < 32)//获取小于16字节的
                            {
                                byteLength = maxLength - starIndex;
                            }
                            else//获取大于16节的
                            {
                                byteLength = 32;
                            }
                            byte[] data = new byte[byteLength];
                            Array.Copy(wData, starIndex, data, 0, byteLength);
                            byte[] address = intTobytenew(starIndex);//得到地址 
                            byte[] bLength = intTobytenew(byteLength); //得到数据长度  
                            foreach (byte v in address)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            foreach (byte v in bLength)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            foreach (byte v in data)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            byte[] Bcheck = BitConverter.GetBytes(CheckCode);
                            byte[] sendData = new byte[4 + data.Length + 1];
                            sendData[0] = HusbHelp.HexStringToByte("w")[0];
                            sendData[1] = address[0];
                            sendData[2] = address[1];
                            Array.Copy(bLength, 1, sendData, 3, 1);
                            Array.Copy(data, 0, sendData, 4, byteLength);
                            Array.Copy(Bcheck, 0, sendData, 4 + byteLength, 1);
                            starIndex = starIndex + byteLength;
                            sModel.Cmdbyte = sendData;
                            listSendModel.Add(sModel);
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    break;
                default:
                    break;
            }
            listSendModel = listSendModel;
        }
        public void SystemConfig_Load(object sender, EventArgs e)
        {
            int w = System.Windows.Forms.SystemInformation.WorkingArea.Width;
            int h = System.Windows.Forms.SystemInformation.WorkingArea.Height;
            this.Location = new Point(w / 2 - 250, h / 2 - 180);
            this.StartPosition = FormStartPosition.CenterScreen;
            Language.SetLanguage(this);
            HusbHelp.DataResponse += new M300Config.HusbHelp.DataResponseEventHandler(DataRead);
            HusbHelp.BDataResponse += new M300Config.HusbHelp.BDataResponseEventHandler(bDataRead);
            DviceType = HusbHelp.dType;
            TitlePanel.TabIndex = 0;
            tabControl1.TabIndex = 10;
            timer1.Start();
            if (Global.Language == "English")
            {
                this.txtAdress_nlx.MaxLength = 100;
                this.txtMonitor1_nlx.MaxLength = 32;
                this.txtMonitor2_nlx.MaxLength = 32;
                this.txtMonitor3_nlx.MaxLength = 32;
                this.txtMonitor4_nlx.MaxLength = 32;
                this.txtDescription.MaxLength = 96;
                this.txtTrackingNumber.MaxLength = 16;
                this.txtCartonNumber.MaxLength = 16;
                this.txtConsignee.MaxLength = 16;
                this.txtConsignor.MaxLength = 16;
            }
            else
            {
                this.txtAdress_nlx.MaxLength = 50;
                this.txtMonitor1_nlx.MaxLength = 42;
                this.txtMonitor2_nlx.MaxLength = 42;
                this.txtMonitor3_nlx.MaxLength = 42;
                this.txtMonitor4_nlx.MaxLength = 42;
                this.txtTrackingNumber.MaxLength = 16;
                this.txtCartonNumber.MaxLength = 16;
                this.txtConsignee.MaxLength = 16;
                this.txtConsignor.MaxLength = 16;
                this.txtDescription.MaxLength = 96;
            }
            this.cbContactType_nlx.SelectedIndex = 0;
            this.cbConnectionType_nlx.SelectedIndex = 0;

            dataGridView1.AutoGenerateColumns = false;
            label15.Text = Language.getMsg("TrackingNumber");
            label16.Text = Language.getMsg("CartonNumber");
            label18.Text = Language.getMsg("Consignor");
            label51.Text = Language.getMsg("Consignee");
            label68.Text = Language.getMsg("Description");
            label69.Text = "是否导出PDF：";
            if (DviceType.IndexOf("TH_T_T") > 0)
            {
                panelMonitor1.Enabled = true;
                txtMonitor1_nlx.Text = Language.getMsg("T1");
                panelMonitor2.Enabled = true;
                txtMonitor2_nlx.Text = Language.getMsg("H");
                panelMonitor3.Enabled = true;
                txtMonitor3_nlx.Text = Language.getMsg("T2");
                panelMonitor4.Enabled = true;
                txtMonitor4_nlx.Text = Language.getMsg("T3");
            }
            else if (DviceType.IndexOf("TH_T") > 0)
            {
                panelMonitor1.Enabled = true;
                txtMonitor1_nlx.Text = Language.getMsg("T1");
                panelMonitor2.Enabled = true;
                txtMonitor2_nlx.Text = Language.getMsg("H");
                panelMonitor3.Enabled = true;
                txtMonitor3_nlx.Text = Language.getMsg("T2");
            }
            else if (DviceType.IndexOf("T_T") > 0)
            {
                panelMonitor3.Enabled = true;
                txtMonitor3_nlx.Text = Language.getMsg("T1");
                panelMonitor1.Enabled = true;
                txtMonitor4_nlx.Text = Language.getMsg("T2");
            }
            else if (DviceType.IndexOf("TH_0") > 0)
            {
                txtMonitor1_nlx.Text = Language.getMsg("T1");
                panelMonitor1.Enabled = true;
                txtMonitor2_nlx.Text = Language.getMsg("H");
                panelMonitor2.Enabled = true;
            }
            else if (DviceType.IndexOf("T_0") > 0)
            {
                panelMonitor1.Enabled = true;
                txtMonitor3_nlx.Text = Language.getMsg("T1");

            }
            else
            {
                txtMonitor1_nlx.Text = Language.getMsg("T1");
                txtMonitor2_nlx.Text = Language.getMsg("H");
                txtMonitor3_nlx.Text = Language.getMsg("T2");
                txtMonitor4_nlx.Text = Language.getMsg("T3");
                MessagShow(Language.getMsg("DeviceTypeErrorMsg"));
                this.Close();
            }

            this.Text = Language.getMsg("DeviceTypeMsg") + " " + DviceType;
            
            this.cmbAllowClose_nlx.Items.Add(Language.getMsg("No Permit"));
            this.cmbAllowClose_nlx.Items.Add(Language.getMsg("Permit"));
            this.cmbAllowClose_nlx.SelectedIndex = 0;

            this.comboBoxPrintMode_nlx.Items.Add(Language.getMsg("All"));
            this.comboBoxPrintMode_nlx.Items.Add(Language.getMsg("Recent Specific Count"));
            this.comboBoxPrintMode_nlx.SelectedIndex = 0;
            
            this.cmbUnit_nlx.Items.Add(Language.getMsg("Centigrade"));
            this.cmbUnit_nlx.Items.Add(Language.getMsg("Degrees Fahrenheit"));
            this.cmbUnit_nlx.SelectedIndex = 0;
            
            this.cbUseGPRS_nlx.Items.Add(Language.getMsg("Y"));
            this.cbUseGPRS_nlx.Items.Add(Language.getMsg("N"));         
            this.cbUseGPRS_nlx.SelectedIndex = 0;
            
            this.Show();

            this.txtDId_nlx.Focus();
            AppCmd cmd = new AppCmd();
            cmd.ReadConfig(ref Ht, Application.StartupPath + "\\set.xml");
            SetModel(true);
            //读取设备信息
            ReadDeviceInfo();
        }
        public void ConfigurationReadBufferAnalyse()
        {
            cmdModel model;
            try
            { 
                AppCmd cmd = new AppCmd();

                //读取配置文件，组成列表
                List<cmdModel> listc= cmd.ReadConfigCmd();
           
                //根据buffer，对列表进行更新
                for (int i = 0; i < listc.Count; i++)
                {
                    if (i == 68)
                    {

                    }
                    if(i ==69)
                    {

                    }
                   model = listc[i]; 
                   int Address = Convert.ToInt32(model.Address, 16);
                   int length = Convert.ToInt32(model.Max);
                   byte[] valueBuffer = new byte[length];
                   Array.Copy(ConfigurationReadBuffer, Address, valueBuffer, 0, length);
                   string val = "";
                   if (model.CmdName == "allowClose_是否允许本地关机_2.2")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);

                       if (Sel == 4 || Sel == 5 || Sel == 6 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else if (model.CmdName == "contactType_数据协议_42")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 2 || Sel == 3 || Sel == 7)
                       {
                           val = "HRT100";
                       }
                       else
                       {
                           val = "modbus/tcp";
                       }
                   }
                    else if (model.CmdName == "connectionType_网络协议_43")
                   {//!@todo 修改成与操作实现
                        int Sel = 0;
                        int.TryParse(valueBuffer[0].ToString(), out Sel);
                        if (Sel == 1 || Sel == 3 || Sel == 5 || Sel == 7)//这个代码写的稀烂的，直接使用bit与操作即可
                    
                        {
                            val = "UDP";
                        }
                        else
                        {
                            val = "TCP";
                        }
                    }
                   else if (model.CmdName == "isGPRS_启用GPRS_44")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 4 || Sel == 6 || Sel == 7 || Sel == 5)
                       {
                           val = Language.getMsg("Y");
                       }
                       else
                       {
                           val = Language.getMsg("N");
                       }
                   }
                   else if (model.CmdName == "alarmPhone_报警前拨打电话_2.1")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 2 || Sel == 3 || Sel == 6 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else if (model.CmdName == "everyOutTime_每日10时发送日报信息_2")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 1 || Sel == 3 || Sel == 5 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else
                   {
                       if (model.Type == "1")
                       {
                           byte[] vals = new byte[4];
                           Array.Copy(valueBuffer, 0, vals, 0, valueBuffer.Length);
                           uint num = 0;
                           num = BitConverter.ToUInt32(vals, 0);
                           val = num.ToString();
                       }
                       else if (model.Type == "2")
                       {
                           float num = 0;
                           num = BitConverter.ToSingle(valueBuffer, 0);
                           val = num.ToString();
                       }
                       else if (model.Type == "3")
                       {
                           val = UCS2Tostring(valueBuffer);                           
                       }
                       else
                       {
                           val = Encoding.UTF8.GetString(valueBuffer);
                       }
                   }
                   Ht[model.CmdName] = val;
                }
            }
            catch (Exception ex)
            {
                Exception s=ex;
            }  
        }
        public void ReadConfigurationUpdateUI()
        {
            TitlePanel.Visible = false;
            SetModel(false);
            progressBar1.Value = 0;
            MessagShow(Language.getMsg("ReadOK"));
        }
        public void SetprogressBar()
        {
            progressBar1.Value = 100; 
        }
        public void wSetprogressBar()
        {
            
            TitlePanel.Visible = false;
            MessagShow(Language.getMsg("DownOK"));
        }
      
        private void DataRead(string Msg)
        {
            switch (ReadType)
            { 
                case  2:
                    if (Msg == "6")
                    {  
                       
                    }
                    break;
                case 1:
                    if (Msg == "6")
                    {
                        if (sendNum == listSendModel.Count)
                        {
                            ReadType=0;
                            this .BeginInvoke(new SetModelCallBack(wSetprogressBar));//异步执行    
                            SendType = 0;
                          
                            HusbHelp.SendData("start");//重启
                        }
                        else
                        {
                            HusbHelp.cmdType = 3;
                           
                            if (HusbHelp.IsOpen)
                            {
                              if(sendNum==listSendModel.Count/3)
                                    this.BeginInvoke(new SetModelCallBack(SetprogressBar));//异步执行   
                                
                                HusbHelp.SendData(listSendModel[sendNum].Cmdbyte);
                                sendNum = sendNum + 1;
                            }
                            else
                            {
                                MessagShow(Language.getMsg("CmdExceptionMsg"));
                            }
                        }
                    }
                    else
                    {
                        MessagShow(Language.getMsg("DownError"));
                    }
                    break;
                default:
                    break;
            }
        }

         
        
        private void button4_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            AllSelect = false;
            int sIndex = tabControl1.SelectedIndex;
            if(sIndex ==0)
            {
                if(txtDId_nlx.Text =="" )
                {
                    MessagShow("设备ID不能为空,请输入设备ID或点击按钮【读取设备参数】");
                    return;
                }
                if(txtDBVersion_nlx.Text == "")
                {
                    MessagShow("数据版本不能为空,请输入数据版本或点击按钮【读取设备参数】");
                    return;
                }
            }
            if(sIndex ==1)
            {
                if(txtSamplePeriod_nlx.Text =="")
                {
                    MessagShow("采样间隔不能为空,请输入采样间隔或点击按钮【读取设备参数】");
                    return;
                } 
                if(txtTakeTime_nlx.Text =="")
                {
                    MessagShow("数据记录间隔不能为空,请输入数据记录间隔或点击按钮【读取设备参数】");
                    return;
                }
                if (txtVisualAlarm_nlx.Text == "")
                {
                    MessagShow("超标记录间隔不能为空,请输入超标记录间隔或点击按钮【读取设备参数】");
                    return;
                }
                if (txtTransfersTime_nlx.Text == "")
                {
                    MessagShow("数据上传时间隔不能为空,请输入数据上传间隔或点击按钮【读取设备参数】");
                    return;
                }
            }
            if(sIndex==2)
            {
                if (txtGPRScontact_nlx.Text == "")
                {
                    MessagShow("GPRS接入点不能为空,请输入GPRS接入点或点击按钮【读取设备参数】");
                    return;
                }
                if (txtDNS_nlx.Text == "")
                {
                    MessagShow("DNS地址不能为空,请输入DNS地址或点击按钮【读取设备参数】");
                    return;
                }
                if (txtIP_nlx.Text == "")
                {
                    MessagShow("服务器地址不能为空,请输入服务器地址或点击按钮【读取设备参数】");
                    return;
                }
                if (txtPort_nlx.Text == "")
                {
                    MessagShow("端口不能为空,请输入端口或点击按钮【读取设备参数】");
                    return;
                }
                if (cbConnectionType_nlx.Text == "")
                {
                    MessagShow("网络协议不能为空,请输入网络协议或点击按钮【读取设备参数】");
                    return;
                }
            }
            if (sIndex < 3) 
            { 
                tabControl1.SelectedIndex = sIndex + 1;
            }
            if (this.btnNext.Text == Language.getMsg("Down"))
            {              
                TitleMsg.Text = Language.getMsg("DownTitle");
                TitlePanel.Visible = true;
                progressBar1.Value = 0;
                download();

                Console.WriteLine("download");

            } 
            else if (sIndex + 1 == 4) // sIndex + 1 == 3
            {
                this.btnNext.Text = Language.getMsg("Down");
                btnSave.Visible = true;
                button1.Visible = false;
                btnRead.Visible = false;
                this.tabControl1.Visible = false;
                this.dataGridView1.Visible = true; 
                this.dataGridView1.BeginInvoke(new GdvAddItemCallback(grvList)); //将所有的数据形成data grid
            }           
            this.btnBack.Visible = true;
        }
        public void grvList()
        {
            Console.WriteLine("grvList");
            GetModel();
            Console.WriteLine("##` grvList");
            list.Clear();
            foreach (DictionaryEntry deHB in Ht)
            { 
                ConfigModel m = new ConfigModel();

                try
                {
                    string _NameKey= deHB.Key.ToString().Split('_')[0];//关键字
                    if(_NameKey== "alarmPhone" || _NameKey == "everyOutTime" || _NameKey== "adress" || _NameKey == "phone1" || _NameKey == "phone2" || _NameKey == "phone3" || _NameKey == "phone4" || _NameKey == "phone5" || _NameKey == "phone6" || _NameKey == "phone7" || _NameKey == "phone8" || _NameKey == "phone9" || _NameKey == "phone10" || _NameKey== "alarmTime" || _NameKey == "delayAlarm" || _NameKey== "GPRSname" || _NameKey== "GPRSpwd" || _NameKey== "beatTime" || _NameKey== "isGPRS" || _NameKey== "outTime" || _NameKey== "backTime")
                    {

                    }
                    else
                    {
                        m.NameKey = _NameKey;
                        //值
                        m.DeviceVal = deHB.Value.ToString() == "False" ? Language.getMsg("N") : deHB.Value.ToString() == "True" ? Language.getMsg("Y") : deHB.Value.ToString();
                        string [] key = deHB.Key.ToString().Split('_');
                        if(key.Length ==1)
                        {
                            m.ChineseKey = key[0].ToString();
                        }
                        else
                        {
                            m.ChineseKey = Language.getMsg(deHB.Key.ToString().Split('_')[0] + "Title");//根据关键字加Title获取关键字对应的参数项名
                            m.Sort = double.Parse(deHB.Key.ToString().Split('_')[2]);//排列方式
                        }
                        if (string.IsNullOrEmpty(m.DeviceVal))
                        {
                            m.DeviceVal = "";
                        }
                        list.Add(m);
                        dataGridView1.DataSource = list.OrderBy(o => o.Sort).ToList<ConfigModel>();
                    }
                }
                catch (System.Exception ex)
                {
                	
                }
            }
           
        }
        /// <summary>
        /// 下载参数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //写入app.config
                GetModel();
                //写入配置文件
                AppCmd cmd = new AppCmd();
                cmd.WriteConfig(Ht);
                this.saveFileDialog1.ShowDialog(); 
            }
            catch (Exception ex)
            {

                MessagShow(Language.getMsg("SaveErrorMsg"));
            }
           
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            AllSelect = false;//禁止选项卡
            AppCmd cmd = new AppCmd(); 
           int sIndex = tabControl1.SelectedIndex;
           sIndex = this.btnNext.Text == Language.getMsg("OK") || this.btnNext.Text == Language.getMsg("Down")  ? 5 : sIndex;// 4:sIndex
           if (sIndex > 0)
           {
              
               tabControl1.SelectedIndex = sIndex - 1;
           }
           if (this.btnNext.Text == Language.getMsg("OK") || this.btnNext.Text == Language.getMsg("Down"))
           {
               GetModel();
               cmd.WriteConfig(Ht);
               btnRead.Visible = true;
               button1.Visible = true;
               btnSave.Visible = false;
               this.dataGridView1.Visible = false;
               this.tabControl1.Visible = true;
               
           }
           this.btnNext.Text =  Language.getMsg("Next");
           if (sIndex - 1 == 0)
           {
               this.btnBack.Visible = false;
           }
             
        }
        /// <summary>
        /// float转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] floatTobyte(float val)
        {
            byte[] aa = BitConverter.GetBytes(val); 
           // List<byte> blist = new List<byte>(); blist.AddRange(aa); blist.RemoveAll(b => b == 0);
            return aa;
        }
        /// <summary>
        /// int转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] intTobyte(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
            List<byte> blist = new List<byte>(); blist.AddRange(aa); blist.RemoveAll(b => b == 0);
            return blist.ToArray();
        }
        /// <summary>
        /// int转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] intTobyteold(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            
         return  aa;
        }
        private byte[] intTobytenew (int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
             byte[] Rebyte=new byte[2];
            Rebyte[0]=aa[2];
            Rebyte[1]=aa[3];
            return Rebyte;
        }
        private byte[] intTobytenew2(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
            byte[] Rebyte = new byte[2];
            Rebyte[0] = aa[0];
            Rebyte[1] = aa[1];
            return Rebyte;
        }
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {               
                string path = this.openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(path))
                {
                    AppCmd cmd = new AppCmd();
                    cmd.ReadConfig(ref Ht, path);
                    SetModel(true);
                    cmd.WriteConfig(Ht);
                    MessagShow(Language.getMsg("ReadFileOk"));
                }
            }
            catch (Exception ex)
            {
                MessagShow(Language.getMsg("ReadFileError"));
            }
        }

        /// <summary>
        /// 下载
        /// </summary>
        public void download()
        {
            try
            {
                SetPROGRAM();

                AppCmd cmd = new AppCmd();

                //根据所有的控件值更新hash table
                GetModel();

                cmd.WriteConfig(Ht);

                ReadType = 1;
                HusbHelp.cmdType = 3;

                cmdlist = cmd.ReadConfigCmd();//从配置文件读取配置组成

                packageSendcmd("w");

                if (HusbHelp.IsOpen)
                {
                    SendType = 1;
                    HusbHelp.SendData(listSendModel[0].Cmdbyte);
                    progressBar1.Value = 10;
                    progressBar1.Value = 20;
                }
                else
                {
                    TitlePanel.Visible = false;
                    MessagShow(Language.getMsg("connetOff"));
                }
                sendNum = 0;
                sendNum = sendNum + 1;

            }
            catch (Exception ex)
            {

                Console.WriteLine("###4    public void down() EXCEPTION : " + ex);
            }
        }
        private void btnDown_Click(object sender, EventArgs e)
        {
          
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e) 
        {
            string filepath = openFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            cmd.copy(filepath);
            
        }
      
        


       /// <summary>
        ///  发送指令
       /// </summary>
       /// <param name="model">发送对象</param>
       /// <param name="type">类型</param>
       /// <returns></returns> 
        public bool sendCmd(cmdModel model,string type)
        { 

             try{ 
                 sendModel = model;
                 KeyName = model.CmdName;
                 byte[] data;
                 int totalLength = 0;
                 byte[] address = intTobytenew(Convert.ToInt32(model.Address, 16));
                 byte[] Btype = HusbHelp.HexStringToByte(type);
                 byte[] Length;
                 if (type == "w")
                 {
                     byte[] val;
                     int check = 0;
                     if (string.IsNullOrEmpty(model.Val))
                     {
                         int  valLength=Convert.ToInt32(Convert.ToInt32(model.Max));
                         val = new byte[valLength];
                         check = 16;
                     }
                     else
                     {
                         val = HusbHelp.HexStringToByte(model.Val);
                         foreach (byte v in val)
                         {
                             int bv = 0;
                             int.TryParse(v.ToString(), out bv);
                             check += bv;
                         } 
                     } 
                     byte[] Bcheck = intTobyte(check); 
                     totalLength = 3 + val.Length + Bcheck.Length;
                     data = new byte[totalLength]; 
                     data[0] = Btype[0];
                     data[1] = address[0];
                     data[2] = address[1];
                     Length = BitConverter.GetBytes(Convert.ToInt32(model.Val.Length));
                     Array.Copy(val, 0, data, 4, val.Length);
                     Array.Copy(Bcheck, 0, data, totalLength - Bcheck.Length, Bcheck.Length);
                     Array.Copy(Length, 0, data, 3, 1);
                 }
                 else
                 {
                   Length=  intTobyte( Convert.ToInt32(model.Max));
                    
                     totalLength = 3 + Length.Length;
                     data = new byte[totalLength];
                     data[0] = Btype[0];
                     data[1] = address[0];
                     data[2] = address[1]; 
                     Array.Copy(Length, 0, data, 3, 1);
                 }
                 HusbHelp.SendData(data);
                 
                    return true;
               
            }
            catch (Exception ex)
            {

                MessagShow(Language.getMsg("CmdExceptionMsg"));
                return false;
            }
             
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string path = this.saveFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            if (cmd.copy(path))
            {
                MessagShow(Language.getMsg("SaveOK") );
            }
            else
            {
                MessagShow(Language.getMsg("SaveError"));
            }
        }

        private void txtPhone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
        public void MathVilid(KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '\b'))
            {
                e.Handled = true;
                MessagShow(Language.getMsg("EnterNumbersMsg"));

            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtPhone2_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
 

        
        private void txtMonitor1min_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;

            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v=0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                senderText.Text = "0";
            }
            else
            {
                double.TryParse(senderText.Text.Trim(), out v);
                if (v < -1000.00 || v > 1000.99)
                {
                    senderText.Text = "0";
                    MessagShow(Language.getMsg("EnterNumbersrangeMsg"));
                } 
            }
        }

        private void txtMonitor1max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor1min_nlx;
            maxCheck(minTxt, senderText);
        }
        private void txtMonitor7max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor1min_nlx;
            maxCheck(  senderText);
        }
        private void maxCheck( TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                maxTxt.Text = "0";
            }
            
        }

        private void maxCheck(TextBox minTxt, TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                maxTxt.Text = "0";
            }
            else
            {
                double.TryParse(maxTxt.Text.Trim(), out v);
                if (v < -1000.00 || v > 1000.99)
                {
                    maxTxt.Text = "0";
                    MessagShow(Language.getMsg("EnterNumbersrangeMsg"));
                }
                else
                {
                    double minv = 0;
                    double.TryParse(minTxt.Text, out minv);
                    if (v < minv)
                    {
                        maxTxt.Text = "0";
                        MessagShow(Language.getMsg("MaxRMinMsg"));
                    }

                }
            }
        }

        private void txtMonitor2max_Leave(object sender, EventArgs e)
        {

            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor2min_nlx;
            maxCheck(minTxt, senderText);
        }

        private void txtMonitor3max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor3min_nlx;
            maxCheck(minTxt, senderText);
        }

        /// <summary>
        /// 进入配置模式
        /// </summary>
        private void SetPROGRAM()
        {
            if (!IsPROGRAM)
            {
                IsPROGRAM = true;//进入配置模式
                HusbHelp.cmdType = 2;
                ReadType = 2;
                if (HusbHelp.IsOpen)
                {
                    HusbHelp.SendData("PROGRAM");//进入计算机模式
                    Thread.Sleep(500);
                }
                else
                {
                    MessagShow(Language.getMsg("CmdExceptionMsg"));
                }
            }
        }

        /// <summary>
        /// 读取设备参数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            ReadDeviceInfo();
        }
        public void ReadDeviceInfo()
        {
            try
            {
                SetPROGRAM();

                TitlePanel.Visible = true;
                progressBar1.Value = 0;

                TitleMsg.Text = Language.getMsg("ReadTitle");
                Read();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 设置Model
        /// 根据hash table中的数据更新 UI信息
        /// </summary>
        private void SetModel(bool ReadIDFlag)
        {
            this.txtDId_nlx.Text = ReadIDFlag == true ? "" : Ht["deviceID_设备ID_1"].ToString();
            //将版本转成正数，然后将整数除以10转成小数，然后转成字符串
            int DBVersion;
            int.TryParse(Ht["DBVersion"].ToString(), out DBVersion);
            this.txtDBVersion_nlx.Text = (DBVersion / 10.0).ToString();

            int AllowClose = 0;
            int.TryParse(Ht["allowClose_是否允许本地关机_2.2"].ToString(), out AllowClose);
            this.cmbAllowClose_nlx.SelectedIndex = AllowClose;
            this.ckeveryOutTime_nlx.Checked = Ht["everyOutTime_每日10时发送日报信息_2"].ToString() == "1" ? true : false;
            this.chkAlarmPhone_nlx.Checked = Ht["alarmPhone_报警前拨打电话_2.1"].ToString() == "1" ? true : false;
            this.chkEnableAlarm_nlx.Checked = Ht["EnableAlarm"].ToString() == "1" ? true : false;
            this.checkexportpdf.Checked= Ht["Enableexportpdf"].ToString() == "1" ? true : false;
            this.chkEnableOverproolRecordPeriod_nlx.Checked = Ht["EnableOverproofRecordPeriod"].ToString() == "1" ? true : false;
            int PrintMode = 0;
            int.TryParse(Ht["PrintMode"].ToString(), out PrintMode);
            this.comboBoxPrintMode_nlx.SelectedIndex = PrintMode;
            this.txtPrintAmount_nlx.Text = Ht["PrintParam"].ToString();

            //this.txtMonitor1_nlx.Text = Ht["monitor1_监测1_14"].ToString();
            this.txtMonitor1min_nlx.Text = Ht["monitor1min_监测1最小值_15"].ToString();
            this.txtMonitor1max_nlx.Text = Ht["monitor1max_监测1最大值_16"].ToString();
            this.txtMonitor1datum_nlx.Text = Ht["monitor1datum_监测1基准值_17"].ToString();
            this.txtMonitor1hysteresis_nlx.Text = Ht["monitor1hysteresis_监测1迟滞值_18"].ToString();

            //this.txtMonitor2_nlx.Text = Ht["monitor2_监测2_19"].ToString();
            this.txtMonitor2min_nlx.Text = Ht["monitor2min_监测2最小值_20"].ToString();
            this.txtMonitor2max_nlx.Text = Ht["monitor2max_监测2最大值_21"].ToString();
            this.txtMonitor2datum_nlx.Text = Ht["monitor2datum_监测2基准值_22"].ToString();
            this.txtMonitor2hysteresis_nlx.Text = Ht["monitor2hysteresis_监测2迟滞值_23"].ToString();

            this.txtMonitor3_nlx.Text = Ht["monitor3_监测3_24"].ToString();
            this.txtMonitor3min_nlx.Text = Ht["monitor3min_监测3最小值_25"].ToString();
            this.txtMonitor3max_nlx.Text = Ht["monitor3max_监测3最大值_26"].ToString();
            this.txtMonitor3datum_nlx.Text = Ht["monitor3datum_监测3基准值_27"].ToString();
            this.txtMonitor3hysteresis_nlx.Text = Ht["monitor3hysteresis_监测3迟滞值_28"].ToString();

            this.txtMonitor4_nlx.Text = Ht["monitor4_监测4_24.1"].ToString();
            this.txtMonitor4min_nlx.Text = Ht["monitor4min_监测4最小值_24.2"].ToString();
            this.txtMonitor4max_nlx.Text = Ht["monitor4max_监测4最大值_24.3"].ToString();
            this.txtMonitor4datum_nlx.Text = Ht["monitor4datum_监测4基准值_24.4"].ToString();
            this.txtMonitor4hysteresis_nlx.Text = Ht["monitor4hysteresis_监测4迟滞值_24.5"].ToString();

            this.txtSamplePeriod_nlx.Text = Ht["samplePeriod"].ToString();
            this.txtTakeTime_nlx.Text = Ht["takeTime_数据记录间隔(秒)_30"].ToString();
            this.txtVisualAlarm_nlx.Text = Ht["visualAlarm_超标记录间隔(秒)_31"].ToString();    
            this.txtTransfersTime_nlx.Text = Ht["transfersTime_数据上传间隔(秒)_33"].ToString();

            this.txtAlarmTime_nlx.Text = Ht["alarmTime_报警短信间隔时间(分)_29"].ToString();
            this.txtDelayAlarm_nlx.Text = Ht["delayAlarm_报警确认时间(分)_32"].ToString();
            this.cmbUnit_nlx.SelectedItem = Ht["unit_温度单位_34"].ToString() == "1" ? Language.getMsg("Degrees Fahrenheit") : Language.getMsg("Centigrade");
           
            this.txtGPRScontact_nlx.Text = Ht["GPRScontact_GPRS接入点_35"].ToString();
            this.txtGPRSname_nlx.Text = Ht["GPRSname_GPRS用户名_36"].ToString();
            this.txtGPRSpwd_nlx.Text = Ht["GPRSpwd_GPRS密码_37"].ToString();
            this.txtDNS_nlx.Text = Ht["DNS_DNS地址_39"].ToString();
            this.txtIP_nlx.Text = Ht["IP_服务器地址_38"].ToString();
            this.txtPort_nlx.Text = Ht["port_端口_40"].ToString();
            this.txtBeatTime_nlx.Text = Ht["beatTime_GPRS心跳时间(秒)_41"].ToString();
            this.cbContactType_nlx.SelectedItem = Ht["contactType_数据协议_42"].ToString();
            //connectionType_数据协议_42   connectionType_网络协议_43
            this.cbConnectionType_nlx.SelectedItem = Ht["connectionType_网络协议_43"].ToString();
            this.cbUseGPRS_nlx.SelectedItem = Ht["isGPRS_启用GPRS_44"].ToString();
            this.txtOutTime_nlx.Text = Ht["outTime_GPRS空闲超时(秒)_45"].ToString();
            this.txtBackTime_nlx.Text = Ht["backTime_回应超时(秒)_46"].ToString();

            this.txtPhone1_nlx.Text = Ht["phone1_电话号码1_4"].ToString();
            this.txtPhone2_nlx.Text = Ht["phone2_电话号码2_5"].ToString();
            this.txtPhone3_nlx.Text = Ht["phone3_电话号码3_6"].ToString();
            this.txtPhone4_nlx.Text = Ht["phone4_电话号码4_7"].ToString();
            this.txtPhone5_nlx.Text = Ht["phone5_电话号码5_8"].ToString();
            this.txtPhone6_nlx.Text = Ht["phone6_电话号码6_9"].ToString();
            this.txtPhone7_nlx.Text = Ht["phone7_电话号码7_10"].ToString();
            this.txtPhone8_nlx.Text = Ht["phone8_电话号码8_11"].ToString();
            this.txtPhone9_nlx.Text = Ht["phone9_电话号码9_12"].ToString();
            this.txtPhone10_nlx.Text = Ht["phone10_电话号码10_13"].ToString();
            this.txtAdress_nlx.Text = Ht["adress_安装位置_3"].ToString();
            //运单号
            this.txtTrackingNumber.Text = Ht["TrackingNumber"].ToString();
            this.txtCartonNumber.Text = Ht["CartonNumber"].ToString();
            this.txtConsignor.Text = Ht["Consignor"].ToString();
            this.txtConsignee.Text = Ht["Consignee"].ToString();
            this.txtDescription.Text = Ht["Description"].ToString();

        }
        /// <summary>
        /// 读取Model
        /// </summary>
        private void GetModel()
        {
            Ht.Clear();

            Ht.Add("deviceID_设备ID_1", this.txtDId_nlx.Text.Trim());
            int DBVersion = (int)(double.Parse(this.txtDBVersion_nlx.Text.ToString()) * 10.0);
            Ht.Add("DBVersion", DBVersion.ToString());

            Ht.Add("allowClose_是否允许本地关机_2.2", this.cmbAllowClose_nlx.SelectedIndex.ToString());
            Ht.Add("EnableAlarm", this.chkEnableAlarm_nlx.Checked == true ? "1" : "0");
            Ht.Add("Enableexportpdf", this.checkexportpdf.Checked == true ? "1" : "0");
            Ht.Add("EnableOverproofRecordPeriod", this.chkEnableOverproolRecordPeriod_nlx.Checked == true ? "1" : "0");
            Ht.Add("everyOutTime_每日10时发送日报信息_2",  this.ckeveryOutTime_nlx.Checked==true?"1":"0");
            Ht.Add("alarmPhone_报警前拨打电话_2.1", this.chkAlarmPhone_nlx.Checked == true ? "1" : "0");

            Ht.Add("PrintMode", this.comboBoxPrintMode_nlx.SelectedIndex.ToString());
            Ht.Add("PrintParam", this.txtPrintAmount_nlx.Text.ToString());

            Ht.Add("monitor1min_监测1最小值_15", this.txtMonitor1min_nlx.Text.Trim());
            Ht.Add("monitor1max_监测1最大值_16", this.txtMonitor1max_nlx.Text.Trim());
            Ht.Add("monitor1datum_监测1基准值_17", this.txtMonitor1datum_nlx.Text.Trim());
            Ht.Add("monitor1hysteresis_监测1迟滞值_18", this.txtMonitor1hysteresis_nlx.Text.Trim());
            Ht.Add("monitor2min_监测2最小值_20", this.txtMonitor2min_nlx.Text.Trim());
            Ht.Add("monitor2max_监测2最大值_21", this.txtMonitor2max_nlx.Text.Trim());
            Ht.Add("monitor2datum_监测2基准值_22", this.txtMonitor2datum_nlx.Text.Trim());
            Ht.Add("monitor2hysteresis_监测2迟滞值_23", this.txtMonitor2hysteresis_nlx.Text.Trim());
            Ht.Add("monitor3min_监测3最小值_25", this.txtMonitor3min_nlx.Text.Trim());
            Ht.Add("monitor3max_监测3最大值_26", this.txtMonitor3max_nlx.Text.Trim());
            Ht.Add("monitor3datum_监测3基准值_27", this.txtMonitor3datum_nlx.Text.Trim());
            Ht.Add("monitor3hysteresis_监测3迟滞值_28", this.txtMonitor3hysteresis_nlx.Text.Trim());
            Ht.Add("monitor4min_监测4最小值_24.2", this.txtMonitor4min_nlx.Text.Trim());
            Ht.Add("monitor4max_监测4最大值_24.3", this.txtMonitor4max_nlx.Text.Trim());
            Ht.Add("monitor4datum_监测4基准值_24.4", this.txtMonitor4datum_nlx.Text.Trim());
            Ht.Add("monitor4hysteresis_监测4迟滞值_24.5", this.txtMonitor4hysteresis_nlx.Text.Trim());

            Ht.Add("monitor1_监测1_14", this.txtMonitor1_nlx.Text.Trim());
            Ht.Add("monitor2_监测2_19", this.txtMonitor2_nlx.Text.Trim());
            Ht.Add("monitor3_监测3_24", this.txtMonitor3_nlx.Text.Trim());
            Ht.Add("monitor4_监测4_24.1", this.txtMonitor4_nlx.Text.Trim());

            Ht.Add("samplePeriod", this.txtSamplePeriod_nlx.Text.Trim());
            Ht.Add("takeTime_数据记录间隔(秒)_30", this.txtTakeTime_nlx.Text.Trim());
            Ht.Add("visualAlarm_超标记录间隔(秒)_31", this.txtVisualAlarm_nlx.Text.Trim());            
            Ht.Add("transfersTime_数据上传间隔(秒)_33", this.txtTransfersTime_nlx.Text.Trim());

            Ht.Add("alarmTime_报警短信间隔时间(分)_29", this.txtAlarmTime_nlx.Text.Trim());
            Ht.Add("delayAlarm_报警确认时间(分)_32", this.txtDelayAlarm_nlx.Text.Trim());
            Ht.Add("unit_温度单位_34", this.cmbUnit_nlx.SelectedItem.ToString() == Language.getMsg("Degrees Fahrenheit") ? "1" : "0");
           
            Ht.Add("GPRScontact_GPRS接入点_35", this.txtGPRScontact_nlx.Text.Trim());
            Ht.Add("GPRSname_GPRS用户名_36", this.txtGPRSname_nlx.Text.Trim());
            Ht.Add("GPRSpwd_GPRS密码_37", this.txtGPRSpwd_nlx.Text.Trim());
            Ht.Add("DNS_DNS地址_39", this.txtDNS_nlx.Text.Trim());
            Ht.Add("IP_服务器地址_38", this.txtIP_nlx.Text.Trim());
            Ht.Add("port_端口_40", this.txtPort_nlx.Text.Trim());
            Ht.Add("beatTime_GPRS心跳时间(秒)_41", this.txtBeatTime_nlx.Text.Trim());
            Ht.Add("contactType_数据协议_42", this.cbContactType_nlx.SelectedItem.ToString());
            Ht.Add("connectionType_网络协议_43", this.cbConnectionType_nlx.SelectedItem.ToString());
            Ht.Add("isGPRS_启用GPRS_44", this.cbUseGPRS_nlx.SelectedItem.ToString());
            Ht.Add("outTime_GPRS空闲超时(秒)_45", this.txtOutTime_nlx.Text.Trim());
            Ht.Add("backTime_回应超时(秒)_46", this.txtBackTime_nlx.Text.Trim());


            Ht.Add("phone1_电话号码1_4", this.txtPhone1_nlx.Text.Trim());
            Ht.Add("phone2_电话号码2_5", this.txtPhone2_nlx.Text.Trim());
            Ht.Add("phone3_电话号码3_6", this.txtPhone3_nlx.Text.Trim());
            Ht.Add("phone4_电话号码4_7", this.txtPhone4_nlx.Text.Trim());
            Ht.Add("phone5_电话号码5_8", this.txtPhone5_nlx.Text.Trim());
            Ht.Add("phone6_电话号码6_9", this.txtPhone6_nlx.Text.Trim());
            Ht.Add("phone7_电话号码7_10", this.txtPhone7_nlx.Text.Trim());
            Ht.Add("phone8_电话号码8_11", this.txtPhone8_nlx.Text.Trim());
            Ht.Add("phone9_电话号码9_12", this.txtPhone9_nlx.Text.Trim());
            Ht.Add("phone10_电话号码10_13", this.txtPhone10_nlx.Text.Trim());
            Ht.Add("adress_安装位置_3", this.txtAdress_nlx.Text.Trim());

            Ht.Add("TrackingNumber", this.txtTrackingNumber.Text.Trim());
            Ht.Add("CartonNumber", this.txtCartonNumber.Text.Trim());
            Ht.Add("Consignor", this.txtConsignor.Text.Trim());
            Ht.Add("Consignee", this.txtConsignee.Text.Trim());
            Ht.Add("Description", this.txtDescription.Text.Trim());
        }
        

        private void timer1_Tick(object sender, EventArgs e)
        {


            if (SendType == 1 && sendNum < listSendModel.Count - 1)
            {
                
                if (ReSendNum == sendNum)
                {
                    RelCount++;
                }
                else
                {
                    ReSendNum = sendNum;
                    RelCount = 1;
                }
                if (RelCount == 2)
                {
                    HusbHelp.SendData(listSendModel[sendNum - 1].Cmdbyte);

                }
                else if (RelCount > 2)
                {
                    SendType = 0;
                    TitlePanel.Visible = false;
                    MessagShow(Language.getMsg("CmdExceptionMsg"));
                   
                }

               
               
            }
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            e.Cancel = AllSelect;
            AllSelect = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TitlePanel.Visible = false;
        }

        private void TitleBtn_Click(object sender, EventArgs e)
         {
        //    if (TitleBtn.Text == "确认下载")
        //    {
        //        down();
        //    }
        //    else
        //    {
        //        Read();
        //    }
          
        }

         
        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] HexStringToBytes(string msg)
        {
            byte[] bstr = new byte[1024];
            bstr = Encoding.BigEndianUnicode.GetBytes(msg);
            return bstr;
        }

       /// <summary>
       /// string To Ucs
       /// </summary>
       /// <param name="str">字符串</param>
       /// <returns></returns>
        private byte[] stringToUCS2(string str)
        {
            byte[] Relval = new byte[str.Length*2];
            string[] strs = new string[str.Length]; 
           char[] arr = str.ToCharArray(); 
            for (int i = 0; i < arr.Length; i++)
            {
                strs[i] = arr[i].ToString();
            }
            for (int i = 0; i < strs.Length; i++)
            {
                string s = strs[i];
                byte[] sUcs = new byte[2];
                sUcs=System.Text.Encoding.Unicode.GetBytes(s);
                sUcs.Reverse();
         
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(sUcs);
                List<byte> blist = new List<byte>(); blist.AddRange(sUcs); 
                Array.Copy(blist.ToArray(), 0, Relval, i * 2, blist.Count);
              
            }
            return Relval;
        }
        /// <summary>
        ///  Ucs  To  string
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns></returns>
        private string UCS2Tostring(byte[] by)
        {
            string RelStr = "";
            int star = 0;
            for (int i = 0; i < by.Length/2; i++)
            {
                
                byte[] val = new byte[2];
                Array.Copy(by, star, val,0,2);
                val.Reverse();
                Array.Reverse(val);
                RelStr = RelStr + System.Text.Encoding.Unicode.GetString(val);
                star = star + 2;

            }
            return RelStr;
        }

        private void SystemConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            HusbHelp.DataResponse -= new M300Config.HusbHelp.DataResponseEventHandler(DataRead);
            HusbHelp.BDataResponse -= new M300Config.HusbHelp.BDataResponseEventHandler(bDataRead); 
            this.Hide();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void MessagShow(string Msg)
        {
            UserMessageBox box = new UserMessageBox(Msg);
           
            box.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDId_nlx_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Enter)
            {
                string EQCodel = txtDId_nlx.Text.Trim().ToUpper();
                int starIndex = 0;
                int endIndex = 0;
                starIndex = EQCodel.IndexOf("ID=");
                if (starIndex > -1)
                {
                    starIndex += 3;
                    endIndex = EQCodel.Length ;
                    this.txtDId_nlx.Text = EQCodel.Substring(starIndex,  endIndex - starIndex  );
                }
                e.Handled = true;
                this.txtAdress_nlx.Focus();
            }
        }

        private void btnTestModel_nlx_Click(object sender, EventArgs e)
        {
            if (test == null)
            {
                test = new TestForm(HusbHelp);   
            }
            test.Show();
        }

    }
}
