﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace M300Config
{
    public static class Global
    {
 
        /// <summary>
        /// 语言
        /// </summary>
        private static string language = ConfigurationManager.AppSettings["language"].ToString(); 
        public static string Language
        {
            get
            { 
                return language;
            }
            set { language = value; }
        } 
    }
}
