﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace M300Config
{
    public partial class TestForm : Form
    {
        HusbHelp HusbHelp;
        public TestForm(HusbHelp HusbHelp1)
        {
            HusbHelp = HusbHelp1;
            InitializeComponent();
        }
        
         bool state=false;
        /// <summary>
        /// 按键测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTestKeyPress_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    this.LabMsg.AppendText("\r\n\r\n  按键测试开始：");
                  
                    HusbHelp.cmdType = 9;
                    byte[] bt = new byte[3];
                    bt[0]=0x24;
                    bt[1]=0x11;
                    bt[2] = 0x24; 
                    HusbHelp.SendData(bt);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void TestForm_Load(object sender, EventArgs e)
        {

        }

        public void ShowMsg(string Msg, bool sendState)
        {
            this.BeginInvoke(new EventHandler(delegate
                                       {
                                           if (Msg == "进入测试模式成功")
                                           {

                                               btnTestKeyPress.Enabled = true;
                                               btnTestLCD.Enabled = true;
                                               btnTestPower.Enabled = true;
                                               btnTestprobe.Enabled = true;
                                               btnModem.Enabled = true;
                                               btnOneKeyTest.Enabled = true;

                                           }
                                           else
                                           {
                                               this.state = sendState;
                                           }
                                           this.LabMsg.AppendText("\r\n " + Msg);
                                       }));
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {
            HusbHelp.TestMsg += new M300Config.HusbHelp.TestEventHandler(ShowMsg);
        }
        /// <summary>
        /// 进入测试模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (HusbHelp.IsOpen && HusbHelp.history)
            {
                HusbHelp.cmdType = 8;
                HusbHelp.SendData("T");
            }
            else
            {

                HusbHelp.IsOpen = false;
            }
        }

        private void btnTestLCD_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    this.LabMsg.AppendText("\r\n\r\n LCD测试开始：");
                    HusbHelp.cmdType = 10;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x21;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnTestprobe_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    this.LabMsg.AppendText("\r\n\r\n 探头测试开始：");
                    HusbHelp.cmdType =11;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x31;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                    ShowMsg("等待探头数据返回，大约10秒左右",true);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnTestPower_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    this.LabMsg.AppendText("\r\n\r\n 电池电量测试开始");
                    HusbHelp.cmdType = 12;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x41;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                    ShowMsg("等待电量数据返回，大约10秒左右", true);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnModem_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    this.LabMsg.AppendText("\r\n\r\n  modem测试开始");
                    HusbHelp.cmdType = 13;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x51;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void TestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    HusbHelp.cmdType = 9;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x60;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
             
        }

        private void btnOneKeyTest_Click(object sender, EventArgs e)
        {

            Thread th = new Thread(KeyOneTest);
            th.IsBackground = true;
            th.Start();
           
        }


        private void KeyOneTest()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; state; j++)
                {
                    if (j > 20)
                    { 
                        break;
                    }
                    Thread.Sleep(500);
                }
                switch (i)
                {
                    case 0:
                        try
                        {
                            if (HusbHelp.IsOpen && HusbHelp.history)
                            {
                                ShowMsg("\r\n\r\n 按键测试开始： ", true); 
                                HusbHelp.cmdType = 9;
                                byte[] bt = new byte[3];
                                bt[0] = 0x24;
                                bt[1] = 0x11;
                                bt[2] = 0x24;
                                 HusbHelp.SendData(bt);
                            }
                            else
                            {

                                HusbHelp.IsOpen = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            HusbHelp.IsOpen = false;
                            MessageBox.Show(ex.Message.ToString());
                        }
                        break;
                    case 1:
                        try
                        {
                            if (HusbHelp.IsOpen && HusbHelp.history)
                            {
                                ShowMsg("\r\n\r\n LCD测试开始：", true);  
                                HusbHelp.cmdType = 10;
                                byte[] bt = new byte[3];
                                bt[0] = 0x24;
                                bt[1] = 0x21;
                                bt[2] = 0x24;
                                HusbHelp.SendData(bt);
                            }
                            else
                            {

                                HusbHelp.IsOpen = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            HusbHelp.IsOpen = false;
                            MessageBox.Show(ex.Message.ToString());
                        }
                        break;
                    case 2:
                        try
                        {
                            if (HusbHelp.IsOpen && HusbHelp.history)
                            {
                                ShowMsg("\r\n\r\n 探头测试开始：", true); 
                                HusbHelp.cmdType = 11;
                                byte[] bt = new byte[3];
                                bt[0] = 0x24;
                                bt[1] = 0x31;
                                bt[2] = 0x24;
                                HusbHelp.SendData(bt);
                                ShowMsg("等待探头数据返回，大约10秒左右", true);
                            }
                            else
                            {

                                HusbHelp.IsOpen = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            HusbHelp.IsOpen = false;
                            MessageBox.Show(ex.Message.ToString());
                        }
                        break;
                    case 3:

                        try
                        {
                            if (HusbHelp.IsOpen && HusbHelp.history)
                            {

                                ShowMsg("\r\n\r\n 电池电量测试开始：", true);
                                HusbHelp.cmdType = 12;
                                byte[] bt = new byte[3];
                                bt[0] = 0x24;
                                bt[1] = 0x41;
                                bt[2] = 0x24;
                                HusbHelp.SendData(bt);
                                ShowMsg("等待电量数据返回，大约10秒左右", true);
                            }
                            else
                            {

                                HusbHelp.IsOpen = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            HusbHelp.IsOpen = false;
                            MessageBox.Show(ex.Message.ToString());
                        }
                        break;
                    case 4:
                        try
                        {
                            if (HusbHelp.IsOpen && HusbHelp.history)
                            {
                                ShowMsg("\r\n\r\n  modem测试开始：", true); 
                                HusbHelp.cmdType = 13;
                                byte[] bt = new byte[3];
                                bt[0] = 0x24;
                                bt[1] = 0x51;
                                bt[2] = 0x24;
                                HusbHelp.SendData(bt);
                            }
                            else
                            {

                                HusbHelp.IsOpen = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            HusbHelp.IsOpen = false;
                            MessageBox.Show(ex.Message.ToString());
                        }
                        break;
                }
            }
            Thread.Sleep(15000);
           ShowMsg("\r\n\r\n测试完成", true);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    HusbHelp.cmdType =14;
                    byte[] bt = new byte[3];
                    bt[0] = 0x24;
                    bt[1] = 0x60;
                    bt[2] = 0x24;
                    HusbHelp.SendData(bt);
                    this.Hide();
                    
                }
                else
                {

                    HusbHelp.IsOpen = false;
                }

            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }


         
    }
}
