﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;

namespace M300Config
{
    public partial class M300SetConfig : Form
    {
        private delegate void SetModelCallBack();    
      
      /// <summary>
      /// 
      /// </summary>
         SelectLanguage Language = new SelectLanguage();
        public bool isCon = false;
        SystemConfig setconfig;
        HusbHelp HusbHelp = new HusbHelp(); 
        public M300SetConfig()
        {
            InitializeComponent();
        } 
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
         
        }

        private void openbtn_Click(object sender, EventArgs e)
        {
            if (!HusbHelp.IsOpen)
            {
                if (HusbHelp.OpenDevice())
                {
                    string dd = "设备已连接\r\n ";
                    this.revtext.AppendText(dd);
                    isCon = true;
                    btnSetParameter.Enabled = true;
                    openbtn.Enabled = false;
                    btnexportpdf.Enabled = true;
                }
                else
                {
                    this.revtext.AppendText(Language.getMsg("DeviceConnetNOAndCheckMsg") + "\r\n ");
                    MessagShow(Language.getMsg("DeviceConnetNOAndCheckMsg"));
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            int w = System.Windows.Forms.SystemInformation.WorkingArea.Width;
            int h = System.Windows.Forms.SystemInformation.WorkingArea.Height;
            this.Location = new Point(w / 2 - 250, h / 2 - 180);
            this.StartPosition = FormStartPosition.CenterScreen;
            Language.SetLanguage(this);
            cmbLanguage_nlx.SelectedItem = Global.Language;
            timer_Check.Start();
            btnSetParameter.Enabled = false;
            HusbHelp.DataRecieved += new M300Config.HusbHelp.DataRecievedEventHandler(Dmsg);
            HusbHelp.GetType += new M300Config.HusbHelp.DataResponseTypeEventHandler(GetType);
            btnexportpdf.Text = "生成PDF";
            btnexportpdf.Enabled = false;
            label2.Text = "信息提示";
        }
        private void shows()
        {
            if (setconfig == null)
            {
                setconfig = new SystemConfig(HusbHelp);
                setconfig.Show();
            }
            else
            {
                setconfig.Close();
                setconfig = null;
                setconfig = new SystemConfig(HusbHelp);
                setconfig.Show();
            }
        }
        public void GetType(string type)
        {
            this.BeginInvoke(new SetModelCallBack(shows));//异步执行    
        }

        private void clear_Click(object sender, EventArgs e)
        {
            this.revtext.Text = "";
        }

        public void Dmsg(byte[] data )
        {
            string isexportpdf = string.Empty;
            if (InvokeRequired)
            {
                try
                { 
                    Invoke(new M300Config.HusbHelp.DataRecievedEventHandler(Dmsg), new object[] { data });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            else
            { 
                string rec_data = BitConverter.ToString(data);
                isexportpdf = rec_data.Substring(12, 5);
                if(isexportpdf =="01-01")
                {
                    this.revtext.SelectionColor = Color.Red;
                    this.revtext.AppendText("该设备未开通导出PDF报表权限，请重新设置。" + "\r\n ");
                }
                if(isexportpdf =="10-10")
                {
                    this.revtext.SelectionColor = Color.Blue;
                    this.revtext.AppendText("生成PDF报表指令发送成功。" + "\r\n ");
                }
            }
        }

        private void btnSetParameter_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确认打开设备参数设置？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    HusbHelp.SendData("$$$$$$$$");//获取设备类型   
                    HusbHelp.cmdType = 1;
                }
            }
            catch (Exception ex )
            {
                MessagShow(Language.getMsg("CmdExceptionMsg"));
            }
        } 
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
        private void timer_Check_Tick(object sender, EventArgs e)
        {
            if (!HusbHelp.IsOpen)
            {
                bool ckbool = HusbHelp.ck_device();
                if (ckbool)
                { 
                    this.openbtn.Enabled = true;
                }
                else
                {
                    this.openbtn.Enabled = false;
                }
            }
        }
        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] HexStringToBytes(string msg)
        {
            byte[] bstr = new byte[1024];
            bstr = Encoding.BigEndianUnicode.GetBytes(msg);
            return bstr;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppCmd app = new AppCmd();
            app.SaveConfig("language",this.cmbLanguage_nlx.SelectedItem.ToString());
            string language = ConfigurationManager.AppSettings[this.cmbLanguage_nlx.SelectedItem.ToString()].ToString();
            Global.Language = this.cmbLanguage_nlx.SelectedItem.ToString();
            Language.SetLanguage(this);
        }
        private void MessagShow(string Msg)
        {
            UserMessageBox box = new UserMessageBox(Msg);
            box.Show();
        }
        private void btnexportpdf_Click(object sender, EventArgs e)
        {
            try
            {
                string text ="PDF";
                text.Trim();
                if (HusbHelp.IsOpen && HusbHelp.history)
                {
                    if (MessageBox.Show("确认是否生成PDF？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        HusbHelp.cmdType = 7;
                        HusbHelp.SendData(text);
                    }
                }
                else
                {
                    HusbHelp.IsOpen = false;
                }
            }
            catch (Exception ex)
            {
                HusbHelp.IsOpen = false;
                MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
