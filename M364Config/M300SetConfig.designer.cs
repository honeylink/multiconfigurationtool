﻿namespace M300Config
{
    partial class M300SetConfig
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M300SetConfig));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.openbtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSetParameter = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer_Check = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLanguage_nlx = new System.Windows.Forms.ComboBox();
            this.btnexportpdf = new System.Windows.Forms.Button();
            this.revtext = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.openbtn);
            this.groupBox1.Location = new System.Drawing.Point(28, 40);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Size = new System.Drawing.Size(390, 219);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设备连接";
            // 
            // openbtn
            // 
            this.openbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openbtn.Location = new System.Drawing.Point(16, 62);
            this.openbtn.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.openbtn.Name = "openbtn";
            this.openbtn.Size = new System.Drawing.Size(342, 117);
            this.openbtn.TabIndex = 4;
            this.openbtn.Text = "连接设备";
            this.openbtn.UseVisualStyleBackColor = true;
            this.openbtn.Click += new System.EventHandler(this.openbtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(426, 40);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "信息提示";
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(1117, 67);
            this.clear.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(150, 114);
            this.clear.TabIndex = 11;
            this.clear.Text = "清空显示";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetParameter);
            this.groupBox2.Location = new System.Drawing.Point(31, 282);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Size = new System.Drawing.Size(390, 209);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设备参数配置";
            // 
            // btnSetParameter
            // 
            this.btnSetParameter.Location = new System.Drawing.Point(12, 68);
            this.btnSetParameter.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnSetParameter.Name = "btnSetParameter";
            this.btnSetParameter.Size = new System.Drawing.Size(346, 116);
            this.btnSetParameter.TabIndex = 15;
            this.btnSetParameter.Text = "设备参数配置";
            this.btnSetParameter.UseVisualStyleBackColor = true;
            this.btnSetParameter.Click += new System.EventHandler(this.btnSetParameter_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // timer_Check
            // 
            this.timer_Check.Interval = 1000;
            this.timer_Check.Tick += new System.EventHandler(this.timer_Check_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1132, 467);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 16;
            this.label4.Text = "语言：";
            this.label4.Visible = false;
            // 
            // cmbLanguage_nlx
            // 
            this.cmbLanguage_nlx.FormattingEnabled = true;
            this.cmbLanguage_nlx.Items.AddRange(new object[] {
            "English",
            "简体中文"});
            this.cmbLanguage_nlx.Location = new System.Drawing.Point(1136, 523);
            this.cmbLanguage_nlx.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cmbLanguage_nlx.Name = "cmbLanguage_nlx";
            this.cmbLanguage_nlx.Size = new System.Drawing.Size(131, 32);
            this.cmbLanguage_nlx.TabIndex = 17;
            this.cmbLanguage_nlx.Visible = false;
            this.cmbLanguage_nlx.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnexportpdf
            // 
            this.btnexportpdf.Location = new System.Drawing.Point(13, 37);
            this.btnexportpdf.Margin = new System.Windows.Forms.Padding(6);
            this.btnexportpdf.Name = "btnexportpdf";
            this.btnexportpdf.Size = new System.Drawing.Size(346, 120);
            this.btnexportpdf.TabIndex = 18;
            this.btnexportpdf.Text = "生成PDF";
            this.btnexportpdf.UseVisualStyleBackColor = true;
            this.btnexportpdf.Click += new System.EventHandler(this.btnexportpdf_Click);
            // 
            // revtext
            // 
            this.revtext.Location = new System.Drawing.Point(430, 67);
            this.revtext.Name = "revtext";
            this.revtext.Size = new System.Drawing.Size(661, 636);
            this.revtext.TabIndex = 19;
            this.revtext.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnexportpdf);
            this.groupBox3.Location = new System.Drawing.Point(28, 523);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(387, 180);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "生成PDF";
            // 
            // M300SetConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 762);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.revtext);
            this.Controls.Add(this.cmbLanguage_nlx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MaximizeBox = false;
            this.Name = "M300SetConfig";
            this.Text = "M364Config_V1.3";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button openbtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSetParameter;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer_Check;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLanguage_nlx;
        private System.Windows.Forms.Button btnexportpdf;
        private System.Windows.Forms.RichTextBox revtext;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

