﻿using System ; 
using System.Collections.Generic ; 
using System.Linq ; 
using System.Text; 

namespace M300Config
{
   public  class ConfigModel
    {
        private string chineseKey;
        public string ChineseKey
        {
            get { return chineseKey; }
            set { chineseKey = value; }
        }
       private  string nameKey;
       public string NameKey
       {
           get { return nameKey; }
           set { nameKey = value; }
       }
       private string deviceVal;
       public string DeviceVal
       {
           get { return deviceVal; }
           set { deviceVal = value; }
       }
       private double  sort;
       public double Sort
       {
           get { return sort; }
           set { sort = value; }
       }
    }
}
