﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;

namespace M3001Config
{
    #region
    /// <summary>
    /// SP_DEVICE_INTERFACE_DATA structure defines a device interface in a device information set.
    /// </summary>
    public struct SP_DEVICE_INTERFACE_DATA
    {
        public int cbSize;
        public Guid interfaceClassGuid;
        public int flags;
        public int reserved;
    }

    /// <summary>
    /// SP_DEVICE_INTERFACE_DETAIL_DATA structure contains the path for a device interface.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
    {
        internal int cbSize;
        internal short devicePath;
    }

    /// <summary>
    /// SP_DEVINFO_DATA structure defines a device instance that is a member of a device information set.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class SP_DEVINFO_DATA
    {
        public int cbSize = Marshal.SizeOf(typeof(SP_DEVINFO_DATA));
        public Guid classGuid = Guid.Empty; // temp
        public int devInst = 0; // dumy
        public int reserved = 0;
    }
    /// <summary>
    /// Flags controlling what is included in the device information set built by SetupDiGetClassDevs
    /// </summary>
    public enum DIGCF
    {
        DIGCF_DEFAULT = 0x00000001, // only valid with DIGCF_DEVICEINTERFACE                 
        DIGCF_PRESENT = 0x00000002,
        DIGCF_ALLCLASSES = 0x00000004,
        DIGCF_PROFILE = 0x00000008,
        DIGCF_DEVICEINTERFACE = 0x00000010
    }
    /// <summary>
    /// The HIDD_ATTRIBUTES structure contains vendor information about a HIDClass device
    /// </summary>
    public struct HIDD_ATTRIBUTES
    {
        public int Size;
        public ushort VendorID;
        public ushort ProductID;
        public ushort VersionNumber;
        public string  iProduct;
    }

    public struct HIDP_CAPS
    {
        public ushort Usage;
        public ushort UsagePage;
        public ushort InputReportByteLength;
        public ushort OutputReportByteLength;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
        public ushort[] Reserved;
        public ushort NumberLinkCollectionNodes;
        public ushort NumberInputButtonCaps;
        public ushort NumberInputValueCaps;
        public ushort NumberInputDataIndices;
        public ushort NumberOutputButtonCaps;
        public ushort NumberOutputValueCaps;
        public ushort NumberOutputDataIndices;
        public ushort NumberFeatureButtonCaps;
        public ushort NumberFeatureValueCaps;
        public ushort NumberFeatureDataIndices;
    }
    /// <summary>
    /// Type of access to the object. 
    ///</summary>
    static class DESIREDACCESS
    {
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint GENERIC_EXECUTE = 0x20000000;
        public const uint GENERIC_ALL = 0x10000000;
    }
    /// <summary>
    /// Action to take on files that exist, and which action to take when files do not exist. 
    /// </summary>
    static class CREATIONDISPOSITION
    {
        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;
        public const uint OPEN_ALWAYS = 4;
        public const uint TRUNCATE_EXISTING = 5;
    }
    /// <summary>
    /// File attributes and flags for the file. 
    /// </summary>
    static class FLAGSANDATTRIBUTES
    {
        public const uint FILE_FLAG_WRITE_THROUGH = 0x80000000;
        public const uint FILE_FLAG_OVERLAPPED = 0x40000000;
        public const uint FILE_FLAG_NO_BUFFERING = 0x20000000;
        public const uint FILE_FLAG_RANDOM_ACCESS = 0x10000000;
        public const uint FILE_FLAG_SEQUENTIAL_SCAN = 0x08000000;
        public const uint FILE_FLAG_DELETE_ON_CLOSE = 0x04000000;
        public const uint FILE_FLAG_BACKUP_SEMANTICS = 0x02000000;
        public const uint FILE_FLAG_POSIX_SEMANTICS = 0x01000000;
        public const uint FILE_FLAG_OPEN_REPARSE_POINT = 0x00200000;
        public const uint FILE_FLAG_OPEN_NO_RECALL = 0x00100000;
        public const uint FILE_FLAG_FIRST_PIPE_INSTANCE = 0x00080000;
    }
    /// <summary>
    /// Serves as a standard header for information related to a device event reported through the WM_DEVICECHANGE message.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DEV_BROADCAST_HDR
    {
        public int dbcc_size;
        public int dbcc_devicetype;
        public int dbcc_reserved;
    }
    /// <summary>
    /// Contains information about a class of devices
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DEV_BROADCAST_DEVICEINTERFACE
    {
        public int dbcc_size;
        public int dbcc_devicetype;
        public int dbcc_reserved;
        public Guid dbcc_classguid;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
        public string dbcc_name;
    }

    static class DEVICE_FLAG
    {
        /// <summary>Windows message sent when a device is inserted or removed</summary>
        public const int WM_DEVICECHANGE = 0x0219;
        /// <summary>WParam for above : A device was inserted</summary>
        public const int DEVICE_ARRIVAL = 0x8000;
        /// <summary>WParam for above : A device was removed</summary>
        public const int DEVICE_REMOVECOMPLETE = 0x8004;
        /// <summary>Used when registering for device insert/remove messages : specifies the type of device</summary>
        public const int DEVTYP_DEVICEINTERFACE = 0x05;
        /// <summary>Used when registering for device insert/remove messages : we're giving the API call a window handle</summary>
        public const int DEVICE_NOTIFY_WINDOW_HANDLE = 0;
    }
    #endregion
   public  class HIDHelp
   {
       #region  常量
       //设备打开状态
       private bool deviceOpened = false;
       public UInt16 VID = 0x0483; // 版本ID
       public UInt16 PID = 0x5710; // 产品ID
       private const int MAX_USB_DEVICES = 64;//最大的设备编号
       private IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);//无效值
       int outputReportLength;//输出报告长度,包含一个字节的报告ID
       public int OutputReportLength { get { return outputReportLength; } }
       int inputReportLength;//输入报告长度,包含一个字节的报告ID   
       public int InputReportLength { get { return inputReportLength; } }


       private FileStream hidDevice = null;
       /// <summary>
       /// HID设备返回值枚举
       /// </summary>
       public enum HID_RETURN
       {
           SUCCESS = 0,//成功
           NO_DEVICE_CONECTED,// 连接设备不成功
           DEVICE_NOT_FIND,//没有找到HID设备
           DEVICE_OPENED,//打开HID设备
           WRITE_FAILD,//数据写入失败
           READ_FAILD//数据读取失败

       }

       #endregion

       #region 微软动态连接库
       // 以下是调用windows的API的函数
       /// <summary>
       ///  HidD_GetHidGuid例程返回的设备接口GUID HIDClass设备。
       /// </summary>
       /// <param name="HidGuid">caller-allocated GUID缓冲区常规使用返回的设备接口GUID HIDClass设备。</param>
       [DllImport("hid.dll")]
       private static extern void HidD_GetHidGuid(ref Guid HidGuid);

       /// <summary>
       /// SetupDiGetClassDevs函数返回一个句柄的设备信息集包含请求的本地机器的设备信息元素。
       /// <summary>
       ///<param name="ClassGuid" > GUID设备设置类或者一个设备接口类。< / Param >
       /// <param name="Enumerator" >一个以null结尾的字符串指针,供应PnP型枚举器的名称或PnP型设备实例标识符。< / Param >
       /// <param name="HwndParent " >顶级窗口的句柄用于用户界面< / param >
       ///<param name="HwndParent" >变量指定控制选项,过滤设备信息的元素添加到设备信息集合。< / param >
       /// <returns>处理设备信息集 </returns>
       [DllImport("setupapi.dll", SetLastError = true)]
       private static extern IntPtr SetupDiGetClassDevs(ref Guid ClassGuid, uint Enumerator, IntPtr HwndParent, DIGCF Flags);

       /// <summary>
       /// SetupDiDestroyDeviceInfoList函数删除设备信息集和释放所有相关的记忆。
       /// </summary>
       /// <param name="DeviceInfoSet">A handle to the device information set to delete.</param>
       /// <returns>返回TRUE,如果它是成功的。否则,它返回FALSE  </returns>
       [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
       private static extern Boolean SetupDiDestroyDeviceInfoList(IntPtr deviceInfoSet);

       /// <summary>
       /// The SetupDiEnumDeviceInterfaces function enumerates the device interfaces that are contained in a device information set. 
       /// </summary>
       /// <param name="deviceInfoSet">A pointer to a device information set that contains the device interfaces for which to return information</param>
       /// <param name="deviceInfoData">A pointer to an SP_DEVINFO_DATA structure that specifies a device information element in DeviceInfoSet</param>
       /// <param name="interfaceClassGuid">a GUID that specifies the device interface class for the requested interface</param>
       /// <param name="memberIndex">A zero-based index into the list of interfaces in the device information set</param>
       /// <param name="deviceInterfaceData">a caller-allocated buffer that contains a completed SP_DEVICE_INTERFACE_DATA structure that identifies an interface that meets the search parameters</param>
       /// <returns></returns>
       [DllImport("setupapi.dll", CharSet = CharSet.Auto, SetLastError = true)]
       private static extern Boolean SetupDiEnumDeviceInterfaces(IntPtr deviceInfoSet, IntPtr deviceInfoData, ref Guid interfaceClassGuid, UInt32 memberIndex, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData);

       /// <summary>
       /// The SetupDiGetDeviceInterfaceDetail function returns details about a device interface.
       /// </summary>
       /// <param name="deviceInfoSet">A pointer to the device information set that contains the interface for which to retrieve details</param>
       /// <param name="deviceInterfaceData">A pointer to an SP_DEVICE_INTERFACE_DATA structure that specifies the interface in DeviceInfoSet for which to retrieve details</param>
       /// <param name="deviceInterfaceDetailData">A pointer to an SP_DEVICE_INTERFACE_DETAIL_DATA structure to receive information about the specified interface</param>
       /// <param name="deviceInterfaceDetailDataSize">The size of the DeviceInterfaceDetailData buffer</param>
       /// <param name="requiredSize">A pointer to a variable that receives the required size of the DeviceInterfaceDetailData buffer</param>
       /// <param name="deviceInfoData">A pointer buffer to receive information about the device that supports the requested interface</param>
       /// <returns></returns>
       [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Auto)]
       private static extern bool SetupDiGetDeviceInterfaceDetail(IntPtr deviceInfoSet, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, IntPtr deviceInterfaceDetailData, int deviceInterfaceDetailDataSize, ref int requiredSize, SP_DEVINFO_DATA deviceInfoData);

       /// <summary>/ / /内容提要
       ///hidd_getattributes例程返回指定顶级集合的属性。
       /// <summary>
       /// <参数名称=“hiddeviceobject”>指定一个打开的句柄到顶级收藏< /参数>
       /// <参数名称=“属性”>来电分配hidd_attributes结构返回属性指定的集合的hiddeviceobject < /参数>
       ///<返回> < /返回> 
       [DllImport("hid.dll")]
       private static extern Boolean HidD_GetAttributes(IntPtr hidDeviceObject, out HIDD_ATTRIBUTES attributes);
       /// <summary>
       /// The HidD_GetSerialNumberString routine returns the embedded string of a top-level collection that identifies the serial number of the collection's physical device.
       /// </summary>
       /// <param name="HidDeviceObject">Specifies an open handle to a top-level collection</param>
       /// <param name="Buffer">a caller-allocated buffer that the routine uses to return the requested serial number string</param>
       /// <param name="BufferLength">Specifies the length, in bytes, of a caller-allocated buffer provided at Buffer</param>
       /// <returns></returns>
       [DllImport("hid.dll")]
       private static extern Boolean HidD_GetSerialNumberString(IntPtr hidDeviceObject, IntPtr buffer, int bufferLength);

       /// <summary>
       /// The HidD_GetPreparsedData routine returns a top-level collection's preparsed data.
       /// </summary>
       /// <param name="hidDeviceObject">Specifies an open handle to a top-level collection. </param>
       /// <param name="PreparsedData">Pointer to the address of a routine-allocated buffer that contains a collection's preparsed data in a _HIDP_PREPARSED_DATA structure.</param>
       /// <returns>HidD_GetPreparsedData returns TRUE if it succeeds; otherwise, it returns FALSE.</returns>
       [DllImport("hid.dll")]
       private static extern Boolean HidD_GetPreparsedData(IntPtr hidDeviceObject, out IntPtr PreparsedData);

       [DllImport("hid.dll")]
       private static extern Boolean HidD_FreePreparsedData(IntPtr PreparsedData);

       [DllImport("hid.dll")]
       private static extern uint HidP_GetCaps(IntPtr PreparsedData, out HIDP_CAPS Capabilities);


       /// <summary>
       /// 这个函数创建、打开或将一个文件,COM端口,设备,服务或控制台。
       /// </summary>
       /// <param name="fileName">一个以null结尾的字符串,指定对象的名称</param>
       /// <param name="desiredAccess">类型的访问对象</param>
       /// <param name="shareMode">共享模式对象</param>
       /// <param name="securityAttributes">忽略;设置为NULL</param>
       /// <param name="creationDisposition">文件存在,采取行动,行动时要进行文件不存在</param>
       /// <param name="flagsAndAttributes">文件属性和标志的文件</param>
       /// <param name="templateFile">忽略</param>
       /// <returns>开放指定的文件句柄表示成功</returns>
       [DllImport("kernel32.dll", SetLastError = true)]
       private static extern IntPtr CreateFile(string fileName, uint desiredAccess, uint shareMode, uint securityAttributes, uint creationDisposition, uint flagsAndAttributes, uint templateFile);

       /// <summary>
       /// This function closes an open object handle.
       /// </summary>
       /// <param name="hObject">Handle to an open object</param>
       /// <returns></returns>
       [DllImport("kernel32.dll")]
       private static extern int CloseHandle(IntPtr hObject);

       /// <summary>
       /// This function reads data from a file, starting at the position indicated by the file pointer.
       /// </summary>
       /// <param name="file">Handle to the file to be read</param>
       /// <param name="buffer">Pointer to the buffer that receives the data read from the file </param>
       /// <param name="numberOfBytesToRead">Number of bytes to be read from the file</param>
       /// <param name="numberOfBytesRead">Pointer to the number of bytes read</param>
       /// <param name="lpOverlapped">Unsupported; set to NULL</param>
       /// <returns></returns>
       [DllImport("Kernel32.dll", SetLastError = true)]
       private static extern bool ReadFile(IntPtr file, byte[] buffer, uint numberOfBytesToRead, out uint numberOfBytesRead, IntPtr lpOverlapped);

       /// <summary>
       ///  This function writes data to a file
       /// </summary>
       /// <param name="file">Handle to the file to be written to</param>
       /// <param name="buffer">Pointer to the buffer containing the data to write to the file</param>
       /// <param name="numberOfBytesToWrite">Number of bytes to write to the file</param>
       /// <param name="numberOfBytesWritten">Pointer to the number of bytes written by this function call</param>
       /// <param name="lpOverlapped">Unsupported; set to NULL</param>
       /// <returns></returns>
       [DllImport("Kernel32.dll", SetLastError = true)]
       private static extern bool WriteFile(IntPtr file, byte[] buffer, uint numberOfBytesToWrite, out uint numberOfBytesWritten, IntPtr lpOverlapped);

       /// <summary>
       /// Registers the device or type of device for which a window will receive notifications
       /// </summary>
       /// <param name="recipient">A handle to the window or service that will receive device events for the devices specified in the NotificationFilter parameter</param>
       /// <param name="notificationFilter">A pointer to a block of data that specifies the type of device for which notifications should be sent</param>
       /// <param name="flags">A Flags that specify the handle type</param>
       /// <returns>If the function succeeds, the return value is a device notification handle</returns>
       [DllImport("User32.dll", SetLastError = true)]
       private static extern IntPtr RegisterDeviceNotification(IntPtr recipient, IntPtr notificationFilter, int flags);

       /// <summary>
       /// Closes the specified device notification handle.
       /// </summary>
       /// <param name="handle">Device notification handle returned by the RegisterDeviceNotification function</param>
       /// <returns></returns>
       [DllImport("user32.dll", SetLastError = true)]
       private static extern bool UnregisterDeviceNotification(IntPtr handle);
       #endregion
       /// <summary>
      /// 检测HID设备是否存在
      /// </summary>
      /// <returns></returns>
       public bool CheckDevice()
       {
           HID_RETURN hid_ret;
           try
           {
               hid_ret = OpenDevice(VID, PID);
               if (hid_ret == HID_RETURN.SUCCESS)
               {
                 //打开设备成功 
                   return true;
               }
               else
               {
                  //打开设备失败 
                   return false;
               }

           }
           catch (System.Exception ex)
           {
               throw;
           }
       }
    
       /// <summary>
       /// 打开指定信息的设备
       /// </summary>
       /// <param name="vID">设备的vID</param>
       /// <param name="pID">设备的pID</param>
       /// <param name="serial">设备的serial</param>,string serial
       /// <returns></returns>
       public HID_RETURN OpenDevice(UInt16 vID, UInt16 pID)
       {
           if (deviceOpened == false)
           {
               //获取连接的HID列表
               List<string> deviceList = new List<string>();
               GetHidDeviceList(ref deviceList);//获取HID的USB设备列表
               if (deviceList.Count == 0)//没有HID的USB设备
                   return HID_RETURN.NO_DEVICE_CONECTED;
               for (int i = 0; i < deviceList.Count; i++)//循环所有的HID设备
               {
                   //通过文件路径得到单个HID设备
                   IntPtr device = CreateFile(deviceList[i], DESIREDACCESS.GENERIC_READ | DESIREDACCESS.GENERIC_WRITE, 0, 0, CREATIONDISPOSITION.OPEN_EXISTING, FLAGSANDATTRIBUTES.FILE_FLAG_OVERLAPPED, 0);
                   if (device != INVALID_HANDLE_VALUE)
                   {
                       HIDD_ATTRIBUTES attributes; 
                       HidD_GetAttributes(device, out attributes); 
                       if (attributes.VendorID == vID && attributes.ProductID == pID)   // && deviceStr == serial
                       {
                           IntPtr preparseData;
                           HIDP_CAPS caps;
                           HidD_GetPreparsedData(device, out preparseData);
                           HidP_GetCaps(preparseData, out caps);
                           HidD_FreePreparsedData(preparseData);
                           outputReportLength = caps.OutputReportByteLength;
                           inputReportLength = caps.InputReportByteLength;

                           hidDevice = new FileStream(new SafeFileHandle(device, false), FileAccess.ReadWrite, inputReportLength, true);
                           deviceOpened = true;
                           BeginAsyncRead();

                           return HID_RETURN.SUCCESS;
                       }
                   }
               }
               return HID_RETURN.DEVICE_NOT_FIND;
           }
           else
               return HID_RETURN.DEVICE_OPENED;
       }
       /// <summary>
       /// 获取所有连接的hid的设备路径
       /// </summary>
       /// <returns>包含每个设备路径的字符串数组</returns>
       public static void GetHidDeviceList(ref List<string> deviceList)
       {
           Guid hUSB = Guid.Empty;
           uint index = 0;

           deviceList.Clear();
           // 取得hid设备全局id
           HidD_GetHidGuid(ref hUSB);
           //取得一个包含所有HID接口信息集合的句柄
           IntPtr hidInfoSet = SetupDiGetClassDevs(ref hUSB, 0, IntPtr.Zero, DIGCF.DIGCF_PRESENT | DIGCF.DIGCF_DEVICEINTERFACE);
           if (hidInfoSet != IntPtr.Zero)
           {
               SP_DEVICE_INTERFACE_DATA interfaceInfo = new SP_DEVICE_INTERFACE_DATA();
               interfaceInfo.cbSize = Marshal.SizeOf(interfaceInfo);
               //查询集合中每一个接口
               for (index = 0; index < MAX_USB_DEVICES; index++)
               {
                   //得到第index个接口信息
                   if (SetupDiEnumDeviceInterfaces(hidInfoSet, IntPtr.Zero, ref hUSB, index, ref interfaceInfo))
                   {
                       int buffsize = 0;
                       // 取得接口详细信息:第一次读取错误,但可以取得信息缓冲区的大小
                       SetupDiGetDeviceInterfaceDetail(hidInfoSet, ref interfaceInfo, IntPtr.Zero, buffsize, ref buffsize, null);
                       //构建接收缓冲
                       IntPtr pDetail = Marshal.AllocHGlobal(buffsize);
                       SP_DEVICE_INTERFACE_DETAIL_DATA detail = new SP_DEVICE_INTERFACE_DETAIL_DATA();
                       detail.cbSize = Marshal.SizeOf(typeof(SP_DEVICE_INTERFACE_DETAIL_DATA));
                       Marshal.StructureToPtr(detail, pDetail, false);
                       if (SetupDiGetDeviceInterfaceDetail(hidInfoSet, ref interfaceInfo, pDetail, buffsize, ref buffsize, null))
                       {
                           deviceList.Add(Marshal.PtrToStringAuto((IntPtr)((int)pDetail + 4)));
                       }
                       Marshal.FreeHGlobal(pDetail);
                   }
               }
           }
           SetupDiDestroyDeviceInfoList(hidInfoSet); 
       }


       /// <summary>
       /// 开始一次异步读
       /// </summary>
       private void BeginAsyncRead()
       {
           byte[] inputBuff = new byte[InputReportLength];
           hidDevice.BeginRead(inputBuff, 0, InputReportLength, new AsyncCallback(ReadCompleted), inputBuff);
       }
       /// <summary>
       /// 异步读取结束,发出有数据到达事件
       /// </summary>
       /// <param name="iResult">这里是输入报告的数组</param>
       private void ReadCompleted(IAsyncResult iResult)
       {
           byte[] readBuff = (byte[])(iResult.AsyncState);
           try
           {
               hidDevice.EndRead(iResult);//读取结束,如果读取错误就会产生一个异常
               byte[] reportData = new byte[readBuff.Length - 1];
               for (int i = 1; i < readBuff.Length; i++)
                   reportData[i - 1] = readBuff[i];
               report e = new report(readBuff[0], reportData);
               OnDataReceived(e); //发出数据到达消息
               BeginAsyncRead();//启动下一次读操作
           }
           catch (IOException e)//读写错误,设备已经被移除
           {
               EventArgs ex = new EventArgs();
               OnDeviceRemoved(ex);//发出设备移除消息
               CloseDevice(); 
           }
       }
       /// <summary>
       /// btye数组转换为字符串
       /// </summary>
       /// <param name="bytes"></param>
       /// <returns></returns>
       public static string ByteToHexString(byte[] bytes)
       {
           string str = string.Empty;
           if (bytes != null)
           {
               for (int i = 0; i < bytes.Length; i++)
               {
                   str += bytes[i].ToString("X2");
               }
           }
           return str;
       }

       /// <summary>
       /// btye数组转换为字符串
       /// </summary>
       /// <param name="bytes"></param>
       /// <returns></returns>
       public static byte[] HexStringToByte(string msg)
       {
           byte[] bstr = new byte[1024];
           bstr = Encoding.UTF8.GetBytes(msg);
           return bstr;
       }
       /// <summary>
       /// 事件:数据到达,处理此事件以接收输入数据
       /// </summary>
       public event EventHandler<report> DataReceived;
       protected virtual void OnDataReceived(report e)
       {
           if (DataReceived != null) DataReceived(this, e);
       }
       /// <summary>
       /// 事件:设备断开
       /// </summary>
       public event EventHandler DeviceRemoved;
       protected virtual void OnDeviceRemoved(EventArgs e)
       {
           if (DeviceRemoved != null) DeviceRemoved(this, e);
       }
       /// <summary>
       /// 关闭打开的设备
       /// </summary>
       public void CloseDevice()
       {
           if (deviceOpened == true)
           {
               hidDevice.Close();
               deviceOpened = false;
           }
       }
       /// <summary>
       /// 写入数据
       /// </summary>
       /// <param name="buffer"></param>
       /// <returns></returns>
       public string Write(report r)
       {
           byte[] buffer = null;
           if (deviceOpened)
           {
               try
               {
                   buffer = new byte[outputReportLength];
                   buffer[0] = r.reportID;
                   int maxBufferLength = 0;
                   if (r.reportBuff.Length < outputReportLength - 1)
                       maxBufferLength = r.reportBuff.Length;
                   else
                       maxBufferLength = outputReportLength - 1;
                   for (int i = 1; i < maxBufferLength; i++)
                       buffer[i] = r.reportBuff[i - 1];
                   buffer[0] = 31;
                   buffer[1] = 32;
                   buffer[2] = 33;
                   hidDevice.Write(buffer, 0, 3);
                   byte[] inputBuff = new byte[InputReportLength];
                   hidDevice.BeginRead(inputBuff, 0, InputReportLength, new AsyncCallback(ReadCompleted), inputBuff);
                   return ByteToHexString(inputBuff);

                    

                 
               }
               catch(Exception ex)
               {
                   string s = ex.ToString(); 
                   throw;
               }
               
           }
           return "123";
       }
   
    }

   public class report : EventArgs
   {
       public readonly byte reportID;
       public readonly byte[] reportBuff;
       public report(byte id, byte[] arrayBuff)
       {
           reportID = id;
           reportBuff = arrayBuff;
       }
   }
}
