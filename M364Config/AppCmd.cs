﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace M300Config
{
    public class AppCmd
    {

        /// <summary>
        /// 读取配置文件
        /// </summary>
        public void ReadConfig(ref Hashtable ht, string path)
        {

            try
            {


                XmlDocument doc = new XmlDocument();
                doc.Load(path);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点  
                ht.Clear();
                XmlNodeList nodes = rootElem.GetElementsByTagName("person");
                foreach (XmlNode node in nodes)
                {
                    string key = ((XmlElement)node).GetAttribute("key");   //获取name属性值  
                    string val = ((XmlElement)node).GetAttribute("value");   //获取name属性值 
                    ht.Add(key, val);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 读取配置文件
        /// </summary>
        public List<cmdModel> ReadConfigCmd()
        {

            List<cmdModel> list = new List<cmdModel>();
            try
            {
                string rootpath = Application.StartupPath + "\\set.xml";
                XmlDocument doc = new XmlDocument();
                doc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点  

                XmlNodeList nodes = rootElem.GetElementsByTagName("person");
                foreach (XmlNode node in nodes)
                {
                    cmdModel model = new cmdModel();
                    model.Address = ((XmlElement)node).GetAttribute("address");
                    model.CmdName = ((XmlElement)node).GetAttribute("key");   
                    model.Max = ((XmlElement)node).GetAttribute("max");   
                    model.Val = ((XmlElement)node).GetAttribute("value");  
                    model.Length = ((XmlElement)node).GetAttribute("length");   
                    model.Type = ((XmlElement)node).GetAttribute("type");   

                    model.Sort = Convert.ToInt32(model.Address,16);
                   
                    list.Add(model);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("参数错误");
            }
            return list;
        }

        /// <summary>
        /// 写入配置文件
        /// 先读取文件，然后将hash table写入
        /// </summary>
        public bool WriteConfig(Hashtable ht)
        {
            bool flag = false;
            string s = "";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                string rootpath = Application.StartupPath + "\\set.xml";

                xmlDoc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = xmlDoc.DocumentElement;   //获取根节点  
                XmlNodeList nodes = rootElem.GetElementsByTagName("person"); 
                foreach (XmlNode node in nodes)
                { 
                    XmlElement xe = (XmlElement)node;//将子节点类型转换为XmlElement类型 
                    string type = ((XmlElement)node).GetAttribute("type");   //获取type属性值  
                    if (type == "5")//type为5的表示只有由配置文件默认配置，不需要更改
                    {
                        continue;
                    }
                    string key = ((XmlElement)node).GetAttribute("key");    //获取key属性值  
                    s = key;
                    string max = ((XmlElement)node).GetAttribute("max");    //获取最大长度max  
                   
                    int maxLength = 0;
                    int.TryParse(max,out maxLength);

                    if (!ht.ContainsKey(key))//有可能此key并不在hash table 中
                        continue;

                    string vl = ht[key].ToString();
                    string length = vl.Length.ToString();
                    //将value和实际length写入
                    xe.SetAttribute("value", vl);
                    xe.SetAttribute("length", length);
                }
                xmlDoc.Save(rootpath); 
                flag = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("####WriteConfig exception:" + ex);
                throw ex ;
            }
            return flag;
        }

        public bool copy(string newPath)
        {
            bool flag = false;
            if (!Directory.Exists(newPath))
            {
                if (File.Exists(newPath))
                {
                    File.Delete(newPath);  
                }
                string rootpath = Application.StartupPath + "\\set.xml";
                //先来移动文件 
                File.Copy(rootpath, newPath, false); //复制文件 
                flag = true;
            }
            
                
            return flag;
        }
        /// <summary>
        /// 保存配置文件
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public   void SaveConfig(string key, string val)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            KeyValueConfigurationCollection sysConfig = configuration.AppSettings.Settings;
            KeyValueConfigurationElement element = sysConfig[key];
            element.Value = val;
            configuration.Save();
        }
        /// <summary>
        ///  字符串转16进制字节数组  
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        } 

    }
}

