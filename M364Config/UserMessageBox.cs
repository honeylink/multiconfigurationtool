﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace M300Config
{
    public partial class UserMessageBox : Form
    {
        SelectLanguage Language = new SelectLanguage();
        string Msg = "";
        public UserMessageBox(string m)
        {
            Msg = m;
            InitializeComponent();
        }

        private void UserMessageBox_Load(object sender, EventArgs e)
        {
           
             this.label1_nlx.Text = Msg+"!";
            Language.SetLanguage(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
