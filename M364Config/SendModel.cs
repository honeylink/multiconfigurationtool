﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M300Config
{
   /// <summary>
   /// 发送命令对象
   /// </summary>
   public class SendModel
    {
       /// <summary>
       /// 数据
       /// </summary>
       private byte[] cmdbyte;
       public byte[] Cmdbyte
       {
            get { return cmdbyte; }
            set { cmdbyte = value; }
       }
       /// <summary>
       /// 排序
       /// </summary>
       private int  sort;
       public int Sort
       {
           get { return sort; }
           set { sort = value; }
       }
    }
}
