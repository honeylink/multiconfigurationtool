﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TH6800ConfigV1
{
    public class cmdModel
    {

        public string Name { get; set; }

        private int address;
        public int Address
        {
            get { return address; }
            set { address = value; }
        }
        private string val;
        public string Val
        {
            get { return val; }
            set { val = value; }
        }
        private int max;
        public int Max
        {
            get { return max; }
            set { max = value; }
        }
        private int length;
        public int Length
        {
            get { return length; }
            set { length = value; }
        }
        private string cmd;
        public string Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }
        private double  sort;
        public double Sort
        {
            get { return sort; }
            set { sort = value; }
        }
        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Ctype{get;set;} 
    }
}
