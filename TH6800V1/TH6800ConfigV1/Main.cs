﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Threading;
using System.Text.RegularExpressions;
namespace TH6800ConfigV1
{
    public partial class Main:Form
    {
        #region 变量
        /// <summary>
        /// USB连接对象
        /// </summary>
       HusbHelp husbHelp ;
      
        /// <summary>
        /// 操作类型
        /// </summary>
       public enum  CmdType
       {
            ReadType=0,//读取设备类型 
            ReadDeviceInfo=1,//读取设备信息
            WriteDeviceInfo = 2,//写入设备信息
            ReadConType =3,//读取设通讯类型
            PROGRAM = 4,//进入PROGRAM
            Random=5 //随机数
          //  ReadConType=6//读取设备通讯类型
       }
       /// <summary>
       /// 设备类型
       /// </summary>
       public enum DeviceType
       {
           TH6800_TH = 6801,//温湿度 
           TH6800_T = 6802,//单温度 
           TH6800_TT = 6803,//双温度 
           TH6800_TH_T = 6804,//温湿度 温度
           Other=99
       }
       /// <summary>
       /// 通讯类型
       /// </summary>
       public enum ConType
       {
           ZB = 1,//Zigbee
           Inter = 2,//以太网
           GPRS = 3,//GPRS
           Other=99
       }
       byte[] ReadByte = new byte[0];
       int sendNum = 0;
       int ReSendNum = 0;
       int RelCount = 0;
       public DeviceType deviceType;//设备类型
       public ConType conType ;//通讯方式
       public CmdType currentCmdType;
       List<SendModel> listSend;///发送命令
    
        #endregion
      public Main()
       {
           this.husbHelp = new HusbHelp();
           InitializeComponent();

       }
      public Main(DeviceType dType, HusbHelp husbHelp)
        {

            this.husbHelp = husbHelp;
            husbHelp.DataRecieved += new HusbHelp.DataRecievedEventHandler(analyze);
            deviceType = dType;
            InitializeComponent();
          
        }
     
        private void Main_Load(object sender, EventArgs e)
        {
         

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {

        }

        

        private void btnContent_Click(object sender, EventArgs e)
        {
            if (!husbHelp.IsOpen)
            {
                
                if (husbHelp.OpenDevice())
                {
                    husbHelp.SendData("$$$$$$$$");       //获取设备类型   
                    currentCmdType = CmdType.ReadType;   
                }
               
            }
        }

        private void Main_Load_1(object sender, EventArgs e)
        {
            try
            {

          
            Inits();
            dataGridView1.Visible = false;
            tabControl1.Visible = true;
          
            SetPROGRAM();
            setDeviceType();
            timer1.Start();
            timer2.Start();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (!husbHelp.IsOpen)
            //{
            //    bool ckbool = husbHelp.ck_device();
            //    if (ckbool)
            //    {
            //        this.btnContent.Enabled = true;
            //    }
            //    else
            //    {
            //        this.btnContent.Enabled = false;
            //    }
            //}
        }

        private void metroButton7_Click(object sender, EventArgs e)
        {
            if (husbHelp.IsOpen)
            {
                husbHelp.CloseDevice();
             
            }
        }

        #region 处理方法
        private void analyze(byte[] bt)
        {
            switch (currentCmdType)
            {
                case CmdType.ReadType://读取设备类型
                  //  setDeviceType(bt);
                    SetPROGRAM();
                    break;
                case CmdType.ReadDeviceInfo://读取设备类型]
                    int Address = bt[1]*256+ bt[2];
                    int Len = bt[3];
                    if (Address == 0)
                    { 
                    
                    }
                    Array.Copy(bt, 4, ReadByte, Address, Len);
                  
                    if (listSend.Count == sendNum)
                    {

                        readAnalyze();
                    }
                    else
                    {
                        husbHelp.SendData(listSend[sendNum].Cmdbyte);
                        sendNum++;
                    }
                    
                    break;
                case CmdType.WriteDeviceInfo://读取设备类型
                    
                    if (listSend.Count == sendNum)
                    {
                        husbHelp.SendData("start");
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            MessageBox.Show("写入完毕");
                        }));
                    }
                    else
                    {
                        husbHelp.SendData(listSend[sendNum].Cmdbyte);
                        sendNum++;
                    }
                    
                    break;
                case CmdType.PROGRAM://进入PROGRAM 
                   //得到随机数
                    byte[] Retda = new byte[1];
                    Retda[0] = bt[0];
                    currentCmdType = CmdType.Random;
                    husbHelp.SendData(Retda);//返回随机模式
                    break;
                case CmdType.Random://进入Random
                    if (bt[0] == 0x06)//进入计算机模式成功
                    {
                        currentCmdType = CmdType.ReadConType;
                        husbHelp.SendData("r");//进入计算机模式
                    }
                    else
                    {
                        this.Invoke(new EventHandler(delegate
                        {
                         MessageBox.Show("进入计算机模式失败");
                        }));
                    }
                    break;
                case CmdType.ReadConType://得到通讯类型
                    int  Type= bt[0];
                    switch (Type)
                    {
                        case (int)ConType.ZB:
                            conType = ConType.ZB;
                            
                           this.Invoke(new EventHandler(delegate
                            {
                                this.tabControl1.TabPages[2].Parent = null;
                                this.label44.Visible = true ;
                                txtNetworkID.Visible = true;
                                label40.Visible = true;
                                label46.Visible = true;
                                txtChannel.Visible = true;
                                label45.Visible = true;
                                this.label17.Visible = false;
                                this.label16.Visible = false;
                                this.label5.Visible = false;
                                this.label7.Visible = false;
                                this.label15.Visible = false;
                                this.label6.Visible = false;
                                this.label8.Visible = false;
                                this.label14.Visible = false;
                                this.label9.Visible = false;
                                this.label10.Visible = false;
                                this.txtPhone1.Visible = false;
                                this.txtPhone2.Visible = false;
                                this.txtPhone3.Visible = false;
                                this.txtPhone4.Visible = false;
                                this.txtPhone5.Visible = false;
                                this.txtPhone6.Visible = false;
                                this.txtPhone7.Visible = false;
                                this.txtPhone8.Visible = false;
                                this.txtPhone9.Visible = false;
                                this.txtPhone10.Visible = false;
                             }));

                            break;
                        case (int)ConType.Inter:
                            conType = ConType.Inter;
                            this.Invoke(new EventHandler(delegate
                            {
                                gbxGPRS.Visible = false;
                                    gbxInter.Enabled = true;
                                    this.label17.Visible = false;
                                     this.label16.Visible = false;
                                     this.label5.Visible = false;
                                     this.label7.Visible = false;
                                     this.label15.Visible = false;
                                     this.label6.Visible = false;
                                     this.label8.Visible = false;
                                     this.label14.Visible = false;
                                     this.label9.Visible = false;
                                     this.label10.Visible = false;
                                     this.txtPhone1.Visible = false;
                                     this.txtPhone2.Visible = false;
                                     this.txtPhone3.Visible = false;
                                     this.txtPhone4.Visible = false; 
                                     this.txtPhone5.Visible = false;
                                     this.txtPhone6.Visible = false;
                                     this.txtPhone7.Visible = false;
                                     this.txtPhone8.Visible = false;
                                     this.txtPhone9.Visible = false;
                                     this.txtPhone10.Visible = false;
                                     label44.Visible = false;
                                     txtNetworkID.Visible = false;
                                     label40.Visible = false;
                                     label46.Visible = false;
                                     txtChannel.Visible = false;
                                     label45.Visible = false;
                            }));
                            break;
                        case (int)ConType.GPRS:
                            conType = ConType.GPRS;
                            this.Invoke(new EventHandler(delegate
                            {
                                gbxGPRS.Enabled = true;
                                gbxInter.Visible = false;
                                label44.Visible = false;
                                txtNetworkID.Visible = false;
                                label40.Visible = false;
                                label46.Visible = false;
                                txtChannel.Visible = false;
                                label45.Visible = false;
                            }));
                            break;
                  
                    } 
                    break;

                 
            }
        
        }


     
       /// <summary>
       /// 初始化界面
       /// </summary>
        public void Inits()
        {
            this.Invoke(new EventHandler(delegate
            {

               
                this.gbxInter.Enabled = false;
                this.gbxGPRS.Enabled = false;
                 
                this.txtChannel.Text = "";
                this.txtItServerIP.Text = "192.168.1.1";
                this.txtItServerPort.Text = "3000";
                this.cmbDHCP.SelectedIndex = 1;
                this.txtMac.Text = "";
                this.txtIP.Text = "192.168.1.1";
                this.txtMas.Text = "255.255.255.255";
                this.txtGatWay.Text = "192.168.1.1";
                this.txtDNS.Text = "8.8.8.8";
                this.txtGPRSServerIP.Text = "192.168.1.1";
                this.txtGPRSServerPort.Text = "3000";
                this.txtConnet.Text = "cmnet"; 
                //初始化
              
              
            }));
        }
	#endregion

    
        /// <summary>
        /// 读取设备信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadDevice_Click(object sender, EventArgs e)
        {
            this.btnNext.Enabled = true;
            currentCmdType = CmdType.ReadDeviceInfo;
            AppCmd cmd = new AppCmd();
            List<cmdModel> list  = cmd.ReadConfigCmd();
            cmdModel model = list.Where(l => l.Type != "5" || l.Type != "6").OrderByDescending(l => l.Address).FirstOrDefault();
            int maxAddress = model.Address + model.Max;//结束地址
            ReadByte = new byte[maxAddress];
            int starAddress = 0;//起始地址
            int i = 0;
            listSend = new List<SendModel>();
            while (starAddress <= maxAddress)
            {
                SendModel sModel = new SendModel();
                //组装数据从0x000->0x366    
                int byteLength = 0;
                byte[] address = intTobytenew(starAddress);
                //每包递减
                if (maxAddress - starAddress <= 32)//获取小于16字节的
                {
                    byteLength = maxAddress - starAddress;
                    maxAddress = 0;
                    starAddress = starAddress + byteLength;
                }
                else//获取大于16节的
                {
                    byteLength = 32;
                    starAddress = starAddress + byteLength;
                }
                byte[] bLength = intTobytenew(byteLength);
                int totalLength = 4;
                byte[] data = new byte[totalLength];
                data[0] = HusbHelp.HexStringToByte("R")[0];
                data[1] = address[0];
                data[2] = address[1];
                data[3] = bLength[1];
                sModel.Cmdbyte = data;
                sModel.Sort = starAddress;
                listSend.Add(sModel);
                i++; 
            }
            if (husbHelp.IsOpen)
            { 
                sendNum = 0;
                husbHelp.SendData(listSend[0].Cmdbyte);
                sendNum = sendNum + 1;
            }
            else
            {
                listSend = new List<SendModel>();
            }
        }
        /// <summary>
        /// 进入配置模式
        /// </summary>
        private void SetPROGRAM()
        { 
               
                 
               
                if (husbHelp.IsOpen)
                {
                    currentCmdType = CmdType.PROGRAM;
                    husbHelp.SendData("PROGRAM");//进入计算机模式
                    Thread.Sleep(100);
                }
                else
                {
                    //MessageBox.Show("111");
                }
            
        }
        #region 方法
       /// <summary>
       /// int 转Bit
       /// </summary>
       /// <param name="val"></param>
       /// <returns></returns>
       private byte[] intTobytenew(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
            byte[] Rebyte = new byte[2];
            Rebyte[0] = aa[2];
            Rebyte[1] = aa[3];
            return Rebyte;
        }
        /// <summary>
        /// 设备类型解析
        /// </summary>
        /// <param name="bt"></param>
       public void setDeviceType(  )
       {
         
            
           
           
               switch (deviceType)
               {
                   case DeviceType.TH6800_T:
                      
                           this.txtC1Name.Enabled = true;
                           this.txtC1Basic.Enabled = true;
                           this.txtC1Hys.Enabled = true;
                           this.txtC1Max.Enabled = true;
                           this.txtC1Min.Enabled = true;
                           this.Text = "TH6800_T配置";
                      
                       break;
                   case DeviceType.TH6800_TT:

                           this.txtC1Name.Enabled = true;
                           this.txtC1Basic.Enabled = true;
                           this.txtC1Hys.Enabled = true;
                           this.txtC1Max.Enabled = true;
                           this.txtC1Min.Enabled = true;

                           this.txtC3Name.Enabled = true;
                           this.txtC3Basic.Enabled = true;
                           this.txtC3Hys.Enabled = true;
                           this.txtC3Max.Enabled = true;
                           this.txtC3Min.Enabled = true;
                           this.Text = "TH6800_TT配置";
                      
                       break; 
                   case DeviceType.TH6800_TH:
                      
                          this.txtC1Name.Enabled = true;
                           this.txtC1Basic.Enabled = true;
                           this.txtC1Hys.Enabled = true;
                           this.txtC1Max.Enabled = true;
                           this.txtC1Min.Enabled = true;
                           this.txtC2Name.Enabled = true;
                           this.txtC2Basic.Enabled = true;
                           this.txtC2Hys.Enabled = true;
                           this.txtC2Max.Enabled = true;
                           this.txtC2Min.Enabled = true;
                           this.Text = "TH6800_TH配置";
                     
                       break;
                   case DeviceType.TH6800_TH_T:
                     
                           this.txtC1Name.Enabled = true;
                           this.txtC1Basic.Enabled = true;
                           this.txtC1Hys.Enabled = true;
                           this.txtC1Max.Enabled = true;
                           this.txtC1Min.Enabled = true;
                           this.txtC2Name.Enabled = true;
                           this.txtC2Basic.Enabled = true;
                           this.txtC2Hys.Enabled = true;
                           this.txtC2Max.Enabled = true;
                           this.txtC2Min.Enabled = true;
                           this.txtC3Name.Enabled = true;
                           this.txtC3Basic.Enabled = true;
                           this.txtC3Hys.Enabled = true;
                           this.txtC3Max.Enabled = true;
                           this.txtC3Min.Enabled = true;
                           this.Text = "TH6800_TH_T配置";
                      
                       break;
               }
              

          
       
       }
        /// <summary>
        /// Byte to string
        /// </summary>
        /// <param name="bt"></param>
        /// <returns></returns>
       public string  BttoInt(byte[] bt)
       {
           byte[] vals = new byte[4];
           Array.Copy(bt, 0, vals, 0, bt.Length);
           uint num = 0;
           num = BitConverter.ToUInt32(vals, 0);
           return num.ToString();
       }
     

        /// <summary>
        /// 解析读取的数据
        /// </summary>
       public void readAnalyze()
       {
           AppCmd cmd = new AppCmd();
           List<cmdModel> listc = cmd.ReadConfigCmd();
           List<cmdModel> listnew = listc;
           cmdModel model;

           for (int i = 0; i < listc.Count; i++)
           {
               try
               {
                   if (i == 21)
                   { 
                    
                   }

                   byte[] btVal = new byte[listc[i].Max];
                   Array.Copy(ReadByte, listc[i].Address, btVal, 0, listc[i].Max);
                   switch (listc[i].Type)
                   {
                       case "0":
                           listnew[i].Val = BttoInt(btVal);
                           break;
                       case "1":

                           listnew[i].Val = UCS2Tostring(btVal);
                           break;
                       case "2":
                           float num = 0;
                           num = BitConverter.ToSingle(btVal, 0);
                           listnew[i].Val = num.ToString();
                           break;
                       case "3":
                           if (i == 30 || i == 31)
                           { 
                           
                           }

                          
                           listnew[i].Val = System.Text.Encoding.ASCII.GetString(btVal).Replace("\0","");  
                           break;
                   }

               }
               catch (Exception ex)
               {


               }
                 
           }
           this.BeginInvoke(new EventHandler(delegate
           {
               Set(listnew);
               MessageBox.Show("读取完毕");
           }));
          
       }

       public void Set(List<cmdModel> list )
       {
           this.txtID.Text = list[0].Val ;
           this.txtAddress.Text = list[40].Val;
           this.txtCfirmTime.Text = list[15].Val;
           this.txtUploadTime.Text=list[18].Val;
           this.txtAlarmLen.Text = list[16].Val;
           this.txtRecordTime.Text = list[17].Val;
           this.txtPhone1.Text = list[30].Val;
           this.txtPhone2.Text = list[31].Val;
           this.txtPhone3.Text = list[32].Val;
           this.txtPhone4.Text = list[33].Val;
           this.txtPhone5.Text = list[34].Val;
           this.txtPhone6.Text = list[35].Val;
           this.txtPhone7.Text = list[36].Val;
           this.txtPhone8.Text = list[37].Val;
           this.txtPhone9.Text = list[38].Val;
           this.txtPhone10.Text = list[39].Val;
           txtNetworkID.Text = list[1].Val;
           txtChannel.Text = list[2].Val;
           txtItServerIP.Text = list[19].Val;
           txtItServerPort.Text = list[20].Val;
           int inx = 0;
           int.TryParse(list[21].Val, out inx);
           cmbDHCP.SelectedIndex = inx > 1 ? 0 : inx;
           txtMac.Text=list[22].Val;
           txtIP.Text = list[23].Val;
           txtMas.Text = list[24].Val;
           txtGatWay.Text = list[25].Val;
           txtDNS.Text = list[26].Val;
           txtGPRSServerIP.Text = list[19].Val;
           txtGPRSServerPort.Text = list[20].Val;
           txtConnet.Text = list[27].Val;
           txtUser.Text = list[28].Val;
           txtPwd.Text = list[29].Val;
           txtC1Name.Text = list[41].Val;
           txtC1Basic.Text = list[3].Val;
           txtC1Max.Text = list[4].Val;
           txtC1Min.Text = list[5].Val;
           txtC1Hys.Text = list[6].Val;
           txtC2Name.Text = list[42].Val;
            txtC2Basic.Text= list[7].Val;
            txtC2Max.Text = list[8].Val;
            txtC2Min.Text = list[9].Val;
            txtC2Hys.Text = list[10].Val;
            txtC3Name.Text = list[43].Val;
            txtC3Basic.Text = list[11].Val;
            txtC3Max.Text = list[12].Val;
            txtC3Min.Text = list[13].Val;
            txtC3Hys.Text = list[14].Val;
            textBox1.Text = list[44].Val;
       }

       public List<cmdModel> Get()
       {
           AppCmd cmd = new AppCmd();
           List<cmdModel> list = cmd.ReadConfigCmd();
           list[0].Ctype = "Basc";
           list[0].Name = "设备ID";
           list[0].Val = this.txtID.Text;
           list[40].Ctype = "Basc";
           list[40].Name = "安装地址";
           list[40].Val = this.txtAddress.Text;
           list[15].Ctype = "Basc";
           list[15].Name = "报警确认";
           list[15].Val = this.txtCfirmTime.Text;
           list[18].Ctype = "Basc";
           list[18].Name = "上传间隔";
           list[18].Val = this.txtUploadTime.Text;
           list[16].Ctype = "Basc";
           list[16].Name = "报警警号时长";
           list[16].Val = this.txtAlarmLen.Text;
           list[17].Ctype = "Basc";
           list[17].Name = "记录频率";
           list[17].Val = this.txtRecordTime.Text;
           list[30].Ctype = "Basc";
           list[30].Name = "电话号码1";
           list[30].Val = this.txtPhone1.Text.Trim();
           list[31].Ctype = "Basc";
           list[31].Name = "电话号码2";
           list[31].Val = this.txtPhone2.Text.Trim();
           list[32].Ctype = "Basc";
           list[32].Name = "电话号码3";
           list[32].Val = this.txtPhone3.Text.Trim();
           list[33].Ctype = "Basc";
           list[33].Name = "电话号码4";
           list[33].Val = this.txtPhone4.Text.Trim();
           list[34].Ctype = "Basc";
           list[34].Name = "电话号码5";
           list[34].Val = this.txtPhone5.Text.Trim();
           list[35].Ctype = "Basc";
           list[35].Name = "电话号码6";
           list[35].Val = this.txtPhone6.Text.Trim();
           list[36].Ctype = "Basc";
           list[36].Name = "电话号码7";
           list[36].Val = this.txtPhone7.Text.Trim();
           list[37].Ctype = "Basc";
           list[37].Name = "电话号码8";
           list[37].Val = this.txtPhone8.Text.Trim();
           list[38].Ctype = "Basc";
           list[38].Name = "电话号码9";
           list[38].Val = this.txtPhone9.Text.Trim();
           list[39].Ctype = "Basc";
           list[39].Name = "电话号码10";
           list[39].Val = this.txtPhone10.Text.Trim();
           list[1].Name = "网络ID";
           list[1].Ctype = "ZB";
            list[1].Val = txtNetworkID.Text;
            list[2].Name = "通道号";
            list[2].Ctype = "ZB";
           list[2].Val = txtChannel.Text;
           list[19].Name = "服务地址";
           list[19].Ctype = "It";
           list[19].Val = txtItServerIP.Text;
           list[20].Ctype = "It";
           list[20].Name = "服务端口";
           list[20].Val = txtItServerPort.Text;
           list[21].Ctype = "It";
           list[21].Name = "DHCP";
           list[21].Val = cmbDHCP.SelectedIndex.ToString();
           list[22].Name = "MAC地址";
           list[22].Ctype = "It";
           list[22].Val = txtMac.Text;
           list[23].Ctype = "It";
           list[23].Name = "本机IP地址";
           list[23].Val = txtIP.Text;
           list[24].Ctype = "It";
           list[24].Name = "子网掩码";
           list[24].Val = txtMas.Text;
           list[25].Ctype = "It";
           list[25].Name = "默认网关";
           list[25].Val = txtGatWay.Text;
           list[26].Ctype = "It";
           list[26].Name = "DNS";
           list[26].Val = txtDNS.Text;
           list[19].Ctype = conType == ConType.Inter ? "It" : "GPRS";
           list[19].Name = "服务地址";
           list[19].Val = txtGPRSServerIP.Text;
           list[20].Ctype = conType == ConType.Inter ? "It" : "GPRS";
           list[20].Name = "服务端口";
           list[20].Val = txtGPRSServerPort.Text;
           list[27].Ctype = "GPRS";
           list[27].Name = "接入点";
           list[27].Val = txtConnet.Text;
           list[28].Ctype = "GPRS";
           list[28].Name = "用户名";
           list[28].Val = txtUser.Text;
           list[29].Ctype = "GPRS";
           list[29].Name = "密码";
           list[29].Val = txtPwd.Text;
           list[41].Ctype = "Basc";
           list[41].Name = "监测点1名称"; 
           list[41].Val = txtC1Name.Text;
           list[3].Ctype = "Basc";
           list[3].Name = "监测点1基准值";
           list[3].Val = txtC1Basic.Text;
           list[4].Ctype = "Basc";
           list[4].Name = "监测点1最大值";
           list[4].Val = txtC1Max.Text;
           list[5].Ctype = "Basc";
           list[5].Name = "监测点1最小值";
           list[5].Val = txtC1Min.Text;
           list[6].Ctype = "Basc";
           list[6].Name = "监测点1迟滞值";
           list[6].Val = txtC1Hys.Text;
           list[42].Ctype = "Basc";
           list[42].Name = "监测点2名称";
           list[42].Val = txtC2Name.Text;
           list[7].Ctype = "Basc";
           list[7].Name = "监测点2基准值";
           list[7].Val = txtC2Basic.Text;
           list[8].Ctype = "Basc";
           list[8].Name = "监测点2最大值";
           list[8].Val = txtC2Max.Text;
           list[9].Ctype = "Basc";
           list[9].Name = "监测点2最小值";
           list[9].Val = txtC2Min.Text;
           list[10].Ctype = "Basc";
           list[10].Name = "监测点2迟滞值";
           list[10].Val = txtC2Hys.Text;
           list[43].Ctype = "Basc";
           list[43].Name = "监测点3名称";
           list[43].Val = txtC3Name.Text;
           list[11].Ctype = "Basc";
           list[11].Name = "监测点3基准值";
           list[11].Val = txtC3Basic.Text;
           list[12].Ctype = "Basc";
           list[12].Name = "监测点3最大值";
           list[12].Val = txtC3Max.Text;
           list[13].Ctype = "Basc";
           list[13].Name = "监测点3最小值";
           list[13].Val = txtC3Min.Text;
           list[14].Ctype = "Basc";
           list[14].Name = "监测点3基准值";
           list[14].Val = txtC3Hys.Text;
           list[44].Name = "目标地址";
           list[44].Val = textBox1.Text;
           list[44].Ctype = "ZB";
           return list;
       }

    #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {

          
            if ((currentCmdType == CmdType.ReadDeviceInfo )&& sendNum < listSend.Count - 1)
            {

                if (ReSendNum == sendNum)
                {
                    RelCount++;
                }
                else
                {
                    ReSendNum = sendNum;
                    RelCount = 1;
                }
                if (RelCount == 2)
                {
                    husbHelp.SendData(listSend[sendNum - 1].Cmdbyte);

                }
                
            }
            }
            catch (Exception ex)
            {

               
            }
        }

        private void btnDevceDown_Click(object sender, EventArgs e)
        {
            currentCmdType = CmdType.WriteDeviceInfo;
            List<cmdModel> list = Get();
             cmdModel model = list.Where(l => l.Type != "5" || l.Type != "6").OrderByDescending(l => l.Address).FirstOrDefault();
            int maxAddress = model.Address + model.Max;//结束地址 
            byte[] bt = new byte[maxAddress];
            foreach (cmdModel m in list)
            {
                byte[] b = new byte[m.Max];
                switch (m.Type)
                {
                    case "0":
                         int num = 0;
                         int.TryParse(m.Val, out num); 
                         byte[] vals = BitConverter.GetBytes(num);
                         Array.Copy(vals, 0, b, 0, vals.Length >= m.Max ? m.Max : vals.Length); 
                        break;
                    case "1":

                        byte[] val2 =stringToUCS2(m.Val);
                         Array.Copy(val2, 0, b, 0, val2.Length >= m.Max ? m.Max : val2.Length); 
                        break;
                    case "2":
                       float num1 = 0;
                       float.TryParse(m.Val, out num1);
                       byte[] val3 = BitConverter.GetBytes(num1);
                       Array.Copy(val3, 0, b, 0, val3.Length >= m.Max ? m.Max : val3.Length); 
                        break;
                    case "3":
                        byte[] val4 = System.Text.Encoding.ASCII.GetBytes(m.Val);
                        Array.Copy(val4, 0, b, 0, val4.Length >= m.Max ? m.Max : val4.Length);
                        break;
                }
                Array.Copy(b, 0, bt, m.Address, b.Length); 


            }
            int starIndex = 0;//起始地址
            int i = 0;
            listSend = new List<SendModel>();

          
            while (starIndex < maxAddress)
            {
                SendModel sModel = new SendModel();
                int byteLength = 0;//本包长度
                int CheckCode = 0;//校验码

                if (maxAddress - starIndex < 32)//获取小于16字节的
                {
                    byteLength = maxAddress - starIndex;
                }
                else//获取大于16节的
                {
                    byteLength = 32;
                }
                byte[] data = new byte[byteLength];
                Array.Copy(bt, starIndex, data, 0, byteLength);
                byte[] address = intTobytenew(starIndex);//得到地址 
                byte[] bLength = intTobytenew(byteLength); //得到数据长度  
                foreach (byte v in address)
                {
                    int bv = 0;
                    int.TryParse(v.ToString(), out bv);
                    CheckCode += bv;
                }
                foreach (byte v in bLength)
                {
                    int bv = 0;
                    int.TryParse(v.ToString(), out bv);
                    CheckCode += bv;
                }
                foreach (byte v in data)
                {
                    int bv = 0;
                    int.TryParse(v.ToString(), out bv);
                    CheckCode += bv;
                }
                byte[] Bcheck = BitConverter.GetBytes(CheckCode);
                byte[] sendData = new byte[4 + data.Length + 1];
                sendData[0] = HusbHelp.HexStringToByte("w")[0];
                sendData[1] = address[0];
                sendData[2] = address[1];
                Array.Copy(bLength, 1, sendData, 3, 1);
                Array.Copy(data, 0, sendData, 4, byteLength);
                Array.Copy(Bcheck, 0, sendData, 4 + byteLength, 1);
                starIndex = starIndex + byteLength;
                sModel.Cmdbyte = sendData;
                listSend.Add(sModel);
            }

 
            if (husbHelp.IsOpen)
            {
                sendNum = 0;
             
                husbHelp.SendData(listSend[0].Cmdbyte);

                sendNum = sendNum + 1;
            }
            else
            {
                listSend = new List<SendModel>();
            }
        }
        
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            { 
                string path = this.openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(path))
                {
                    AppCmd cmd = new AppCmd();
                    List<cmdModel>  list=new List<cmdModel>();
                    cmd.ReadConfig(ref list, path);
                    Set(list);
                   // SetModel();
                  //  MessagShow(Language.getMsg("ReadFileOk"));
                }
            }
            catch (Exception ex)
            {
               

            }
        }
        
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
           string path = this.saveFileDialog1.FileName;
           AppCmd cmd = new AppCmd();
           if (cmd.copy(path))
           {
              MessageBox.Show("保存成功");
           }
           else
           {
              MessageBox.Show("保存失败");
           }
        }
        /// <summary>
        ///  Ucs  To  string
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns></returns>
        private string UCS2Tostring(byte[] by)
        {
            string RelStr = "";
            int star = 0;
            for (int i = 0; i < by.Length / 2; i++)
            {

                byte[] val = new byte[2];
                Array.Copy(by, star, val, 0, 2);
                val.Reverse();
                Array.Reverse(val);
                RelStr = RelStr + System.Text.Encoding.Unicode.GetString(val);
                star = star + 2;

            }
            return RelStr;
        }
        /// <summary>
        /// string To Ucs
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns></returns>
        private byte[] stringToUCS2(string str)
        {
            byte[] Relval = new byte[str.Length * 2];
            string[] strs = new string[str.Length];
            char[] arr = str.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                strs[i] = arr[i].ToString();
            }
            for (int i = 0; i < strs.Length; i++)
            {
                string s = strs[i];
                byte[] sUcs = new byte[2];
                sUcs = System.Text.Encoding.Unicode.GetBytes(s);
                sUcs.Reverse();

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(sUcs);
                List<byte> blist = new List<byte>(); blist.AddRange(sUcs);
                Array.Copy(blist.ToArray(), 0, Relval, i * 2, blist.Count);

            }
            return Relval;
        }
       

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<cmdModel> list = Get();
            AppCmd cmd = new AppCmd();
            if (cmd.WriteConfig(list))
            {
                this.saveFileDialog1.ShowDialog();
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        
        public void MathVilid(KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '\b'))
            {
                e.Handled = true;
                MessageBox.Show("只能输入数字");

            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtC1Min_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender; 
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "0";
            }
            else
            {
                double.TryParse(senderText.Text.Trim(), out v);
                if (v < -100.00 || v > 1000.99)
                {
                    senderText.Text = "0";
                    MessageBox.Show("输入范围必须在：-100.00~1000.99之间");
                    
                }
            }
        }

        private void txtC1Max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtC1Min;
            maxCheck(minTxt, senderText);
        }

        private void maxCheck(TextBox minTxt, TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessageBox.Show("只能输入数字");
                maxTxt.Text = "0";
            }
            else
            {
                double.TryParse(maxTxt.Text.Trim(), out v);
                if (v < -100.00 || v > 1000.99)
                {
                    maxTxt.Text = "0";
                    MessageBox.Show("输入范围必须在：-100.00~1000.99之间");
                    
                }
                else
                {
                    double minv = 0;
                    double.TryParse(minTxt.Text, out minv);
                    if (v < minv)
                    {
                        maxTxt.Text = "0";
                        MessageBox.Show(" 最大值输入必须大于或者等于最小值"); 
                    }

                }
            }
        }

        private void txtC1Basic_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            
            maxCheck(senderText);
        }
        private void maxCheck(TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessageBox.Show("只能输入数字");
                maxTxt.Text = "0";
            }

        }

        private void txtC1Hys_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender; 
            maxCheck(senderText);
        }

        private void txtC2Min_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender; 
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "0";
            }
            else
            {
                double.TryParse(senderText.Text.Trim(), out v);
                if (v < -100.00 || v > 1000.99)
                {
                    senderText.Text = "0";
                    MessageBox.Show("输入范围必须在：-100.00~1000.99之间");

                }
            }
        } 
        private void txtC2Max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtC2Min;
            maxCheck(minTxt, senderText);
        }

        private void txtC2Basic_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender; 
            maxCheck(senderText);
        }

        private void txtC3Max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtC3Min;
            maxCheck(minTxt, senderText);
        }

        private void txtC3Min_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "0";
            }
            else
            {
                double.TryParse(senderText.Text.Trim(), out v);
                if (v < -100.00 || v > 1000.99)
                {
                    senderText.Text = "0";
                    MessageBox.Show("输入范围必须在：-100.00~1000.99之间");

                }
            }
        }

        private void txtGPRSServerPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }

        private void txtItServerPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {

            int Index = tabControl1.SelectedIndex;

            btnNext.Enabled = true;
            tabControl1.Visible = true;
            if (this.dataGridView1.Visible)
            {
                dataGridView1.Visible = false;
              
            }
            else
            { 
                if (Index > 0)
                {
                    this.btnBack.Visible = true;
                    tabControl1.SelectedIndex = Index - 1;
                }
                if (Index - 1 == 0)
                {
                    this.btnBack.Visible = false;
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
           int Index= this.tabControl1.SelectedIndex;
           this.btnBack.Enabled = true;
           this.btnBack.Visible = true;
           if (conType != ConType.ZB)
           {
               if (Index + 1 == 3)
               {
                   List<cmdModel> list = Get();
                   string type = conType == ConType.GPRS ? "GPRS" : "It";
                   List<cmdModel> list1 = list.Where(l => l.Ctype == "Basc" || l.Ctype == type).ToList(); ;
                   this.BeginInvoke(new EventHandler(delegate
                   {
                       this.dataGridView1.DataSource = list1;
                   }));
                   dataGridView1.Visible = true;
                   this.tabControl1.Visible = false;
                   this.btnDevceDown.Enabled = true;
                   btnNext.Enabled = false;
               }
               else
               {
                   this.btnDevceDown.Enabled = false;
                   btnNext.Enabled = true;
                   this.tabControl1.SelectedIndex = this.tabControl1.SelectedIndex + 1;

               }
              
           }
           else
           {
               if (Index + 1 == 2)
               {
                   List<cmdModel> list = Get();
                   string type = conType == ConType.GPRS ? "GPRS" : "It";
                   List<cmdModel> list1 = list.Where(l => (l.Ctype == "Basc" || l.Ctype == "ZB") && l.Name.Contains("电话号码") == false).ToList(); ;
                   this.BeginInvoke(new EventHandler(delegate
                   {
                       dataGridView1.Visible = true;
                       this.tabControl1.Visible = false;
                       this.btnDevceDown.Enabled = true;
                       btnNext.Enabled = false;
                       this.dataGridView1.DataSource = list1;
                   }));
                   
               }
               else
               {
                   this.btnDevceDown.Enabled = false;
                   btnNext.Enabled = true;
                   this.tabControl1.SelectedIndex = this.tabControl1.SelectedIndex + 1;

               }
           }
        }
        public void gridlist()
        {
            List<cmdModel> list = Get();
            this.dataGridView1.DataSource = list;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panelMonitor1.Enabled=true;
            this.txtC1Name.Enabled = true;
            this.txtC1Basic.Enabled = true;
            this.txtC1Hys.Enabled = true;
            this.txtC1Max.Enabled = true;
            this.txtC1Min.Enabled = true;
            this.txtC2Name.Enabled = true;
            this.txtC2Basic.Enabled = true;
            this.txtC2Hys.Enabled = true;
            this.txtC2Max.Enabled = true;
            this.txtC2Min.Enabled = true;
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            husbHelp.DataRecieved -= new HusbHelp.DataRecievedEventHandler(analyze);
        }

        private void txtID_Leave(object sender, EventArgs e)
        {
    
            /*TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "1";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v <= 0 || v > 65535)
                {
                    senderText.Text = "1";
                    MessageBox.Show("输入范围必须在：1~65535之间");
                }
            }*/
        }

        private void txtPhone1_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$"); 
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            } 
        }

        private void txtPhone2_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            } 
        }

       

        private void txtUploadTime_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v=0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
          else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 0 || v > 65535)
                {
                    senderText.Text = "1";
                    MessageBox.Show("输入范围必须在：0~65535之间");
                }
            }
         }

        private void txtRecordTime_Leave_1(object sender, EventArgs e)
        {
         TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v=0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
          else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 0 || v > 65535)
                {
                    senderText.Text = "1";
                    MessageBox.Show("输入范围必须在：0~65535之间");
                }
            }
        }

        private void txtAlarmLen_Leave(object sender, EventArgs e)
        {
        TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v=0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
          else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 1 || v > 255)
                {
                    senderText.Text = "1";
                    MessageBox.Show("输入范围必须在：1~255之间");
                }
            }
        }

        private void txtCfirmTime_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 1 || v > 255)
                {
                    senderText.Text = "1";
                    MessageBox.Show("输入范围必须在：1~255之间");
                }
            }
        }

        private void txtGPRSServerPort_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v <0 || v > 65535)
                {
                    senderText.Text = "3000";
                    MessageBox.Show("输入范围必须在：0~65535之间");
                }
            }

        }

        private void txtItServerPort_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 0 || v > 65535)
                {
                    senderText.Text = "3000";
                    MessageBox.Show("输入范围必须在：0~65535之间");
                }
            }
        }

        private void txtNetworkID_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 1 || v > 65535)
                {
                    senderText.Text = "7212";
                    MessageBox.Show("输入范围必须在：1~65535之间");
                }
            }
        }

        private void txtChannel_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            Regex regex = new Regex(@"^[0-9]*$");
            int v = 0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessageBox.Show("只能输入数字");
                senderText.Text = "";
            }
            else
            {
                int.TryParse(senderText.Text.Trim(), out v);
                if (v < 1 || v > 16)
                {
                    senderText.Text = "4";
                    MessageBox.Show("输入范围必须在：1~16之间");
                }
            }
        }
        
         

    }
}
