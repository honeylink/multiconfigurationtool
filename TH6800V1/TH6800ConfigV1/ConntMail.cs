﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TH6800ConfigV1
{ 
    public partial class ConntMail : Form
    {
        /// <summary>
        /// USB连接对象
        /// </summary>
        HusbHelp husbHelp = new HusbHelp();
        public TH6800ConfigV1.Main.DeviceType deviceType;//设备类型
        public TH6800ConfigV1.Main.CmdType currentCmdType;
        public TH6800ConfigV1.Main.ConType conType;//通讯方式
        public ConntMail()
        {
            InitializeComponent();
        }
        Main main ;
        private void btnConnet_Click(object sender, EventArgs e)
        {
            if (!husbHelp.IsOpen)
            {
                if (husbHelp.OpenDevice())
                {
                   // husbHelp.DataRecieved -= new HusbHelp.DataRecievedEventHandler(analyze1);
                    MessageBox.Show("设备连接成功"); 
                    btnConfig.Enabled = true;
                    btnConnet.Enabled = false;

                }
                else
                {
                    MessageBox.Show("设备连接失败"); 

                }
            }
        }

        private void timer_CkDevice_Tick(object sender, EventArgs e)
        {
            if (!husbHelp.IsOpen)
            {
                bool ckbool = husbHelp.ck_device();
                if (ckbool)
                {
                    this.btnConnet.Enabled = true;
                }
                else
                {
                    this.btnConnet.Enabled = false;
                }
            }
 
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            try
            {
                husbHelp.DataRecieved += new HusbHelp.DataRecievedEventHandler(analyze1);
                husbHelp.SendData("$$$$$$$$");//获取设备类型   
                currentCmdType = TH6800ConfigV1.Main.CmdType.ReadType;   

            }
            catch (Exception ex)
            {
                 MessageBox.Show( "通讯异常"); 
            }
        }

        private void ConntMail_Load(object sender, EventArgs e)
        {
            timer_CkDevice.Start();
         
        }

        #region 处理方法
        private void analyze1(byte[] bt)
        {
            try
            {

          
            switch (currentCmdType)
            {
                    
                case TH6800ConfigV1.Main.CmdType.ReadType://读取设备类型
                    husbHelp.DataRecieved -= new HusbHelp.DataRecievedEventHandler(analyze1);
                    setDeviceType(bt);
                    break;
            }
            }
            catch (Exception ex)
            {

              
            }
        }
        /// <summary>
        /// 设备类型解析
        /// </summary>
        /// <param name="bt"></param>
        public void setDeviceType(byte[] bt)
        {
            try
            {


                deviceType = TH6800ConfigV1.Main.DeviceType.Other;
                conType = TH6800ConfigV1.Main.ConType.Other;
                if (bt[0].ToString() == "37" && bt[bt.Length - 1].ToString() == "37")//获取设备类型
                {
                    string deviceInfo = bt[1].ToString("X") + bt[2].ToString("X").PadLeft(2, '0') + "_" + bt[3].ToString("X");
                    deviceType = (TH6800ConfigV1.Main.DeviceType)int.Parse(deviceInfo.Split('_')[0]);

                    this.Invoke(new EventHandler(delegate
                    {
                        if (main == null || main.IsDisposed)
                        {
                            main = new Main(deviceType, husbHelp);
                            main.Show();
                        }
                    }));

                }
                else
                {
                    this.Invoke(new EventHandler(delegate
                    {
                        MessageBox.Show("数据异常");
                    }));
                }
            }
            catch (Exception ex)
            {


            }

        }
        #endregion
    }
}
