﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace TH6800ConfigV1
{
    public class AppCmd
    {

        /// <summary>
        /// 读取配置文件
        /// </summary>
        public void ReadConfig(ref List<cmdModel>  list , string path)
        {

            try
            {


                XmlDocument doc = new XmlDocument();
                doc.Load(path);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点  
               
                XmlNodeList nodes = rootElem.GetElementsByTagName("parm");
                foreach (XmlNode node in nodes)
                {
                    cmdModel cm = new cmdModel();
                    cm.Address = Convert.ToInt32(((XmlElement)node).GetAttribute("address"), 16);
                    cm.Max = int.Parse(((XmlElement)node).GetAttribute("max"));   //获取name属性值  
                    cm.Val = ((XmlElement)node).GetAttribute("value");   //获取name属性值 
                    int len = 0;
                    int.TryParse(((XmlElement)node).GetAttribute("length"), out len);
                    cm.Length = len;   //获取name属性值 
                    cm.Type = ((XmlElement)node).GetAttribute("type");   //获取name属性值 
                    int sort = 0;
                    int.TryParse(((XmlElement)node).GetAttribute("sort"), out sort);
                    cm.Sort = sort;
                    list.Add(cm);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 读取配置文件
        /// </summary>
        public List<cmdModel> ReadConfigCmd()
        {

            List<cmdModel> list = new List<cmdModel>();
            try
            {
                string rootpath = Application.StartupPath + "\\template.xml";
                XmlDocument doc = new XmlDocument();
                doc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点  

                XmlNodeList nodes = rootElem.GetElementsByTagName("parm");
                foreach (XmlNode node in nodes)
                {
                    cmdModel model = new cmdModel(); 
                    model.Address =Convert.ToInt32(((XmlElement)node).GetAttribute("address"), 16) ; 
                    model.Max =int.Parse(((XmlElement)node).GetAttribute("max"));   //获取name属性值  
                    model.Val = ((XmlElement)node).GetAttribute("value");   //获取name属性值 
                    int len = 0;
                    int.TryParse(((XmlElement)node).GetAttribute("length"), out len);
                    model.Length = len;   //获取name属性值 
                    model.Type = ((XmlElement)node).GetAttribute("type");   //获取name属性值 
                    int sort = 0;
                    int.TryParse(((XmlElement)node).GetAttribute("sort"), out sort);
                    model.Sort =sort ;
                   
                    list.Add(model);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("参数错误");
            }
            return list;
        }

        /// <summary>
        /// 写入配置文件
        /// </summary>
        public bool WriteConfig(List<cmdModel>  list)
        {
            bool flag = false;
            string s = "";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                string rootpath = Application.StartupPath + "\\template.xml"; 
                xmlDoc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = xmlDoc.DocumentElement;   //获取根节点  
                XmlNodeList nodes = rootElem.GetElementsByTagName("parm"); 
                foreach (XmlNode node in nodes)
                { 
                    XmlElement xe = (XmlElement)node;//将子节点类型转换为XmlElement类型 
                    string type = ((XmlElement)node).GetAttribute("type");   //获取name属性值   
                    int key =Convert.ToInt32(((XmlElement)node).GetAttribute("address"), 16);  //获取name属性值   
                    string max = ((XmlElement)node).GetAttribute("max");   //获取name属性值   
                    int maxLength = 0;
                    int.TryParse(max,out maxLength);
                    cmdModel cm = list.Where(l => l.Address==  key).FirstOrDefault();
                    string vl = cm.Val;
                    string length = vl.Length.ToString();  
                    xe.SetAttribute("value", vl);
                    xe.SetAttribute("length", length);
               

                }
                xmlDoc.Save(rootpath); 
                flag = true;
            }
            catch (Exception ex)
            {
              
                throw ex ;
            }



            return flag;
        }

        public bool copy(string newPath)
        {
            bool flag = false;
            if (!Directory.Exists(newPath))
            {
                if (File.Exists(newPath))
                {
                    File.Delete(newPath);  
                }
                string rootpath = Application.StartupPath + "\\template.xml";
                //先来移动文件 
                File.Copy(rootpath, newPath, false); //复制文件 
                flag = true;
            }
            
                
            return flag;
        }
        /// <summary>
        /// 保存配置文件
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public   void SaveConfig(string key, string val)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            KeyValueConfigurationCollection sysConfig = configuration.AppSettings.Settings;
            KeyValueConfigurationElement element = sysConfig[key];
            element.Value = val;
            configuration.Save();
        }
        /// <summary>
        ///  字符串转16进制字节数组  
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        } 

    }
}

