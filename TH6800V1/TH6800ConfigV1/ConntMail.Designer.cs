﻿namespace TH6800ConfigV1
{
    partial class ConntMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnConnet = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.timer_CkDevice = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnConnet
            // 
            this.btnConnet.Enabled = false;
            this.btnConnet.Location = new System.Drawing.Point(42, 43);
            this.btnConnet.Name = "btnConnet";
            this.btnConnet.Size = new System.Drawing.Size(180, 50);
            this.btnConnet.TabIndex = 0;
            this.btnConnet.Text = "连接设备";
            this.btnConnet.UseVisualStyleBackColor = true;
            this.btnConnet.Click += new System.EventHandler(this.btnConnet_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Enabled = false;
            this.btnConfig.Location = new System.Drawing.Point(42, 124);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(180, 50);
            this.btnConfig.TabIndex = 1;
            this.btnConfig.Text = "进入配置";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // timer_CkDevice
            // 
            this.timer_CkDevice.Tick += new System.EventHandler(this.timer_CkDevice_Tick);
            // 
            // ConntMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 204);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnConnet);
            this.Name = "ConntMail";
            this.Text = "TH6800配置工具V1.1";
            this.Load += new System.EventHandler(this.ConntMail_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnet;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Timer timer_CkDevice;
    }
}