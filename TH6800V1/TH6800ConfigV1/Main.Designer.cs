﻿namespace TH6800ConfigV1
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.gbxInter = new System.Windows.Forms.GroupBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.cmbDHCP = new System.Windows.Forms.ComboBox();
            this.txtMac = new System.Windows.Forms.TextBox();
            this.txtGatWay = new System.Windows.Forms.TextBox();
            this.txtMas = new System.Windows.Forms.TextBox();
            this.txtDNS = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.txtItServerPort = new System.Windows.Forms.TextBox();
            this.txtItServerIP = new System.Windows.Forms.TextBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.gbxGPRS = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtConnet = new System.Windows.Forms.TextBox();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.txtGPRSServerPort = new System.Windows.Forms.TextBox();
            this.txtGPRSServerIP = new System.Windows.Forms.TextBox();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPhone10 = new System.Windows.Forms.TextBox();
            this.txtPhone1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPhone2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPhone5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhone3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhone8 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPhone4 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPhone7 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPhone6 = new System.Windows.Forms.TextBox();
            this.txtPhone9 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtChannel = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtNetworkID = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.panelMonitor3 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.txtC3Basic = new System.Windows.Forms.TextBox();
            this.txtC3Hys = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtC3Name = new System.Windows.Forms.TextBox();
            this.txtC3Min = new System.Windows.Forms.TextBox();
            this.txtC3Max = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panelMonitor2 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.txtC2Basic = new System.Windows.Forms.TextBox();
            this.txtC2Name = new System.Windows.Forms.TextBox();
            this.txtC2Hys = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtC2Min = new System.Windows.Forms.TextBox();
            this.txtC2Max = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panelMonitor1 = new System.Windows.Forms.Panel();
            this.txtC1Name = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtC1Min = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtC1Basic = new System.Windows.Forms.TextBox();
            this.txtC1Hys = new System.Windows.Forms.TextBox();
            this.txtC1Max = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtRecordTime = new System.Windows.Forms.TextBox();
            this.txtCfirmTime = new System.Windows.Forms.TextBox();
            this.txtUploadTime = new System.Windows.Forms.TextBox();
            this.txtAlarmLen = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnDevceDown = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReadDevice1 = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.val = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbxInter.SuspendLayout();
            this.gbxGPRS.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panelMonitor3.SuspendLayout();
            this.panelMonitor2.SuspendLayout();
            this.panelMonitor1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "配置文件|*.xml";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // gbxInter
            // 
            this.gbxInter.Controls.Add(this.label53);
            this.gbxInter.Controls.Add(this.label52);
            this.gbxInter.Controls.Add(this.label51);
            this.gbxInter.Controls.Add(this.label47);
            this.gbxInter.Controls.Add(this.label50);
            this.gbxInter.Controls.Add(this.label49);
            this.gbxInter.Controls.Add(this.label48);
            this.gbxInter.Controls.Add(this.label38);
            this.gbxInter.Controls.Add(this.cmbDHCP);
            this.gbxInter.Controls.Add(this.txtMac);
            this.gbxInter.Controls.Add(this.txtGatWay);
            this.gbxInter.Controls.Add(this.txtMas);
            this.gbxInter.Controls.Add(this.txtDNS);
            this.gbxInter.Controls.Add(this.txtIP);
            this.gbxInter.Controls.Add(this.txtItServerPort);
            this.gbxInter.Controls.Add(this.txtItServerIP);
            this.gbxInter.Controls.Add(this.metroLabel19);
            this.gbxInter.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gbxInter.Location = new System.Drawing.Point(23, 151);
            this.gbxInter.Name = "gbxInter";
            this.gbxInter.Size = new System.Drawing.Size(604, 178);
            this.gbxInter.TabIndex = 1;
            this.gbxInter.TabStop = false;
            this.gbxInter.Text = "以太网通信";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(348, 94);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(35, 14);
            this.label53.TabIndex = 55;
            this.label53.Text = "DNS:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(313, 124);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(70, 14);
            this.label52.TabIndex = 55;
            this.label52.Text = "子网掩码:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(325, 57);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 14);
            this.label51.TabIndex = 55;
            this.label51.Text = "本机IP:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(340, 23);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 14);
            this.label47.TabIndex = 54;
            this.label47.Text = "端口:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(46, 124);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(42, 14);
            this.label50.TabIndex = 53;
            this.label50.Text = "DHCP:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(18, 89);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 14);
            this.label49.TabIndex = 53;
            this.label49.Text = "默认网关:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(25, 54);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(63, 14);
            this.label48.TabIndex = 53;
            this.label48.Text = "MAC地址:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(84, 14);
            this.label38.TabIndex = 53;
            this.label38.Text = "服务器地址:";
            // 
            // cmbDHCP
            // 
            this.cmbDHCP.FormattingEnabled = true;
            this.cmbDHCP.Items.AddRange(new object[] {
            "关闭",
            "启用"});
            this.cmbDHCP.Location = new System.Drawing.Point(90, 121);
            this.cmbDHCP.Name = "cmbDHCP";
            this.cmbDHCP.Size = new System.Drawing.Size(139, 22);
            this.cmbDHCP.TabIndex = 52;
            // 
            // txtMac
            // 
            this.txtMac.Location = new System.Drawing.Point(90, 54);
            this.txtMac.Name = "txtMac";
            this.txtMac.Size = new System.Drawing.Size(139, 23);
            this.txtMac.TabIndex = 51;
            // 
            // txtGatWay
            // 
            this.txtGatWay.Location = new System.Drawing.Point(89, 85);
            this.txtGatWay.Name = "txtGatWay";
            this.txtGatWay.Size = new System.Drawing.Size(139, 23);
            this.txtGatWay.TabIndex = 51;
            this.txtGatWay.Text = "192.168.1.1";
            // 
            // txtMas
            // 
            this.txtMas.Location = new System.Drawing.Point(388, 121);
            this.txtMas.Name = "txtMas";
            this.txtMas.Size = new System.Drawing.Size(139, 23);
            this.txtMas.TabIndex = 50;
            this.txtMas.Text = "255.255.255.255";
            // 
            // txtDNS
            // 
            this.txtDNS.Location = new System.Drawing.Point(390, 85);
            this.txtDNS.Name = "txtDNS";
            this.txtDNS.Size = new System.Drawing.Size(139, 23);
            this.txtDNS.TabIndex = 49;
            this.txtDNS.Text = "8.8.8.8";
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(390, 54);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(139, 23);
            this.txtIP.TabIndex = 48;
            this.txtIP.Text = "192.168.1.1";
            // 
            // txtItServerPort
            // 
            this.txtItServerPort.Location = new System.Drawing.Point(390, 22);
            this.txtItServerPort.Name = "txtItServerPort";
            this.txtItServerPort.Size = new System.Drawing.Size(139, 23);
            this.txtItServerPort.TabIndex = 47;
            this.txtItServerPort.Text = "3000";
            this.txtItServerPort.Leave += new System.EventHandler(this.txtItServerPort_Leave);
            // 
            // txtItServerIP
            // 
            this.txtItServerIP.Location = new System.Drawing.Point(89, 20);
            this.txtItServerIP.Name = "txtItServerIP";
            this.txtItServerIP.Size = new System.Drawing.Size(139, 23);
            this.txtItServerIP.TabIndex = 43;
            this.txtItServerIP.Text = "192.168.1.1";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(18, 85);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(0, 0);
            this.metroLabel19.TabIndex = 27;
            // 
            // gbxGPRS
            // 
            this.gbxGPRS.Controls.Add(this.label35);
            this.gbxGPRS.Controls.Add(this.label4);
            this.gbxGPRS.Controls.Add(this.label3);
            this.gbxGPRS.Controls.Add(this.label2);
            this.gbxGPRS.Controls.Add(this.label1);
            this.gbxGPRS.Controls.Add(this.txtUser);
            this.gbxGPRS.Controls.Add(this.txtConnet);
            this.gbxGPRS.Controls.Add(this.txtPwd);
            this.gbxGPRS.Controls.Add(this.txtGPRSServerPort);
            this.gbxGPRS.Controls.Add(this.txtGPRSServerIP);
            this.gbxGPRS.Controls.Add(this.metroLabel22);
            this.gbxGPRS.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gbxGPRS.Location = new System.Drawing.Point(23, 9);
            this.gbxGPRS.Name = "gbxGPRS";
            this.gbxGPRS.Size = new System.Drawing.Size(604, 136);
            this.gbxGPRS.TabIndex = 2;
            this.gbxGPRS.TabStop = false;
            this.gbxGPRS.Text = "GPRS通信";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(-1, 30);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(84, 14);
            this.label35.TabIndex = 51;
            this.label35.Text = "服务器地址:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 50;
            this.label4.Text = "接入点：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 49;
            this.label3.Text = "用户:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(341, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 14);
            this.label2.TabIndex = 48;
            this.label2.Text = "密码:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(339, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 47;
            this.label1.Text = "端口:";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(89, 62);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(139, 23);
            this.txtUser.TabIndex = 46;
            // 
            // txtConnet
            // 
            this.txtConnet.Location = new System.Drawing.Point(89, 97);
            this.txtConnet.Name = "txtConnet";
            this.txtConnet.Size = new System.Drawing.Size(139, 23);
            this.txtConnet.TabIndex = 45;
            this.txtConnet.Text = "cmnet";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(388, 63);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(139, 23);
            this.txtPwd.TabIndex = 44;
            // 
            // txtGPRSServerPort
            // 
            this.txtGPRSServerPort.Location = new System.Drawing.Point(390, 24);
            this.txtGPRSServerPort.Name = "txtGPRSServerPort";
            this.txtGPRSServerPort.Size = new System.Drawing.Size(139, 23);
            this.txtGPRSServerPort.TabIndex = 43;
            this.txtGPRSServerPort.Text = "3000";
            this.txtGPRSServerPort.Leave += new System.EventHandler(this.txtGPRSServerPort_Leave);
            // 
            // txtGPRSServerIP
            // 
            this.txtGPRSServerIP.Location = new System.Drawing.Point(89, 27);
            this.txtGPRSServerIP.Name = "txtGPRSServerIP";
            this.txtGPRSServerIP.Size = new System.Drawing.Size(139, 23);
            this.txtGPRSServerIP.TabIndex = 42;
            this.txtGPRSServerIP.Text = "192.168.1.1";
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(343, 27);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(0, 0);
            this.metroLabel22.TabIndex = 35;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(11, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(660, 362);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage4
            // 
            this.tabPage4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage4.Controls.Add(this.txtID);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.txtAddress);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.txtPhone10);
            this.tabPage4.Controls.Add(this.txtPhone1);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.txtPhone2);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.txtPhone5);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.txtPhone3);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.txtPhone8);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.txtPhone4);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.txtPhone7);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtPhone6);
            this.tabPage4.Controls.Add(this.txtPhone9);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(652, 336);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "基本参数";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(89, 26);
            this.txtID.MaxLength = 15;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(178, 21);
            this.txtID.TabIndex = 1;
            this.txtID.Text = "TH6800151201001";
            this.txtID.Leave += new System.EventHandler(this.txtID_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 11;
            this.label13.Text = "设备ID：";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(87, 63);
            this.txtAddress.MaxLength = 24;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(504, 40);
            this.txtAddress.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 7;
            this.label11.Text = "安装地址：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1, 257);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 6;
            this.label10.Text = "电话号码10：";
            // 
            // txtPhone10
            // 
            this.txtPhone10.Location = new System.Drawing.Point(89, 254);
            this.txtPhone10.MaxLength = 11;
            this.txtPhone10.Name = "txtPhone10";
            this.txtPhone10.Size = new System.Drawing.Size(107, 21);
            this.txtPhone10.TabIndex = 13;
            this.txtPhone10.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // txtPhone1
            // 
            this.txtPhone1.Location = new System.Drawing.Point(87, 121);
            this.txtPhone1.MaxLength = 11;
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Size = new System.Drawing.Size(107, 21);
            this.txtPhone1.TabIndex = 4;
            this.txtPhone1.Leave += new System.EventHandler(this.txtPhone1_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(404, 218);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "电话号码9：";
            // 
            // txtPhone2
            // 
            this.txtPhone2.Location = new System.Drawing.Point(283, 121);
            this.txtPhone2.MaxLength = 11;
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Size = new System.Drawing.Size(107, 21);
            this.txtPhone2.TabIndex = 5;
            this.txtPhone2.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(402, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "电话号码6：";
            // 
            // txtPhone5
            // 
            this.txtPhone5.Location = new System.Drawing.Point(283, 167);
            this.txtPhone5.MaxLength = 11;
            this.txtPhone5.Name = "txtPhone5";
            this.txtPhone5.Size = new System.Drawing.Size(107, 21);
            this.txtPhone5.TabIndex = 8;
            this.txtPhone5.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(401, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "电话号码3：";
            // 
            // txtPhone3
            // 
            this.txtPhone3.Location = new System.Drawing.Point(484, 120);
            this.txtPhone3.MaxLength = 11;
            this.txtPhone3.Name = "txtPhone3";
            this.txtPhone3.Size = new System.Drawing.Size(107, 21);
            this.txtPhone3.TabIndex = 6;
            this.txtPhone3.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "电话号码7：";
            // 
            // txtPhone8
            // 
            this.txtPhone8.Location = new System.Drawing.Point(283, 215);
            this.txtPhone8.MaxLength = 11;
            this.txtPhone8.Name = "txtPhone8";
            this.txtPhone8.Size = new System.Drawing.Size(107, 21);
            this.txtPhone8.TabIndex = 11;
            this.txtPhone8.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "电话号码4：";
            // 
            // txtPhone4
            // 
            this.txtPhone4.Location = new System.Drawing.Point(88, 166);
            this.txtPhone4.MaxLength = 11;
            this.txtPhone4.Name = "txtPhone4";
            this.txtPhone4.Size = new System.Drawing.Size(107, 21);
            this.txtPhone4.TabIndex = 7;
            this.txtPhone4.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(206, 219);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 12);
            this.label14.TabIndex = 4;
            this.label14.Text = "电话号码8：";
            // 
            // txtPhone7
            // 
            this.txtPhone7.Location = new System.Drawing.Point(88, 212);
            this.txtPhone7.MaxLength = 11;
            this.txtPhone7.Name = "txtPhone7";
            this.txtPhone7.Size = new System.Drawing.Size(107, 21);
            this.txtPhone7.TabIndex = 10;
            this.txtPhone7.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(206, 170);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 12);
            this.label15.TabIndex = 4;
            this.label15.Text = "电话号码5：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(205, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 12);
            this.label16.TabIndex = 4;
            this.label16.Text = "电话号码2：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 125);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 12);
            this.label17.TabIndex = 4;
            this.label17.Text = "电话号码1：";
            // 
            // txtPhone6
            // 
            this.txtPhone6.Location = new System.Drawing.Point(484, 171);
            this.txtPhone6.MaxLength = 11;
            this.txtPhone6.Name = "txtPhone6";
            this.txtPhone6.Size = new System.Drawing.Size(107, 21);
            this.txtPhone6.TabIndex = 9;
            this.txtPhone6.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // txtPhone9
            // 
            this.txtPhone9.Location = new System.Drawing.Point(486, 215);
            this.txtPhone9.MaxLength = 11;
            this.txtPhone9.Name = "txtPhone9";
            this.txtPhone9.Size = new System.Drawing.Size(107, 21);
            this.txtPhone9.TabIndex = 12;
            this.txtPhone9.Leave += new System.EventHandler(this.txtPhone2_Leave);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label54);
            this.tabPage5.Controls.Add(this.textBox1);
            this.tabPage5.Controls.Add(this.label55);
            this.tabPage5.Controls.Add(this.label45);
            this.tabPage5.Controls.Add(this.txtChannel);
            this.tabPage5.Controls.Add(this.label46);
            this.tabPage5.Controls.Add(this.label40);
            this.tabPage5.Controls.Add(this.txtNetworkID);
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.panelMonitor3);
            this.tabPage5.Controls.Add(this.panelMonitor2);
            this.tabPage5.Controls.Add(this.panelMonitor1);
            this.tabPage5.Controls.Add(this.label33);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.txtRecordTime);
            this.tabPage5.Controls.Add(this.txtCfirmTime);
            this.tabPage5.Controls.Add(this.txtUploadTime);
            this.tabPage5.Controls.Add(this.txtAlarmLen);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.label41);
            this.tabPage5.Controls.Add(this.label42);
            this.tabPage5.Controls.Add(this.label43);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(652, 336);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "温湿度参数";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(223, 300);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 12);
            this.label54.TabIndex = 46;
            this.label54.Text = "(1到65535)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(120, 297);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 47;
            this.textBox1.Text = "7212";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(50, 300);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(65, 12);
            this.label55.TabIndex = 45;
            this.label55.Text = "目标地址：";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(519, 279);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(47, 12);
            this.label45.TabIndex = 43;
            this.label45.Text = "(1到16)";
            // 
            // txtChannel
            // 
            this.txtChannel.Location = new System.Drawing.Point(417, 275);
            this.txtChannel.Name = "txtChannel";
            this.txtChannel.Size = new System.Drawing.Size(100, 21);
            this.txtChannel.TabIndex = 44;
            this.txtChannel.Text = "4";
            this.txtChannel.Leave += new System.EventHandler(this.txtChannel_Leave);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(357, 279);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 42;
            this.label46.Text = "通道号：";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(223, 273);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(65, 12);
            this.label40.TabIndex = 40;
            this.label40.Text = "(1到65535)";
            // 
            // txtNetworkID
            // 
            this.txtNetworkID.Location = new System.Drawing.Point(120, 270);
            this.txtNetworkID.Name = "txtNetworkID";
            this.txtNetworkID.Size = new System.Drawing.Size(100, 21);
            this.txtNetworkID.TabIndex = 41;
            this.txtNetworkID.Text = "7212";
            this.txtNetworkID.Leave += new System.EventHandler(this.txtNetworkID_Leave);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(64, 273);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(53, 12);
            this.label44.TabIndex = 39;
            this.label44.Text = "网络ID：";
            // 
            // panelMonitor3
            // 
            this.panelMonitor3.Controls.Add(this.label18);
            this.panelMonitor3.Controls.Add(this.txtC3Basic);
            this.panelMonitor3.Controls.Add(this.txtC3Hys);
            this.panelMonitor3.Controls.Add(this.label19);
            this.panelMonitor3.Controls.Add(this.txtC3Name);
            this.panelMonitor3.Controls.Add(this.txtC3Min);
            this.panelMonitor3.Controls.Add(this.txtC3Max);
            this.panelMonitor3.Controls.Add(this.label23);
            this.panelMonitor3.Controls.Add(this.label28);
            this.panelMonitor3.Controls.Add(this.label25);
            this.panelMonitor3.Location = new System.Drawing.Point(6, 127);
            this.panelMonitor3.Name = "panelMonitor3";
            this.panelMonitor3.Size = new System.Drawing.Size(631, 33);
            this.panelMonitor3.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 12);
            this.label18.TabIndex = 3;
            this.label18.Text = "监测点3：";
            // 
            // txtC3Basic
            // 
            this.txtC3Basic.Enabled = false;
            this.txtC3Basic.Location = new System.Drawing.Point(447, 6);
            this.txtC3Basic.MaxLength = 8;
            this.txtC3Basic.Name = "txtC3Basic";
            this.txtC3Basic.Size = new System.Drawing.Size(43, 21);
            this.txtC3Basic.TabIndex = 27;
            this.txtC3Basic.Text = "0";
            this.txtC3Basic.Leave += new System.EventHandler(this.txtC1Hys_Leave);
            // 
            // txtC3Hys
            // 
            this.txtC3Hys.Enabled = false;
            this.txtC3Hys.Location = new System.Drawing.Point(577, 6);
            this.txtC3Hys.MaxLength = 7;
            this.txtC3Hys.Name = "txtC3Hys";
            this.txtC3Hys.Size = new System.Drawing.Size(43, 21);
            this.txtC3Hys.TabIndex = 28;
            this.txtC3Hys.Text = "0";
            this.txtC3Hys.Leave += new System.EventHandler(this.txtC1Hys_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(298, 18);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "~";
            // 
            // txtC3Name
            // 
            this.txtC3Name.Enabled = false;
            this.txtC3Name.Location = new System.Drawing.Point(67, 4);
            this.txtC3Name.MaxLength = 8;
            this.txtC3Name.Name = "txtC3Name";
            this.txtC3Name.Size = new System.Drawing.Size(100, 21);
            this.txtC3Name.TabIndex = 24;
            this.txtC3Name.Text = "温度1";
            // 
            // txtC3Min
            // 
            this.txtC3Min.Enabled = false;
            this.txtC3Min.Location = new System.Drawing.Point(249, 7);
            this.txtC3Min.MaxLength = 8;
            this.txtC3Min.Name = "txtC3Min";
            this.txtC3Min.Size = new System.Drawing.Size(43, 21);
            this.txtC3Min.TabIndex = 25;
            this.txtC3Min.Text = "0";
            this.txtC3Min.Leave += new System.EventHandler(this.txtC3Min_Leave);
            // 
            // txtC3Max
            // 
            this.txtC3Max.Enabled = false;
            this.txtC3Max.Location = new System.Drawing.Point(315, 7);
            this.txtC3Max.MaxLength = 8;
            this.txtC3Max.Name = "txtC3Max";
            this.txtC3Max.Size = new System.Drawing.Size(43, 21);
            this.txtC3Max.TabIndex = 26;
            this.txtC3Max.Text = "0";
            this.txtC3Max.Leave += new System.EventHandler(this.txtC3Max_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(377, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 10;
            this.label23.Text = "基准值：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(500, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 10;
            this.label28.Text = "迟滞值：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(186, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 12;
            this.label25.Text = "上下限：";
            // 
            // panelMonitor2
            // 
            this.panelMonitor2.Controls.Add(this.label20);
            this.panelMonitor2.Controls.Add(this.txtC2Basic);
            this.panelMonitor2.Controls.Add(this.txtC2Name);
            this.panelMonitor2.Controls.Add(this.txtC2Hys);
            this.panelMonitor2.Controls.Add(this.label21);
            this.panelMonitor2.Controls.Add(this.txtC2Min);
            this.panelMonitor2.Controls.Add(this.txtC2Max);
            this.panelMonitor2.Controls.Add(this.label22);
            this.panelMonitor2.Controls.Add(this.label27);
            this.panelMonitor2.Controls.Add(this.label24);
            this.panelMonitor2.Location = new System.Drawing.Point(6, 82);
            this.panelMonitor2.Name = "panelMonitor2";
            this.panelMonitor2.Size = new System.Drawing.Size(631, 30);
            this.panelMonitor2.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 12);
            this.label20.TabIndex = 7;
            this.label20.Text = "监测点2：";
            // 
            // txtC2Basic
            // 
            this.txtC2Basic.Enabled = false;
            this.txtC2Basic.Location = new System.Drawing.Point(447, 5);
            this.txtC2Basic.MaxLength = 8;
            this.txtC2Basic.Name = "txtC2Basic";
            this.txtC2Basic.Size = new System.Drawing.Size(43, 21);
            this.txtC2Basic.TabIndex = 22;
            this.txtC2Basic.Text = "0";
            this.txtC2Basic.Leave += new System.EventHandler(this.txtC2Basic_Leave);
            // 
            // txtC2Name
            // 
            this.txtC2Name.Enabled = false;
            this.txtC2Name.Location = new System.Drawing.Point(67, 4);
            this.txtC2Name.MaxLength = 8;
            this.txtC2Name.Name = "txtC2Name";
            this.txtC2Name.Size = new System.Drawing.Size(100, 21);
            this.txtC2Name.TabIndex = 19;
            this.txtC2Name.Text = "湿度";
            // 
            // txtC2Hys
            // 
            this.txtC2Hys.Enabled = false;
            this.txtC2Hys.Location = new System.Drawing.Point(577, 5);
            this.txtC2Hys.MaxLength = 7;
            this.txtC2Hys.Name = "txtC2Hys";
            this.txtC2Hys.Size = new System.Drawing.Size(43, 21);
            this.txtC2Hys.TabIndex = 23;
            this.txtC2Hys.Text = "0";
            this.txtC2Hys.Leave += new System.EventHandler(this.txtC1Hys_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(299, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 12);
            this.label21.TabIndex = 6;
            this.label21.Text = "~";
            // 
            // txtC2Min
            // 
            this.txtC2Min.Enabled = false;
            this.txtC2Min.Location = new System.Drawing.Point(249, 6);
            this.txtC2Min.MaxLength = 8;
            this.txtC2Min.Name = "txtC2Min";
            this.txtC2Min.Size = new System.Drawing.Size(43, 21);
            this.txtC2Min.TabIndex = 20;
            this.txtC2Min.Text = "0";
            this.txtC2Min.Leave += new System.EventHandler(this.txtC2Min_Leave);
            // 
            // txtC2Max
            // 
            this.txtC2Max.Enabled = false;
            this.txtC2Max.Location = new System.Drawing.Point(315, 5);
            this.txtC2Max.MaxLength = 8;
            this.txtC2Max.Name = "txtC2Max";
            this.txtC2Max.Size = new System.Drawing.Size(43, 21);
            this.txtC2Max.TabIndex = 21;
            this.txtC2Max.Text = "0";
            this.txtC2Max.Leave += new System.EventHandler(this.txtC2Max_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(379, 11);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 10;
            this.label22.Text = "基准值：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(501, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 10;
            this.label27.Text = "迟滞值：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(188, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 11;
            this.label24.Text = "上下限：";
            // 
            // panelMonitor1
            // 
            this.panelMonitor1.Controls.Add(this.txtC1Name);
            this.panelMonitor1.Controls.Add(this.label26);
            this.panelMonitor1.Controls.Add(this.label29);
            this.panelMonitor1.Controls.Add(this.txtC1Min);
            this.panelMonitor1.Controls.Add(this.label30);
            this.panelMonitor1.Controls.Add(this.label31);
            this.panelMonitor1.Controls.Add(this.label32);
            this.panelMonitor1.Controls.Add(this.txtC1Basic);
            this.panelMonitor1.Controls.Add(this.txtC1Hys);
            this.panelMonitor1.Controls.Add(this.txtC1Max);
            this.panelMonitor1.Location = new System.Drawing.Point(5, 38);
            this.panelMonitor1.Name = "panelMonitor1";
            this.panelMonitor1.Size = new System.Drawing.Size(631, 31);
            this.panelMonitor1.TabIndex = 17;
            // 
            // txtC1Name
            // 
            this.txtC1Name.Location = new System.Drawing.Point(67, 4);
            this.txtC1Name.MaxLength = 8;
            this.txtC1Name.Name = "txtC1Name";
            this.txtC1Name.Size = new System.Drawing.Size(100, 21);
            this.txtC1Name.TabIndex = 14;
            this.txtC1Name.Text = "温度";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "监测点1：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(298, 11);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "~";
            // 
            // txtC1Min
            // 
            this.txtC1Min.Enabled = false;
            this.txtC1Min.Location = new System.Drawing.Point(249, 4);
            this.txtC1Min.MaxLength = 8;
            this.txtC1Min.Name = "txtC1Min";
            this.txtC1Min.Size = new System.Drawing.Size(43, 21);
            this.txtC1Min.TabIndex = 15;
            this.txtC1Min.Text = "0";
            this.txtC1Min.Leave += new System.EventHandler(this.txtC1Min_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(189, 8);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "上下限：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(377, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 0;
            this.label31.Text = "基准值：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(502, 11);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 0;
            this.label32.Text = "迟滞值：";
            // 
            // txtC1Basic
            // 
            this.txtC1Basic.Enabled = false;
            this.txtC1Basic.Location = new System.Drawing.Point(447, 5);
            this.txtC1Basic.MaxLength = 8;
            this.txtC1Basic.Name = "txtC1Basic";
            this.txtC1Basic.Size = new System.Drawing.Size(43, 21);
            this.txtC1Basic.TabIndex = 17;
            this.txtC1Basic.Text = "0";
            this.txtC1Basic.Leave += new System.EventHandler(this.txtC1Basic_Leave);
            // 
            // txtC1Hys
            // 
            this.txtC1Hys.Enabled = false;
            this.txtC1Hys.Location = new System.Drawing.Point(577, 4);
            this.txtC1Hys.MaxLength = 7;
            this.txtC1Hys.Name = "txtC1Hys";
            this.txtC1Hys.Size = new System.Drawing.Size(43, 21);
            this.txtC1Hys.TabIndex = 18;
            this.txtC1Hys.Text = "0";
            this.txtC1Hys.Leave += new System.EventHandler(this.txtC1Hys_Leave);
            // 
            // txtC1Max
            // 
            this.txtC1Max.Enabled = false;
            this.txtC1Max.Location = new System.Drawing.Point(315, 4);
            this.txtC1Max.MaxLength = 8;
            this.txtC1Max.Name = "txtC1Max";
            this.txtC1Max.Size = new System.Drawing.Size(43, 21);
            this.txtC1Max.TabIndex = 16;
            this.txtC1Max.Text = "0";
            this.txtC1Max.Leave += new System.EventHandler(this.txtC1Max_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(520, 198);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 15;
            this.label33.Text = "（秒）";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(524, 238);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 12);
            this.label36.TabIndex = 15;
            this.label36.Text = "（秒）";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(223, 191);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 12);
            this.label39.TabIndex = 15;
            this.label39.Text = "（秒）";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(224, 231);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 15;
            this.label34.Text = "（分钟）";
            // 
            // txtRecordTime
            // 
            this.txtRecordTime.Location = new System.Drawing.Point(417, 194);
            this.txtRecordTime.Name = "txtRecordTime";
            this.txtRecordTime.Size = new System.Drawing.Size(100, 21);
            this.txtRecordTime.TabIndex = 35;
            this.txtRecordTime.Text = "5";
            this.txtRecordTime.Leave += new System.EventHandler(this.txtRecordTime_Leave_1);
            // 
            // txtCfirmTime
            // 
            this.txtCfirmTime.Location = new System.Drawing.Point(416, 233);
            this.txtCfirmTime.Name = "txtCfirmTime";
            this.txtCfirmTime.Size = new System.Drawing.Size(100, 21);
            this.txtCfirmTime.TabIndex = 37;
            this.txtCfirmTime.Text = "5";
            this.txtCfirmTime.Leave += new System.EventHandler(this.txtCfirmTime_Leave);
            // 
            // txtUploadTime
            // 
            this.txtUploadTime.Location = new System.Drawing.Point(120, 187);
            this.txtUploadTime.Name = "txtUploadTime";
            this.txtUploadTime.Size = new System.Drawing.Size(100, 21);
            this.txtUploadTime.TabIndex = 38;
            this.txtUploadTime.Text = "5";
            this.txtUploadTime.Leave += new System.EventHandler(this.txtUploadTime_Leave);
            // 
            // txtAlarmLen
            // 
            this.txtAlarmLen.Location = new System.Drawing.Point(121, 227);
            this.txtAlarmLen.Name = "txtAlarmLen";
            this.txtAlarmLen.Size = new System.Drawing.Size(100, 21);
            this.txtAlarmLen.TabIndex = 36;
            this.txtAlarmLen.Text = "5";
            this.txtAlarmLen.Leave += new System.EventHandler(this.txtAlarmLen_Leave);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(326, 236);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 12);
            this.label37.TabIndex = 13;
            this.label37.Text = "报警确认时间：";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(52, 190);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 12);
            this.label41.TabIndex = 13;
            this.label41.Text = "上传间隔：";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(33, 230);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(89, 12);
            this.label42.TabIndex = 13;
            this.label42.Text = "报警警号时长：";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(345, 198);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(65, 12);
            this.label43.TabIndex = 13;
            this.label43.Text = "记录频率：";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.gbxGPRS);
            this.tabPage6.Controls.Add(this.gbxInter);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(652, 336);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "通讯设置";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btnDevceDown
            // 
            this.btnDevceDown.Enabled = false;
            this.btnDevceDown.Location = new System.Drawing.Point(595, 387);
            this.btnDevceDown.Name = "btnDevceDown";
            this.btnDevceDown.Size = new System.Drawing.Size(75, 31);
            this.btnDevceDown.TabIndex = 10;
            this.btnDevceDown.Text = "下载参数";
            this.btnDevceDown.UseVisualStyleBackColor = true;
            this.btnDevceDown.Click += new System.EventHandler(this.btnDevceDown_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(203, 387);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 31);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "保存配置文件";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReadDevice1
            // 
            this.btnReadDevice1.Location = new System.Drawing.Point(111, 387);
            this.btnReadDevice1.Name = "btnReadDevice1";
            this.btnReadDevice1.Size = new System.Drawing.Size(86, 31);
            this.btnReadDevice1.TabIndex = 12;
            this.btnReadDevice1.Text = "读取设备配置";
            this.btnReadDevice1.UseVisualStyleBackColor = true;
            this.btnReadDevice1.Click += new System.EventHandler(this.btnReadDevice_Click);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(14, 387);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(91, 31);
            this.btnRead.TabIndex = 11;
            this.btnRead.Text = "读取配置文件";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnBack
            // 
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(430, 387);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 31);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "上一步";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(512, 387);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 31);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "下一步";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.val});
            this.dataGridView1.Location = new System.Drawing.Point(2, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(667, 363);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.Visible = false;
            // 
            // name
            // 
            this.name.DataPropertyName = "Name";
            this.name.HeaderText = "名称";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 200;
            // 
            // val
            // 
            this.val.DataPropertyName = "Val";
            this.val.HeaderText = "数据";
            this.val.Name = "val";
            this.val.ReadOnly = true;
            this.val.Width = 400;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 422);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnReadDevice1);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDevceDown);
            this.Controls.Add(this.tabControl1);
            this.Name = "Main";
            this.Text = "配置";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load_1);
            this.gbxInter.ResumeLayout(false);
            this.gbxInter.PerformLayout();
            this.gbxGPRS.ResumeLayout(false);
            this.gbxGPRS.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.panelMonitor3.ResumeLayout(false);
            this.panelMonitor3.PerformLayout();
            this.panelMonitor2.ResumeLayout(false);
            this.panelMonitor2.PerformLayout();
            this.panelMonitor1.ResumeLayout(false);
            this.panelMonitor1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox gbxGPRS;
        private System.Windows.Forms.GroupBox gbxInter;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPhone10;
        private System.Windows.Forms.TextBox txtPhone1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPhone2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPhone5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhone3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPhone8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPhone4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPhone7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPhone6;
        private System.Windows.Forms.TextBox txtPhone9;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panelMonitor3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtC3Basic;
        private System.Windows.Forms.TextBox txtC3Hys;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtC3Name;
        private System.Windows.Forms.TextBox txtC3Min;
        private System.Windows.Forms.TextBox txtC3Max;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panelMonitor2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtC2Basic;
        private System.Windows.Forms.TextBox txtC2Name;
        private System.Windows.Forms.TextBox txtC2Hys;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtC2Min;
        private System.Windows.Forms.TextBox txtC2Max;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panelMonitor1;
        private System.Windows.Forms.TextBox txtC1Name;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtC1Min;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtC1Basic;
        private System.Windows.Forms.TextBox txtC1Hys;
        private System.Windows.Forms.TextBox txtC1Max;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtRecordTime;
        private System.Windows.Forms.TextBox txtCfirmTime;
        private System.Windows.Forms.TextBox txtUploadTime;
        private System.Windows.Forms.TextBox txtAlarmLen;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtChannel;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtNetworkID;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmbDHCP;
        private System.Windows.Forms.TextBox txtMac;
        private System.Windows.Forms.TextBox txtGatWay;
        private System.Windows.Forms.TextBox txtMas;
        private System.Windows.Forms.TextBox txtDNS;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.TextBox txtItServerPort;
        private System.Windows.Forms.TextBox txtItServerIP;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtConnet;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.TextBox txtGPRSServerPort;
        private System.Windows.Forms.TextBox txtGPRSServerIP;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button btnDevceDown;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReadDevice1;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn val;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label55;
    }
}

