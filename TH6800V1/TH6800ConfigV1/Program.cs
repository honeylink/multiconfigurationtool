﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace TH6800ConfigV1
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
          Application.Run(new ConntMail());
          //Application.Run(new Main());
             Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException); 
             AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string s = e.Exception.ToString();

        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception s1 = e.ExceptionObject as Exception;
            string s = s1.ToString();
        }
    }
}
