﻿namespace M433ConfigTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTools = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.send = new System.Windows.Forms.Button();
            this.sendtext = new System.Windows.Forms.TextBox();
            this.revtext = new System.Windows.Forms.TextBox();
            this.paneltools = new System.Windows.Forms.Panel();
            this.panelConfig = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnReadFileConfig = new System.Windows.Forms.Button();
            this.BtnReadDeviceConfig = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DataBuffer = new System.Windows.Forms.TextBox();
            this.NetworkPower = new System.Windows.Forms.TextBox();
            this.MaxNodeData = new System.Windows.Forms.TextBox();
            this.MaxNode = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.NetworkID = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LabGetWayNo = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.EDNS_d = new System.Windows.Forms.TextBox();
            this.EGetWay_d = new System.Windows.Forms.TextBox();
            this.EMask_d = new System.Windows.Forms.TextBox();
            this.EIp_d = new System.Windows.Forms.TextBox();
            this.Mac = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadEOpen_n = new System.Windows.Forms.RadioButton();
            this.RadEClose_n = new System.Windows.Forms.RadioButton();
            this.label31 = new System.Windows.Forms.Label();
            this.DHCPSClose = new System.Windows.Forms.RadioButton();
            this.DHCPSOpen = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.EPort = new System.Windows.Forms.TextBox();
            this.EServer = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.radGPRSOpen_n = new System.Windows.Forms.RadioButton();
            this.radGPRSClose_n = new System.Windows.Forms.RadioButton();
            this.label43 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.GPRSDNS = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.AccessPoint = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.User = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Pwd = new System.Windows.Forms.TextBox();
            this.GPRSPoert = new System.Windows.Forms.TextBox();
            this.GPRSServerIP = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.rad232Close_n = new System.Windows.Forms.RadioButton();
            this.rad232Open_n = new System.Windows.Forms.RadioButton();
            this.label22 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.rad485Close_n = new System.Windows.Forms.RadioButton();
            this.rad485Open_n = new System.Windows.Forms.RadioButton();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.paneltools.SuspendLayout();
            this.panelConfig.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTools
            // 
            this.btnTools.Location = new System.Drawing.Point(12, 12);
            this.btnTools.Name = "btnTools";
            this.btnTools.Size = new System.Drawing.Size(75, 67);
            this.btnTools.TabIndex = 0;
            this.btnTools.Text = "USB口工具";
            this.btnTools.UseVisualStyleBackColor = true;
            this.btnTools.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Location = new System.Drawing.Point(12, 92);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(75, 68);
            this.btnConfig.TabIndex = 1;
            this.btnConfig.Text = "参数配置";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 375);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 21;
            this.label3.Text = "发送指令";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "自动接收";
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(603, 391);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(68, 61);
            this.clear.TabIndex = 19;
            this.clear.Text = "清空显示";
            this.clear.UseVisualStyleBackColor = true;
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(494, 392);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(75, 60);
            this.send.TabIndex = 18;
            this.send.Text = "发送";
            this.send.UseVisualStyleBackColor = true;
            // 
            // sendtext
            // 
            this.sendtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sendtext.Location = new System.Drawing.Point(3, 390);
            this.sendtext.Multiline = true;
            this.sendtext.Name = "sendtext";
            this.sendtext.Size = new System.Drawing.Size(449, 61);
            this.sendtext.TabIndex = 16;
            // 
            // revtext
            // 
            this.revtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.revtext.Location = new System.Drawing.Point(3, 27);
            this.revtext.Multiline = true;
            this.revtext.Name = "revtext";
            this.revtext.ReadOnly = true;
            this.revtext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.revtext.Size = new System.Drawing.Size(679, 336);
            this.revtext.TabIndex = 17;
            // 
            // paneltools
            // 
            this.paneltools.Controls.Add(this.revtext);
            this.paneltools.Controls.Add(this.clear);
            this.paneltools.Controls.Add(this.label3);
            this.paneltools.Controls.Add(this.send);
            this.paneltools.Controls.Add(this.label2);
            this.paneltools.Controls.Add(this.sendtext);
            this.paneltools.Location = new System.Drawing.Point(104, 12);
            this.paneltools.Name = "paneltools";
            this.paneltools.Size = new System.Drawing.Size(695, 484);
            this.paneltools.TabIndex = 22;
            // 
            // panelConfig
            // 
            this.panelConfig.Controls.Add(this.button1);
            this.panelConfig.Controls.Add(this.btnChange);
            this.panelConfig.Controls.Add(this.btnReadFileConfig);
            this.panelConfig.Controls.Add(this.BtnReadDeviceConfig);
            this.panelConfig.Controls.Add(this.button4);
            this.panelConfig.Controls.Add(this.button3);
            this.panelConfig.Controls.Add(this.tabControl1);
            this.panelConfig.Location = new System.Drawing.Point(93, 15);
            this.panelConfig.Name = "panelConfig";
            this.panelConfig.Size = new System.Drawing.Size(719, 484);
            this.panelConfig.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 453);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "上一步";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnChange
            // 
            this.btnChange.Location = new System.Drawing.Point(420, 453);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(75, 23);
            this.btnChange.TabIndex = 6;
            this.btnChange.Text = "下一步";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnReadFileConfig
            // 
            this.btnReadFileConfig.Location = new System.Drawing.Point(129, 453);
            this.btnReadFileConfig.Name = "btnReadFileConfig";
            this.btnReadFileConfig.Size = new System.Drawing.Size(101, 23);
            this.btnReadFileConfig.TabIndex = 5;
            this.btnReadFileConfig.Text = "读取参数文件";
            this.btnReadFileConfig.UseVisualStyleBackColor = true;
            this.btnReadFileConfig.Click += new System.EventHandler(this.btnReadFileConfig_Click);
            // 
            // BtnReadDeviceConfig
            // 
            this.BtnReadDeviceConfig.Location = new System.Drawing.Point(18, 453);
            this.BtnReadDeviceConfig.Name = "BtnReadDeviceConfig";
            this.BtnReadDeviceConfig.Size = new System.Drawing.Size(101, 23);
            this.BtnReadDeviceConfig.TabIndex = 4;
            this.BtnReadDeviceConfig.Text = "读取设备参数";
            this.BtnReadDeviceConfig.UseVisualStyleBackColor = true;
            this.BtnReadDeviceConfig.Click += new System.EventHandler(this.BtnReadDeviceConfig_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(582, 453);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "下载";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(501, 453);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "保存参数";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(708, 448);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DataBuffer);
            this.tabPage1.Controls.Add(this.NetworkPower);
            this.tabPage1.Controls.Add(this.MaxNodeData);
            this.tabPage1.Controls.Add(this.MaxNode);
            this.tabPage1.Controls.Add(this.textBox5);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.NetworkID);
            this.tabPage1.Controls.Add(this.ID);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.LabGetWayNo);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Font = new System.Drawing.Font("宋体", 9F);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(700, 422);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "配置基本参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DataBuffer
            // 
            this.DataBuffer.Font = new System.Drawing.Font("宋体", 9F);
            this.DataBuffer.Location = new System.Drawing.Point(237, 275);
            this.DataBuffer.Name = "DataBuffer";
            this.DataBuffer.Size = new System.Drawing.Size(219, 21);
            this.DataBuffer.TabIndex = 2;
            // 
            // NetworkPower
            // 
            this.NetworkPower.Font = new System.Drawing.Font("宋体", 9F);
            this.NetworkPower.Location = new System.Drawing.Point(237, 192);
            this.NetworkPower.Name = "NetworkPower";
            this.NetworkPower.Size = new System.Drawing.Size(219, 21);
            this.NetworkPower.TabIndex = 2;
            // 
            // MaxNodeData
            // 
            this.MaxNodeData.Font = new System.Drawing.Font("宋体", 9F);
            this.MaxNodeData.Location = new System.Drawing.Point(237, 313);
            this.MaxNodeData.Name = "MaxNodeData";
            this.MaxNodeData.Size = new System.Drawing.Size(219, 21);
            this.MaxNodeData.TabIndex = 2;
            // 
            // MaxNode
            // 
            this.MaxNode.Font = new System.Drawing.Font("宋体", 9F);
            this.MaxNode.Location = new System.Drawing.Point(237, 235);
            this.MaxNode.Name = "MaxNode";
            this.MaxNode.Size = new System.Drawing.Size(219, 21);
            this.MaxNode.TabIndex = 2;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox5.Location = new System.Drawing.Point(148, 15);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(219, 21);
            this.textBox5.TabIndex = 2;
            this.textBox5.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox4.Location = new System.Drawing.Point(237, 151);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(219, 21);
            this.textBox4.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox3.Location = new System.Drawing.Point(449, 15);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(219, 21);
            this.textBox3.TabIndex = 2;
            this.textBox3.Visible = false;
            // 
            // NetworkID
            // 
            this.NetworkID.Font = new System.Drawing.Font("宋体", 9F);
            this.NetworkID.Location = new System.Drawing.Point(237, 107);
            this.NetworkID.Name = "NetworkID";
            this.NetworkID.Size = new System.Drawing.Size(219, 21);
            this.NetworkID.TabIndex = 2;
            // 
            // ID
            // 
            this.ID.Font = new System.Drawing.Font("宋体", 9F);
            this.ID.Location = new System.Drawing.Point(235, 67);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(219, 21);
            this.ID.TabIndex = 2;
            this.ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ID_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(118, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "最大数据缓冲时间:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 9F);
            this.label30.Location = new System.Drawing.Point(464, 316);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "(条)";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(464, 278);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "(秒)";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("宋体", 9F);
            this.label28.Location = new System.Drawing.Point(464, 235);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "(个)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(464, 195);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(23, 12);
            this.label27.TabIndex = 0;
            this.label27.Text = "dBm";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(464, 154);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "MAH(毫安时)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(171, 153);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "电池容量:";
            // 
            // LabGetWayNo
            // 
            this.LabGetWayNo.AutoSize = true;
            this.LabGetWayNo.Font = new System.Drawing.Font("宋体", 9F);
            this.LabGetWayNo.Location = new System.Drawing.Point(184, 70);
            this.LabGetWayNo.Name = "LabGetWayNo";
            this.LabGetWayNo.Size = new System.Drawing.Size(47, 12);
            this.LabGetWayNo.TabIndex = 0;
            this.LabGetWayNo.Text = "序列号:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(81, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "软件版本:";
            this.label25.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(156, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "最大节点数:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(382, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "硬件版本:";
            this.label12.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(132, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "最大数据记录数:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(184, 110);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "网络ID:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(170, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "网络功率:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.EDNS_d);
            this.tabPage3.Controls.Add(this.EGetWay_d);
            this.tabPage3.Controls.Add(this.EMask_d);
            this.tabPage3.Controls.Add(this.EIp_d);
            this.tabPage3.Controls.Add(this.Mac);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.DHCPSClose);
            this.tabPage3.Controls.Add(this.DHCPSOpen);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.EPort);
            this.tabPage3.Controls.Add(this.EServer);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(700, 422);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "以太网参数";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // EDNS_d
            // 
            this.EDNS_d.Location = new System.Drawing.Point(213, 200);
            this.EDNS_d.Name = "EDNS_d";
            this.EDNS_d.Size = new System.Drawing.Size(167, 21);
            this.EDNS_d.TabIndex = 52;
            this.EDNS_d.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // EGetWay_d
            // 
            this.EGetWay_d.Location = new System.Drawing.Point(213, 171);
            this.EGetWay_d.Name = "EGetWay_d";
            this.EGetWay_d.Size = new System.Drawing.Size(167, 21);
            this.EGetWay_d.TabIndex = 52;
            this.EGetWay_d.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // EMask_d
            // 
            this.EMask_d.Location = new System.Drawing.Point(213, 141);
            this.EMask_d.Name = "EMask_d";
            this.EMask_d.Size = new System.Drawing.Size(167, 21);
            this.EMask_d.TabIndex = 52;
            this.EMask_d.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // EIp_d
            // 
            this.EIp_d.Location = new System.Drawing.Point(213, 112);
            this.EIp_d.Name = "EIp_d";
            this.EIp_d.Size = new System.Drawing.Size(167, 21);
            this.EIp_d.TabIndex = 51;
            // 
            // Mac
            // 
            this.Mac.Location = new System.Drawing.Point(213, 47);
            this.Mac.Name = "Mac";
            this.Mac.Size = new System.Drawing.Size(167, 21);
            this.Mac.TabIndex = 50;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadEOpen_n);
            this.panel1.Controls.Add(this.RadEClose_n);
            this.panel1.Location = new System.Drawing.Point(213, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(167, 23);
            this.panel1.TabIndex = 49;
            // 
            // RadEOpen_n
            // 
            this.RadEOpen_n.AutoSize = true;
            this.RadEOpen_n.Location = new System.Drawing.Point(11, 4);
            this.RadEOpen_n.Name = "RadEOpen_n";
            this.RadEOpen_n.Size = new System.Drawing.Size(35, 16);
            this.RadEOpen_n.TabIndex = 48;
            this.RadEOpen_n.TabStop = true;
            this.RadEOpen_n.Text = "开";
            this.RadEOpen_n.UseVisualStyleBackColor = true;
            this.RadEOpen_n.CheckedChanged += new System.EventHandler(this.RadEOpen_n_CheckedChanged);
            // 
            // RadEClose_n
            // 
            this.RadEClose_n.AutoSize = true;
            this.RadEClose_n.Checked = true;
            this.RadEClose_n.Location = new System.Drawing.Point(65, 4);
            this.RadEClose_n.Name = "RadEClose_n";
            this.RadEClose_n.Size = new System.Drawing.Size(35, 16);
            this.RadEClose_n.TabIndex = 47;
            this.RadEClose_n.TabStop = true;
            this.RadEClose_n.Text = "关";
            this.RadEClose_n.UseVisualStyleBackColor = true;
            this.RadEClose_n.CheckedChanged += new System.EventHandler(this.RadEClose_n_CheckedChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("宋体", 9F);
            this.label31.Location = new System.Drawing.Point(168, 17);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 12);
            this.label31.TabIndex = 46;
            this.label31.Text = "状态：";
            // 
            // DHCPSClose
            // 
            this.DHCPSClose.AutoSize = true;
            this.DHCPSClose.Checked = true;
            this.DHCPSClose.Location = new System.Drawing.Point(271, 78);
            this.DHCPSClose.Name = "DHCPSClose";
            this.DHCPSClose.Size = new System.Drawing.Size(35, 16);
            this.DHCPSClose.TabIndex = 44;
            this.DHCPSClose.TabStop = true;
            this.DHCPSClose.Text = "关";
            this.DHCPSClose.UseVisualStyleBackColor = true;
            this.DHCPSClose.CheckedChanged += new System.EventHandler(this.DHCPSClose_CheckedChanged_1);
            // 
            // DHCPSOpen
            // 
            this.DHCPSOpen.AutoSize = true;
            this.DHCPSOpen.Location = new System.Drawing.Point(215, 78);
            this.DHCPSOpen.Name = "DHCPSOpen";
            this.DHCPSOpen.Size = new System.Drawing.Size(35, 16);
            this.DHCPSOpen.TabIndex = 45;
            this.DHCPSOpen.TabStop = true;
            this.DHCPSOpen.Text = "开";
            this.DHCPSOpen.UseVisualStyleBackColor = true;
            this.DHCPSOpen.CheckedChanged += new System.EventHandler(this.DHCPSOpen_CheckedChanged_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(120, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "DHCP开关状态：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(154, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 30;
            this.label8.Text = "IP地址：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(168, 263);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 31;
            this.label18.Text = "端口：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(132, 235);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "服务器地址：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(144, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 35;
            this.label9.Text = "子网掩码：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(138, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 12);
            this.label11.TabIndex = 36;
            this.label11.Text = "DNS服务器：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(145, 174);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "默认网关：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(150, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 12);
            this.label21.TabIndex = 33;
            this.label21.Text = "MAC地址：";
            // 
            // EPort
            // 
            this.EPort.Font = new System.Drawing.Font("宋体", 9F);
            this.EPort.Location = new System.Drawing.Point(213, 260);
            this.EPort.Name = "EPort";
            this.EPort.Size = new System.Drawing.Size(68, 21);
            this.EPort.TabIndex = 43;
            // 
            // EServer
            // 
            this.EServer.Font = new System.Drawing.Font("宋体", 9F);
            this.EServer.Location = new System.Drawing.Point(213, 230);
            this.EServer.Name = "EServer";
            this.EServer.Size = new System.Drawing.Size(167, 21);
            this.EServer.TabIndex = 42;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.radGPRSOpen_n);
            this.tabPage4.Controls.Add(this.radGPRSClose_n);
            this.tabPage4.Controls.Add(this.label43);
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.GPRSDNS);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.AccessPoint);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.User);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.Pwd);
            this.tabPage4.Controls.Add(this.GPRSPoert);
            this.tabPage4.Controls.Add(this.GPRSServerIP);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(700, 422);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "GPRS参数";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // radGPRSOpen_n
            // 
            this.radGPRSOpen_n.AutoSize = true;
            this.radGPRSOpen_n.Location = new System.Drawing.Point(207, 51);
            this.radGPRSOpen_n.Name = "radGPRSOpen_n";
            this.radGPRSOpen_n.Size = new System.Drawing.Size(35, 16);
            this.radGPRSOpen_n.TabIndex = 49;
            this.radGPRSOpen_n.TabStop = true;
            this.radGPRSOpen_n.Text = "开";
            this.radGPRSOpen_n.UseVisualStyleBackColor = true;
            this.radGPRSOpen_n.CheckedChanged += new System.EventHandler(this.radGPRSOpen_n_CheckedChanged);
            // 
            // radGPRSClose_n
            // 
            this.radGPRSClose_n.AutoSize = true;
            this.radGPRSClose_n.Checked = true;
            this.radGPRSClose_n.Location = new System.Drawing.Point(261, 51);
            this.radGPRSClose_n.Name = "radGPRSClose_n";
            this.radGPRSClose_n.Size = new System.Drawing.Size(35, 16);
            this.radGPRSClose_n.TabIndex = 50;
            this.radGPRSClose_n.TabStop = true;
            this.radGPRSClose_n.Text = "关";
            this.radGPRSClose_n.UseVisualStyleBackColor = true;
            this.radGPRSClose_n.CheckedChanged += new System.EventHandler(this.radGPRSClose_n_CheckedChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("宋体", 9F);
            this.label43.Location = new System.Drawing.Point(154, 55);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 12);
            this.label43.TabIndex = 51;
            this.label43.Text = "状态:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(157, 187);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 12);
            this.label33.TabIndex = 47;
            this.label33.Text = "DNS:";
            // 
            // GPRSDNS
            // 
            this.GPRSDNS.Font = new System.Drawing.Font("宋体", 9F);
            this.GPRSDNS.Location = new System.Drawing.Point(206, 180);
            this.GPRSDNS.Name = "GPRSDNS";
            this.GPRSDNS.Size = new System.Drawing.Size(133, 21);
            this.GPRSDNS.TabIndex = 48;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(131, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 12);
            this.label19.TabIndex = 39;
            this.label19.Text = "接入点名:";
            // 
            // AccessPoint
            // 
            this.AccessPoint.Font = new System.Drawing.Font("宋体", 9F);
            this.AccessPoint.Location = new System.Drawing.Point(206, 85);
            this.AccessPoint.Name = "AccessPoint";
            this.AccessPoint.Size = new System.Drawing.Size(134, 21);
            this.AccessPoint.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(145, 123);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 12);
            this.label14.TabIndex = 37;
            this.label14.Text = "用户名:";
            // 
            // User
            // 
            this.User.Font = new System.Drawing.Font("宋体", 9F);
            this.User.Location = new System.Drawing.Point(207, 118);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(134, 21);
            this.User.TabIndex = 46;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(156, 160);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 12);
            this.label20.TabIndex = 38;
            this.label20.Text = "密码:";
            // 
            // Pwd
            // 
            this.Pwd.Font = new System.Drawing.Font("宋体", 9F);
            this.Pwd.Location = new System.Drawing.Point(207, 149);
            this.Pwd.Name = "Pwd";
            this.Pwd.Size = new System.Drawing.Size(134, 21);
            this.Pwd.TabIndex = 44;
            // 
            // GPRSPoert
            // 
            this.GPRSPoert.Font = new System.Drawing.Font("宋体", 9F);
            this.GPRSPoert.Location = new System.Drawing.Point(207, 240);
            this.GPRSPoert.Name = "GPRSPoert";
            this.GPRSPoert.Size = new System.Drawing.Size(45, 21);
            this.GPRSPoert.TabIndex = 43;
            // 
            // GPRSServerIP
            // 
            this.GPRSServerIP.Font = new System.Drawing.Font("宋体", 9F);
            this.GPRSServerIP.Location = new System.Drawing.Point(206, 207);
            this.GPRSServerIP.Name = "GPRSServerIP";
            this.GPRSServerIP.Size = new System.Drawing.Size(134, 21);
            this.GPRSServerIP.TabIndex = 42;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 9F);
            this.label24.Location = new System.Drawing.Point(158, 243);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 12);
            this.label24.TabIndex = 40;
            this.label24.Text = "端口:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F);
            this.label23.Location = new System.Drawing.Point(122, 214);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 12);
            this.label23.TabIndex = 41;
            this.label23.Text = "服务器地址:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.rad232Close_n);
            this.tabPage5.Controls.Add(this.rad232Open_n);
            this.tabPage5.Controls.Add(this.label22);
            this.tabPage5.Controls.Add(this.label32);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(700, 422);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "RS232参数";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // rad232Close_n
            // 
            this.rad232Close_n.AutoSize = true;
            this.rad232Close_n.Checked = true;
            this.rad232Close_n.Location = new System.Drawing.Point(228, 37);
            this.rad232Close_n.Name = "rad232Close_n";
            this.rad232Close_n.Size = new System.Drawing.Size(35, 16);
            this.rad232Close_n.TabIndex = 36;
            this.rad232Close_n.TabStop = true;
            this.rad232Close_n.Text = "关";
            this.rad232Close_n.UseVisualStyleBackColor = true;
            this.rad232Close_n.CheckedChanged += new System.EventHandler(this.rad232Close_n_CheckedChanged);
            // 
            // rad232Open_n
            // 
            this.rad232Open_n.AutoSize = true;
            this.rad232Open_n.Location = new System.Drawing.Point(170, 37);
            this.rad232Open_n.Name = "rad232Open_n";
            this.rad232Open_n.Size = new System.Drawing.Size(35, 16);
            this.rad232Open_n.TabIndex = 35;
            this.rad232Open_n.TabStop = true;
            this.rad232Open_n.Text = "开";
            this.rad232Open_n.UseVisualStyleBackColor = true;
            this.rad232Open_n.CheckedChanged += new System.EventHandler(this.rad232Open_n_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(120, 39);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 34;
            this.label22.Text = "状态：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("宋体", 9F);
            this.label32.Location = new System.Drawing.Point(168, 68);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 34;
            this.label32.Text = "暂无";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.rad485Close_n);
            this.tabPage6.Controls.Add(this.rad485Open_n);
            this.tabPage6.Controls.Add(this.label35);
            this.tabPage6.Controls.Add(this.label34);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(700, 422);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "RS485参数";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // rad485Close_n
            // 
            this.rad485Close_n.AutoSize = true;
            this.rad485Close_n.Checked = true;
            this.rad485Close_n.Location = new System.Drawing.Point(237, 36);
            this.rad485Close_n.Name = "rad485Close_n";
            this.rad485Close_n.Size = new System.Drawing.Size(35, 16);
            this.rad485Close_n.TabIndex = 39;
            this.rad485Close_n.TabStop = true;
            this.rad485Close_n.Text = "关";
            this.rad485Close_n.UseVisualStyleBackColor = true;
            this.rad485Close_n.CheckedChanged += new System.EventHandler(this.rad485Close_n_CheckedChanged);
            // 
            // rad485Open_n
            // 
            this.rad485Open_n.AutoSize = true;
            this.rad485Open_n.Location = new System.Drawing.Point(179, 36);
            this.rad485Open_n.Name = "rad485Open_n";
            this.rad485Open_n.Size = new System.Drawing.Size(35, 16);
            this.rad485Open_n.TabIndex = 38;
            this.rad485Open_n.TabStop = true;
            this.rad485Open_n.Text = "开";
            this.rad485Open_n.UseVisualStyleBackColor = true;
            this.rad485Open_n.CheckedChanged += new System.EventHandler(this.rad485Open_n_CheckedChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("宋体", 9F);
            this.label35.Location = new System.Drawing.Point(129, 38);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 12);
            this.label35.TabIndex = 37;
            this.label35.Text = "状态：";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("宋体", 9F);
            this.label34.Location = new System.Drawing.Point(177, 70);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 34;
            this.label34.Text = "暂无";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "配置文件(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "配置文件(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 508);
            this.Controls.Add(this.panelConfig);
            this.Controls.Add(this.paneltools);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.btnTools);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.paneltools.ResumeLayout(false);
            this.paneltools.PerformLayout();
            this.panelConfig.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTools;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.TextBox sendtext;
        private System.Windows.Forms.TextBox revtext;
        private System.Windows.Forms.Panel paneltools;
        private System.Windows.Forms.Panel panelConfig;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabGetWayNo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox DataBuffer;
        private System.Windows.Forms.TextBox NetworkPower;
        private System.Windows.Forms.TextBox MaxNodeData;
        private System.Windows.Forms.TextBox MaxNode;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox NetworkID;
        private System.Windows.Forms.TextBox ID;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnReadFileConfig;
        private System.Windows.Forms.Button BtnReadDeviceConfig;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox GPRSDNS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox AccessPoint;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox User;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Pwd;
        private System.Windows.Forms.TextBox GPRSPoert;
        private System.Windows.Forms.TextBox GPRSServerIP;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.RadioButton DHCPSClose;
        private System.Windows.Forms.RadioButton DHCPSOpen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox EPort;
        private System.Windows.Forms.TextBox EServer;
        private System.Windows.Forms.RadioButton RadEClose_n;
        private System.Windows.Forms.RadioButton RadEOpen_n;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.RadioButton radGPRSClose_n;
        private System.Windows.Forms.RadioButton radGPRSOpen_n;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rad232Close_n;
        private System.Windows.Forms.RadioButton rad232Open_n;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.RadioButton rad485Close_n;
        private System.Windows.Forms.RadioButton rad485Open_n;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox EMask_d;
        private System.Windows.Forms.TextBox EIp_d;
        private System.Windows.Forms.TextBox Mac;
        private System.Windows.Forms.TextBox EDNS_d;
        private System.Windows.Forms.TextBox EGetWay_d;
    }
}