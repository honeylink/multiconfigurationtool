﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Threading;
using M433ConfigTool.log;

namespace M433ConfigTool
{
    public partial class Form1 : Form
    {
        public Hashtable Ht = new Hashtable();
        HusbHelp husbHelp = new HusbHelp();
        Thread sendth; 
        private int dState = 0;//连接状态
        int DeviceNO =12345678;
        bool readState =false;
        bool writeState = false;
        string cmdTypes = "";
        byte[] sendMsgbyte=new byte[0];
        byte[] getMsgByte = new byte[0];
        List<SendModel> listsend = new List<SendModel>();
        int  sendNo =0;
        public Form1()
        {
            InitializeComponent();
        }
        int No=1;
        /// <summary>
        /// 选项卡切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChange_Click(object sender, EventArgs e)
        { 
           
            if (btnChange.Text == "下一步")
            {
                this.tabControl1.SelectedIndex = this.tabControl1.SelectedIndex+1;
            }
            else
            {
                this.tabControl1.SelectedIndex = 0;
            }
            int tabIndex = this.tabControl1.SelectedIndex;
            if (tabIndex == 4)
            {
                btnChange.Visible = false;
            }
            button1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
             
                this.paneltools.Visible = true;
                this.panelConfig.Visible = false;
          
        }
        private void btnConfig_Click(object sender, EventArgs e)
        {
           
                this.paneltools.Visible = false;
                this.panelConfig.Visible = true;
            
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void label31_Click(object sender, EventArgs e)
        {

        }

        #region 读取配置文件
        /// <summary>
        /// 读取配置文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadFileConfig_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            { 

                string path = this.openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(path))
                {
                    AppCmd cmd = new AppCmd();
                    cmd.ReadConfig(ref Ht, path);
                    SetModel();
                    MessageBox.Show("读取配置文件成功");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("读取配置文件失败");

            }
        } 
        #endregion
        private void SetModel()
        {
            this.ID.Text = Ht["ID"].ToString().Trim();
            this.NetworkID.Text = Ht["NetworkID"].ToString().Trim();
            this.MaxNode.Text = Ht["MaxNode"].ToString().Trim();
            this.MaxNodeData.Text = Ht["MaxNodeData"].ToString().Trim();
         if (Ht["EthernetState"].ToString().Trim() == "1")
            {
                RadEOpen_n.Checked = true;

            }
            else
            {
                RadEClose_n.Checked = true;
            }
         AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked);
            if (Ht["GPRSState"].ToString().Trim() == "1")
            {
                radGPRSOpen_n.Checked = true;
               
            }
            else
            {
                radGPRSClose_n.Checked = true;
            }
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageGprs"], radGPRSOpen_n.Checked);
            if (Ht["485State"].ToString().Trim() == "1")
            {
                rad485Open_n.Checked = true;

            }
            else
            {
                rad485Open_n.Checked = true;
            }
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs485"], rad485Open_n.Checked);
            if (Ht["232State"].ToString().Trim() == "1")
            {
                rad232Open_n.Checked = true;
                
            }
            else
            {
                rad232Close_n.Checked = true;
            }
            AppCmd.SetEnabled(tabControl1.TabPages["tabPage232"], rad232Open_n.Checked);
            this.Mac.Text = Ht["Mac"].ToString().Trim();
            this.DHCPSOpen.Checked = Ht["DHCP"].ToString().Trim() == "1" ? true : false;
            this.DHCPSClose.Checked = Ht["DHCP"].ToString().Trim() == "0" ? true : false;
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");
                if (!this.DHCPSOpen.Checked)
                { 
                    this.EIp_d.Text = Ht["EIp"].ToString().Trim();
                    this.EMask_d.Text = Ht["EMask"].ToString().Trim();
                    this.EGetWay_d.Text = Ht["EGetWay"].ToString().Trim();
                    this.EDNS_d.Text = Ht["EDNS"].ToString().Trim(); 
                }
            this.EServer.Text = Ht["EServer"].ToString().Trim();
            this.EPort.Text = Ht["EPort"].ToString().Trim();  
            this.AccessPoint.Text = Ht["AccessPoint"].ToString().Trim();
            this.User.Text = Ht["User"].ToString().Trim();

            this.Pwd.Text = Ht["Pwd"].ToString().Trim();
            this.GPRSDNS.Text = Ht["GPRSDNS"].ToString().Trim();
            this.GPRSServerIP.Text = Ht["GPRSServerIP"].ToString().Trim();
            this.GPRSPoert.Text = Ht["GPRSPoert"].ToString().Trim();
            this.Mac.Text = Ht["Mac"].ToString().Trim();
            //this.DHCP.Text = Ht["DHCP"].ToString().Trim();
            this.EIp_d.Text = Ht["EIp"].ToString().Trim();
            this.EMask_d.Text = Ht["EMask"].ToString().Trim();
            this.EGetWay_d.Text = Ht["EGetWay"].ToString().Trim();
            this.EDNS_d.Text = Ht["EDNS"].ToString().Trim();
            this.EServer.Text = Ht["EServer"].ToString().Trim();
            this.EPort.Text = Ht["EPort"].ToString().Trim(); 
        }
        private void GetModel()
        {
            Ht.Clear();
           
            Ht["ID"] = this.ID.Text.Trim();
            Ht["NetworkID"] = this.NetworkID.Text.Trim();
            Ht["MaxNode"] = this.MaxNode.Text.Trim();
            Ht["MaxNodeData"] = this.MaxNodeData.Text.Trim();
            Ht["DataBuffer"] = this.DataBuffer.Text.Trim();
            Ht["EthernetState"] = this.RadEOpen_n.Text.Trim() == "以太网" ? "1" : "0";
            Ht["GPRSState"] = this.radGPRSOpen_n.Text.Trim() == "GPRS" ? "1" : "0";
            Ht["485State"] = this.rad485Open_n.Text.Trim() == "RS485" ? "1" : "0";
            Ht["232State"] = this.rad232Open_n.Text.Trim() == "RS232" ? "1" : "0";
            Ht["NetworkID"] = this.NetworkID.Text.Trim();
            Ht["NetworkPower"] = this.NetworkPower.Text.Trim(); 
            Ht["AccessPoint"] = this.AccessPoint.Text.Trim();
            Ht["User"] = this.User.Text.Trim();
            Ht["Pwd"] = this.Pwd.Text.Trim();
            Ht["GPRSDNS"] = this.GPRSDNS.Text.Trim();
            Ht["GPRSServerIP"] = this.GPRSServerIP.Text.Trim();
            Ht["GPRSPoert"] = this.GPRSPoert.Text.Trim();
            Ht["Mac"] = this.Mac.Text.Trim();
            Ht["EIp"] = this.EIp_d.Text.Trim();
            Ht["EMask"] = this.EMask_d.Text.Trim();
            Ht["EGetWay"] = this.EGetWay_d.Text.Trim();
            Ht["EDNS"] = this.EDNS_d.Text.Trim();
            Ht["EServer"] = this.EServer.Text.Trim();
            Ht["EPort"] = this.EPort.Text.Trim();
            Ht["DHCP"] = this.DHCPSOpen.Checked == true ? "1" : "0"; 
        }
        public void SetReadState(System.Windows.Forms.Control control, bool ReadState)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                if (control.Controls[i].Name.IndexOf("_n") > 0)
                {
                    continue;
                }
                switch (control.Controls[i].GetType().Name)
                {
                    case "TextBox":
                        TextBox textBox = (TextBox)control.Controls[i];
                        textBox.ReadOnly = ReadState;
                        break;
                    case "CheckBox":
                        CheckBox checkBox = (CheckBox)control.Controls[i];
                        checkBox.Enabled = !ReadState;
                        break;
                    default:
                        break;

                }
            }

        }

        /// <summary>
        /// 清除控件中的值
        /// </summary>
        /// <param name="control"></param>
        public void ClearControl(System.Windows.Forms.Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                if (control.Controls[i].Name.IndexOf("_n") > 0)
                {
                    continue;
                }
                switch (control.Controls[i].GetType().Name)
                {
                    case "TextBox":
                        TextBox textBox = (TextBox)control.Controls[i];
                        textBox.Text = "";
                        break;
                    case "CheckBox":
                        CheckBox checkBox = (CheckBox)control.Controls[i];
                        checkBox.Checked = false;
                        break;
                    default:
                        break;

                }
            }

        }
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                //写入app.config
                GetModel();
                //写入配置文件
                AppCmd cmd = new AppCmd();
                cmd.WriteConfig(Ht);
                this.saveFileDialog1.ShowDialog();
            }
            catch (Exception ex)
            {

                MessageBox.Show("保存失败");
            }
        }  
        private void Form1_Load(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked);
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageGprs"], radGPRSOpen_n.Checked);
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs232"], rad232Open_n.Checked);
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs485"], rad485Open_n.Checked);
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");
            this.paneltools.Visible = true;
            this.panelConfig.Visible = false;
            husbHelp.DataRecieved += new HusbHelp.DataRecievedEventHandler(analyze);
            Thread th = new Thread(SendUsb1);
            th.IsBackground = true; 

        } 
        private void DHCPSOpen_CheckedChanged(object sender, EventArgs e)
        {
            this.EIp_d.Text = "";
            this.EMask_d.Text = "";
            this.EDNS_d.Text = "";
            this.EGetWay_d.Text = "";
            this.EIp_d.Enabled = false;
            this.EMask_d.Enabled = false;
            this.EDNS_d.Enabled = false;
            this.EGetWay_d.Enabled = false;
           
        } 
        private void DHCPSClose_CheckedChanged(object sender, EventArgs e)
        {
            this.EIp_d.Enabled = true;
            this.EMask_d.Enabled = true;
            this.EDNS_d.Enabled = true;
            this.EGetWay_d.Enabled = true;
        } 
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string path = this.saveFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            if (cmd.copy(path))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
            }
        } 
        private void button4_Click(object sender, EventArgs e)
        {
            //Wirte();
            sendCommand("WriteAll");
        }
        /// <summary>
        /// 读取设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReadDeviceConfig_Click(object sender, EventArgs e)
        { 
            try
            { 
               Read(); 
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        /// <summary>
        /// 读取参数
        /// </summary>
        private void Read()
        {
            
            try
            {
                if (husbHelp.ck_device())
                {
                    if (!husbHelp.IsOpen)
                    {

                        if (husbHelp.OpenDevice())//连接设备
                        { 
                           if (dState == 1)
                           {
                               cmdTypes = "readAll";
                               sendCommand("readAll");
                           }
                           else
                           {
                               cmdTypes = "readAll";
                               sendCommand("connect"); 
                           }
                        }
                        else
                        {
                            MessageBox.Show("设备连接失败");
                        }
                    }
                    else
                    {
                        sendCommand("connect");
                    }
                }
                else
                {
                    MessageBox.Show("没有监测到设备");
                }

            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        /// <summary>
        /// 读取参数
        /// </summary>
        private void Wirte()
        {
            try
            {
                if (husbHelp.ck_device())
                {
                    if (!husbHelp.IsOpen)
                    {

                        if (husbHelp.OpenDevice())//连接设备
                        {
                           
                            if (dState == 1)
                            {
                                cmdTypes = "WriteAll";
                                sendCommand("WriteAll");
                            }
                            else
                            {
                                cmdTypes = "WriteAll";
                                sendCommand("connect");
                               
                            }
                        }
                        else
                        {
                            MessageBox.Show("设备连接失败");
                        }
                    }
                    else
                    {
                        sendCommand("connect");
                    }
                }
                else
                {
                    MessageBox.Show("没有监测到设备");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void  sendCommand(string CommandType)
        {

            try
            { 
            byte[] byteCommand;
            switch (CommandType)
            {
                #region  连接
                case "connect":
                    byteCommand = new byte[7];
                    byte[] conbyteCommand = new byte[5];
                    byteCommand[0] = (byte)(byteCommand.Length - 4);
                    byteCommand[1] = 0;
                    byteCommand[2] = 0;
                    byteCommand[3] = (byte)No;
                    byteCommand[4] = 3;
                    Array.Copy(byteCommand, 0, conbyteCommand, 0, 5);
                    int[] cck = AppCmd.GetCheckData(conbyteCommand);
                    byteCommand[5] = (byte)cck[0];
                    byteCommand[6] = (byte)cck[1];
                    sendMsgbyte = new byte[byteCommand.Length];
                    sendMsgbyte = byteCommand;
                    husbHelp.SendData(sendMsgbyte);
                    if (No >= 255)
                    {
                        No = 1;
                    }
                    else
                    {
                        No += 1;
                    }
                    Thread.Sleep(200);
                   #endregion 
                    break;
                case "readAll":
                    #region 读
                    //GetModel(); 
                    int starIndex = 0;

                    byte[] bts = new byte[254];
                    //设备ID    长度5
                    bts[0] = 0X90; 
                    //网关能够容纳的网络节点的最大数量   长度3
                    bts[5] = 0X91;
                    //网关记录网络节点数据的最大条数  长度3
                    bts[8] = 0X92;
                    //网关实时数据包缓冲最大时间  长度3
                    bts[11] = 0X93;
                    //网络ID   长度6
                    bts[14] = 0X94;
                    bts[15] = 0X00;
                    //网络功率  长度3
                    bts[20] = 0X94;
                    bts[21] = 0X01;
                    //GPRS网关功能开关状态 长度2
                    bts[23] = 0X95; 
                    //以太网网关功能开关状态 长度2
                    bts[25] = 0X96;
                    //485网关功能开关状态 长度2
                    bts[27] = 0X97;
                    //232网关功能开关状态 长度2
                    bts[29] = 0X98; 
                    //接入点   长度18
                    bts[31] = 0X99;
                    bts[32] = 0X00;
                    //用户名   长度18
                     bts[49] = 0X99;
                     bts[50] = 0X01;
                     //密码   长度18
                     bts[67] = 0X99;
                     bts[68] = 0X02;
                     //DNS地址 长度18
                     bts[85] = 0X99;
                     bts[86] = 0X03;
                     //服务器地址 长度34
                     bts[85] = 0X99;
                     bts[86] = 0X04;
                     //服务器端口  长度4
                     bts[119] = 0X99;
                     bts[120] = 0X05;
                     //MAC地址  长度18
                     bts[123] = 0X9A;
                     bts[124] = 0X00;
                     //DHCP开关状态 长度3
                     bts[141] = 0X9A;
                     bts[142] = 0X01;
                     //IP地址  长度18
                     bts[144] = 0X9A;
                     bts[145] = 0X02;
                     //子网掩码  长度18
                     bts[162] = 0X9A;
                     bts[163] = 0X03;
                     //默认网关 长度18
                     bts[180] = 0X9A;
                     bts[181] = 0X04;
                     //DNS地址 长度18
                     bts[198] = 0X9A;
                     bts[199] = 0X05;
                     //服务器地址  长度34
                     bts[216] = 0X9A;
                     bts[217] = 0X06;
                     //服务器端口  长度4
                     bts[250] = 0X9A;
                     bts[251] = 0X07; 
                        byte[] sendData = new byte[bts.Length + 13];
                        int DataLength = sendData.Length - 4;
                        byte[] nd = BitConverter.GetBytes(DataLength);
                        sendData[0] = nd[0];
                        sendData[1] = nd[1];
                        sendData[2] = 0;
                        sendData[3] = (byte)No;
                        sendData[4] = 1;
                        sendData[5] = 1;
                        byte[] sDviceData = AppCmd.strToToHexByte(DeviceNO.ToString());
                        Array.Reverse(sDviceData);
                        DeviceNO = AppCmd.ByteToInt16(sDviceData, 0, 4);
                        Array.Copy(sDviceData, 0, sendData, 6, 4);
                        sendData[10] = (byte)bts.Length;
                        Array.Copy(bts, 0, sendData, 11, bts.Length);
                        byte[] ckby = new byte[sendData.Length - 2];
                        Array.Copy(sendData, 0, ckby, 0, sendData.Length - 2);
                        int[] ckcode = AppCmd.GetCheckData(ckby);
                        sendData[sendData.Length - 2] = (byte)ckcode[0];
                        sendData[sendData.Length - 1] = (byte)ckcode[1]; 
                        sendMsgbyte = new byte[sendData.Length]; 
                        sendMsgbyte = sendData;  
                        husbHelp.SendData(sendMsgbyte); 
                            if (No >= 255)
                            {
                                No = 1;
                            }
                            else
                            {
                                No += 1;
                            }
                            LogHelper.WriteRight("序列包"+No.ToString());
                      
                    //}
                    
                    #endregion
                    break;
                case "WriteAll":
                    #region   写
                    GetModel();
                    byte[] writeBt = new byte[1];
                    int wStarIndex = 0;
                    foreach (DictionaryEntry de in Ht)
                    {
                        int i = 0;

                        while (writeState)
                        {
                            Thread.Sleep(500);
                            if (i > 3)
                            {  
                                break; }
                           
                            i++;
                        }  
                        byte[] bt = new byte[1];
                        switch (de.Key.ToString())
                        {
                            case "ID"://设备ID
                                byte[] btDviceData = AppCmd.strToToHexByte(Ht["ID"].ToString());
                                Array.Reverse(btDviceData);
                               // byte[] btDviceData = AppCmd.strToToHexByte(Ht["ID"].ToString());
                                writeBt = new byte[5];
                                writeBt[0] = 0X90;
                                 Array.Copy(btDviceData, 0, bt, 1, btDviceData.Length); 
                              //  wStarIndex += 5;
                                break;
                            case "MaxNode"://网关能够容纳的网络节点的最大数量
                                writeBt = new byte[3];
                                writeBt[0] = 0X91;
                                int MaxNode = 0;
                                int.TryParse(Ht["MaxNode"].ToString(), out MaxNode);
                                writeBt[1] = AppCmd.IntToByte(MaxNode, 2)[0];
                                writeBt[2] = AppCmd.IntToByte(MaxNode, 2)[1];

                             //   wStarIndex += 3;
                                break;
                            case "MaxNodeData"://网关记录网络节点数据的最大条数
                                writeBt = new byte[3];
                                writeBt[0] = 0X92;
                                int MaxNodeData = 0;
                                int.TryParse(Ht["MaxNodeData"].ToString(), out MaxNodeData);
                                writeBt[1] = AppCmd.IntToByte(MaxNodeData, 2)[0];
                                writeBt[2] = AppCmd.IntToByte(MaxNodeData, 2)[1];

                              //  wStarIndex += 3;

                                break;
                            case "DataBuffer"://网关实时数据包缓冲最大时间
                                writeBt = new byte[3];
                                writeBt[0] = 0X92;
                                int DataBuffer = 0;
                                int.TryParse(Ht["DataBuffer"].ToString(), out DataBuffer);
                                writeBt[1] = AppCmd.IntToByte(DataBuffer, 2)[0];
                                writeBt[2] = AppCmd.IntToByte(DataBuffer, 2)[1];

                            //    wStarIndex += 3;
                                break;
                            case "NetworkID"://网络ID
                                writeBt = new byte[6];
                                writeBt[0] = 0X94;
                                writeBt[1] = 0X00;
                                byte[] NetworkID = AppCmd.strToToHexByte(Ht["NetworkID"].ToString());
                                Array.Copy(NetworkID, 0, writeBt, 2, 4);

                             //   wStarIndex += 6;
                                break;
                            case "NetworkPower"://网络功率
                                writeBt = new byte[3];
                                writeBt[0] = 0X94;
                                writeBt[1] = 0X01;
                                int NetworkPower = 0;
                                int.TryParse(Ht["NetworkPower"].ToString(), out NetworkPower);
                                writeBt[2] = AppCmd.IntToByte(NetworkPower, 2)[0];

                              //  wStarIndex += 3;
                                break;
                            case "GPRSState"://GPRS网关功能开关状态
                                writeBt = new byte[2];
                                writeBt[0] = 0X95;
                                int GPRSState = 0;
                                int.TryParse(Ht["GPRSState"].ToString(), out GPRSState);
                                writeBt[1] = (byte)GPRSState;

                               // wStarIndex += 3;

                                break;
                            case "EthernetState"://以太网网关功能开关状态
                                writeBt = new byte[2];
                                writeBt[0] = 0X96;
                                int EthernetState = 0;
                                int.TryParse(Ht["EthernetState"].ToString(), out EthernetState);
                                writeBt[1] = (byte)EthernetState;

                               // wStarIndex += 3;
                                break;
                            case "485State"://485网关功能开关状态
                                writeBt = new byte[2];
                                writeBt[0] = 0X97;
                                int State485 = 0;
                                int.TryParse(Ht["485State"].ToString(), out State485);
                                writeBt[1] = (byte)State485;

                                wStarIndex += 3;
                                break;
                            case "232State"://232网关功能开关状态
                                writeBt = new byte[2];
                                writeBt[0] = 0X97;
                                int State232 = 0;
                                int.TryParse(Ht["232State"].ToString(), out State232);
                                writeBt[1] = (byte)State232;

                             //   wStarIndex += 3;
                                break;
                            case "AccessPoint"://接入点名
                                string AccessPoint = Ht["AccessPoint"].ToString();
                                byte[] btAccessPoint = Encoding.ASCII.GetBytes(AccessPoint);
                                writeBt = new byte[btAccessPoint.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X00;
                             //   Array.Copy(btAccessPoint, 0, bt, 2, btAccessPoint.Length);

                            //    wStarIndex += btAccessPoint.Length + 2;
                                break;
                            case "User"://用户名
                                string User = Ht["User"].ToString();
                                byte[] btUser = Encoding.ASCII.GetBytes(User);
                                writeBt = new byte[btUser.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X01;
                             //   Array.Copy(btUser, 0, bt, 2, btUser.Length);

                             //   wStarIndex += btUser.Length + 2;
                                break;
                            case "Pwd"://密码
                                string Pwd = Ht["Pwd"].ToString();
                                byte[] btPwd = Encoding.ASCII.GetBytes(Pwd);
                                writeBt = new byte[btPwd.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X02;
                            //    Array.Copy(btPwd, 0, bt, 2, btPwd.Length);

                               // wStarIndex += btPwd.Length + 2;
                                break;
                            case "GPRSDNS"://DNS地址
                                string GPRSDNS = Ht["GPRSDNS"].ToString();
                                byte[] btGPRSDNS = Encoding.ASCII.GetBytes(GPRSDNS);
                                bt = new byte[btGPRSDNS.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X03;
                             //   Array.Copy(btGPRSDNS, 0, bt, 2, btGPRSDNS.Length);

                             //   wStarIndex += btGPRSDNS.Length + 2;
                                break;
                            case "GPRSServerIP"://服务器地址
                                string GPRSServerIP = Ht["GPRSServerIP"].ToString();
                                byte[] btGPRSServerIP = Encoding.ASCII.GetBytes(GPRSServerIP);
                                writeBt = new byte[btGPRSServerIP.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X04;
                              //  Array.Copy(btGPRSServerIP, 0, bt, 2, btGPRSServerIP.Length);

                              //  wStarIndex += btGPRSServerIP.Length + 2;
                                break;
                            case "GPRSPoert"://服务器端口D
                                string GPRSPoert = Ht["GPRSPoert"].ToString();
                                byte[] btGPRSPoert = Encoding.ASCII.GetBytes(GPRSPoert);
                                writeBt = new byte[btGPRSPoert.Length + 2];
                                writeBt[0] = 0X99;
                                writeBt[1] = 0X05;
                              //  Array.Copy(btGPRSPoert, 0, bt, 2, btGPRSPoert.Length);

                             //   wStarIndex += btGPRSPoert.Length + 2;
                                break;
                            case "Mac"://服务器端口
                                string Mac = Ht["Mac"].ToString();
                                byte[] btMac = Encoding.ASCII.GetBytes(Mac);
                                writeBt = new byte[btMac.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X00;
                              //  Array.Copy(btMac, 0, bt, 2, btMac.Length);

                            //    wStarIndex += btMac.Length + 2;
                                break;
                            case "DHCP"://DHCP开关状态
                                string DHCP = Ht["DHCP"].ToString();
                                byte[] btDHCP = Encoding.ASCII.GetBytes(DHCP);
                                writeBt = new byte[btDHCP.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X01;
                             //   Array.Copy(btDHCP, 0, bt, 2, btDHCP.Length);

                              //  wStarIndex += btDHCP.Length + 2;
                                break;
                            case "EIp"://IP地址
                                string EIp = Ht["EIp"].ToString();
                                byte[] btEIp = Encoding.ASCII.GetBytes(EIp);
                                writeBt = new byte[btEIp.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X02;
                                //Array.Copy(btEIp, 0, bt, 2, btEIp.Length);

                               // wStarIndex += btEIp.Length + 2;
                                break;
                            case "EMask"://子网掩码
                                string EMask = Ht["EMask"].ToString();
                                byte[] btEMask = Encoding.ASCII.GetBytes(EMask);
                                writeBt = new byte[btEMask.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X02;
                               // Array.Copy(btEMask, 0, bt, 2, btEMask.Length);

                               // wStarIndex += btEMask.Length + 2;
                                break;
                            case "EGetWay"://默认网关
                                string EGetWay = Ht["EGetWay"].ToString();
                                byte[] btEGetWay = Encoding.ASCII.GetBytes(EGetWay);
                                writeBt = new byte[btEGetWay.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X02;
                              //  Array.Copy(btEGetWay, 0, bt, 2, btEGetWay.Length);

                               // wStarIndex += btEGetWay.Length + 2;
                                break;
                            case "EDNS"://DNS地址
                                string EDNS = Ht["EDNS"].ToString();
                                byte[] btEDNS = Encoding.ASCII.GetBytes(EDNS);
                                writeBt = new byte[btEDNS.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X02;
                                //Array.Copy(btEDNS, 0, bt, 2, btEDNS.Length);

                             //   wStarIndex += btEDNS.Length + 2;
                                break;
                            case "EServer"://服务器地址
                                string EServer = Ht["EServer"].ToString();
                                byte[] btEServer = Encoding.ASCII.GetBytes(EServer);
                                writeBt = new byte[btEServer.Length + 2];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X02;
                              //  Array.Copy(btEServer, 0, bt, 2, btEServer.Length);

                               // wStarIndex += btEServer.Length + 2;
                                break;
                            case "EPort"://服务器端口
                                writeBt = new byte[4];
                                writeBt[0] = 0X9A;
                                writeBt[1] = 0X07;

                              //  wStarIndex += 2;
                                break;
                        }
                        byte[] wsendData = new byte[writeBt.Length + 13];
                        int sDataLength = wsendData.Length - 4;
                        byte[] nd1 = BitConverter.GetBytes(sDataLength);
                        wsendData[0] = nd1[0];
                        wsendData[1] = nd1[1]; 
                        wsendData[2] = 0;
                        wsendData[3] = (byte)No;
                        wsendData[4] = 1;
                        wsendData[5] = 1;
                      //  byte[] wDviceData = AppCmd.strToToHexByte(DeviceNO.ToString());
                        byte[] wDviceData = AppCmd.strToToHexByte(DeviceNO.ToString());
                        Array.Reverse(wDviceData);
                        Array.Copy(wDviceData, 0, wsendData, 6, 4);
                        wsendData[10] = (byte)writeBt.Length;
                        Array.Copy(writeBt, 0, wsendData, 11, writeBt.Length);
                        byte[] wckby = new byte[wsendData.Length - 2];
                        Array.Copy(wsendData, 0, wckby, 0, wsendData.Length - 2);
                        int[] wckcode = AppCmd.GetCheckData(wckby);
                        wsendData[wsendData.Length - 2] = (byte)wckcode[0];
                        wsendData[wsendData.Length - 1] = (byte)wckcode[1];
                        string sdf=BitConverter.ToString(wsendData); 
                        sendMsgbyte = new byte[wsendData.Length];
                        sendMsgbyte = wsendData;
                        husbHelp.SendData(sendMsgbyte);
                        if (writeState)
                        {
                            if (No >= 255)
                            {
                                No = 1;
                            }
                            else
                            {
                                No += 1;
                            }
                        }
 
                    }
                  


                    #endregion
                    break;

                case "ACK":

                    break;
            }
           
            }
            catch (Exception ex)
            {
                
              MessageBox.Show("命令发送异常，请检查连接 ");
            }
        }
        public void SendUsb1( )
        {
         
        husbHelp.SendData(sendMsgbyte);
        }
        /// <summary>
        /// 接收数据解析
        /// </summary>
        /// <param name="?"></param>
        public void analyze(byte[] data)
        {
            string  strData=BitConverter.ToString(data);
          
            try
            { 
                int DataTotleLength = AppCmd.ByteToInt(data, 0, 2);
                string CheckCode = "";//校验码 
                byte[] bCk = new byte[2]; //校验码
                byte[] ckBtye = new byte[DataTotleLength + 2]; //校验数据
                Array.Copy(data, ckBtye.Length, bCk, 0, 2);//获取校验数据
                Array.Copy(data, 0, ckBtye, 0, ckBtye.Length);//获取校验数据
                CheckCode = BitConverter.ToString(bCk);
                if (CheckCode == AppCmd.CheckData(ckBtye))//校验位验证
                {

                    if (data[2] == 0x01)
                    {
                        return;
                    }
                    #region 回复ACK

                    byte[] ACKByte = new byte[6];
                    ACKByte[0] = 2;
                    ACKByte[1] = 0;
                    ACKByte[2] = 1;
                    ACKByte[3] = data[3];
                    byte[] CKByte = new byte[4];
                    Array.Copy(ACKByte, 0, CKByte, 0, 4);
                    int[] ck1 = AppCmd.GetCheckData(CKByte);
                    ACKByte[4] = (byte)ck1[0];
                    ACKByte[5] = (byte)ck1[1]; 
                    #endregion

                    int DataPackNo = AppCmd.ByteToInt(data, 3, 4);
                    //服务类型
                    int ServerType = AppCmd.ByteToInt(data, 4, 5);

                    switch (ServerType)
                    {
                        case 3://连接回复 
                            break;
                        case 1: //读取 
                            #region


                            #endregion
                            break;
                        case 2:///负载 
                            //sendMsgbyte = new byte[ACKByte.Length]; 
                            //sendMsgbyte = ACKByte;
                            //Thread th = new Thread(SendUsb1);
                            //th.IsBackground = true;
                            //th.Start();
                            husbHelp.SendData(sendMsgbyte);
                            readState = false;
                            int parameterLength = AppCmd.ByteToInt(data, 10, 11);//参数总长度 
                            int DataStar = 0;//开始位置 
                            while (DataStar < parameterLength)
                            {
                                int DataType = AppCmd.ByteToInt(data, DataStar + 11, DataStar + +12);
                                
                                switch (DataType)
                                {
                                    case 0X90: //网关ID
                                        DeviceNO = AppCmd.ByteToInt16(data, DataStar + 12, DataStar + +16);//网关序列号
                                        this.BeginInvoke(new EventHandler(delegate
                                        {
                                            this.ID.Text = DeviceNO.ToString();
                                        }));

                                        DataStar += 5;
                                        break;
                                    case 0X91://网关能够容纳的网络节点的最大数量
                                        int  MaxNode = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14) ;
                                          this.BeginInvoke(new EventHandler(delegate
                                     {
                                         this.MaxNode.Text = MaxNode.ToString();
                                     }));
                                        DataStar += 3;
                                        break;
                                    case 0X92: //网关记录网络节点数据的最大条数
                                        int MaxNodeData = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                     //   Ht["MaxNodeData"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                        this.BeginInvoke(new EventHandler(delegate
                                 {
                                     this.MaxNodeData.Text = MaxNodeData.ToString();
                                 }));
                                        DataStar += 3;
                                        break;
                                    case 0X93: //网关实时数据包缓冲最大时间
                                     //   Ht["DataBuffer"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                        int DataBuffer = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                        this.BeginInvoke(new EventHandler(delegate
                                        { this.DataBuffer.Text = DataBuffer.ToString(); }));
                                        DataStar += 3;
                                        break;
                                    case 0X94: //433网络参数
                                        int pType = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        switch (pType)
                                        {
                                            case 0x00://网络ID
                                                int NetworkID = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 16);
                                               // Ht["NetworkID"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 16);
                                                this.BeginInvoke(new EventHandler(delegate
                                          { this.NetworkID.Text = NetworkID.ToString(); }));
                                                DataStar += 5;
                                                break;
                                            case 0x01://网络功率
                                               int  NetworkPower=AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                              //  Ht["NetworkPower"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.NetworkPower.Text = NetworkPower.ToString(); }));
                                                DataStar += 2;
                                                break;
                                        }
                                        break;
                                    case 0X95://GPRS网关功能开关状态
                                        int GPRSState = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                       // Ht["GPRSState"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                     {
                                         this.radGPRSClose_n.Checked = false;
                                         this.radGPRSOpen_n.Checked = GPRSState == 1 ? true : false;
                                     }));
                                        DataStar += 2;
                                        break;
                                    case 0X96://以太网网关功能开关状态 
                                        int EthernetState = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                       // Ht["EthernetState"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                       {
                                           this.RadEClose_n.Checked = false;
                                           this.RadEOpen_n.Checked = EthernetState ==  1  ? true : false;
                                       }));
                                        DataStar += 2;
                                        break;
                                    case 0X97: //485网关功能开关状态
                                        int State485=  AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);;
                                       // Ht["485State"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                       {
                                           this.rad485Open_n.Checked = State485 ==  1  ? true : false;
                                           this.rad485Close_n.Checked = State485 ==  0  ? true : false;
                                       }));
                                        DataStar += 2;
                                        break;
                                    case 0X98: //232网关功能开关状态
                                        int State232 = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                      //  Ht["232State"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                         {
                                             this.rad232Open_n.Checked = State232 ==  1 ? true : false;
                                             this.rad232Close_n.Checked = State232 == 0  ? true : false;
                                         }));
                                        DataStar += 2;
                                        break;
                                    case 0X99://GPRS参数
                                        int GPRSType = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        switch (GPRSType)
                                        {
                                            case 0x00://接入点名
                                                string AccessPoint = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                              //  Ht["AccessPoint"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                          { this.AccessPoint.Text = AccessPoint; }));
                                                DataStar += 17;
                                                break;
                                            case 0x01://用户名
                                                string User = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                               // Ht["User"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                          { this.User.Text = User; }));
                                                DataStar += 17;
                                                break;
                                            case 0x02://密码
                                                string Pwd = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);;
                                              //  Ht["Pwd"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.Pwd.Text = Pwd; }));
                                                DataStar += 17;
                                                break;
                                            case 0x03://DNS地址
                                                string GPRSDNS = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                              //  Ht["GPRSDNS"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSDNS.Text = GPRSDNS; }));
                                                DataStar += 17;
                                                break;
                                            case 0x04://服务器地址
                                                string GPRSServerIP = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 44);
                                               // Ht["GPRSServerIP"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 44);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSServerIP.Text = GPRSServerIP; }));
                                                DataStar += 33;
                                                break;
                                            case 0x05://服务器端口
                                                int GPRSPoert = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14) ;
                                               // Ht["GPRSPoert"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSPoert.Text = GPRSPoert.ToString(); }));
                                                DataStar += 3;
                                                break;
                                        }
                                        break;
                                    case 0X9A: //以太网参数
                                        int EType = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        switch (EType)
                                        {
                                            case 0x00://MAC地址
                                                string Mac = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                               // Ht["Mac"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.Mac.Text = Mac.ToString(); }));
                                                DataStar += 17;
                                                break;
                                            case 0x01://DHCP开关状态
                                                int DHCP = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                               // Ht["DHCP"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                                this.BeginInvoke(new EventHandler(delegate
                                         {
                                             this.DHCPSOpen.Checked = DHCP == 1 ? true : false;
                                             this.DHCPSClose.Checked = DHCP == 0? true : false;
                                         }));
                                                DataStar += 2;
                                                break;
                                            case 0x02://Ip
                                                string EIp = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                              //  Ht["EIp"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EIp_d.Text = EIp.ToString(); }));
                                                DataStar += 17;
                                                break;
                                            case 0x03://子网掩码
                                                string EMask = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                             //   Ht["EMask"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EMask_d.Text = EMask.ToString(); }));
                                                DataStar += 17;
                                                break;
                                            case 0x04://默认网关
                                                string EGetWay = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28).ToString();
                                                //Ht["EGetWay"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EGetWay_d.Text = EGetWay.ToString(); }));
                                                DataStar += 17;
                                                break;
                                            case 0x05://DNS地址
                                                string EDNS = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28).ToString();
                                             //   Ht["EDNS"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 28).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EDNS_d.Text = EDNS; }));
                                                DataStar += 17;
                                                break;
                                            case 0x06://服务器地址
                                              string   EServer  = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 44).ToString();
                                                //Ht["EServer"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 44).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EServer.Text = EServer; }));
                                                DataStar += 33;
                                                break;
                                            case 0x07://服务器端口
                                                int EPort = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14) ;
                                               // Ht["EPort"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EPort.Text = EPort.ToString(); }));
                                                DataStar += 3;
                                                break;
                                        }

                                        break;
                                    case 0X9B: //485参数
                                        break;
                                    case 0X9C: //232参数
                                        break;

                                }
                                Thread.Sleep(1);
                            }
                          //  SetModel();
                            break;
                        case 4:///连接回复  
                            //0x00 成功 0x01 失败

                            //连接结果(1)+网关序列号(4)+网关网络号(2)
                            int state = 1;

                            DeviceNO = AppCmd.ByteToInt16(data, 6, 10);//网关序列号
                            state = AppCmd.ByteToInt(data, 5, 6);
                            if (state > 0)
                            {
                                dState = 0;
                            }
                            else
                            {
                                dState = 1;
                                switch (cmdTypes)
                                {
                                    case "readAll":
                                        sendCommand("readAll");
                                        break;
                                    case "WriteAll":
                                        sendCommand("WriteAll");
                                        break;
                                }
                            }
                            int DeviceSN = AppCmd.ByteToInt(data, 10, 11);//网关网络号  
                            break;
                        case 0: //写入

                            break;
                        case 5://断开
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteException(ex);
              
                LogHelper.WriteRight("异常数据：" + strData);
                MessageBox.Show("数据解析异常");
            }
        }

        private void ID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                string EQCodel = ID.Text.Trim();
                int starIndex = 0;
                int endIndex = 0; 
                starIndex = EQCodel.IndexOf("did=");
                if (starIndex > -1)
                {
                    starIndex += 4;
                    endIndex = EQCodel.Length - 1;
                    this.ID.Text = ID.Text.Trim().Substring(starIndex, endIndex - starIndex) ;
                } 
                e.Handled = true;
                this.NetworkID.Focus();   
            }
             
        }

         

        private void RadEOpen_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked);
        }

        private void RadEClose_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked);
        }

        private void radGPRSOpen_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageGprs"], radGPRSOpen_n.Checked);
        }

        private void radGPRSClose_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageGprs"], radGPRSOpen_n.Checked);
        }

        private void rad232Open_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs232"], rad232Open_n.Checked);

        }
        private void rad232Close_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs232"], rad232Open_n.Checked);
        }
        private void rad485Open_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs485"], rad485Open_n.Checked);
        } 
        private void rad485Close_n_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageRs485"], rad485Open_n.Checked);
        }

        private void DHCPSOpen_CheckedChanged_1(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");
        } 
        private void DHCPSClose_CheckedChanged_1(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 0)
            {
                if (btnChange.Text == "上一步")
                {
                    this.tabControl1.SelectedIndex = this.tabControl1.SelectedIndex - 1;
                }
                else
                {
                    this.tabControl1.SelectedIndex = 0;
                }
                int tabIndex = this.tabControl1.SelectedIndex;
                if (tabIndex == 0)
                {
                    button1.Visible = true;
                } 
            }
            btnChange.Visible = true;
        } 
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        ///封装数据包
        /// </summary>
        /// <param name="bt">包含服务类型和负载</param> 
        /// <returns></returns>
        //private SendModel packagObject(byte[] bt)
        //{
        //    if (No >= 255)
        //    {
        //        No = 1;
        //    }
        //    else
        //    {
        //        No += 1;
        //    }
        //    SendModel smodel = new SendModel();
        //    int Length = bt.Length + 6;
        //    int DataLength = bt.Length + 2;
        //    byte[] sbt = new byte[Length];
        //    byte[] nd = BitConverter.GetBytes(DataLength);
        //    sbt[0] = nd[0];
        //    sbt[1] = nd[1];
        //    sbt[2] = 0x00;
        //    sbt[3] = (byte)No;
        //    Array.Copy(bt, 0, sbt, 4, bt.Length);
        //    int[] ck = AppCmd.GetCheckData(sbt, 0, sbt.Length - 2);
        //    sbt[sbt.Length - 2] = (byte)ck[0];
        //    sbt[sbt.Length - 1] = (byte)ck[1];
        //    smodel.Cmdbyte = sbt;
        //    smodel.NO = No;
        //    smodel.SendNum = 0;
        //    smodel.State = false;
        //    return smodel;
        //}
    }
}
