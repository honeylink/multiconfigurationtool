﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace M433ConfigTool
{
    public class AppCmd
    {

        /// <summary>
        /// 读取配置文件
        /// </summary>
        public void ReadConfig(ref Hashtable ht, string path)
        { 
            try
            { 
                XmlDocument doc = new XmlDocument();
                doc.Load(path);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点  
                ht.Clear();
                XmlNodeList nodes = rootElem.GetElementsByTagName("person");
                foreach (XmlNode node in nodes)
                {
                    string key = ((XmlElement)node).GetAttribute("key");   //获取name属性值  
                    string val = ((XmlElement)node).GetAttribute("value");   //获取name属性值 
                    ht.Add(key, val);
                }
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
        /// <summary>
        /// 读取配置文件
        /// </summary>
        public List<cmdModel> ReadConfigCmd()
        {

            List<cmdModel> list = new List<cmdModel>();
            try
            {
                string rootpath = Application.StartupPath + "\\set.xml";
                XmlDocument doc = new XmlDocument();
                doc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = doc.DocumentElement;   //获取根节点   
                XmlNodeList nodes = rootElem.GetElementsByTagName("person");
                foreach (XmlNode node in nodes)
                {
                    cmdModel model = new cmdModel();
                    model.Address = ((XmlElement)node).GetAttribute("address");
                    model.CmdName = ((XmlElement)node).GetAttribute("key");   //获取name属性值  
                    model.Max = ((XmlElement)node).GetAttribute("max");   //获取name属性值  
                    model.Val = ((XmlElement)node).GetAttribute("value");   //获取name属性值 
                    model.Length = ((XmlElement)node).GetAttribute("length");   //获取name属性值 
                    model.Type = ((XmlElement)node).GetAttribute("type");   //获取name属性值 

                    model.Sort = Convert.ToInt32(model.Address,16);
                   
                    list.Add(model);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("参数错误");
            }
            return list;
        }

        /// <summary>
        /// 写入配置文件
        /// </summary>
        public bool WriteConfig(Hashtable ht)
        {
            bool flag = false;
            string s = "";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                string rootpath = Application.StartupPath + "\\set.xml"; 
                xmlDoc.Load(rootpath);    //加载Xml文件  
                XmlElement rootElem = xmlDoc.DocumentElement;   //获取根节点  
                XmlNodeList nodes = rootElem.GetElementsByTagName("person"); 
                foreach (XmlNode node in nodes)
                { 
                    XmlElement xe = (XmlElement)node;//将子节点类型转换为XmlElement类型 
                    string key = ((XmlElement)node).GetAttribute("key");   //获取name属性值  
                    s=key;
                    string max = ((XmlElement)node).GetAttribute("max");   //获取name属性值  
                    int maxLength = 0;
                    int.TryParse(max,out maxLength);
                    try
                    {
                        string vl = ht[key].ToString();
                        string length = vl.Length.ToString();
                        xe.SetAttribute("value", vl);
                        xe.SetAttribute("length", length);
                    }
                    catch (Exception)
                    {
                    }

                }
                xmlDoc.Save(rootpath); 
                flag = true;
            }
            catch (Exception ex)
            { 
                throw new Exception(ex.ToString()) ;
            }
            return flag;
        }

        public bool copy(string newPath)
        {
            bool flag = false;
            if (!Directory.Exists(newPath))
            {
                if (File.Exists(newPath))
                {
                    File.Delete(newPath);  
                }
                string rootpath = Application.StartupPath + "\\set.xml";
                //先来移动文件 
                File.Copy(rootpath, newPath, false); //复制文件 
                flag = true;
            }
            
                
            return flag;
        }
        /// <summary>
        /// 保存配置文件
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public   void SaveConfig(string key, string val)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            KeyValueConfigurationCollection sysConfig = configuration.AppSettings.Settings;
            KeyValueConfigurationElement element = sysConfig[key];
            element.Value = val;
            configuration.Save();
        }
       /// <summary>
       /// 验证码
       /// </summary>
       /// <param name="val"></param>
       /// <returns></returns>
        public static string CheckData(byte[] val)
        {

            byte[] by = val;
            byte crc_Low = 0xFF;
            byte crc_High = 0xFF;

            for (int i = 0; i < by.Length; i++)
            {
                int crcIndex = crc_High ^ by[i];
                crc_High = (byte)(crc_Low ^ CRCHigh[crcIndex]);
                crc_Low = CRCLow[crcIndex];
            }

            byte[] reCrc16 = new byte[2];
            reCrc16[0] = crc_Low;
            reCrc16[1] = crc_High;
            string ck = BitConverter.ToString(reCrc16);
            return ck;

        }
        /// <summary>
        /// 验证码
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string CheckData(byte[] val, int StarIndex, int Length)
        { 
            byte[] by = new byte[Length];
            Array.Copy(val, StarIndex, by, 0, Length);
            byte crc_Low = 0xFF;
            byte crc_High = 0xFF;

            for (int i = 0; i < by.Length; i++)
            {
                int crcIndex = crc_High ^ by[i];
                crc_High = (byte)(crc_Low ^ CRCHigh[crcIndex]);
                crc_Low = CRCLow[crcIndex];
            }

            byte[] reCrc16 = new byte[2];
            reCrc16[0] = crc_Low;
            reCrc16[1] = crc_High;
            string ck = BitConverter.ToString(reCrc16);
            return ck;

        }
        /// <summary>
        /// 验证码
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int[] GetCheckData(byte[] val,int StarIndex,int Length)
        {

            byte[] by =new byte[Length] ;
            Array.Copy(val, StarIndex, by, 0, Length);

            byte crc_Low = 0xFF;
            byte crc_High = 0xFF;

            for (int i = 0; i < by.Length; i++)
            {
                int crcIndex = crc_High ^ by[i];
                crc_High = (byte)(crc_Low ^ CRCHigh[crcIndex]);
                crc_Low = CRCLow[crcIndex];
            }

            int[] reCrc16 = new int[2];
            reCrc16[0] = crc_Low;
            reCrc16[1] = crc_High;
            return reCrc16; ;

        }

        /// <summary>
        /// 验证码
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int[] GetCheckData(byte[] val)
        {

            byte[] by = val;
            byte crc_Low = 0xFF;
            byte crc_High = 0xFF;

            for (int i = 0; i < by.Length; i++)
            {
                int crcIndex = crc_High ^ by[i];
                crc_High = (byte)(crc_Low ^ CRCHigh[crcIndex]);
                crc_Low = CRCLow[crcIndex];
            }

            int[] reCrc16 = new int[2];
            reCrc16[0] = crc_Low;
            reCrc16[1] = crc_High;
            return reCrc16; ;

        }
        readonly static byte[] CRCHigh = new byte[]{  
            0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};
        //低位表  
        readonly static byte[] CRCLow = new byte[]{  
            0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 
0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 
0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 
0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4, 
0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 
0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 
0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 
0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 
0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 
0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 
0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 
0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 
0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5, 
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 
0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 
0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 
0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 
0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C, 
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 
0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};


        /// <summary>
        /// Byte转Int
        /// </summary>
        /// <param name="tem">原始byte</param>
        /// <param name="starIndex">开始位置</param>
        /// <param name="EndIndex">结束位置</param>
        /// <returns>结果Int</returns>
        public static int ByteToInt(byte[] tem, int starIndex, int EndIndex)
        {
            int I = 0;


            byte[] bte = new byte[4];
            int length = EndIndex - starIndex;
            Array.Copy(tem, starIndex, bte, 0, length);
            I = BitConverter.ToInt32(bte, 0);
            return I;
        }
        /// <summary>
        /// 16进制Byte序列码转Int
        /// </summary>
        /// <param name="tem">原始byte</param>
        /// <param name="starIndex">开始位置</param>
        /// <param name="EndIndex">结束位置</param>
        /// <returns>结果Int</returns>
        public static int ByteToInt16(byte[] tem, int starIndex, int EndIndex)
        {
            int I = 0;
            int length = EndIndex - starIndex;
            for (int i = starIndex + length - 1; i >= starIndex; i--)
            {
                I = (I << 8) + tem[i];
            }
            return I;
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToHexStringASC(byte[] tem, int starIndex, int EndIndex )
        {
            //定义临时变量
            byte[] bte = new byte[EndIndex - starIndex];
            Array.Copy(tem, starIndex, bte, 0, EndIndex - starIndex);
            //转换
            ASCIIEncoding encoding = new ASCIIEncoding();
            string strTemp = encoding.GetString(bte, 0, bte.Length);
            return strTemp;
        }
        /// <summary>  
        /// 10进制格式的字符串表示的整数转换成16进制的little endian字节数组 
        /// 如果"12345678" 对应的16进制数为0xBC6E4E，byte[] = {0x00, 0xBC, 0x6E, 0x4E}  
        /// </summary>  
        /// <param name="hexString"></param>  
        /// <returns></returns>  
        public static byte[] strToToHexByte(string str)
        {
            UInt32 value = System.UInt32.Parse(str);
            byte[] tmpArray1 = new byte[8]{0,0,0,0,0,0,0,0};
            int i = 0;
            for (; ; )
            {               
                if (value != 0)
                {
                    tmpArray1[i] = (byte)(value % 256);
                    value = value >> 8;
                    i ++;
                }
                else
                    break;
            }
            if (i == 0)
            {
                i = 1;
            }
            byte[] tmpArray2 = new byte[i];
            Array.Copy(tmpArray1, tmpArray2, i);

            return tmpArray2; 
         }
        /// <summary>  
        /// 16进制格式字符串表示的整数转换成16进制的big endian字节数组 
        /// 如果"12345678" 转换结果byte[] = {0x12, 0x34, 0x56 , 0x78 }  
        /// </summary>  
        /// <param name="hexString">要求：长度必须为2的倍数</param>  
        /// <returns></returns>  
        public static byte[] hexStrToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if (hexString.Contains("DOO"))
            {
                byte[] bTemp = System.Text.Encoding.Default.GetBytes(hexString);
                return bTemp;
            }

            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2).Trim(), 16);
            //!@todo 正确的处理方式应该是从最后的两个字符的子字符串开始
            return returnBytes;
        }

        /// <summary>  
        /// 16进制格式字符串表示的整数转换成16进制的little endian字节数组 
        /// 如果"12345678" 转换结果byte[] = {0x78, 0x56, 0x34, 0x12}  
        /// </summary>  
        /// <param name="hexString">要求：长度必须为2的倍数</param>  
        /// <returns></returns>  
        public static byte[] hexStrToLittleHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if (hexString.Contains("DOO"))
            {
                byte[] bTemp = System.Text.Encoding.Default.GetBytes(hexString);
                return bTemp;
            }

            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[returnBytes.Length - 1 - i] = Convert.ToByte(hexString.Substring(i * 2, 2).Trim(), 16);
            return returnBytes;
        }

        /// <summary>
        /// Byte转Int
        /// </summary>
        /// <param name="tem">原始byte</param>
        /// <param name="starIndex">开始位置</param>
        /// <param name="EndIndex">结束位置</param>
        /// <returns>结果Int</returns>
        public static byte[] IntToByte(int A, int length)
        {
            int I = 0; 
            byte[] intbyte= BitConverter.GetBytes(A);
            byte[] bt = new byte[length];
            Array.Copy(intbyte, 0, bt, 0, length);
            return bt;
        }
        private string formName;
        public static void SetEnabled(System.Windows.Forms.Control control, bool state)
        {
             
           
            try
            {


                for (int i = 0; i < control.Controls.Count; i++)
                {
                    if (control.Controls[i].Name.IndexOf("_n") > 0)
                    {
                        continue;
                    }

                    string s = control.Controls[i].Name;
                    switch (control.Controls[i].GetType().Name)
                    {
                        case "Label":
                        case "Button":
                        case "RadioButton":
                        case "CheckBox":
                        case "LinkLabel":
                        case "TextBox":
                        case "IpTextBox":
                        case "MACTextBox":
                            //MessageBox.Show(formName + control.Controls[i].Name);
                            control.Controls[i].Enabled = state;
                            break;
                        case "Panel":
                        case "GroupBox":

                            SetEnabled(control.Controls[i], state);
                            break;
                        case "TabControl":

                            TabControl tbc = (TabControl)control.Controls[i];
                            for (int j = 0; j < tbc.TabCount; j++)
                            {

                                SetEnabled(tbc.TabPages[j], state);
                            }
                            break;
                        case "ComboBox": 
                            ComboBox cmbox = (ComboBox)control.Controls[i];
                            cmbox.Enabled = state;
                            break; 
                      
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public static void SetEnabled(System.Windows.Forms.Control control, bool state,string key)
        { 
            try
            {  
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    if (control.Controls[i].Name.IndexOf(key) > 0)
                    {
                        string s = control.Controls[i].Name;
                        switch (control.Controls[i].GetType().Name)
                        {
                            case "Label":
                            case "Button":
                            case "RadioButton":
                            case "CheckBox":
                            case "LinkLabel":
                            case "TextBox":
                            case "IpTextBox":
                            case "MACTextBox":
                                //MessageBox.Show(formName + control.Controls[i].Name);
                                control.Controls[i].Enabled = state;
                                break;
                            case "Panel":
                            case "GroupBox":

                                SetEnabled(control.Controls[i], state, key);
                                break;
                            case "TabControl":

                                TabControl tbc = (TabControl)control.Controls[i];
                                for (int j = 0; j < tbc.TabCount; j++)
                                {

                                    SetEnabled(tbc.TabPages[j], state, key);
                                }
                                break;
                            case "ComboBox":
                                ComboBox cmbox = (ComboBox)control.Controls[i];
                                cmbox.Enabled = state;
                                break;

                            default:
                                break;
                        }
                    }

                   
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

    
}

