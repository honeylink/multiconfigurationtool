﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtCommuProtocol
{
    public enum EXTACPRO_BUSINESS_DEF
    {
        //数据类型定义
        EXTACPRO_SAMPLE_PERIOD		 = 0x01,
        EXTACPRO_UPLOAD_PERIOD		 = 0x02,
									        // = 0x03 reserved
									        // = 0x04 reserved
        EXTACPRO_DATA_MAX			 = 0x05,
        EXTACPRO_DATA_MIN			 = 0x06,
        EXTACPRO_DATA				 = 0x07,
        EXTACPRO_SIGNAL				 = 0x08,
        EXTACPRO_BATTERY			 = 0x09,
        EXTACPRO_POWER				 = 0x0A,
        EXTACPRO_SOFTWARE_VERSION	 = 0x0B,
        EXTACPRO_HARDWARE_VERSION	 = 0x0C,
        EXTACPRO_TIME				 = 0x0D,
        EXTACPRO_LOCATION_INFO		 = 0x0E,
        EXTACPRO_AUXILIARY_FUNCTION  = 0x0F,
        EXTACPRO_RUNTIME_STATUS		 = 0x10,
        EXTACPRO_UPDATE				 = 0x11,
									        // = 0x0F- 0x7F reserved
        EXTACPRO_DEVICE_IN_SET		 = 0x80,
        EXTACPRO_DEVICE_NEW			 = 0x81,
        EXTACPRO_DEVICE_REMOVE		 = 0x82,
									        // = 0x83- 0x8F reserved

        /*!下述为网关特有定义*/
        EXTACPRO_GW_ID						 = 0x90,//网关ID
        EXTACPRO_GW_MAXNODE_IN_NET			 = 0x91,//网关能够容纳的网络节点的最大数量
        EXTACPRO_GW_MAXRECORD_PER_NODE		 = 0x92,//网关记录网络节点数据的最大条数
        EXTACPRO_GW_REALDATA_BUFFER_DELAY	 = 0x93,//网关实时数据包缓冲最大时间
        EXTACPRO_GW_433NET_PARAM			 = 0x94,//433网络参数
        EXTACPRO_GW_GPRS_ONOFF_STATE		 = 0x95,//GPRS网关功能开关状态
        EXTACPRO_GW_ETHERNET_ONOFF_STATE	 = 0x96,//以太网网关功能开关状态
        EXTACPRO_GW_485_ONOFF_STATE			 = 0x97,//485网关功能开关状态
        EXTACPRO_GW_232_ONOFF_STATE			 = 0x98,//232网关功能开关状态
        EXTACPRO_GW_GPRS_PARAM				 = 0x99,//GPRS参数
        EXTACPRO_GW_ETHERNET_PARAM			 = 0x9A,//以太网参数
        EXTACPRO_GW_485_PARAM				 = 0x9B,//485参数
        EXTACPRO_GW_232_PARAM				 = 0x9C,//232参数
									        // = 0x9D-  0xFF reserved

        //传感数据子定义--传感类型定义
        EXTACPRO_SENSOR_TYPE_TEMPERATURE	 = 0x01,
        EXTACPRO_SENSOR_TYPE_HUMIDITY		 = 0x02,
        EXTACPRO_SENSOR_TYPE_CO				 = 0x03,
        EXTACPRO_SENSOR_TYPE_CO2			 = 0x04,
        //附属功能子定义
        EXTACPRO_AUXILIARY_PERMIT_SHUTWODN	 = 0x00,
        EXTACPRO_AUXILIARY_SHUTDOWN			 = 0x01,
        EXTACPRO_AUXILIARY_RESTART			 = 0x02,
        EXTACPRO_AUXILIARY_FACTORY_SETTING	 = 0x03,


        //433网络参数子类型定义
        EXTACPRO_433NET_NID				 = 0x00,
        EXTACPRO_433NET_POWER			 = 0x01,
        EXTACPRO_433NET_CHANNEL_NUMBER	 = 0x02,
        EXTACPRO_433NET_SYNC_WORD		 = 0x03,
        //GPRS参数子类型定义
        EXTACPRO_GPRS_ACCESSPOINT	 = 0x00,
        EXTACPRO_GPRS_ACCOUNT		 = 0x01,
        EXTACPRO_GPRS_PASSWORD		 = 0x02,
        EXTACPRO_GPRS_DNSADDRESS	 = 0x03,
        EXTACPRO_GPRS_SERVERADDRESS	 = 0x04,
        EXTACPRO_GPRS_SERVERPORT	 = 0x05,
        //以太网参数子类型定义
        EXTACPRO_ETHERNET_MACADDRESS	 = 0x00,
        EXTACPRO_ETHERNET_DHCPSTATE		 = 0x01,
        EXTACPRO_ETHERNET_IPADDRESS		 = 0x02,
        EXTACPRO_ETHERNET_NETMASK		 = 0x03,
        EXTACPRO_ETHERNET_GATEWAYDEFAULT	 = 0x04,
        EXTACPRO_ETHERNET_DNSADDRESS	 = 0x05,
        EXTACPRO_ETHERNET_SERVERADDRESS	 = 0x06,
        EXTACPRO_ETHERNET_SERVERPORT	 = 0x07,
        //485参数子类型定义，待后续协议确定后实现
        //232参数子类型定义，待后续协议确定后实现
    }
}
