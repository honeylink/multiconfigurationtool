﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using M433ConfigTool.log;
using System.Collections;

using ExtCommuProtocol;

namespace M433ConfigTool
{
    unsafe public partial class Main : Form
    {
        public Hashtable HtKeyValue = new Hashtable();//对象
        HusbHelp husbHelp = new HusbHelp();//通讯实例

        //通信协议定义和通信实体
        extcommu commu = new extcommu();
        extcommu.ExtLinkEntity_t linkEntity = new extcommu.ExtLinkEntity_t();

        bool bPermitSendNext = true;
        bool bIsReading = false;//暂时不使用，reading是否完成看接收到的某个参数作为标记
        bool bIsWriting = false;//根据当前是否在写，以及如果写的包缓冲中只有1个包，且收到ACK，则表示写完成
        

        int No = 1;
        int cNo = -99;
        private int dState = 0;//连接状态
        List<SendModel> ListSend = new List<SendModel>();//命令池
        string CmdType = "";
        private const UInt32 DeviceDefaultNo = 0x00000001;
        private int DeviceNo;//仅表示通信时应该指定的设备ID

        public Main()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;

            //根据配置，如果不是工厂模式，部分界面隐藏
            if (Global.UserVersion != "Factory")
            {
                tabControl1.TabPages.RemoveByKey("tabPageNet");
                EthernetFactoryPanel.Visible = false;
                BaseParamFactoryPanel.Visible = false;
            }
        }
        /// <summary>
        /// 从设备读取数据
        /// </summary>
        private void ReadConfigFromDevice()
        {
            try
            {
                byte[] sDeviceId = Enumerable.Repeat((byte)0x00, 4).ToArray();

                //byte[] sDviceIdTmp = AppCmd.strToToHexByte(DeviceNo.ToString().PadLeft(8, '0'));
                byte[] sDeviceIdTmp = AppCmd.hexStrToLittleHexByte(DeviceNo.ToString("x2").PadLeft(8, '0'));

                Array.Copy(sDeviceIdTmp, 0, sDeviceId, 0, sDeviceIdTmp.Length);   

                byte[] BuildBuffer = new byte[300];
                int index;

                const int paraTotalLenIndex = 5; 
                /*构建第一个read*/
                index = 0;

                //设备数
                BuildBuffer[index] = 1;
                index += 1;
                //设备ID
                Array.Copy(sDeviceId, 0, BuildBuffer, index, 4);
                index += 4;
                //参数总长度
                index += 1;
                //获取设备ID    长度5
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ID;
                index += 1;
                index += 4;
                //网关能够容纳的网络节点的最大数量   长度3
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXNODE_IN_NET;
                index += 1;
                index += 2;
                //网关记录网络节点数据的最大条数  长度3
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXRECORD_PER_NODE;
                index += 1;
                index += 2;
                //网关实时数据包缓冲最大时间  长度3
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_REALDATA_BUFFER_DELAY;
                index += 1;
                index += 2;
                //专网网络ID   长度6
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_NID;
                index += 1;
                index += 4;
                //网络功率  长度3
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_POWER;
                index += 1;
                index += 1;
                //GPRS网关功能开关状态 长度2
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_ONOFF_STATE;
                index += 1;
                index += 1;
                //以太网网关功能开关状态 长度2
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_ONOFF_STATE;
                index += 1;
                index += 1;
                //485网关功能开关状态 长度2
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_485_ONOFF_STATE;
                index += 1;
                index += 1;
                //232网关功能开关状态 长度2
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_232_ONOFF_STATE;
                index += 1;
                index += 1;
                //GPRS 接入点   长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCESSPOINT;
                index += 1;
                index += 16;
                //GPRS 用户名   长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCOUNT;
                index += 1;
                index += 16;
                //GPRS 密码   长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_PASSWORD;
                index += 1;
                index += 16;
                //GPRS DNS地址 长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_DNSADDRESS;
                index += 1;
                index += 16;
                //GPRS 服务器地址 长度34
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERADDRESS;
                index += 1;
                index += 32;
                //GPRS 服务器端口  长度4
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERPORT;
                index += 1;
                index += 2;
                //ETHERNET MAC地址  长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_MACADDRESS;
                index += 1;
                index += 16;
                //ETHERNET DHCP开关状态 长度3
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DHCPSTATE;
                index += 1;
                index += 1;
                //ETHERNET IP地址  长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_IPADDRESS;
                index += 1;
                index += 16;
                //ETHERNET 子网掩码  长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_NETMASK;
                index += 1;
                index += 16;
                //ETHERNET 默认网关 长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_GATEWAYDEFAULT;
                index += 1;
                index += 16;
                //ETHERNET DNS地址 长度18
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DNSADDRESS;
                index += 1;
                index += 16;
                //ETHERNET 服务器端口  长度4
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERPORT;
                index += 1;
                index += 2;

                //赋值参数总长度
                BuildBuffer[paraTotalLenIndex] = (byte)(index - paraTotalLenIndex - 1);
                fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
                fixed (byte* pGet = BuildBuffer)
                {
                    commu.extCommu_send_get(pLinkEntity, (UInt16)index, pGet, null);
                }

                /*第二个read 包*/
                index = 0;

                BuildBuffer[index] = 1;
                index += 1;
                //设备ID
                Array.Copy(sDeviceId, 0, BuildBuffer, index, 4);
                index += 4;
                //参数总长度
                index += 1;
                //ETHERNET 服务器地址  长度34
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERADDRESS;
                index += 1;
                index += 32;
                //软件版本  长度7
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_SOFTWARE_VERSION;
                index += 1;
                index += 6;
                //硬件版本 长度7
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_HARDWARE_VERSION;
                index += 1;
                index += 6;
                //同步码  长度4
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_SYNC_WORD;
                index += 1;
                index += 2;
                //通信信道 长度3      总是将其作为最后项从而确定在收到此后就表示读结束
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                index += 1;
                BuildBuffer[index] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_CHANNEL_NUMBER;
                index += 1;
                index += 1;

                //赋值参数总长度
                BuildBuffer[paraTotalLenIndex] = (byte)(index - paraTotalLenIndex - 1);
                fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
                fixed (byte* pGet = BuildBuffer)
                {
                    commu.extCommu_send_get(pLinkEntity, (UInt16)index, pGet, null);
                }

                //设置完成进度
                this.BeginInvoke(new EventHandler(delegate
                {
                    progressBar1.Value = 30;
                }));

            }
            catch (Exception ex)
            {
                //进度完成
                this.BeginInvoke(new EventHandler(delegate
                {
                    progressBar1.Value = 100;
                    TitlePanel.Visible = false;
                    MessageBox.Show(ex.ToString());              
                }));

            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void WriteConfigToDevice()
        {
            try
            {
                /*Build buffer*/
                byte[] BuildBuffer = new byte[300];
                int index;
                //const int paraTotalLenIndex = 5;

                GetModelToKeyValueCollection();

                //id
                byte[] sDviceData = Enumerable.Repeat((byte)0x00, 4).ToArray();                
                byte[] sdbt = AppCmd.strToToHexByte(DeviceDefaultNo.ToString().PadLeft(8, '0'));
                Array.Copy(sdbt, 0, sDviceData, 0, sdbt.Length);             

                byte[] writeBt = new byte[300];
                int wStarIndex = 0;
                foreach (DictionaryEntry de in HtKeyValue)
                {
                    byte[] bt = new byte[1];
                    switch (de.Key.ToString())
                    {
                        case "ID"://设备ID
                            bt = new byte[5];
                            byte[] btDviceData = AppCmd.hexStrToToHexByte(HtKeyValue["ID"].ToString());
                            Array.Reverse(btDviceData);
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ID;
                            Array.Copy(btDviceData, 0, bt, 1, btDviceData.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "MaxNode"://网关能够容纳的网络节点的最大数量
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXNODE_IN_NET;
                            int MaxNode = 0;
                            int.TryParse(HtKeyValue["MaxNode"].ToString(), out MaxNode);
                            bt[1] = AppCmd.IntToByte(MaxNode, 2)[0];
                            bt[2] = AppCmd.IntToByte(MaxNode, 2)[1];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "MaxNodeData"://网关记录网络节点数据的最大条数
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXRECORD_PER_NODE;
                            int MaxNodeData = 0;
                            int.TryParse(HtKeyValue["MaxNodeData"].ToString(), out MaxNodeData);
                            bt[1] = AppCmd.IntToByte(MaxNodeData, 2)[0];
                            bt[2] = AppCmd.IntToByte(MaxNodeData, 2)[1];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "DataBuffer"://网关实时数据包缓冲最大时间
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_REALDATA_BUFFER_DELAY;
                            int DataBuffer = 0;
                            int.TryParse(HtKeyValue["DataBuffer"].ToString(), out DataBuffer);
                            bt[1] = AppCmd.IntToByte(DataBuffer, 2)[0];
                            bt[2] = AppCmd.IntToByte(DataBuffer, 2)[1];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "NetworkID"://网络ID
                            bt = new byte[6];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_NID;
                            byte[] NetworkID = AppCmd.strToToHexByte(HtKeyValue["NetworkID"].ToString());
                            Array.Copy(NetworkID, 0, bt, 2, NetworkID.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "NetworkPower"://网络功率
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_POWER;
                            int NetworkPower = 0;
                            int.TryParse(HtKeyValue["NetworkPower"].ToString(), out NetworkPower);
                            bt[2] = AppCmd.IntToByte(NetworkPower, 2)[0];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "GPRSState"://GPRS网关功能开关状态
                            bt = new byte[2];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_ONOFF_STATE;
                            int GPRSState = 0;
                            int.TryParse(HtKeyValue["GPRSState"].ToString(), out GPRSState);
                            bt[1] = (byte)GPRSState;
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EthernetState"://以太网网关功能开关状态
                            bt = new byte[2];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_ONOFF_STATE;
                            int EthernetState = 0;
                            int.TryParse(HtKeyValue["EthernetState"].ToString(), out EthernetState);
                            bt[1] = (byte)EthernetState;
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "485State"://485网关功能开关状态
                            bt = new byte[2];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_485_ONOFF_STATE;
                            int State485 = 0;
                            int.TryParse(HtKeyValue["485State"].ToString(), out State485);
                            bt[1] = (byte)State485;
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "232State"://232网关功能开关状态
                            bt = new byte[2];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_232_ONOFF_STATE;
                            int State232 = 0;
                            int.TryParse(HtKeyValue["232State"].ToString(), out State232);
                            bt[1] = (byte)State232;
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "AccessPoint"://接入点名
                            string AccessPoint = HtKeyValue["AccessPoint"].ToString();
                            byte[] btAccessPoint = Encoding.ASCII.GetBytes(AccessPoint);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCESSPOINT;
                            Array.Copy(btAccessPoint, 0, bt, 2, btAccessPoint.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "User"://用户名
                            string User = HtKeyValue["User"].ToString();
                            byte[] btUser = Encoding.ASCII.GetBytes(User);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCOUNT;
                            Array.Copy(btUser, 0, bt, 2, btUser.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "Pwd"://密码
                            string Pwd = HtKeyValue["Pwd"].ToString();
                            byte[] btPwd = Encoding.ASCII.GetBytes(Pwd);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_PASSWORD;
                            Array.Copy(btPwd, 0, bt, 2, btPwd.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "GPRSDNS"://DNS地址
                            string GPRSDNS = HtKeyValue["GPRSDNS"].ToString();
                            byte[] btGPRSDNS = Encoding.ASCII.GetBytes(GPRSDNS);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_DNSADDRESS;
                            Array.Copy(btGPRSDNS, 0, bt, 2, btGPRSDNS.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "GPRSServerIP"://服务器地址
                            string GPRSServerIP = HtKeyValue["GPRSServerIP"].ToString();
                            byte[] btGPRSServerIP = Encoding.ASCII.GetBytes(GPRSServerIP);
                            bt = new byte[34];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERADDRESS;
                            Array.Copy(btGPRSServerIP, 0, bt, 2, btGPRSServerIP.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "GPRSPoert"://服务器端口
                            bt = new byte[4];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM;
                            bt[1] = 0X05;
                            int GPRSPoert = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERPORT;
                            int.TryParse(HtKeyValue["GPRSPoert"].ToString(), out GPRSPoert);
                            bt[2] = AppCmd.IntToByte(GPRSPoert, 2)[0];
                            bt[3] = AppCmd.IntToByte(GPRSPoert, 2)[1];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "Mac"://ETHERNET mac address
                            string Mac = HtKeyValue["Mac"].ToString();
                            byte[] btMac = Encoding.ASCII.GetBytes(Mac);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_MACADDRESS;
                            Array.Copy(btMac, 0, bt, 2, btMac.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "DHCP"://DHCP开关状态
                            int DHCP = 0;
                            int.TryParse(HtKeyValue["DHCP"].ToString(), out DHCP);
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DHCPSTATE;
                            bt[2] = (byte)DHCP;
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EIp"://IP地址
                            string EIp = HtKeyValue["EIp"].ToString();
                            byte[] btEIp = Encoding.ASCII.GetBytes(EIp);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_IPADDRESS;
                            Array.Copy(btEIp, 0, bt, 2, btEIp.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EMask"://子网掩码
                            string EMask = HtKeyValue["EMask"].ToString();
                            byte[] btEMask = Encoding.ASCII.GetBytes(EMask);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_NETMASK;
                            Array.Copy(btEMask, 0, bt, 2, btEMask.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EGetWay"://默认网关
                            string EGetWay = HtKeyValue["EGetWay"].ToString();
                            byte[] btEGetWay = Encoding.ASCII.GetBytes(EGetWay);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_GATEWAYDEFAULT;
                            Array.Copy(btEGetWay, 0, bt, 2, btEGetWay.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EDNS"://DNS地址
                            string EDNS = HtKeyValue["EDNS"].ToString();
                            byte[] btEDNS = Encoding.ASCII.GetBytes(EDNS);
                            bt = new byte[18];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DNSADDRESS;
                            Array.Copy(btEDNS, 0, bt, 2, btEDNS.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "EPort"://服务器端口
                            bt = new byte[4];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERPORT;
                            int EPort = 0;
                            int.TryParse(HtKeyValue["EPort"].ToString(), out EPort);
                            bt[2] = AppCmd.IntToByte(EPort, 2)[0];
                            bt[3] = AppCmd.IntToByte(EPort, 2)[1];
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;

                    }
                }
                /*构建第一个write*/
                index = 0;

                //设备数
                BuildBuffer[index] = 1;
                index += 1;
                //设备ID
                Array.Copy(sDviceData, 0, BuildBuffer, index, 4);
                index += 4;
                //参数总长度
                BuildBuffer[index] = (byte)(wStarIndex);
                index += 1;
                //复制所有的参数数据
                Array.Copy(writeBt, 0, BuildBuffer, index, wStarIndex);
                index += wStarIndex;

                fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
                fixed (byte* pSet = BuildBuffer)
                {
                    commu.extCommu_send_set(pLinkEntity, (UInt16)index, pSet, null);
                }


                /*Build 第二write 包*/
                wStarIndex = 0;
                foreach (DictionaryEntry de in HtKeyValue)
                {
                    byte[] bt = new byte[1];
                    switch (de.Key.ToString())
                    {
                        case "EServer"://服务器地址
                            string EServer = HtKeyValue["EServer"].ToString();
                            byte[] btEServer = Encoding.ASCII.GetBytes(EServer);
                            bt = new byte[34];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERADDRESS;
                            Array.Copy(btEServer, 0, bt, 2, btEServer.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "Channel"://信道
                            byte[] Channel = AppCmd.strToToHexByte(HtKeyValue["Channel"].ToString());
                            bt = new byte[3];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_CHANNEL_NUMBER;
                            Array.Copy(Channel, 0, bt, 2, Channel.Length);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                        case "SynCode"://同步码
                            byte[] SynCode = AppCmd.hexStrToToHexByte(HtKeyValue["SynCode"].ToString());
                            Array.Reverse(SynCode);
                            bt = new byte[4];
                            bt[0] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM;
                            bt[1] = (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_SYNC_WORD;
                            Array.Copy(SynCode, 0, bt, 2, 2);
                            Array.Copy(bt, 0, writeBt, wStarIndex, bt.Length);
                            wStarIndex += bt.Length;
                            break;
                    }
                }
                /*构建第二个write*/
                index = 0;

                //设备数
                BuildBuffer[index] = 1;
                index += 1;
                //设备ID
                Array.Copy(sDviceData, 0, BuildBuffer, index, 4);
                index += 4;
                //参数总长度
                BuildBuffer[index] = (byte)(wStarIndex);
                index += 1;
                //复制所有的参数数据
                Array.Copy(writeBt, 0, BuildBuffer, index, wStarIndex);
                index += wStarIndex;

                fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
                fixed (byte* pSet = BuildBuffer)
                {
                    commu.extCommu_send_set(pLinkEntity, (UInt16)index, pSet, null);
                }

                //标记正在写
                bIsWriting = true;
            }
            catch (Exception ex)
            {
                //进度完成
                this.BeginInvoke(new EventHandler(delegate
                {
                    progressBar1.Value = 100;
                    TitlePanel.Visible = false;
                    MessageBox.Show(ex.ToString());

                }));
            }

        }
        /// <summary>
        /// 连接设备
        /// </summary>
        public void connetDevice()
        {
            if (husbHelp.ck_device())
            {
                if (!husbHelp.IsOpen)
                {
                    husbHelp.OpenDevice();//连接设备
                }
                if (husbHelp.IsOpen)
                {                    
                    fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
                    {
                        commu.linkEntityInit(pLinkEntity);
                        commu.extCommu_send_connect(pLinkEntity);
                    }
                }
            }
            else
            {
                //进度完成
                
                progressBar1.Value = 100;
                TitlePanel.Visible = false;
               
                MessageBox.Show("设备连接失败");
            }
        }
        /// <summary>
        /// 封装
        /// </summary>
        /// <param name="bt">数据包</param>
        /// <returns></returns>
        private SendModel packagObject(byte[] bt,int ServerType)
        {

          
            SendModel smodel = new SendModel();
            if (ServerType == -1)
            {
                byte[] sbt = new byte[6];
                sbt[0] = 0x02;
                sbt[1] = 0x00;
                sbt[2] = 0x01;
                sbt[3] = bt[0];
                int[] ck = AppCmd.GetCheckData(sbt, 0, sbt.Length - 2);
                sbt[sbt.Length - 2] = (byte)ck[0];
                sbt[sbt.Length - 1] = (byte)ck[1];
                smodel.Cmdbyte = sbt;
                smodel.No = No;
                smodel.Num = 0;
                smodel.ServerType = -1;
                smodel.State = 0;
                 
            }
            else
            {
                int Length = bt.Length + 6;
                int DataLength = bt.Length + 2;
                byte[] sbt = new byte[Length];
                byte[] nd = BitConverter.GetBytes(DataLength);
                sbt[0] = nd[0];
                sbt[1] = nd[1];
                sbt[2] = 0x00;
                sbt[3] = (byte)No;
                Array.Copy(bt, 0, sbt, 4, bt.Length);
                int[] ck = AppCmd.GetCheckData(sbt, 0, sbt.Length - 2);
                sbt[sbt.Length - 2] = (byte)ck[0];
                sbt[sbt.Length - 1] = (byte)ck[1];
                smodel.Cmdbyte = sbt;
                smodel.No = No;
                smodel.Num = 0;
                smodel.ServerType = ServerType;
                smodel.State = 0;
                if (No >= 255)
                {
                    No = 1;
                }
                else
                {
                    No += 1;
                }
            }
            return smodel;
        } 
        /// <summary>
        /// 出装
        /// </summary>
        /// <param name="bt">数据包</param>
        /// <returns></returns>
        private byte[] unpackObject(byte[] bt)
        {
            byte[] reBt = new byte[0]; 
            try
            {
                // LogHelper.WriteRight("\r\n  接收:" + BitConverter.ToString(bt) + "\r\n");
                if (bt.Length >5)
                {
                    int DataTotleLength = AppCmd.ByteToInt(bt, 0, 2);
                    if (DataTotleLength != (bt.Length-4))//数据长度错误
                    {
                        LogHelper.WriteRight("数据长度错误");
                        return reBt; 
                    }
                    int[] ck = AppCmd.GetCheckData(bt, 0, bt.Length - 2);
                    if (bt[bt.Length - 2] != (byte)ck[0] || bt[bt.Length -1] != (byte)ck[1])//校验码
                    {
                        LogHelper.WriteRight("校验码错误");
                        return reBt; 
                    }
                    if (bt[2] == 0x01)
                    {
                     
                        int PackNO = bt[3];
                        if (ListSend.Count > 0)
                        {
                            SendModel sm = new SendModel();
                            sm.No = -99;
                            sm = ListSend.Where(l => l.No == PackNO).FirstOrDefault();
                            if (sm!=null&&sm.No > -99)
                            {
                                ListSend.Remove(sm);
                            }
                        }
                        return reBt;
                    }
                    reBt = new byte[bt.Length -3];
                    Array.Copy(bt, 3, reBt, 0, bt.Length-3);
                    return reBt; 
                   // ACK回复
                    byte[] ACKByte = new byte[2];
                    ACKByte[0] = 0x01;
                    ACKByte[1]= bt[3];
                    ListSend.Insert(0, packagObject(ACKByte,-1));        
                }        

            }
            catch (Exception ex)
            { 
                LogHelper.WriteException(ex);
                LogHelper.WriteRight("异常数据包:" + bt);
            }           
            return reBt;
             
        }
        /// <summary>
        /// 初始化界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, EventArgs e)
        {
            Thread sendTh = new Thread(Send);
            sendTh.IsBackground = true;
            sendTh.Start();
            husbHelp.DataRecieved += new HusbHelp.DataRecievedEventHandler(CommuDataInput);
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");

            //通信协议初始化
            commu.init();
            commu.entity_tx = new extcommu.entity_tx_t(CommuDataOutput);
            commu.ackProc = new extcommu.ExtCommuCallback_t(ackGet);
            commu.connectProc = new extcommu.ExtCommuCallback_t(connectReplyGet);
            commu.indicationProc = new extcommu.ExtCommuCallback_t(indicationGet);

            fixed (extcommu.ExtLinkEntity_t* pLinkEntity = &linkEntity)
            {
                commu.linkEntityInit(pLinkEntity);
            }
        }
        private void CommuDataOutput(UInt16 length, byte* pData)
        {
            //添加进队列
            SendModel smodel = new SendModel();
            byte[] data = extcommu.bytePointerConvertToByteArray(length, pData);
            smodel.Cmdbyte = data;
            ListSend.Add(smodel); 
        }
        private void CommuDataInput(byte[] data)
        {            
            fixed(byte * pData = data)
            fixed(extcommu.ExtLinkEntity_t * pLinkEntity = &linkEntity)
            {
                commu.extCommu_in(pLinkEntity, (UInt16)data.Length, pData);
            }
        }
        
        private void ackGet(extcommu.ExtLinkEntity_t* linkEntity, UInt16 len, byte* p, bool success)
        {
            
            bPermitSendNext = true;

            //检查是否是最后一个写包的ACK
            if (bIsWriting && ListSend.Count <= 1)
            {            
                //进度完成
                this.BeginInvoke(new EventHandler(delegate
                {
                    progressBar1.Value = 100;
                    TitlePanel.Visible = false;
                }));
                notify_messageBox("完成！");
                bIsWriting = false;
            }

        }
        /// <summary>
        /// 连接回复
        /// </summary>
        /// <param name="linkEntity"></param>
        /// <param name="len"></param>
        /// <param name="p"></param>
        /// <param name="success"></param>
        private void connectReplyGet(extcommu.ExtLinkEntity_t* linkEntity, UInt16 len, byte* p, bool success)
        {
            Console.WriteLine("connectReplyGet\r\n");
            bPermitSendNext = true;

            byte[] packData = extcommu.bytePointerConvertToByteArray(len, p);

            //0x00 成功 0x01 失败

            //连接结果(1)+网关序列号(4)+网关网络号(2)
            int state = 1;
            state = AppCmd.ByteToInt(packData, 0, 1);
            DeviceNo = AppCmd.ByteToInt16(packData, 1, 5);//网关序列号
            
            if (state > 0)
            {
                husbHelp.IsOpen = false;
            }
            else
            {
                husbHelp.IsOpen = true;
            }

            Console.WriteLine("connectReplyGet DeviceNo:" + DeviceNo.ToString("x2"));
           

            if (CmdType == "readConfig")
            {
                Console.WriteLine("send read config data\r\n");
                ReadConfigFromDevice();
            }
            else if (CmdType == "writeDevice")
            {
                Console.WriteLine("send write config data\r\n");
                WriteConfigToDevice();
            }
            //进度完成
            this.BeginInvoke(new EventHandler(delegate
            {
                progressBar1.Value = 40;

            }));
        }
        private void indicationGet(extcommu.ExtLinkEntity_t* linkEntity, UInt16 len, byte* p, bool success)
        {
            Console.WriteLine("indicationGet\r\n");

            byte[] packData = extcommu.bytePointerConvertToByteArray(len, p);
            bool bLastPacket = false;

            int parameterLength = AppCmd.ByteToInt(packData, 5, 6);//参数总长度 
            int DataStar = 0;//开始位置 
            int findex = 0;
            while (DataStar < parameterLength)
            {
                findex += 2;
                if (findex > parameterLength)
                {
                    break;
                }
                int DataType = AppCmd.ByteToInt(packData, DataStar + 6, DataStar + 7);
                if (DataType == 0)
                {
                    break;
                }
                int majorParamIndex = DataStar + 7;
                int minorParamIndex;
                switch (DataType)
                {
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ID: //网关ID
                        int identity = AppCmd.ByteToInt16(packData, majorParamIndex, majorParamIndex+4);//网关序列号
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            byte[] hexArray = BitConverter.GetBytes(identity);
                            Array.Reverse(hexArray);
                            string tmp = "";
                            //过滤掉0x00
                            UInt16 i = 0;
                            for (; i < hexArray.Length; i++)
                            {
                                if (hexArray[i] != 0x00)
                                    break;
                            }
                            for (; i < hexArray.Length; i++)
                            {
                                tmp = tmp + (hexArray[i].ToString("X2"));
                            }
                            this.ID.Text = tmp;
                        }));
                        DataStar += 5;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXNODE_IN_NET://网关能够容纳的网络节点的最大数量
                        int MaxNode = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex+2);
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            this.MaxNode.Text = MaxNode.ToString();
                        }));
                        DataStar += 3;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_MAXRECORD_PER_NODE: //网关记录网络节点数据的最大条数
                        int MaxNodeData = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 2);
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            this.MaxNodeData.Text = MaxNodeData.ToString();
                        }));
                        DataStar += 3;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_REALDATA_BUFFER_DELAY: //网关实时数据包缓冲最大时间
                        int DataBuffer = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 2);
                        this.BeginInvoke(new EventHandler(delegate
                        { this.DataBuffer.Text = DataBuffer.ToString(); }));
                        DataStar += 3;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_433NET_PARAM: //433网络参数
                        int pType = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        minorParamIndex = majorParamIndex + 1;
                        switch (pType)
                        {
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_NID://网络ID
                                int NetworkID = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 4);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.NetworkID.Text = NetworkID.ToString(); }));
                                DataStar += 6;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_POWER://网络功率
                                int NetworkPower = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 1);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.NetworkPower.Text = NetworkPower.ToString(); }));
                                DataStar += 3;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_CHANNEL_NUMBER://通信信道
                                int Channel = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 1);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.txtChannel.Text = Channel.ToString(); }));
                                DataStar += 3;
                                bLastPacket = true;//收到此表示最后一个包结束
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_433NET_SYNC_WORD://通信同步码
                                int SynCode = AppCmd.ByteToInt16(packData, minorParamIndex, minorParamIndex + 2);
                                this.BeginInvoke(new EventHandler(delegate
                                {
                                    byte[] hexArray = BitConverter.GetBytes(SynCode);
                                    Array.Reverse(hexArray);
                                    string tmp = "";
                                    UInt16 i = 0;
                                    //过滤掉0x00
                                    for (; i < hexArray.Length; i++)
                                    {
                                        if (hexArray[i] != 0x00)
                                            break;
                                    }
                                    for (; i < hexArray.Length; i++)
                                    {            
                                       tmp = tmp + hexArray[i].ToString("X2");
                                    }
                                    this.txtSynCode.Text = tmp;
                                }));
                                DataStar += 4;
                                break;
                        }
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_ONOFF_STATE://以太网网关功能开关状态 
                        int EthernetState = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            //this.RadEClose_n.Checked = EthernetState == 0 ? true : false;
                            //this.RadEOpen_n.Checked = EthernetState == 1 ? true : false;
                            //AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked);
                            if (EthernetState == 1)
                                this.gatewayTypeSelectBox.SelectedIndex = 0;
                        }));
                        DataStar += 2;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_ONOFF_STATE://GPRS网关功能开关状态
                        int GPRSState = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            if (GPRSState == 1)
                                this.gatewayTypeSelectBox.SelectedIndex = 1;
                        }));
                        DataStar += 2;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_232_ONOFF_STATE: //232网关功能开关状态
                        int State232 = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            if (State232 == 1)
                                this.gatewayTypeSelectBox.SelectedIndex = 2;
                        }));
                        DataStar += 2;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_485_ONOFF_STATE: //485网关功能开关状态
                        int State485 = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1); ;
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            if (State485 == 1)
                                this.gatewayTypeSelectBox.SelectedIndex = 3;
                        }));
                        DataStar += 2;
                        break;

                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_GPRS_PARAM://GPRS参数
                        int GPRSType = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        minorParamIndex = majorParamIndex + 1;
                        switch (GPRSType)
                        {
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCESSPOINT://接入点名
                                string AccessPoint = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.AccessPoint.Text = AccessPoint; }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_ACCOUNT://用户名
                                string User = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.User.Text = User; }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_PASSWORD://密码
                                string Pwd = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16); ;
                                this.BeginInvoke(new EventHandler(delegate
                                { this.Pwd.Text = Pwd; }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_DNSADDRESS://DNS地址
                                string GPRSDNS = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).Replace("\0", "");
                                this.BeginInvoke(new EventHandler(delegate
                                { this.GPRSDNS.Text = GPRSDNS; }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERADDRESS://服务器地址
                                string GPRSServerIP = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 32);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.GPRSServerIP.Text = GPRSServerIP; }));
                                DataStar += 34;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GPRS_SERVERPORT://服务器端口
                                int GPRSPoert = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 2);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.GPRSPoert.Text = GPRSPoert.ToString(); }));
                                DataStar += 4;
                                break;
                            default:
                                Console.WriteLine("GOT gprs unexpected type:" + GPRSType.ToString());
                                break;
                        }
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_ETHERNET_PARAM: //以太网参数
                        int EType = AppCmd.ByteToInt(packData, majorParamIndex, majorParamIndex + 1);
                        minorParamIndex = majorParamIndex + 1;
                        switch (EType)
                        {
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_MACADDRESS://MAC地址
                                string MacV = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).Replace("\0", "");
                                string Mac = "";
                                if (MacV.Length > 0)
                                {
                                    for (int i = 0; i < 6; i++)
                                    {
                                        Mac = Mac + MacV.Substring(i * 2, 2).Trim() + "-";
                                    }
                                    Mac = Mac.Substring(0, 17);

                                }
                                this.BeginInvoke(new EventHandler(delegate
                                { this.Mac.Text = Mac.ToString(); }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DHCPSTATE://DHCP开关状态
                                int DHCP = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 1);
                                this.BeginInvoke(new EventHandler(delegate
                                {
                                    this.DHCPSOpen.Checked = DHCP == 1 ? true : false;
                                    this.DHCPSClose.Checked = DHCP == 0 ? true : false;
                                    AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], !DHCPSOpen.Checked, "_d");
                                }));
                                DataStar += 3;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_IPADDRESS://Ip
                                string EIp = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).Replace("\0", "");
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EIp_d.Text = EIp.ToString(); }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_NETMASK://子网掩码
                                string EMask = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).Replace("\0", "");
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EMask_d.Text = EMask.ToString(); }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_GATEWAYDEFAULT://默认网关
                                string EGetWay = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).ToString().Replace("\0", "");
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EGetWay_d.Text = EGetWay.ToString(); }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_DNSADDRESS://DNS地址
                                string EDNS = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 16).ToString().Replace("\0", "");
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EDNS_d.Text = EDNS; }));
                                DataStar += 18;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERADDRESS://服务器地址
                                string EServer = AppCmd.ByteToHexStringASC(packData, minorParamIndex, minorParamIndex + 32).ToString();
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EServer.Text = EServer; }));
                                DataStar += 34;
                                break;
                            case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_ETHERNET_SERVERPORT://服务器端口
                                int EPort = AppCmd.ByteToInt(packData, minorParamIndex, minorParamIndex + 2);
                                this.BeginInvoke(new EventHandler(delegate
                                { this.EPort.Text = EPort.ToString(); }));
                                DataStar += 4;
                                break;
                            default:
                                Console.WriteLine("GOT ETHERNET unexpected type:" + EType.ToString());
                                break;
                        }

                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_485_PARAM: //485参数
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_GW_232_PARAM: //232参数
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_SOFTWARE_VERSION: //软件版本
                        string SoftVesion = AppCmd.ByteToHexStringASC(packData, majorParamIndex, majorParamIndex + 6);

                        this.BeginInvoke(new EventHandler(delegate
                        { this.txtSoftVersion.Text = SoftVesion; }));
                        DataStar += 7;
                        break;
                    case (byte)EXTACPRO_BUSINESS_DEF.EXTACPRO_HARDWARE_VERSION: //硬件版本
                        string DeviceVesion = AppCmd.ByteToHexStringASC(packData, majorParamIndex, majorParamIndex + 6);

                        this.BeginInvoke(new EventHandler(delegate
                        { this.txtDeviceVersion.Text = DeviceVesion; }));
                        DataStar += 7;
                        break;
                }
            }
            if (bLastPacket)
            {
                //进度完成
                this.BeginInvoke(new EventHandler(delegate
                {
                    progressBar1.Value = 100;
                    TitlePanel.Visible = false;
                }));
                notify_messageBox("完成！");
            }
        }        
        /// <summary>
        /// 发送命令
        /// </summary>
        private void Send()
        {
            while (true)
            {
                try
                {
                    if (bPermitSendNext)
                    {
                        if (ListSend.Count > 0)
                        {
                            //最简单的实现，将包发送出去，然后删除，1秒后发送下一个包
                            if (ListSend[0].Cmdbyte[2] == 0)
                            {
                                //如果为命令，则等回复，否则不需要等回复可直接发送下个包
                                bPermitSendNext = false;
                            }                            
                            husbHelp.SendData(ListSend[0].Cmdbyte);

                            while (bPermitSendNext == false)
                                Thread.Sleep(1000);
                            ListSend.RemoveAt(0);                     
                        }
                    }                   
                    Thread.Sleep(1);
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("通讯异常，请检查设备连接");
                    LogHelper.WriteException(ex);
                }                 
            }
        }

#if false

        /// <summary>
        /// 接收数据解析
        /// </summary>
        /// <param name="?"></param>
        public void analyze(byte[] data)
        {
            string strData = BitConverter.ToString(data);
            LogHelper.WriteRight("\r\n  接收:" + strData + "\r\n");
            try
            {
                byte[] packData = unpackObject(data);
                if (packData.Length == 0)
                {
                    return;
                }
                int ServerType = AppCmd.ByteToInt(packData, 1, 2);
                int DataPackNo = AppCmd.ByteToInt(packData, 0, 1);
                if (cNo == DataPackNo)
                {
                    return;
                }
                else
                {
                    cNo = DataPackNo; 
                }
               
                switch (ServerType)
                {
                    case 4:///连接回复  
                        //0x00 成功 0x01 失败

                        //连接结果(1)+网关序列号(4)+网关网络号(2)
                        int state = 1; 
                        DeviceNo = AppCmd.ByteToInt16(packData, 3, 7);//网关序列号
                        state = AppCmd.ByteToInt(packData, 2, 3);
                        if (state > 0)
                        {
                            husbHelp.IsOpen = false;
                        }
                        else
                        {
                            husbHelp.IsOpen = true; 
                        }
                        if (ListSend.Count > 0)
                        {
                            SendModel smodel = ListSend.Where(l => l.ServerType == 3).FirstOrDefault();
                            if (smodel.No > 0)
                            {
                                ListSend.Remove(smodel);
                            }
                        }
                      
                        if (CmdType == "readConfig")
                        {                        
                          
                            readConfig();
                        }
                        else if (CmdType == "writeDevice")
                        {
                            write();
                        }
                        //进度完成
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            progressBar1.Value = 40;

                        }));
                        break;
                    case 0:///负载 
                    case 2:///负载 
                           
                            byte[]  ack=new byte[1];
                            ack[0] = packData[0];
                            ListSend.Insert(0, packagObject(ack,-1));

                            int parameterLength = AppCmd.ByteToInt(packData, 7, 8);//参数总长度 
                            int DataStar = 0;//开始位置 
                            int findex = 0;
                            while (DataStar < parameterLength)
                            {
                                findex += 2;
                                if (findex > parameterLength)
                                {
                                    break;
                                }
                                int DataType = AppCmd.ByteToInt(packData, DataStar + 8, DataStar  +9);
                                if (DataType==0)
                                {
                                    break;
                                }
                                switch (DataType)
                                {
                                    case 0X90: //网关ID
                                        DeviceNo= AppCmd.ByteToInt16(packData, DataStar + 9, DataStar + +13);//网关序列号
                                        this.BeginInvoke(new EventHandler(delegate
                                        {
                                            this.ID.Text = DeviceNo.ToString();
                                        }));

                                        DataStar += 5;
                                        break;
                                    case 0X91://网关能够容纳的网络节点的最大数量
                                        int MaxNode = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 11);
                                          this.BeginInvoke(new EventHandler(delegate
                                     {
                                         this.MaxNode.Text = MaxNode.ToString();
                                     }));
                                        DataStar += 3;
                                        break;
                                    case 0X92: //网关记录网络节点数据的最大条数
                                        int MaxNodeData = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 11);
                                        //   Ht["MaxNodeData"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                        this.BeginInvoke(new EventHandler(delegate
                                        {
                                            this.MaxNodeData.Text = MaxNodeData.ToString();
                                        }));
                                        DataStar += 3;
                                        break;
                                    case 0X93: //网关实时数据包缓冲最大时间
                                     //   Ht["DataBuffer"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14);
                                        int DataBuffer = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 11);
                                        this.BeginInvoke(new EventHandler(delegate
                                        { this.DataBuffer.Text = DataBuffer.ToString(); }));
                                        DataStar += 3;
                                        break;
                                    case 0X94: //433网络参数
                                        int pType = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                        switch (pType)
                                        {
                                            case 0x00://网络ID
                                                int NetworkID = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 14);
                                               // Ht["NetworkID"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 16);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.NetworkID.Text = NetworkID.ToString(); }));
                                                DataStar += 6;
                                                break;
                                            case 0x01://网络功率
                                                int NetworkPower = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 11);
                                              //  Ht["NetworkPower"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.NetworkPower.Text = NetworkPower.ToString(); }));
                                                DataStar += 3;
                                                break;
                                            case 0x02://通信信道
                                                int Channel = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 11); 
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.txtChannel.Text = Channel.ToString(); }));
                                                DataStar += 3;
                                                break; 
                                            case 0x03://通信同步码
                                                 int   SynCode= AppCmd.ByteToInt16(packData, DataStar + 10, DataStar + +12);//网关序列号
                                                   this.BeginInvoke(new EventHandler(delegate
                                                     {
                                                         this.txtSynCode.Text = SynCode.ToString();
                                                     }));

                                                    DataStar += 4;
                                                break;
                                        }
                                        break;
                                    case 0X95://GPRS网关功能开关状态
                                        int GPRSState = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                       // Ht["GPRSState"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                     {
                                         this.radGPRSClose_n.Checked = GPRSState == 0 ? true : false;
                                         this.radGPRSOpen_n.Checked = GPRSState == 1 ? true : false;
                                         AppCmd.SetEnabled(tabControl1.TabPages["tabPageGprs"], radGPRSOpen_n.Checked);                                         
                                     }));             
                                        DataStar += 2;
                                        break;
                                    case 0X96://以太网网关功能开关状态 
                                        int EthernetState = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                       // Ht["EthernetState"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                       {
                                           this.RadEClose_n.Checked =  EthernetState == 0 ? true : false;
                                           this.RadEOpen_n.Checked = EthernetState ==  1  ? true : false;
                                           AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], RadEOpen_n.Checked); 
                                       }));
                                        DataStar += 2;
                                        break;
                                    case 0X97: //485网关功能开关状态
                                        int State485=  AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);;
                                       // Ht["485State"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                       {
                                           this.rad485Open_n.Checked = State485 ==  1  ? true : false;
                                           this.rad485Close_n.Checked = State485 ==  0  ? true : false;
                                       }));
                                        DataStar += 2;
                                        break;
                                    case 0X98: //232网关功能开关状态
                                        int State232 = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                      //  Ht["232State"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                        this.BeginInvoke(new EventHandler(delegate
                                         {
                                             this.rad232Open_n.Checked = State232 ==  1 ? true : false;
                                             this.rad232Close_n.Checked = State232 == 0  ? true : false;
                                         }));
                                        DataStar += 2;
                                        break;
                                    case 0X99://GPRS参数
                                        int GPRSType = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                        switch (GPRSType)
                                        {
                                            case 0x00://接入点名
                                                string AccessPoint = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26);
                                              //  Ht["AccessPoint"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                          { this.AccessPoint.Text = AccessPoint; }));
                                                DataStar += 18;
                                                break;
                                            case 0x01://用户名
                                                string User = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26);
                                               // Ht["User"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                          { this.User.Text = User; }));
                                                DataStar += 18;
                                                break;
                                            case 0x02://密码
                                                string Pwd = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26);;
                                              //  Ht["Pwd"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.Pwd.Text = Pwd; }));
                                                DataStar += 18;
                                                break;
                                            case 0x03://DNS地址
                                                string GPRSDNS = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).Replace("\0", "");
                                              //  Ht["GPRSDNS"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSDNS.Text = GPRSDNS; }));
                                                DataStar += 18;
                                                break;
                                            case 0x04://服务器地址
                                                string GPRSServerIP = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 42);
                                               // Ht["GPRSServerIP"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 44);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSServerIP.Text = GPRSServerIP; }));
                                                DataStar += 34;
                                                break;
                                            case 0x05://服务器端口
                                                int GPRSPoert = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 12);
                                               // Ht["GPRSPoert"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.GPRSPoert.Text = GPRSPoert.ToString(); }));
                                                DataStar +=4;
                                                break;
                                        }
                                        break;
                                    case 0X9A: //以太网参数
                                        int EType = AppCmd.ByteToInt(packData, DataStar + 9, DataStar + 10);
                                        switch (EType)
                                        {
                                            case 0x00://MAC地址
                                                string MacV = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).Replace("\0", "");
                                                string Mac = "";
                                                if (MacV.Length > 0)
                                                {
                                                    for (int i = 0; i < 6; i++)
                                                    {
                                                        Mac = Mac + MacV.Substring(i * 2, 2).Trim() + "-";
                                                    }
                                                    Mac = Mac.Substring(0, 17);

                                                }
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.Mac.Text = Mac.ToString(); }));
                                                DataStar += 18;
                                                break;
                                            case 0x01://DHCP开关状态
                                                int DHCP = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 11);
                                               // Ht["DHCP"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 13);
                                                this.BeginInvoke(new EventHandler(delegate
                                                {
                                                    this.DHCPSOpen.Checked = DHCP == 1 ? true : false;
                                                    this.DHCPSClose.Checked = DHCP == 0 ? true : false;
                                                    AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], !DHCPSOpen.Checked, "_d");
                                         }));
                                                DataStar += 3;
                                                break;
                                            case 0x02://Ip
                                                string EIp = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).Replace("\0","");
                                              //  Ht["EIp"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EIp_d.Text = EIp.ToString(); }));
                                                DataStar += 18;
                                                break;
                                            case 0x03://子网掩码
                                                string EMask = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).Replace("\0", "");
                                             //   Ht["EMask"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28);
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EMask_d.Text = EMask.ToString(); }));
                                                DataStar += 18;
                                                break;
                                            case 0x04://默认网关
                                                string EGetWay = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).ToString().Replace("\0", "");
                                                //Ht["EGetWay"] = AppCmd.ByteToHexStringASC(data, DataStar + 12, DataStar + 28).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EGetWay_d.Text = EGetWay.ToString(); }));
                                                DataStar += 18;
                                                break;
                                            case 0x05://DNS地址
                                                string EDNS = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 26).ToString().Replace("\0", "");
                                             //   Ht["EDNS"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 28).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EDNS_d.Text = EDNS; }));
                                                DataStar += 18;
                                                break;
                                            case 0x06://服务器地址
                                                string EServer = AppCmd.ByteToHexStringASC(packData, DataStar + 10, DataStar + 42).ToString();
                                                //Ht["EServer"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 44).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EServer.Text = EServer; }));
                                                DataStar += 34;
                                                break;
                                            case 0x07://服务器端口
                                                int EPort = AppCmd.ByteToInt(packData, DataStar + 10, DataStar + 12);
                                               // Ht["EPort"] = AppCmd.ByteToInt(data, DataStar + 12, DataStar + 14).ToString();
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.EPort.Text = EPort.ToString(); }));
                                                DataStar += 4;
                                                break;
                                        }

                                        break;
                                    case 0X9B: //485参数
                                        break;
                                    case 0X9C: //232参数
                                        break;
                                    case 0X0B: //软件版本
                                          string SoftVesion = AppCmd.ByteToHexStringASC(packData, DataStar + 9, DataStar + 15);
                                               
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.txtSoftVersion.Text = SoftVesion; }));
                                                DataStar += 7;
                                        break;
                                    case 0X0C: //硬件版本
                                        string DeviceVesion = AppCmd.ByteToHexStringASC(packData, DataStar + 9, DataStar + 15);
                                             
                                                this.BeginInvoke(new EventHandler(delegate
                                                { this.txtDeviceVersion.Text = DeviceVesion; }));
                                                DataStar += 7;
                                        break;
                                       

                                }
                            }
                        //进度完成
                        this.BeginInvoke(new EventHandler(delegate
                        {
                            progressBar1.Value = 100;
                            TitlePanel.Visible = false;
                        }));
                        notify_messageBox("完成！");
                        break;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteException(ex);
            }

        }
#endif

        private void SetModelFromKeyValueCollection()
        {
            this.ID.Text = HtKeyValue["ID"].ToString().Trim();
            this.NetworkID.Text = HtKeyValue["NetworkID"].ToString().Trim();
            this.MaxNode.Text = HtKeyValue["MaxNode"].ToString().Trim();
            this.MaxNodeData.Text = HtKeyValue["MaxNodeData"].ToString().Trim();

            gatewayTypeSelectBox.SelectedIndex = -1;
            if (HtKeyValue["EthernetState"].ToString().Trim() == "1")
            {
                gatewayTypeSelectBox.SelectedIndex = 0;
            }
            else if (HtKeyValue["GPRSState"].ToString().Trim() == "1")
            {
                gatewayTypeSelectBox.SelectedIndex = 1;
            }
            else if (HtKeyValue["232State"].ToString().Trim() == "1")
            {
                gatewayTypeSelectBox.SelectedIndex = 2;
            }
            else if (HtKeyValue["485State"].ToString().Trim() == "1")
            {
                gatewayTypeSelectBox.SelectedIndex = 3;
            }
            
            this.Mac.Text = HtKeyValue["Mac"].ToString().Trim();
            this.DHCPSOpen.Checked = HtKeyValue["DHCP"].ToString().Trim() == "1" ? true : false;
            this.DHCPSClose.Checked = HtKeyValue["DHCP"].ToString().Trim() == "0" ? true : false;
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], DHCPSOpen.Checked, "_d");
            if (!this.DHCPSOpen.Checked)
            {
                this.EIp_d.Text = HtKeyValue["EIp"].ToString().Trim();
                this.EMask_d.Text = HtKeyValue["EMask"].ToString().Trim();
                this.EGetWay_d.Text = HtKeyValue["EGetWay"].ToString().Trim();
                this.EDNS_d.Text = HtKeyValue["EDNS"].ToString().Trim();
            }
            this.EServer.Text = HtKeyValue["EServer"].ToString().Trim();
            this.EPort.Text = HtKeyValue["EPort"].ToString().Trim();
            this.AccessPoint.Text = HtKeyValue["AccessPoint"].ToString().Trim();
            this.User.Text = HtKeyValue["User"].ToString().Trim();

            this.Pwd.Text = HtKeyValue["Pwd"].ToString().Trim();
            this.GPRSDNS.Text = HtKeyValue["GPRSDNS"].ToString().Trim();
            this.GPRSServerIP.Text = HtKeyValue["GPRSServerIP"].ToString().Trim();
            this.GPRSPoert.Text = HtKeyValue["GPRSPoert"].ToString().Trim();
            this.Mac.Text = HtKeyValue["Mac"].ToString().Trim();
            //this.DHCP.Text = Ht["DHCP"].ToString().Trim();
            this.EIp_d.Text = HtKeyValue["EIp"].ToString().Trim();
            this.EMask_d.Text = HtKeyValue["EMask"].ToString().Trim();
            this.EGetWay_d.Text = HtKeyValue["EGetWay"].ToString().Trim();
            this.EDNS_d.Text = HtKeyValue["EDNS"].ToString().Trim();
            this.EServer.Text = HtKeyValue["EServer"].ToString().Trim();
            this.EPort.Text = HtKeyValue["EPort"].ToString().Trim();
        }

        /// <summary>
        /// GetModelToKeyValueCollection
        /// </summary>
        private void GetModelToKeyValueCollection()
        {
            HtKeyValue.Clear();

            HtKeyValue["ID"] = this.ID.Text.Trim();
            HtKeyValue["MaxNode"] = this.MaxNode.Text.Trim();
            HtKeyValue["MaxNodeData"] = this.MaxNodeData.Text.Trim();
            HtKeyValue["DataBuffer"] = this.DataBuffer.Text.Trim();

            HtKeyValue["NetworkID"] = this.NetworkID.Text.Trim();
            HtKeyValue["Channel"] = this.txtChannel.Text.Trim();
            HtKeyValue["SynCode"] = this.txtSynCode.Text.Trim();


            HtKeyValue["EthernetState"] = "0";
            HtKeyValue["GPRSState"] = "0";
            HtKeyValue["485State"] = "0";
            HtKeyValue["232State"] = "0";

            switch(this.gatewayTypeSelectBox.SelectedIndex)
            {
                case 0:
                    HtKeyValue["EthernetState"] = "1";
                    break;
                case 1:
                    HtKeyValue["GPRSState"] = "1";
                    break;
                case 2:
                    HtKeyValue["232State"] = "1";
                    break;
                case 3:
                    HtKeyValue["485State"] = "1";
                    break;
            }

            HtKeyValue["NetworkID"] = this.NetworkID.Text.Trim();
            HtKeyValue["NetworkPower"] = this.NetworkPower.Text.Trim();
            HtKeyValue["AccessPoint"] = this.AccessPoint.Text.Trim();
            HtKeyValue["User"] = this.User.Text.Trim();
            HtKeyValue["Pwd"] = this.Pwd.Text.Trim();
            HtKeyValue["GPRSDNS"] = this.GPRSDNS.Text.Trim();
            HtKeyValue["GPRSServerIP"] = this.GPRSServerIP.Text.Trim();
            HtKeyValue["GPRSPoert"] = this.GPRSPoert.Text.Trim();
            HtKeyValue["Mac"] = this.Mac.Text.Trim().Replace("-", "");
            HtKeyValue["EIp"] = this.EIp_d.Text.Trim();
            HtKeyValue["EMask"] = this.EMask_d.Text.Trim();
            HtKeyValue["EGetWay"] = this.EGetWay_d.Text.Trim();
            HtKeyValue["EDNS"] = this.EDNS_d.Text.Trim();
            HtKeyValue["EServer"] = this.EServer.Text.Trim();
            HtKeyValue["EPort"] = this.EPort.Text.Trim();
            HtKeyValue["DHCP"] = this.DHCPSOpen.Checked == true ? "1" : "0";
        }
        
        /// <summary>
        /// 读取设备参数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReadDeviceConfig_Click(object sender, EventArgs e)
        {           
            TitlePanel.Visible = true;
            progressBar1.Value = 0;
            /*每一次读取都重新建立连接，否则有可能由于序列号发生改变导致后续的通信包被拒绝*/
            CmdType = "readConfig";
            connetDevice();        
        }
        
        private void button6_Click(object sender, EventArgs e)
        {
            WriteConfigToDevice();
        }
        /// <summary>
        /// 打开配置文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                string path = this.openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(path))
                {
                    AppCmd cmd = new AppCmd();
                    cmd.ReadConfig(ref HtKeyValue, path);
                    SetModelFromKeyValueCollection();
                    MessageBox.Show("读取配置文件成功");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("读取配置文件失败");

            }
        }
        /// <summary>
        /// 保存配置文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string path = this.saveFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            if (cmd.copy(path))
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败");
            }
        }

        /// <summary>
        /// 启用DHCP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DHCPSOpen_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], !DHCPSOpen.Checked, "_d");
        }
        /// <summary>
        /// 关闭DHCP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DHCPSClose_CheckedChanged(object sender, EventArgs e)
        {
            AppCmd.SetEnabled(tabControl1.TabPages["tabPageEthernet"], !DHCPSOpen.Checked, "_d");
        }

        /// <summary>
        /// 读取配置文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadFileConfig_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        } 


        private void button1_Click(object sender, EventArgs e)
        {
            husbHelp.CloseDevice();
        } 

        /// <summary>
        /// 保存到设备
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownload_Click(object sender, EventArgs e)
        { 
            TitlePanel.Visible = true;
            progressBar1.Value = 0;
            /*每一次读取都重新建立连接，否则有可能由于序列号发生改变导致后续的通信包被拒绝*/
            CmdType = "writeDevice";
            connetDevice();              
        }

        /// <summary>
        /// 保存到文件
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //写入app.config
                GetModelToKeyValueCollection();
                //写入配置文件
                AppCmd cmd = new AppCmd();
                cmd.WriteConfig(HtKeyValue);
                this.saveFileDialog1.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存失败");
                Console.WriteLine("exception:" + ex.ToString());
            }
        } 
        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {         
           
        } 

        private void button3_Click(object sender, EventArgs e)
        {
        } 
        private void button3_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(this.EIp_d.Text);
        } 
        private void ID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                string EQCodel = ID.Text.Trim();
                int starIndex = 0;
                int endIndex = 0;
                starIndex = EQCodel.IndexOf("did=");
                if (starIndex > -1)
                {
                    starIndex += 4;
                    endIndex = EQCodel.Length - 1;
                    this.ID.Text = EQCodel.Substring(starIndex, endIndex - starIndex);
                }
                e.Handled = true;
                this.NetworkID.Focus();
            }
        }
        /// <summary>
        /// 数字验证
        /// </summary>
        /// <param name="e"></param>
        public void MathVilid(KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '\b'))
            {
                e.Handled = true;
                MessageBox.Show("必须是数字"); 
            }
            else
            {
                e.Handled = false;
            }
        }
        /// <summary>
        /// 数字验证
        /// </summary>
        /// <param name="e"></param>
        public void MathVilid2(KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '6') || e.KeyChar == '\b'))
            {
                e.Handled = true;
                MessageBox.Show("必须是0~6数字");
            }
            else
            {
                e.Handled = false;
            }
        }
        private void notify_messageBox(string s)
        {
            //提示写入成功
            this.Invoke(new EventHandler(delegate
            {
                MessageBox.Show(s);
            }));
        }
        private void NetworkID_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid2(e);
        }  
        private void NetworkPower_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        } 
        private void MaxNode_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
        private void DataBuffer_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        } 
        private void MaxNodeData_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        } 
        private void EPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
        private void GPRSPoert_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
    }
}
