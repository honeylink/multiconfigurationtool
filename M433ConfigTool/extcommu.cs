﻿#define EXTACPRO_CRC_ENABLE 		//开关是否使用CRC校验
//#define IMPLEMENT_FULL

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using M433ConfigTool;//for crc compute
using M433ConfigTool.log;


namespace ExtCommuProtocol
{
    using LOCK_TYPE_T = System.Int32;
    using uint16_t = System.UInt16;
    using uint8_t = System.Byte;
    using DEVICE_SERIALNUMBER_TYPE = System.UInt32;

    unsafe class extcommu
    {
        //服务类型与服务处理方法
        public delegate void ExtLinkServiceFunction_t(ExtLinkEntity_t * linkEntity, UInt16 len, byte * p);
        //回调接口：param linkEntity，确认源；len:包长度；p:包指针；success:是否成功
        public delegate void ExtCommuCallback_t(ExtLinkEntity_t * linkEntity, uint16_t len, uint8_t * p, bool success);
        //发送接口，相当于extCommu_send_indication等接口的通用虚接口
        public delegate uint16_t ExtCommuSend_t(ExtLinkEntity_t * linkEntity, uint16_t len, uint8_t * p);

        private int timeId = -1;  //normal timer id
        private byte connectPackageSeq = 0;
        private int tickTid = -1; //tick timer id

        private LOCK_TYPE_T commuLock;//作为访问链路链表、链路实体、ACK表、DUPLICATE 表的锁

        public delegate void entity_tx_t(uint16_t length, uint8_t* pData);	//发送接口
        public delegate void entity_reset_t();//复位接口
        public entity_tx_t entity_tx = null;
        public ExtCommuCallback_t ackProc = null;
        public ExtCommuCallback_t connectProc = null;
        public ExtCommuCallback_t setReplyProc = null;
        public ExtCommuCallback_t getReplyProc = null;
        public ExtCommuCallback_t indicationProc = null;

        private int activeCommunicationProtocolVersion = 0;//初始通信版本为0
        
        /*服务对应的处理接口*/
        private static readonly ExtLinkServiceFunction_t[] ServiceFunctionTable = 
        {
            //编译器提示必须使用静态函数，而静态函数又不好实现
	        null, //extCommu_proc_set,
	        null, //extCommu_proc_get,
	        null, //extCommu_proc_indication,
	        null, //extCommu_proc_connect,
	        null, //extCommu_proc_connectRpy,
	        null, //extCommu_proc_disconnect,
	        null, //extCommu_proc_disconnectRpy,
	        null,//for tick service
	        null,//extCommu_proc_get_reply
        };

        //每种传感类型对应的数值长度
        private static readonly byte[] SensorTypeMapValueLength =
        {
		        0,	//no point
		        4,	//temperature
		        2,	//humidity
		        2,	//CO
		        2 	//CO2
        };
        
        /**
        * 外部链路状态定义
        */
        public enum ExtLinkState_t 
        {
            EXTLINK_STATE_DEAD,			//not work
            EXTLINK_STATE_ACTIVE,		//working
            EXTLINK_STATE_READY,		//link established
            EXTLINK_STATE_LINKING,		//is linking
            EXTLINK_STATE_DISLINKING,	//is dis linking
            EXTLINK_STATE_BUSY			//is communication
        };

        public enum Event
        {
            EXTCOMMU_EVENT_TIMEOUT = 0,			//模块超时
            EXTCOMMU_EVENT_LINKCONNECT,		//连接建立
            EXTCOMMU_EVENT_LINKDISCONNECT,	//连接丢失
            EXTCOMMU_EVENT_SET,				//设置
            EXTCOMMU_EVENT_SET_REPLY,		//设置回复
            EXTCOMMU_EVENT_GET,				//获取
            EXTCOMMU_EVENT_GETREPLY,		//获取回复
            EXTCOMMU_EVENT_TICK_TIMEOUT		//TICK超时
        }
        /******************************************************************
         * 协议定义
         */
        public enum EXTACPRO
        {
            //最高版本定义
            EXTACPRO_VERSION=0x01,
            //包类型定义
            EXTACPRO_TYPE_COMMAND = 0x00,
            EXTACPRO_TYPE_ACK =	0x01,

            //包服务类型定义
            EXTACPRO_SERVICE_SET =	0x00,
            EXTACPRO_SERVICE_GET =	0x01,
            EXTACPRO_SERVICE_INDICATION = 0x02,
            EXTACPRO_SERVICE_CONNECT = 0x03,
            EXTACPRO_SERVICE_CONNECT_REPY =	0x04,
            EXTACPRO_SERVICE_DISCONNECT	= 0x05,
            EXTACPRO_SERVICE_DISCONNECT_REPY = 0x06,
            EXTACPRO_SERVICE_TICK = 0x07,
            EXTACPRO_SERVICE_GET_REPLY= 0x08,

            //连接回复定义
            EXTACPRO_RESULT_OK = 0x00,
            EXTACPRO_RESULT_FAIL =0x01,

            EXTACPRO_PACK_MIN =	6,  //最小包字节
            EXTACPRO_PACK_COMMAND_MIN =	7,  //命令包最小长度字节
            EXTACPRO_AckWaitDuration = 	10, //秒，后续估计需要继续将此时间延长，以避免GPRS信号弱时返回数据很慢的问题
            EXTACPRO_retxCount = 3, //重发次数
            EXTACPRO_TICK_PERIOD =	120,//单位S
        }
        public enum misc
        {
              EXTLINK_ATTRIBUTE_PASSIVE	= 1,//被动连接
              EXTLINK_ATTRIBUTE_ACTIVE = 0//主动连接
        }
        public struct ExtAcPro_t
        {
	        public UInt16 length;
	        public byte controlHeader;
	        public byte count;
	        public byte serviceType;
	        public UInt16 payloadLength;
	        public byte * payload;
	        public UInt16 crc;
        };

        //public delegate void notify_tx_t(EXTCOMMU_msg_t* msg);
        public struct ExtLinkEntity_t
        {	            
	        public byte count;			//包序号
	        public ExtLinkState_t state;	//链路状态
	        public byte continousPacketCountFailed;//连续失败的包的计数，在一个包重发次数结束后，依然没有收到回复即认为一次包失败，
									        //当连续包失败次数大于一定的阈值（例如：3），认为链路失败；任何一个包收到回复即清0
        };

        /*****************************************************************************
         * 函数实现
         */

        /**************************************************************
         * public 函数
         */

        public void init()
        {
            ServiceFunctionTable[(byte)EXTACPRO.EXTACPRO_SERVICE_INDICATION] = extCommu_proc_indication;
            ServiceFunctionTable[(byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT] = extCommu_proc_connect;
	        ServiceFunctionTable[(byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT_REPY] = extCommu_proc_connectRpy;
	        ServiceFunctionTable[(byte)EXTACPRO.EXTACPRO_SERVICE_DISCONNECT] = extCommu_proc_disconnect;
	        ServiceFunctionTable[(byte)EXTACPRO.EXTACPRO_SERVICE_DISCONNECT_REPY] = extCommu_proc_disconnectRpy;
        }
        public void linkEntityInit(ExtLinkEntity_t* linkEntity)
        {
            linkEntity->state = ExtLinkState_t.EXTLINK_STATE_ACTIVE;
            linkEntity->continousPacketCountFailed = 0;
        }

        /**
         * 模块初始化
         */
        void extCommu_init()
        {
#if IMPLEMENT_FULL
	        event_register(EVENT_TARGET_EXT_COMMU, extCommu_procEvt);

	        LOCK_INIT(&commuLock);

	        /*创建ACK管理定时器*/
	        timeId = timer_new(1000, ack_timeOut, 0);
	        if(timeId < 0)
	        {
		        _PANIC("extCommu_init get no timer");
	        }
	        timer_start(timeId);

	        /*创建心跳包管理定时器*/
	        tickTid = timer_new(EXTACPRO_TICK_PERIOD*1000, tick_timeOut, 0);
	        if(tickTid < 0)
	        {
		        _PANIC("extCommu_init get no timer");
	        }
	        timer_start(tickTid);
#endif
        }



        /**
         * 链路变化通知，有链路调用本接口，通知本模块链路状态
         * @param linkEntity 变化链路
         * @param flag true:链路好 false:链路坏
         */
        public void extCommu_linkChange(ExtLinkEntity_t * linkEntity, bool flag)
        {
#if IMPLEMENT_FULL
	        if(flag)
	        {
		        linkEntity->state = EXTLINK_STATE_ACTIVE;

	        }
	        else
	        {
		        //此处加锁在本模块调用gprs_reset时有可能造成死锁
		        //链路失败，将包管理复原
		        extCommu_resetPacketMgr();

		        if(linkEntity->state == EXTLINK_STATE_READY)
		        {
			        linkEntity->state = EXTLINK_STATE_DEAD;//这里为了规避此消息接收者具有更高优先级并访问此link状态
			        EXT_COMMU_PUTMESSAGE(EVENT_TARGET_APP, EXTCOMMU_EVENT_LINKDISCONNECT, linkEntity);
		        }
		        linkEntity->state = EXTLINK_STATE_DEAD;
	        }
#endif
        }

        /**
         * 接收来自链路的数据
         * @param linkEntity 数据来源链路
         * @param length 负载长度
         * @param pData 负载指针
         */
        public void extCommu_in(ExtLinkEntity_t * linkEntity, UInt16 length, byte * pData)
        {
        #if TRACE_EXTCOMMU_ON
	        {
		        Console.WriteLine("extCommu_in(len:%d):", length);
		        for(UInt16 i = 0; i < length; i++)
			        TRACE_("%x, ", pData[i]);
		        TRACE_("\r\n");
	        }
        #endif

	        ExtAcPro_t acPro;
	        while(true)
	        {
		        //!@note 考虑到输入包有可能含有多个包，所以，分解多个包进行解析
		        if(length < (int)EXTACPRO.EXTACPRO_PACK_MIN)
                {                    
                    LogHelper.WriteRight("length smaller packet minimum\r\n");
                    break;
                }
			        
		        UInt16 packetLen = (UInt16)(pData[0] + ((UInt16)pData[1] << 8) + 2 + 2);
		        if(length < packetLen)
                {
                    LogHelper.WriteRight("extCommu_in packet len error\r\n");
                    break;
                }
		        if(extCommu_depack(&acPro, packetLen, pData) == false)
		        {
                    LogHelper.WriteRight("extCommu_depack error\r\n");
			        pData += packetLen;
			        length -= packetLen;
			        continue;
		        }
		        pData += packetLen;
		        length -= packetLen;

		        switch(EXTACPRO_CONTROLHEADER_TO_PACKTYPE(acPro.controlHeader))
		        {
			        case (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND:
                        LogHelper.WriteRight("EXTACPRO_TYPE_COMMAND\r\n");
                        {
				        //根据特定的包进行ACK回复
				        {
					        switch(acPro.serviceType)
					        {
						        case (byte)EXTACPRO.EXTACPRO_SERVICE_SET:
						        case (byte)EXTACPRO.EXTACPRO_SERVICE_GET:
						        case (byte)EXTACPRO.EXTACPRO_SERVICE_INDICATION:
						        case (byte)EXTACPRO.EXTACPRO_SERVICE_TICK:
						        case (byte)EXTACPRO.EXTACPRO_SERVICE_GET_REPLY:
							        {
								        Console.WriteLine("send ack\r\n");
								        //发送确认包
                                        byte[] ackProBufferX = new byte[(byte)EXTACPRO.EXTACPRO_PACK_MIN]; 
								        fixed (byte* ackProBuffer = ackProBufferX)
                                        {
                                            UInt16 len = extCommu_pack(ackProBuffer,
                                                (byte)EXTACPRO.EXTACPRO_TYPE_ACK,
                                                acPro.count,
                                                0,
                                                0,
                                                null);
                                                if(len != 0)
                                                entity_tx(len, ackProBuffer);
                                        }
					
							        }
							        break;
					        }

				        }
				        //处理包
				        {
                            #if IMPLEMENT_FULL
					        LOCK(&commuLock);
					        if(acPro.serviceType == (byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT_REPY)
					        {//由于connect包目前没有加入ACK包机制，则必须在收到connect rpy包后，删除对应的包
						        AckWait_t * p;
						        ACK_WAIT_FIND(linkEntity, connectPackageSeq, p);
						        if(p)
						        {
							        free(p->pPack);
							        int8_t result;
							        ACK_WAIT_DEL(p, result);
							        if(result != 0)
								        _PANIC("del connect package failed\r\n");
						        }
					        }

					        DuplicateRejection_t * p;
					        DUPLICATE_REJECTION_FIND(linkEntity, acPro.count, p);
					        if(p)
					        {
						        Console.WriteLine("duplicate reject\r\n");
					        }
					        else

					        {
						        //在包类型为CONNECT时，不需要添加进重复拒绝表，即允许重复CONNECT
						        if(acPro.serviceType != (byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT)
							        DUPLICATE_REJECTION_ADD(linkEntity, acPro.count);
                            #endif
						        if(ServiceFunctionTable[acPro.serviceType] != null)
							        ServiceFunctionTable[acPro.serviceType](linkEntity, acPro.payloadLength, acPro.payload);
                            #if IMPLEMENT_FULL
					        }
					        UNLOCK(&commuLock);
                            #endif
				        }
			        }
			        break;
			        case (byte)EXTACPRO.EXTACPRO_TYPE_ACK:
                        LogHelper.WriteRight("EXTACPRO_TYPE_ACK\r\n");
                    {
                        #if IMPLEMENT_FULL
				        //包类型为ACK包，对包等待表进行管理
				        LOCK(&commuLock);
				        AckWait_t * p;
				        ACK_WAIT_FIND(linkEntity, acPro.count, p);
				        if(p)
				        {
					        extCommu_delPackInAckWaitTable(p, true);
				        }
				        linkEntity->continousPacketCountFailed = 0;
				        UNLOCK(&commuLock);
                        #else
                        if (ackProc != null)
                            ackProc(null, 0, null, true);
                        #endif

			        }
			        break;
		        }
	        }
        }
        /**
        * 发送get
        * @param linkEntity 数据链路
        * @param length 负载长度
        * @param pGet 负载指针
        * @return UInt16 send get data length
        */
        public UInt16 extCommu_send_set(ExtLinkEntity_t* linkEntity, UInt16 length, byte* pSet,
                ExtCommuCallback_t func)
        {
            if (linkEntity == null || linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
                return 0;
            return extCommu_send_cmd(linkEntity, (byte)EXTACPRO.EXTACPRO_SERVICE_SET, length, pSet, func);
        }

        /**
         * 发送get
         * @param linkEntity 数据链路
         * @param length 负载长度
         * @param pGet 负载指针
         * @return UInt16 send get data length
         */
        public UInt16 extCommu_send_get(ExtLinkEntity_t * linkEntity, UInt16 length, byte * pGet,
		        ExtCommuCallback_t func)
        {
	        if(linkEntity == null || linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
		        return 0;
	        return extCommu_send_cmd(linkEntity, (byte)EXTACPRO.EXTACPRO_SERVICE_GET, length, pGet, func);
        }
        /**
         * 发送getReply
         * @param linkEntity 数据链路
         * @param getReplyLength 负载长度
         * @param pGetReply 负载指针
         * @return UInt16 send getReplyLength data length
         */
        UInt16 extCommu_send_getReply(ExtLinkEntity_t * linkEntity, UInt16 getReplyLength, byte * pGetReply,
		        ExtCommuCallback_t func)
        {
	        if(linkEntity == null || linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
		        return 0;
	        return extCommu_send_cmd(linkEntity, (byte)EXTACPRO.EXTACPRO_SERVICE_GET_REPLY, getReplyLength, pGetReply, func);
        }   


        /********************************************************************
         * 局部函数
         *
         */

        /**
         * 处理事件
         * @param evt 消息，在本实现中指针值即为事件本身
         * @return byte no meaning
         */
#if xx
        byte extCommu_procEvt(void * evt)
        {
	        switch(((AppMsg_t*)evt)->event)
	        {
		        case (byte)EXTACPRO.EXTCOMMU_EVENT_TIMEOUT:
		        {
			        LOCK(&commuLock);
			        byte position = (ackWaitNextPos - ackWaitNum + MinWaitAckTableSize)%MinWaitAckTableSize;
			        AckWait_t * pAckWait = null;
			        for(byte i = 0; i < ackWaitNum; i++)
			        {
				        pAckWait = &ackWaitTable[position];
				        if(pAckWait->sToTimeOut == 0)
				        {
					        ExtLinkEntity_t * linkEntity = pAckWait->pFlag;
					        if(pAckWait->triesLeft)
					        {
						        //进行数据重发
						        UInt16 ret =
								        linkEntity->entity_tx(pAckWait->packLength, pAckWait->pPack);
						        if(ret < pAckWait->packLength)
						        {
							        //!@todo根据返回的错误状态进行区别处理
							        Console.WriteLine("reTx failed, ret:%d\r\n", ret);
						        }
						        pAckWait->triesLeft--;
						        pAckWait->sToTimeOut = EXTACPRO_AckWaitDuration;

					        }
					        else
					        {
						        //重发次数结束，需要处理失败
						        byte packetToDelCount = pAckWait->count;
						        Console.WriteLine("packet transmit failed, seq count:%d\r\n", packetToDelCount);
						        extCommu_delPackInAckWaitTable(pAckWait, false);
						        if(++linkEntity->continousPacketCountFailed >= 3	//连续包失败次数超过阈值
								        || packetToDelCount == connectPackageSeq)	//删除的是连接包
						        {
							        //通知上层链路失败
							        if(linkEntity->state == EXTLINK_STATE_READY)//通知上层
								        EXT_COMMU_PUTMESSAGE(EVENT_TARGET_APP, EXTCOMMU_EVENT_LINKDISCONNECT, linkEntity);

							        //不管上述删除的是普通数据包还是connect包，都已经删除
							        /*复位包管理*/
							        extCommu_resetPacketMgr();

							        linkEntity->count = 0;
							        linkEntity->state = EXTLINK_STATE_DEAD;
							        linkEntity->continousPacketCountFailed = 0;
							        //链路复位
							        linkEntity->entity_reset();
						        }
					        }
				        }
				        position = (position+1)%MinWaitAckTableSize;
			        }
			        UNLOCK(&commuLock);
		        }
		        break;
		        case EXTCOMMU_EVENT_TICK_TIMEOUT:
		        {
			        //给每个链接发送tick包
			        byte entityListSize = List_size(entityList);
			        for(byte i = 0; i < entityListSize; i++)
			        {
				        ExtLinkEntity_t * link = List_get(entityList, i);
				        if(link->attribute.bit8.passive == EXTLINK_ATTRIBUTE_ACTIVE)
					        extCommu_send_tick(link);
			        }
		        }
		        break;
	        }
	        return 0;
        }
#endif
        /**
         * 解包
         * @param extAcPro 存储包的解析结果
         * @param length 包长度
         * @param pData 包指针
         * @return 解析结果
         */
        public bool extCommu_depack(ExtAcPro_t * extAcPro , UInt16 length, byte * pData)
        {
	        if(extAcPro == null || length == 0|| pData == null)
		        return false;
	        if(length < (byte)EXTACPRO.EXTACPRO_PACK_MIN)
		        return false;
        #if EXTACPRO_CRC_ENABLE
	        UInt16 crc = (UInt16)(pData[length-2] + ((UInt16)pData[length-1] << 8));
            if (crc != crcCompute((UInt16)(length - 2), pData))
	        {
		        //CRC校验错误
		        Console.WriteLine("extCommu_depack crc error\r\n");
		        return false;
	        }
        #endif

            UInt16 offset = (UInt16)((UInt32)(&extAcPro->serviceType) -(UInt32)extAcPro);
	        memcpy((byte*)extAcPro, pData, offset);//复制固有包头
	        length -= offset;
	        pData += offset;
	        if(length <= 0)
		        return false;
            activeCommunicationProtocolVersion = EXTACPRO_CONTROLHEADER_TO_VERSION(extAcPro->controlHeader);
	        if(EXTACPRO_CONTROLHEADER_TO_PACKTYPE(extAcPro->controlHeader) == (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND)
	        {
		        extAcPro->serviceType = *pData++;
		        length--;
		        if(length < 2)
			        return false;
		        if(length > 2)
		        {
			        //extAcPro->payloadLength = pData[0] + 0xFF * (UInt16)pData[1];
			        extAcPro->payloadLength = (UInt16)(length-2);
			        extAcPro->payload = pData;
		        }
	        }
            return true;
        }

        /**
         * 打包；
         * 自动加入CRC校验
         * @param buffer	包存储结果buffer
         * @param packType
         * @param count
         * @param serviceType
         * @param payloadLength
         * @param payload
         * @return 结果包长度
         */
        public UInt16 extCommu_pack(byte * buffer, byte packType, byte count, byte serviceType, UInt16 payloadLength, byte * payload)
        {
	        UInt16 len = (byte)EXTACPRO.EXTACPRO_PACK_MIN;
	        byte * pBufferHeader = buffer;
	        if(packType == (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND)
	        {
		        len++;
		        len += payloadLength;
	        }

	        *buffer++ = (byte)((len-4) & 0x00FF);//len-数据长度字段2-CRC长度2
	        *buffer++ = (byte)(((len-4) & 0xFF00)>>8);
	        *buffer++ = EXTACPRO_CONTROLHEADER_CONSTRUCT(packType);
	        *buffer++ = count;

	        if(packType == (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND)
	        {
		        *buffer++ = serviceType;
		        memcpy(buffer, payload, payloadLength);
		        buffer += payloadLength;
	        }
        #if EXTACPRO_CRC_ENABLE
	        UInt16 crc = crcCompute((uint16_t)(len-2), pBufferHeader);
	        *buffer++ = (byte)(crc & 0x00FF);
	        *buffer++ = (byte)((crc & 0xFF00)>>8);
        #else
	        *buffer++ = 0;
	        *buffer++ = 0;
        #endif
            return len;
        }

        /**
         * 发送connect
         * 同样加入ACK表。收到connect reply后删除此表项。
         * @param linkEntity 数据链路
         */
        public void extCommu_send_connect(ExtLinkEntity_t * linkEntity)
        {
	        if(linkEntity == null)
		        return;
        
	        //byte[] payload = new byte[sizeof(DEVICE_SERIALNUMBER_TYPE)+sizeof(UInt16)];
            //fixed(uint8_t * pPayload = payload)
            {
                extCommu_send_cmd(linkEntity, (byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT, 0, null, null);
            }	        
        }

        /**
         * 发送tick
         * @param linkEntity 数据链路
         */
        private void extCommu_send_tick(ExtLinkEntity_t * linkEntity)
        {
	        if(linkEntity == null|| linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
		        return;
	        extCommu_send_cmd(linkEntity, (byte)EXTACPRO.EXTACPRO_SERVICE_TICK, 0, null, null);
        }
        /**
         * 发送command
         * @param linkEntity 数据链路
         * @param cmd 命令
         * @param length 负载长度
         * @param pCmd 负载指针
         * @return UInt16 send indicationLength data length
         */
        private UInt16 extCommu_send_cmd(ExtLinkEntity_t * linkEntity, byte cmd, UInt16 length, byte * pCmd,
		        ExtCommuCallback_t func)
        {
#if IMPLEMENT_FULL
	        byte * ackProBuffer = malloc((byte)EXTACPRO.EXTACPRO_PACK_COMMAND_MIN+length);
	        if(!ackProBuffer)
	        {
		        _PANIC("extCommu_send_indication allocate error\r\n");
		        return 0;
	        }

	        LOCK(&commuLock);
	        byte count = linkEntity->count;
	        UInt16 len = extCommu_pack(ackProBuffer,
			        (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND,
			        count,
			        cmd,
			        length,
			        pCmd);
	        linkEntity->count++;
	        UNLOCK(&commuLock);

	        if(len)
	        {
		        //只有在表未满情况下允许发送和加入ACK表，防止表进行溢出替换而导致动态内存泄露
		        LOCK(&commuLock);
		        if(!ACK_WAIT_FULL())
		        {
			        if(cmd == EXTACPRO_SERVICE_CONNECT)
				        connectPackageSeq = count;
			        linkEntity->entity_tx(len, ackProBuffer);
			        ACK_WAIT_ADD(linkEntity, count, EXTACPRO_retxCount, EXTACPRO_AckWaitDuration, len, ackProBuffer, (void*)func);
			        UNLOCK(&commuLock);
		        }
		        else
		        {
			        Console.WriteLine("ACK WAIT TABLE IS FULL, ABORT THE PACKET\r\n");

			        UNLOCK(&commuLock);
			        free(ackProBuffer);
			        return 0;
		        }
	        }
	        else
	        {
		        free(ackProBuffer);
		        return 0;
	        }
#else
            byte[] ackProBufferT = new byte[((byte)EXTACPRO.EXTACPRO_PACK_COMMAND_MIN+length)];
            fixed(uint8_t *ackProBuffer = ackProBufferT)
            {
	            byte count = linkEntity->count;
	            UInt16 len = extCommu_pack(ackProBuffer,
			            (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND,
			            count,
			            cmd,
			            length,
			            pCmd);
	            linkEntity->count++;

	            if(len != 0)
	            {	            
			        if(cmd == (byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT)
				        connectPackageSeq = count;
			        entity_tx(len, ackProBuffer);		        
	            }
	            else
	            {
		            return 0;
	            }
            }
#endif
	        return length;
        }

        /**
         * 处理connect
         * @param linkEntity 数据来源链路
         * @param connLength 负载长度
         * @param pConnect 负载指针
         * @note 由于是外部访问者主动连接本模块，所以本模块将不主动发送通知；
         *       外部访问者全程具有主动权。
         */
        private void extCommu_proc_connect(ExtLinkEntity_t* linkEntity, UInt16 connLength, byte* pConnect)
        {
	        Console.WriteLine("extCommu_proc_connect\r\n");

	        linkEntity->state = ExtLinkState_t.EXTLINK_STATE_READY;

	        byte[] buffer = new byte[(byte)EXTACPRO.EXTACPRO_PACK_MIN + 1
			        + 1
			        +sizeof(DEVICE_SERIALNUMBER_TYPE)
			        +sizeof(UInt16)];

	        byte[] payload = new byte[1+sizeof(DEVICE_SERIALNUMBER_TYPE)+sizeof(UInt16)];
	        payload[0] = (byte)EXTACPRO.EXTACPRO_RESULT_OK;//连接结果

            fixed(uint8_t * pBuffer = buffer, pPayload = payload)
            {
	            UInt16 len = extCommu_pack(pBuffer,
			            (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND,
			            linkEntity->count,
			            (byte)EXTACPRO.EXTACPRO_SERVICE_CONNECT_REPY,
			            (uint16_t)payload.Length,
			            pPayload);
	            linkEntity->count++;

	            if(len != 0)
		            entity_tx(len, pBuffer);
            }

        }


        /**
         * 处理connect reply
         * 置连接标志；
         * 删除等待项；
         * @param linkEntity 数据来源链路
         * @param connectRpyLength 负载长度
         * @param pConnectRpy 负载指针
         */
        private void extCommu_proc_connectRpy(ExtLinkEntity_t* linkEntity, UInt16 connectRpyLength, byte* pConnectRpy)
        {
	        Console.WriteLine("extCommu_proc_connectRpy\r\n");
	        if(linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
	        {
		        linkEntity->state = ExtLinkState_t.EXTLINK_STATE_READY;
		       // EXT_COMMU_PUTMESSAGE(EVENT_TARGET_APP, (byte)EXTACPRO.EXTCOMMU_EVENT_LINKCONNECT, linkEntity);
                if (connectProc != null)
                    connectProc(linkEntity, connectRpyLength, pConnectRpy, true);
	        }	   
        }

        /**
         * 处理disconnect
         * @param linkEntity 数据来源链路
         * @param disconnLength 负载长度
         * @param pDisconnect 负载指针
         */
        private void extCommu_proc_disconnect(ExtLinkEntity_t* linkEntity, UInt16 disconnLength, byte* pDisconnect)
        {
	        Console.WriteLine("extCommu_proc_disconnect\r\n");
	        linkEntity->state =  ExtLinkState_t.EXTLINK_STATE_DEAD;

	        byte result = (byte)EXTACPRO.EXTACPRO_RESULT_OK;

	        byte[] ackProBuffer = new byte[(byte)EXTACPRO.EXTACPRO_PACK_MIN + 4];

            fixed(uint8_t * pAckProBuffer = ackProBuffer)
            {
	            UInt16 len = extCommu_pack(pAckProBuffer,
			            (byte)EXTACPRO.EXTACPRO_TYPE_COMMAND,
			            linkEntity->count,
			            (byte)EXTACPRO.EXTACPRO_SERVICE_DISCONNECT_REPY,
			            1,
			            &result);
	            linkEntity->count++;

	            if(len != 0)
		            entity_tx(len, pAckProBuffer);
            }
        }

        /**
         * 处理disconnect reply
         * @param linkEntity 数据来源链路
         * @param disconnectRpyLength 负载长度
         * @param pDisconnectRpy 负载指针
         */
        private void extCommu_proc_disconnectRpy(ExtLinkEntity_t* linkEntity, UInt16 disconnectRpyLength, byte* pDisconnectRpy)
        {
	        Console.WriteLine("extCommu_proc_disconnectRpy\r\n");
	        linkEntity->state = ExtLinkState_t.EXTLINK_STATE_DEAD;
        }

        #if (EXTACPRO_ROLE == EXTACPRO_HOST)
        /**
         * 处理indication
         * @param linkEntity 数据来源链路
         * @param indicationLength 负载长度
         * @param pIndication 负载指针
         */
        private void extCommu_proc_indication(ExtLinkEntity_t * linkEntity, UInt16 indicationLength, byte * pIndication)
        {
	        Console.WriteLine("extCommu_proc_indication\r\n");
	        if(linkEntity->state != ExtLinkState_t.EXTLINK_STATE_READY)
		        return;
	        if(indicationLength == 0 || pIndication == null)
		        return;
            if (indicationProc != null)
                indicationProc(linkEntity, indicationLength, pIndication, true);
            /*
	        //形成消息抛到消息队列中
	        while(deviceNumber-- != 0)
	        {
		        EXTCOMMU_devParam_t * pDev = (EXTCOMMU_devParam_t *)pSet;
		        len = pDev->len + sizeof(pDev->deviceId) + sizeof(pDev->len);
		        EXTCOMMU_msg_t * pMsg =
				        malloc(offsetof(EXTCOMMU_msg_t, deviceId) + len);
		        if(!pMsg)
			        _PANIC("extCommu_proc_set allocate message error\r\n");
		        pMsg->src = linkEntity;
		        pMsg->command = (byte)EXTACPRO.EXTACPRO_SERVICE_SET;
		        memcpy((byte*)(pDev)+sizeof(pMsg->command), pSet, len);

		        EXTCOMMU_PUTMESSAGE(pMsg);

		        pSet += len;
	        }*/
        }
        #endif
#if IMPLEMENT_FULL
        /**
         * @brief 业务层处理完毕EXTCOMMU_msg_t后回调本接口
         */
        void extComm_comfirmExtCommuMsg(EXTCOMMU_msg_t * pMsg)
        {
	        //由于是动态分配的，此处必须进行释放
	        free(pMsg);
        }

        /**
         * 对链表中的数据进行比对，寻找节点
         * @param data 链表项中带的数据指针
         * @param tag 比较参数
         * @retval true 比较为真
         * @retval false 比较为假
         */
        private bool extCommu_find_linkEntity(void * data, unsigned long tag)
        {
	        if( data == (void*)tag )
		        return true;
	        else
		        return false;
        }

        /**
         * @brief 复位包管理
         * 由调用此接口的函数加锁
         */
        private void extCommu_resetPacketMgr()
        {
	        DUPLICATE_REJECTION_DEL_ALL();

	        //因为知道表就是一个从第一项到最后一项的数据，所以从最后一项一直删除到第一项
	        byte lastPoistion;
	        while(ackWaitNum > 0)
	        {
		        lastPoistion = ackWaitNum-1;
		        extCommu_delPackInAckWaitTable(&ackWaitTable[lastPoistion], false);
	        }
        }

        /**
         * @breif ACK超时处理
         *   多个ACK定时管理方案：
         *   使用一个统一定时器，定时间隔为1秒，定时到了之后，轮询整个ACK等待表；
         * 	    减小ACK等待表中的时间，到0则超时；
         * @param tag 定时器参数
         * @param tid 定时器ID
         * @return int 没有意义
         */
        private int ack_timeOut(unsigned long tag, int tid)
        {
	        LOCK(&commuLock);
	        bool timeOut = false;
	        //轮询ACK表，将超时标识置位，并告知主线程处理
	        byte position = (ackWaitNextPos - ackWaitNum + MinWaitAckTableSize)%MinWaitAckTableSize;

	        for(byte i = 0; i < ackWaitNum; i++)
	        {
		        if(ackWaitTable[position].sToTimeOut > 0)
		        {
			        ackWaitTable[position].sToTimeOut--;
			        if(ackWaitTable[position].sToTimeOut == 0)
				        timeOut = true;
		        }
		        position = (position+1)%MinWaitAckTableSize;
	        }
	        if(timeOut == true)
	        {
		        //通知处理
                byte state = 0;
		        EXT_COMMU_PUTMESSAGE(EVENT_TARGET_EXT_COMMU, EXTCOMMU_EVENT_TIMEOUT, state);//state is no meaning
	        }
	        UNLOCK(&commuLock);
	        return -1;
        }

        /**
         * @brief 心跳超时处理
         * @param tag 定时器参数
         * @param tid 定时器ID
         * @return int 没有意义
         */
        private int tick_timeOut(unsigned long tag, int tid)
        {
	        byte state = 0;
	        EXT_COMMU_PUTMESSAGE(EVENT_TARGET_EXT_COMMU, EXTCOMMU_EVENT_TICK_TIMEOUT, state);
	        return -1;
        }

        /**
         * @brief 删除ACK等待表中的指定包
         * @param pAckWait 包指针，位于AckWait表中
         * @param success
         * 			true 表示此包时收到了对应的ACK
         * 			false 表示此包没有收到对应的ACK
         */
        private void extCommu_delPackInAckWaitTable(AckWait_t * pAckWait, bool success)
        {
	        //ACK包中是含有动态内存的，如果直接将包删除内存泄露，所以需要free pack
	        if(pAckWait->callBack)
	        {
		        ExtAcPro_t pro;
		        if(extCommu_depack(&pro , pAckWait->packLength, pAckWait->pPack)
				        && pro.serviceType == (byte)EXTACPRO.EXTACPRO_SERVICE_INDICATION)
		        {
			        ((ExtCommuCallback_t)pAckWait->callBack)(pAckWait->pFlag, pro.payloadLength, pro.payload, success);
		        }
	        }
	        free(pAckWait->pPack);
	        int8_t result;
	        ACK_WAIT_DEL(pAckWait, result);
	        if(result != 0)
	        {
		        _PANIC("error when ack wait table del:%d", result);
	        }
        }
#endif
 

        #if EXTACPRO_CRC_ENABLE
        /**
         * 计算CRC
         * @param len 要计算的数据长度
         * @param p 要计算的数据指针
         * @return 计算出的结果
         */
        private UInt16 crcCompute(UInt16 len, byte * p)
        {
            byte[] data = bytePointerConvertToByteArray(len, p);

            int[] crc = AppCmd.GetCheckData(data, 0, len);
            return (UInt16)(crc[0] + (crc[1] << 8));
	        //return crc16_calc_data_alter(p, len);
        }
        #endif
        public void LOCK(LOCK_TYPE_T* x)
        {
        }
        public void UNLOCK(LOCK_TYPE_T* x)
        {

        }
        //byte pointer memcpy
        public void memcpy(byte* dst, byte* src, UInt32 len)
        {
            for (UInt32 i = 0; i < len; i++)
            {
                *dst = *src;
                dst += 1;
                src += 1;
            }
        }
        public static byte[] bytePointerConvertToByteArray(UInt16 len, byte* p)
        {
            byte[] data = new byte[len];
            for (UInt16 i = 0; i < len; i++)
            {
                data[i] = p[i];
            }
            return data;
        }

        private byte EXTACPRO_CONTROLHEADER_CONSTRUCT(byte packType)
        {
            return (byte)((((byte)activeCommunicationProtocolVersion) << 4) | packType);
        }
        private byte EXTACPRO_CONTROLHEADER_TO_PACKTYPE(byte h)
        {
            return (byte)(h & 0x0F);
        }
        private byte EXTACPRO_CONTROLHEADER_TO_VERSION(byte h)
        {
            return (byte)((h & 0xF0) >> 4);
        }
    }
}
