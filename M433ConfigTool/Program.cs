﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using M433ConfigTool.log;


using System.Runtime.InteropServices;

namespace M433ConfigTool
{
    static class Program
    {
         [DllImport("kernel32.dll")]
         static extern bool FreeConsole();
         [DllImport("kernel32.dll")]
         public static extern bool AllocConsole();

        static Thread th = null;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>

        [STAThread]
        static void Main()
        {
            //AllocConsole();//调用系统API，调用控制台窗口
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException); 
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            //AllocConsole();//调用系统API，调用控制台窗口
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            string s = e.Exception.ToString();
            LogHelper.WriteException(e.Exception);            
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception s1 = e.ExceptionObject as Exception;
            LogHelper.WriteException(s1);
            
        }
    }
}
