﻿namespace M433ConfigTool
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReadDevice = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panelConfig = new System.Windows.Forms.Panel();
            this.btnReadFileConfig = new System.Windows.Forms.Button();
            this.BtnReadDeviceConfig = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageBase = new System.Windows.Forms.TabPage();
            this.BaseParamFactoryPanel = new System.Windows.Forms.Panel();
            this.MaxNodeData = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.DataBuffer = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.TitlePanel = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.TitleMsg = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.gatewayTypeSelectBox = new System.Windows.Forms.ComboBox();
            this.txtSoftVersion = new System.Windows.Forms.TextBox();
            this.txtDeviceVersion = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.TextBox();
            this.LabGetWayNo = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPageNet = new System.Windows.Forms.TabPage();
            this.NetworkID = new System.Windows.Forms.TextBox();
            this.NetworkPower = new System.Windows.Forms.TextBox();
            this.MaxNode = new System.Windows.Forms.TextBox();
            this.txtChannel = new System.Windows.Forms.TextBox();
            this.txtSynCode = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageEthernet = new System.Windows.Forms.TabPage();
            this.EthernetFactoryPanel = new System.Windows.Forms.Panel();
            this.Mac = new IpBoxControl.MACTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.EDNS_d = new IpBoxControl.IpTextBox();
            this.EGetWay_d = new IpBoxControl.IpTextBox();
            this.EMask_d = new IpBoxControl.IpTextBox();
            this.EIp_d = new IpBoxControl.IpTextBox();
            this.DHCPSClose = new System.Windows.Forms.RadioButton();
            this.DHCPSOpen = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.EPort = new System.Windows.Forms.TextBox();
            this.EServer = new System.Windows.Forms.TextBox();
            this.tabPageGprs = new System.Windows.Forms.TabPage();
            this.GPRSDNS = new IpBoxControl.IpTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.AccessPoint = new System.Windows.Forms.TextBox();
            this.User = new System.Windows.Forms.TextBox();
            this.Pwd = new System.Windows.Forms.TextBox();
            this.GPRSPoert = new System.Windows.Forms.TextBox();
            this.GPRSServerIP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panelConfig.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageBase.SuspendLayout();
            this.BaseParamFactoryPanel.SuspendLayout();
            this.TitlePanel.SuspendLayout();
            this.tabPageNet.SuspendLayout();
            this.tabPageEthernet.SuspendLayout();
            this.EthernetFactoryPanel.SuspendLayout();
            this.tabPageGprs.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnReadDevice
            // 
            this.btnReadDevice.Location = new System.Drawing.Point(0, 0);
            this.btnReadDevice.Name = "btnReadDevice";
            this.btnReadDevice.Size = new System.Drawing.Size(75, 23);
            this.btnReadDevice.TabIndex = 33;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 32;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 31;
            // 
            // panelConfig
            // 
            this.panelConfig.Controls.Add(this.btnReadFileConfig);
            this.panelConfig.Controls.Add(this.BtnReadDeviceConfig);
            this.panelConfig.Controls.Add(this.btnDownload);
            this.panelConfig.Controls.Add(this.btnSave);
            this.panelConfig.Controls.Add(this.tabControl1);
            this.panelConfig.Location = new System.Drawing.Point(0, 3);
            this.panelConfig.Name = "panelConfig";
            this.panelConfig.Size = new System.Drawing.Size(716, 482);
            this.panelConfig.TabIndex = 27;
            // 
            // btnReadFileConfig
            // 
            this.btnReadFileConfig.Location = new System.Drawing.Point(523, 453);
            this.btnReadFileConfig.Name = "btnReadFileConfig";
            this.btnReadFileConfig.Size = new System.Drawing.Size(101, 23);
            this.btnReadFileConfig.TabIndex = 5;
            this.btnReadFileConfig.Text = "读取文件";
            this.btnReadFileConfig.UseVisualStyleBackColor = true;
            this.btnReadFileConfig.Visible = false;
            this.btnReadFileConfig.Click += new System.EventHandler(this.btnReadFileConfig_Click);
            // 
            // BtnReadDeviceConfig
            // 
            this.BtnReadDeviceConfig.BackColor = System.Drawing.Color.DodgerBlue;
            this.BtnReadDeviceConfig.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BtnReadDeviceConfig.Location = new System.Drawing.Point(19, 453);
            this.BtnReadDeviceConfig.Name = "BtnReadDeviceConfig";
            this.BtnReadDeviceConfig.Size = new System.Drawing.Size(96, 29);
            this.BtnReadDeviceConfig.TabIndex = 4;
            this.BtnReadDeviceConfig.Text = "读取设备";
            this.BtnReadDeviceConfig.UseVisualStyleBackColor = false;
            this.BtnReadDeviceConfig.Click += new System.EventHandler(this.BtnReadDeviceConfig_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.Color.MediumOrchid;
            this.btnDownload.ForeColor = System.Drawing.Color.White;
            this.btnDownload.Location = new System.Drawing.Point(125, 453);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(84, 29);
            this.btnDownload.TabIndex = 3;
            this.btnDownload.Text = "保存到设备";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnSave
            // 
            this.btnSave.CausesValidation = false;
            this.btnSave.Location = new System.Drawing.Point(630, 453);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "保存到文件";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageBase);
            this.tabControl1.Controls.Add(this.tabPageNet);
            this.tabControl1.Controls.Add(this.tabPageEthernet);
            this.tabControl1.Controls.Add(this.tabPageGprs);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(708, 448);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // tabPageBase
            // 
            this.tabPageBase.Controls.Add(this.BaseParamFactoryPanel);
            this.tabPageBase.Controls.Add(this.TitlePanel);
            this.tabPageBase.Controls.Add(this.label6);
            this.tabPageBase.Controls.Add(this.gatewayTypeSelectBox);
            this.tabPageBase.Controls.Add(this.txtSoftVersion);
            this.tabPageBase.Controls.Add(this.txtDeviceVersion);
            this.tabPageBase.Controls.Add(this.ID);
            this.tabPageBase.Controls.Add(this.LabGetWayNo);
            this.tabPageBase.Controls.Add(this.label25);
            this.tabPageBase.Controls.Add(this.label12);
            this.tabPageBase.Font = new System.Drawing.Font("宋体", 9F);
            this.tabPageBase.Location = new System.Drawing.Point(4, 22);
            this.tabPageBase.Name = "tabPageBase";
            this.tabPageBase.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBase.Size = new System.Drawing.Size(700, 422);
            this.tabPageBase.TabIndex = 0;
            this.tabPageBase.Text = "配置基本参数";
            this.tabPageBase.UseVisualStyleBackColor = true;
            // 
            // BaseParamFactoryPanel
            // 
            this.BaseParamFactoryPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BaseParamFactoryPanel.Controls.Add(this.MaxNodeData);
            this.BaseParamFactoryPanel.Controls.Add(this.label4);
            this.BaseParamFactoryPanel.Controls.Add(this.label15);
            this.BaseParamFactoryPanel.Controls.Add(this.label26);
            this.BaseParamFactoryPanel.Controls.Add(this.DataBuffer);
            this.BaseParamFactoryPanel.Controls.Add(this.label29);
            this.BaseParamFactoryPanel.Controls.Add(this.label30);
            this.BaseParamFactoryPanel.Controls.Add(this.label5);
            this.BaseParamFactoryPanel.Controls.Add(this.textBox4);
            this.BaseParamFactoryPanel.Location = new System.Drawing.Point(71, 240);
            this.BaseParamFactoryPanel.Name = "BaseParamFactoryPanel";
            this.BaseParamFactoryPanel.Size = new System.Drawing.Size(333, 133);
            this.BaseParamFactoryPanel.TabIndex = 58;
            // 
            // MaxNodeData
            // 
            this.MaxNodeData.Font = new System.Drawing.Font("宋体", 9F);
            this.MaxNodeData.Location = new System.Drawing.Point(129, 64);
            this.MaxNodeData.MaxLength = 6;
            this.MaxNodeData.Name = "MaxNodeData";
            this.MaxNodeData.Size = new System.Drawing.Size(138, 21);
            this.MaxNodeData.TabIndex = 2;
            this.MaxNodeData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaxNodeData_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F);
            this.label4.Location = new System.Drawing.Point(25, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "最大数据记录数：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F);
            this.label15.Location = new System.Drawing.Point(60, 105);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "电池容量：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(279, 106);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(23, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "mAh";
            // 
            // DataBuffer
            // 
            this.DataBuffer.Font = new System.Drawing.Font("宋体", 9F);
            this.DataBuffer.Location = new System.Drawing.Point(129, 25);
            this.DataBuffer.MaxLength = 6;
            this.DataBuffer.Name = "DataBuffer";
            this.DataBuffer.Size = new System.Drawing.Size(138, 21);
            this.DataBuffer.TabIndex = 2;
            this.DataBuffer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataBuffer_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("宋体", 9F);
            this.label29.Location = new System.Drawing.Point(274, 25);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "(秒)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("宋体", 9F);
            this.label30.Location = new System.Drawing.Point(275, 70);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 0;
            this.label30.Text = "(条)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.Location = new System.Drawing.Point(15, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "最大数据缓冲时间：";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("宋体", 9F);
            this.textBox4.Location = new System.Drawing.Point(129, 103);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(138, 21);
            this.textBox4.TabIndex = 2;
            // 
            // TitlePanel
            // 
            this.TitlePanel.BackColor = System.Drawing.SystemColors.Control;
            this.TitlePanel.Controls.Add(this.label59);
            this.TitlePanel.Controls.Add(this.TitleMsg);
            this.TitlePanel.Controls.Add(this.progressBar1);
            this.TitlePanel.Location = new System.Drawing.Point(431, 161);
            this.TitlePanel.Name = "TitlePanel";
            this.TitlePanel.Size = new System.Drawing.Size(200, 93);
            this.TitlePanel.TabIndex = 52;
            this.TitlePanel.Visible = false;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(8, 44);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 12);
            this.label59.TabIndex = 13;
            this.label59.Text = "进度：";
            // 
            // TitleMsg
            // 
            this.TitleMsg.AutoSize = true;
            this.TitleMsg.Location = new System.Drawing.Point(15, 12);
            this.TitleMsg.Name = "TitleMsg";
            this.TitleMsg.Size = new System.Drawing.Size(53, 12);
            this.TitleMsg.TabIndex = 12;
            this.TitleMsg.Text = "进度提示";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(72, 39);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(119, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(93, 57);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.label6.Size = new System.Drawing.Size(106, 27);
            this.label6.TabIndex = 57;
            this.label6.Text = "网关类型：";
            // 
            // gatewayTypeSelectBox
            // 
            this.gatewayTypeSelectBox.DropDownHeight = 200;
            this.gatewayTypeSelectBox.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gatewayTypeSelectBox.FormattingEnabled = true;
            this.gatewayTypeSelectBox.IntegralHeight = false;
            this.gatewayTypeSelectBox.Items.AddRange(new object[] {
            "以太网",
            "GPRS",
            "RS232",
            "RS485"});
            this.gatewayTypeSelectBox.Location = new System.Drawing.Point(203, 57);
            this.gatewayTypeSelectBox.Name = "gatewayTypeSelectBox";
            this.gatewayTypeSelectBox.Size = new System.Drawing.Size(137, 27);
            this.gatewayTypeSelectBox.TabIndex = 56;
            // 
            // txtSoftVersion
            // 
            this.txtSoftVersion.Font = new System.Drawing.Font("宋体", 9F);
            this.txtSoftVersion.Location = new System.Drawing.Point(204, 147);
            this.txtSoftVersion.Name = "txtSoftVersion";
            this.txtSoftVersion.ReadOnly = true;
            this.txtSoftVersion.Size = new System.Drawing.Size(137, 21);
            this.txtSoftVersion.TabIndex = 2;
            // 
            // txtDeviceVersion
            // 
            this.txtDeviceVersion.Font = new System.Drawing.Font("宋体", 9F);
            this.txtDeviceVersion.Location = new System.Drawing.Point(204, 187);
            this.txtDeviceVersion.Name = "txtDeviceVersion";
            this.txtDeviceVersion.ReadOnly = true;
            this.txtDeviceVersion.Size = new System.Drawing.Size(137, 21);
            this.txtDeviceVersion.TabIndex = 2;
            // 
            // ID
            // 
            this.ID.Font = new System.Drawing.Font("宋体", 9F);
            this.ID.Location = new System.Drawing.Point(204, 107);
            this.ID.MaxLength = 8;
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(137, 21);
            this.ID.TabIndex = 2;
            this.ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ID_KeyPress);
            // 
            // LabGetWayNo
            // 
            this.LabGetWayNo.AutoSize = true;
            this.LabGetWayNo.Font = new System.Drawing.Font("宋体", 9F);
            this.LabGetWayNo.Location = new System.Drawing.Point(153, 110);
            this.LabGetWayNo.Name = "LabGetWayNo";
            this.LabGetWayNo.Size = new System.Drawing.Size(53, 12);
            this.LabGetWayNo.TabIndex = 0;
            this.LabGetWayNo.Text = "序列号：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(137, 146);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 12);
            this.label25.TabIndex = 0;
            this.label25.Text = "软件版本：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 9F);
            this.label12.Location = new System.Drawing.Point(137, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "硬件版本：";
            // 
            // tabPageNet
            // 
            this.tabPageNet.Controls.Add(this.NetworkID);
            this.tabPageNet.Controls.Add(this.NetworkPower);
            this.tabPageNet.Controls.Add(this.MaxNode);
            this.tabPageNet.Controls.Add(this.txtChannel);
            this.tabPageNet.Controls.Add(this.txtSynCode);
            this.tabPageNet.Controls.Add(this.label16);
            this.tabPageNet.Controls.Add(this.label27);
            this.tabPageNet.Controls.Add(this.label1);
            this.tabPageNet.Controls.Add(this.label17);
            this.tabPageNet.Controls.Add(this.label2);
            this.tabPageNet.Controls.Add(this.label3);
            this.tabPageNet.Location = new System.Drawing.Point(4, 22);
            this.tabPageNet.Name = "tabPageNet";
            this.tabPageNet.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNet.Size = new System.Drawing.Size(700, 422);
            this.tabPageNet.TabIndex = 6;
            this.tabPageNet.Text = "专网参数";
            this.tabPageNet.UseVisualStyleBackColor = true;
            // 
            // NetworkID
            // 
            this.NetworkID.Font = new System.Drawing.Font("宋体", 9F);
            this.NetworkID.Location = new System.Drawing.Point(240, 86);
            this.NetworkID.MaxLength = 8;
            this.NetworkID.Name = "NetworkID";
            this.NetworkID.Size = new System.Drawing.Size(96, 21);
            this.NetworkID.TabIndex = 63;
            // 
            // NetworkPower
            // 
            this.NetworkPower.Font = new System.Drawing.Font("宋体", 9F);
            this.NetworkPower.Location = new System.Drawing.Point(240, 130);
            this.NetworkPower.MaxLength = 5;
            this.NetworkPower.Name = "NetworkPower";
            this.NetworkPower.Size = new System.Drawing.Size(96, 21);
            this.NetworkPower.TabIndex = 60;
            // 
            // MaxNode
            // 
            this.MaxNode.Font = new System.Drawing.Font("宋体", 9F);
            this.MaxNode.Location = new System.Drawing.Point(240, 174);
            this.MaxNode.MaxLength = 6;
            this.MaxNode.Name = "MaxNode";
            this.MaxNode.Size = new System.Drawing.Size(96, 21);
            this.MaxNode.TabIndex = 61;
            // 
            // txtChannel
            // 
            this.txtChannel.Font = new System.Drawing.Font("宋体", 9F);
            this.txtChannel.Location = new System.Drawing.Point(240, 262);
            this.txtChannel.MaxLength = 8;
            this.txtChannel.Name = "txtChannel";
            this.txtChannel.Size = new System.Drawing.Size(96, 21);
            this.txtChannel.TabIndex = 55;
            // 
            // txtSynCode
            // 
            this.txtSynCode.Font = new System.Drawing.Font("宋体", 9F);
            this.txtSynCode.Location = new System.Drawing.Point(240, 218);
            this.txtSynCode.MaxLength = 8;
            this.txtSynCode.Name = "txtSynCode";
            this.txtSynCode.Size = new System.Drawing.Size(96, 21);
            this.txtSynCode.TabIndex = 53;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F);
            this.label16.Location = new System.Drawing.Point(192, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 62;
            this.label16.Text = "网络ID：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F);
            this.label27.Location = new System.Drawing.Point(356, 133);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(23, 12);
            this.label27.TabIndex = 56;
            this.label27.Text = "dBm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.Location = new System.Drawing.Point(164, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 57;
            this.label1.Text = "最大节点数：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F);
            this.label17.Location = new System.Drawing.Point(177, 139);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 12);
            this.label17.TabIndex = 58;
            this.label17.Text = "网络功率：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.Location = new System.Drawing.Point(174, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 54;
            this.label2.Text = "通信信道：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.Location = new System.Drawing.Point(165, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 52;
            this.label3.Text = "通信同步码：";
            // 
            // tabPageEthernet
            // 
            this.tabPageEthernet.Controls.Add(this.EthernetFactoryPanel);
            this.tabPageEthernet.Controls.Add(this.EDNS_d);
            this.tabPageEthernet.Controls.Add(this.EGetWay_d);
            this.tabPageEthernet.Controls.Add(this.EMask_d);
            this.tabPageEthernet.Controls.Add(this.EIp_d);
            this.tabPageEthernet.Controls.Add(this.DHCPSClose);
            this.tabPageEthernet.Controls.Add(this.DHCPSOpen);
            this.tabPageEthernet.Controls.Add(this.label7);
            this.tabPageEthernet.Controls.Add(this.label8);
            this.tabPageEthernet.Controls.Add(this.label18);
            this.tabPageEthernet.Controls.Add(this.label13);
            this.tabPageEthernet.Controls.Add(this.label9);
            this.tabPageEthernet.Controls.Add(this.label11);
            this.tabPageEthernet.Controls.Add(this.label10);
            this.tabPageEthernet.Controls.Add(this.EPort);
            this.tabPageEthernet.Controls.Add(this.EServer);
            this.tabPageEthernet.Location = new System.Drawing.Point(4, 22);
            this.tabPageEthernet.Name = "tabPageEthernet";
            this.tabPageEthernet.Size = new System.Drawing.Size(700, 422);
            this.tabPageEthernet.TabIndex = 2;
            this.tabPageEthernet.Text = "以太网参数";
            this.tabPageEthernet.UseVisualStyleBackColor = true;
            // 
            // EthernetFactoryPanel
            // 
            this.EthernetFactoryPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EthernetFactoryPanel.Controls.Add(this.Mac);
            this.EthernetFactoryPanel.Controls.Add(this.label21);
            this.EthernetFactoryPanel.Location = new System.Drawing.Point(144, 26);
            this.EthernetFactoryPanel.Name = "EthernetFactoryPanel";
            this.EthernetFactoryPanel.Size = new System.Drawing.Size(310, 92);
            this.EthernetFactoryPanel.TabIndex = 58;
            // 
            // Mac
            // 
            this.Mac.Location = new System.Drawing.Point(65, 30);
            this.Mac.Name = "Mac";
            this.Mac.Size = new System.Drawing.Size(220, 26);
            this.Mac.TabIndex = 57;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F);
            this.label21.Location = new System.Drawing.Point(3, 37);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 12);
            this.label21.TabIndex = 33;
            this.label21.Text = "MAC地址：";
            // 
            // EDNS_d
            // 
            this.EDNS_d.Location = new System.Drawing.Point(209, 256);
            this.EDNS_d.Name = "EDNS_d";
            this.EDNS_d.Size = new System.Drawing.Size(220, 26);
            this.EDNS_d.TabIndex = 56;
            // 
            // EGetWay_d
            // 
            this.EGetWay_d.Location = new System.Drawing.Point(209, 224);
            this.EGetWay_d.Name = "EGetWay_d";
            this.EGetWay_d.Size = new System.Drawing.Size(220, 26);
            this.EGetWay_d.TabIndex = 55;
            // 
            // EMask_d
            // 
            this.EMask_d.Location = new System.Drawing.Point(209, 194);
            this.EMask_d.Name = "EMask_d";
            this.EMask_d.Size = new System.Drawing.Size(220, 26);
            this.EMask_d.TabIndex = 54;
            // 
            // EIp_d
            // 
            this.EIp_d.Location = new System.Drawing.Point(209, 159);
            this.EIp_d.Name = "EIp_d";
            this.EIp_d.Size = new System.Drawing.Size(220, 26);
            this.EIp_d.TabIndex = 53;
            // 
            // DHCPSClose
            // 
            this.DHCPSClose.AutoSize = true;
            this.DHCPSClose.Checked = true;
            this.DHCPSClose.Location = new System.Drawing.Point(268, 137);
            this.DHCPSClose.Name = "DHCPSClose";
            this.DHCPSClose.Size = new System.Drawing.Size(35, 16);
            this.DHCPSClose.TabIndex = 44;
            this.DHCPSClose.TabStop = true;
            this.DHCPSClose.Text = "关";
            this.DHCPSClose.UseVisualStyleBackColor = true;
            this.DHCPSClose.CheckedChanged += new System.EventHandler(this.DHCPSClose_CheckedChanged);
            // 
            // DHCPSOpen
            // 
            this.DHCPSOpen.AutoSize = true;
            this.DHCPSOpen.Location = new System.Drawing.Point(212, 137);
            this.DHCPSOpen.Name = "DHCPSOpen";
            this.DHCPSOpen.Size = new System.Drawing.Size(35, 16);
            this.DHCPSOpen.TabIndex = 45;
            this.DHCPSOpen.TabStop = true;
            this.DHCPSOpen.Text = "开";
            this.DHCPSOpen.UseVisualStyleBackColor = true;
            this.DHCPSOpen.CheckedChanged += new System.EventHandler(this.DHCPSOpen_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(117, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 29;
            this.label7.Text = "DHCP开关状态：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F);
            this.label8.Location = new System.Drawing.Point(151, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 30;
            this.label8.Text = "IP地址：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 9F);
            this.label18.Location = new System.Drawing.Point(444, 294);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 31;
            this.label18.Text = "端口：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F);
            this.label13.Location = new System.Drawing.Point(129, 294);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "服务器地址：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(141, 202);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 35;
            this.label9.Text = "子网掩码：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 9F);
            this.label11.Location = new System.Drawing.Point(135, 262);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 12);
            this.label11.TabIndex = 36;
            this.label11.Text = "DNS服务器：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 9F);
            this.label10.Location = new System.Drawing.Point(142, 233);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 34;
            this.label10.Text = "默认网关：";
            // 
            // EPort
            // 
            this.EPort.Font = new System.Drawing.Font("宋体", 9F);
            this.EPort.Location = new System.Drawing.Point(489, 291);
            this.EPort.Name = "EPort";
            this.EPort.Size = new System.Drawing.Size(68, 21);
            this.EPort.TabIndex = 43;
            this.EPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EPort_KeyPress);
            // 
            // EServer
            // 
            this.EServer.Font = new System.Drawing.Font("宋体", 9F);
            this.EServer.Location = new System.Drawing.Point(210, 291);
            this.EServer.MaxLength = 32;
            this.EServer.Name = "EServer";
            this.EServer.Size = new System.Drawing.Size(219, 21);
            this.EServer.TabIndex = 42;
            // 
            // tabPageGprs
            // 
            this.tabPageGprs.Controls.Add(this.GPRSDNS);
            this.tabPageGprs.Controls.Add(this.label33);
            this.tabPageGprs.Controls.Add(this.label19);
            this.tabPageGprs.Controls.Add(this.AccessPoint);
            this.tabPageGprs.Controls.Add(this.User);
            this.tabPageGprs.Controls.Add(this.Pwd);
            this.tabPageGprs.Controls.Add(this.GPRSPoert);
            this.tabPageGprs.Controls.Add(this.GPRSServerIP);
            this.tabPageGprs.Controls.Add(this.label14);
            this.tabPageGprs.Controls.Add(this.label20);
            this.tabPageGprs.Controls.Add(this.label24);
            this.tabPageGprs.Controls.Add(this.label23);
            this.tabPageGprs.Location = new System.Drawing.Point(4, 22);
            this.tabPageGprs.Name = "tabPageGprs";
            this.tabPageGprs.Size = new System.Drawing.Size(700, 422);
            this.tabPageGprs.TabIndex = 3;
            this.tabPageGprs.Text = "GPRS参数";
            this.tabPageGprs.UseVisualStyleBackColor = true;
            // 
            // GPRSDNS
            // 
            this.GPRSDNS.Location = new System.Drawing.Point(200, 218);
            this.GPRSDNS.Name = "GPRSDNS";
            this.GPRSDNS.Size = new System.Drawing.Size(220, 26);
            this.GPRSDNS.TabIndex = 52;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("宋体", 9F);
            this.label33.Location = new System.Drawing.Point(149, 225);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 12);
            this.label33.TabIndex = 47;
            this.label33.Text = "DNS：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F);
            this.label19.Location = new System.Drawing.Point(119, 124);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 39;
            this.label19.Text = "接入点名：";
            // 
            // AccessPoint
            // 
            this.AccessPoint.Font = new System.Drawing.Font("宋体", 9F);
            this.AccessPoint.Location = new System.Drawing.Point(200, 121);
            this.AccessPoint.MaxLength = 16;
            this.AccessPoint.Name = "AccessPoint";
            this.AccessPoint.Size = new System.Drawing.Size(219, 21);
            this.AccessPoint.TabIndex = 45;
            // 
            // User
            // 
            this.User.Font = new System.Drawing.Font("宋体", 9F);
            this.User.Location = new System.Drawing.Point(200, 152);
            this.User.MaxLength = 16;
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(219, 21);
            this.User.TabIndex = 46;
            // 
            // Pwd
            // 
            this.Pwd.Font = new System.Drawing.Font("宋体", 9F);
            this.Pwd.Location = new System.Drawing.Point(200, 187);
            this.Pwd.MaxLength = 16;
            this.Pwd.Name = "Pwd";
            this.Pwd.Size = new System.Drawing.Size(219, 21);
            this.Pwd.TabIndex = 44;
            // 
            // GPRSPoert
            // 
            this.GPRSPoert.Font = new System.Drawing.Font("宋体", 9F);
            this.GPRSPoert.Location = new System.Drawing.Point(469, 254);
            this.GPRSPoert.Name = "GPRSPoert";
            this.GPRSPoert.Size = new System.Drawing.Size(58, 21);
            this.GPRSPoert.TabIndex = 43;
            this.GPRSPoert.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GPRSPoert_KeyPress);
            // 
            // GPRSServerIP
            // 
            this.GPRSServerIP.Font = new System.Drawing.Font("宋体", 9F);
            this.GPRSServerIP.Location = new System.Drawing.Point(200, 254);
            this.GPRSServerIP.MaxLength = 32;
            this.GPRSServerIP.Name = "GPRSServerIP";
            this.GPRSServerIP.Size = new System.Drawing.Size(219, 21);
            this.GPRSServerIP.TabIndex = 42;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(131, 161);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 37;
            this.label14.Text = "用户名：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 9F);
            this.label20.Location = new System.Drawing.Point(143, 196);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 38;
            this.label20.Text = "密码：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 9F);
            this.label24.Location = new System.Drawing.Point(434, 257);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 12);
            this.label24.TabIndex = 40;
            this.label24.Text = "端口：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F);
            this.label23.Location = new System.Drawing.Point(107, 257);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 12);
            this.label23.TabIndex = 41;
            this.label23.Text = "服务器地址：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(749, 99);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 28;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(749, 141);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 21);
            this.textBox2.TabIndex = 29;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(793, 360);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 30;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "配置文件(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "配置文件(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(717, 489);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panelConfig);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnReadDevice);
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "GM400 Config Tool V1.4";
            this.Load += new System.EventHandler(this.Main_Load);
            this.panelConfig.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageBase.ResumeLayout(false);
            this.tabPageBase.PerformLayout();
            this.BaseParamFactoryPanel.ResumeLayout(false);
            this.BaseParamFactoryPanel.PerformLayout();
            this.TitlePanel.ResumeLayout(false);
            this.TitlePanel.PerformLayout();
            this.tabPageNet.ResumeLayout(false);
            this.tabPageNet.PerformLayout();
            this.tabPageEthernet.ResumeLayout(false);
            this.tabPageEthernet.PerformLayout();
            this.EthernetFactoryPanel.ResumeLayout(false);
            this.EthernetFactoryPanel.PerformLayout();
            this.tabPageGprs.ResumeLayout(false);
            this.tabPageGprs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReadDevice;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panelConfig;
        private System.Windows.Forms.Button btnReadFileConfig;
        private System.Windows.Forms.Button BtnReadDeviceConfig;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageBase;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox gatewayTypeSelectBox;
        private System.Windows.Forms.Panel TitlePanel;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label TitleMsg;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox DataBuffer;
        private System.Windows.Forms.TextBox MaxNodeData;
        private System.Windows.Forms.TextBox txtSoftVersion;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox txtDeviceVersion;
        private System.Windows.Forms.TextBox ID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label LabGetWayNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPageNet;
        private System.Windows.Forms.TextBox NetworkID;
        private System.Windows.Forms.TextBox NetworkPower;
        private System.Windows.Forms.TextBox MaxNode;
        private System.Windows.Forms.TextBox txtChannel;
        private System.Windows.Forms.TextBox txtSynCode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPageEthernet;
        private IpBoxControl.MACTextBox Mac;
        private IpBoxControl.IpTextBox EDNS_d;
        private IpBoxControl.IpTextBox EGetWay_d;
        private IpBoxControl.IpTextBox EMask_d;
        private IpBoxControl.IpTextBox EIp_d;
        private System.Windows.Forms.RadioButton DHCPSClose;
        private System.Windows.Forms.RadioButton DHCPSOpen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox EPort;
        private System.Windows.Forms.TextBox EServer;
        private System.Windows.Forms.TabPage tabPageGprs;
        private IpBoxControl.IpTextBox GPRSDNS;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox AccessPoint;
        private System.Windows.Forms.TextBox User;
        private System.Windows.Forms.TextBox Pwd;
        private System.Windows.Forms.TextBox GPRSPoert;
        private System.Windows.Forms.TextBox GPRSServerIP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel EthernetFactoryPanel;
        private System.Windows.Forms.Panel BaseParamFactoryPanel;
    }
}