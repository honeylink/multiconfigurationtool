﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Threading;
using System.Collections;
using M433ConfigTool.log;

namespace M433ConfigTool
{
  public   class HusbHelp :MsUsbAPI
  {
      public delegate void DataRecievedEventHandler(byte[] data);
      public event DataRecievedEventHandler DataRecieved;
    
      public static List<InputReport> Relist=new List<InputReport>();
      public Hashtable Ht = new Hashtable();
      public string  dType = "";//设备类型
      public int cmdType = 0;
 
      #region  常量

      /// <summary>
      /// usb是否已存在了
      /// </summary>
      public bool history = false;
      /// <summary>
      /// usb是否已打开
      /// </summary>
      public bool IsOpen = false; 
      /// <summary>
      /// 设备访问地址
      /// </summary>
      private  string devicePath = string.Empty; 
      /// <summary>设备句柄</summary>
      private IntPtr m_hHandle;
      /// <summary>文件流用于读写</summary>
      private FileStream m_oFile;
      /// <summary>输入长度</summary>
      private int m_nInputReportLength;
      /// <summary>输出长度</summary>
      private int m_nOutputReportLength;
      private IntPtr hDevInfo;
      public byte[] bt=new byte[3];
      /// <summary>
      /// 包序号
      /// </summary> 
      private int DataPackNo;
      ////收发状态
      public int RevType;
      /// <summary>
      /// 接收服务类型
      /// </summary>
      private int ServerType;
      /// <summary>
      /// 发送服务类型
      /// </summary>
      private int sendType;
      private byte[] TData;
      /// <summary>
      /// 总长度
      /// </summary>
      private int DataTotleLength;
      private int DataIndex;
      #endregion

      #region  获取HID 设备
      /// <summary>
      /// 检验是否有HID设备
      /// </summary>
      /// <returns>成功true 失败false</returns>
      public bool ck_device()
      {
           
            FindDevice("0483", "5710" );	 
            return history;
      }
      /// <summary>
      /// 获取可用的HID的对象集合
      /// </summary>
      /// <returns></returns>
      public List<string> getGetHusbList()
      {
          List<string> list = new List<string>();
          
       //   GetHidDeviceList(ref list);//获取HID的USB设备列表
          return list;
      }
   
      #endregion

      #region   建立连接
      /// <summary>
      /// 打开指定信息的设备
      /// </summary> 
      /// <returns>成功：true  失败：false</returns>
      public bool OpenDevice( )
      {
          try
          {
            
              Initialise();//初始化设备
          }
          catch (Exception ex)
          { 
              throw ex;
          }
          return IsOpen;
      }
      /// <summary>
      /// 初始化设备
      /// </summary>
      /// <param name="strPath">设备访问地址</param>
      private void Initialise( )
      { 
        m_hHandle = CreateFile(devicePath, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
           
          if (m_hHandle != InvalidHandleValue || m_hHandle == null)	 
          {
              IntPtr lpData;
              if (HidD_GetPreparsedData(m_hHandle, out lpData))	 
              {
                  try
                  {  
                      HidCaps oCaps;
                      HidP_GetCaps(lpData, out oCaps);	 //获取设备输入输出上限
                      m_nInputReportLength = oCaps.InputReportByteLength;	 //得到输入报文长度
                      m_nOutputReportLength = oCaps.OutputReportByteLength;  //得到输出报文长度    
                      

                    m_oFile = new FileStream(new SafeFileHandle(m_hHandle, false), FileAccess.ReadWrite, m_nInputReportLength, true);
                   //   m_oFile = new FileStream(new SafeFileHandle(m_hHandle, false), FileAccess.ReadWrite, 3, true);
                     // m_oFile = new FileStream(new SafeFileHandle(m_hHandle, false), FileAccess.ReadWrite, inputReportLength, true);
                      IsOpen = true;
                      Thread th = new Thread(BeginAsyncRead);
                      th.IsBackground = true;
                      th.Start();
                      
                  }
                  catch (Exception ex)
                  {
                      throw ex;
                  }
                  finally
                  {
                      HidD_FreePreparsedData(ref lpData);	// before we quit the funtion, we must free the internal buffer reserved in GetPreparsedData
                  }
              } 
          }
          else	 
          {
              m_hHandle = IntPtr.Zero; 
          }
      }  
      public void Dispost()
      {
          //释放设备资源(hDevInfo是SetupDiGetClassDevs获取的)
          SetupDiDestroyDeviceInfoList(hDevInfo);
          //关闭连接(HidHandle是Create的时候获取的)
         // CloseHandle(HidHandle);
      }


  

      /// <summary>
      /// 关闭打开的设备
      /// </summary>
      public void CloseDevice()
      {
          if (history)
          {
           
              m_oFile.Close();
              IsOpen = false;
              history = false;
          }
      }
      #endregion
      #region   发送数据
      public void SendData(string sendMsg)
      {

          string Msg = "\r\n 发送:" + sendMsg + "\r\n";
          LogHelper.WriteRight(Msg);
             byte[] data = HexStringToByte(sendMsg);
              int Num=0;
              int totalLength=data.Length;
             int index = 0;
             while (sendMsg.Length > index)
             {
                 byte[] sData = new byte[61];
                 int DataLength = 0;
                 OutputReport oRep = new OutputReport();
                 if ((index + 64) <= data.Length)
                 { 
                     Array.Copy(data, index, sData,0, 61);
                     DataLength = 64;
                     index += 64;
                 }
                 else
                 {
                     DataLength = sendMsg.Length - index;
                     Array.Copy(data, index, sData, 0, sendMsg.Length - index); 
                     index += sendMsg.Length - index; 
                 } 
                 Num++;
                 oRep.SetData(sData  ); 
                 m_oFile.Write(oRep.Buffer, 0, oRep.BufferLength);
               
             } 
           
          
      }
      /// <summary>
      /// 发送命令
      /// </summary>
      /// <param name="sendMsg"></param>
      public void SendData(byte[] data)
      {
          try
          {
              string Msg = "\r\n 发送:" + BitConverter.ToString(data) + "\r\n";
              LogHelper.WriteRight(Msg);
              int totalLength = data.Length;
              int index = 0;
            
              while (data.Length > index)
              {
                  byte[] sData = new byte[64];
                  int DataLength = 0;
                  OutputReport oRep = new OutputReport();
                  if ((index + 64) <= data.Length)
                  {
                      Array.Copy(data, index, sData, 0, 64);
                      DataLength = 64;
                      index += 64;
                  }
                  else
                  {
                      DataLength = data.Length - index;
                      Array.Copy(data, index, sData, 0, data.Length - index);
                      index += data.Length - index;
                  }
                  oRep.SetData(sData); 
                  m_oFile.Write(oRep.Buffer, 0, oRep.BufferLength); 
                  RevType = 1;
              }
 
          }
          catch (IOException ex)
          {
              LogHelper.WriteException(ex);
             // throw new Exception("设备通讯异常");
          }

      }
      #endregion
      #region   接收数据
      /// <summary> 
      ///异步读取
      /// </summary>
      private void BeginAsyncRead()
      {

          try
          { 
              byte[] inputBuff = new byte[m_nInputReportLength]; 
              m_oFile.BeginRead(inputBuff, 0, m_nInputReportLength, new AsyncCallback(ReadCompleted), inputBuff); 
          }
          catch (Exception ex)
          {
              if (IsOpen)
              {
                  Thread th = new Thread(BeginAsyncRead);
                  th.IsBackground = true;
                  th.Start();
              } 
          }
          GC.Collect();

      }
      /// <summary>
      /// 回调，数据读完后的方法
      /// </summary>
      /// <param name="iResult">结果</param>
      public void ReadCompleted(IAsyncResult iResult)
      {     
          byte[] arrBuff = (byte[])iResult.AsyncState;	// 检索读缓冲  
          try
          {
              string State = Encoding.UTF8.GetString(arrBuff).Replace("\0", null);
              if (string.IsNullOrEmpty(State))
              {
                  IsOpen = false;
              }
              else
              {
                  InputReport oInRep = new InputReport();	//创建设备输入报文
                  oInRep.SetData(arrBuff);	// 数据设置   
                  LogHelper.WriteRight("\r\n  接收到数据包：" + BitConverter.ToString(oInRep.Buffer) + "\r\n");
                  analyze(oInRep.Buffer);//解析 
              }

          }
          catch (Exception ex)
          {
              //  LogHelper.WriteRight("\r\n 异常数据：" + BitConverter.ToString(arrBuff) + "\r\n");
              LogHelper.WriteException(ex);
              //     throw ex;
          }
          finally {
              Thread th = new Thread(BeginAsyncRead);
              th.IsBackground = true;
              th.Start();
          }
      }



        /// <summary>
        /// USB HID相关包的整合
        /// 需要根据上层应用的协议进行相关控制处理
        /// !@todo 完全与上层协议隔开，自动完成组包的工作
        /// </summary>
        /// <param name="data"></param>
      public void analyze(byte[] data)
      {
          int ckstate = 0;//检查是否校验通过

          if (RevType == 1)
          {
              DataTotleLength = AppCmd.ByteToInt(data, 0, 2);
              if (DataTotleLength <= 60  )
              {
                  int[] ck = AppCmd.GetCheckData(data, 0, DataTotleLength + 2);
                  if (data[DataTotleLength + 2] == (byte)ck[0] && data[DataTotleLength + 3] == (byte)ck[1])//校验码通过
                  {
                      ckstate = 1;
                      int PackType = AppCmd.ByteToInt(data, 2, 3);
                      if ((PackType&0x01) == 1)
                      {
                          RevType = 1;
                      }
                      else
                      {
                          RevType = 0;
                      }
                      //数据包位置
                      DataIndex = 0;
                      TData = new byte[DataTotleLength + 4];
                      Array.Copy(data, 0, TData, 0, DataTotleLength + 4);
                  }
                  else
                  {
                      if (DataTotleLength > 60)
                      {
                          TData = new byte[DataTotleLength + 4];
                          DataIndex = 0;
                          RevType = 0;
                      }
                  }
              }
              else
              {
                  TData = new byte[DataTotleLength + 4];
                  DataIndex = 0;
                  RevType = 0;
              }
          }

          if (ckstate == 1)
          {
              DataRecieved(TData);
          }
          else
          {
              if ((DataTotleLength + 4) - DataIndex > 64)
              {
                  Array.Copy(data, 0, TData, DataIndex, data.Length);
                  DataIndex += 64;
              }
              else
              {
                  Array.Copy(data, 0, TData, DataIndex, (DataTotleLength + 4) - DataIndex);
                  DataIndex += (DataTotleLength + 4) - DataIndex;
              } 
              if (DataIndex == DataTotleLength + 4)
              {
                  DataRecieved(TData);
              } 
          }          
      }        

      #endregion 
      #region 数据转换

      /// <summary>
      /// btye数组转换为字符串
      /// </summary>
      /// <param name="bytes"></param>
      /// <returns></returns>
      public static string ByteToHexString(byte[] bytes)
      {
          string str = string.Empty;
          if (bytes != null)
          {
              for (int i = 0; i < bytes.Length; i++)
              {
                  str += bytes[i].ToString("X2");
              }
          }
          return str;
      }

     


      /// <summary>
      /// btye数组转换为字符串
      /// </summary>
      /// <param name="bytes"></param>
      /// <returns></returns>
      public static byte[] HexStringToByte(string msg)
      {
          byte[] bstr = new byte[1024];
             bstr = Encoding.UTF8.GetBytes(msg);
          return bstr;
      }
      #endregion

      #region  操作方法
      /// <summary>
      ///搜索USB设备通过vid和pid
      /// </summary>
      /// <param name="nVid">供应商ID</param>
      /// <param name="nPid">产品ID</param>
      public   void FindDevice(string nVid, string nPid )
      {
          bool ckflag = false;
          string strPath = string.Empty; 
          string strSearch = string.Format("vid_{0}&pid_{1}", nVid, nPid); // first, build the path search string
          Guid gHid = HIDGuid;
          IntPtr hInfoSet = SetupDiGetClassDevs(ref gHid, null, IntPtr.Zero, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);	//得到当前的hid设备
          try
          {
              DeviceInterfaceData oInterface = new DeviceInterfaceData();	// 建立一个设备接口的数据块
              oInterface.Size = Marshal.SizeOf(oInterface);
              //遍历所有的hid接口得到所需的hid接口
              int nIndex = 0;
             
              while (SetupDiEnumDeviceInterfaces(hInfoSet, 0, ref gHid, (uint)nIndex, ref oInterface))	// 获取hid设备描述
              {
                  string strDevicePath = GetDevicePath(hInfoSet, ref oInterface);	//获取hid设备信息集合中访问路径
                  if (strDevicePath.IndexOf(strSearch) >= 0)
                  {
                      devicePath = strDevicePath;
                      ckflag = true;
                      break;
                  }
                  nIndex++;
              }
              history = ckflag;
          }
          catch (Exception ex)
          {
              devicePath = "";
              history = false;
          }
         
           
      }
      /// <summary>
      
      /// 用于返回在hid设备信息集合中返回访问路径
      /// </summary>
      /// <param name="hInfoSet">处理的信息集</param>
      /// <param name="oInterface">设备接口结构/参数</param>
      /// <returns>设备的访问路径</returns>
      private static string GetDevicePath(IntPtr hInfoSet, ref DeviceInterfaceData oInterface)
      {
          uint nRequiredSize = 0;
          // 获取设备接口的描述
          if (!SetupDiGetDeviceInterfaceDetail(hInfoSet, ref oInterface, IntPtr.Zero, 0, ref nRequiredSize, IntPtr.Zero))
          {
              DeviceInterfaceDetailData oDetail = new DeviceInterfaceDetailData();
              oDetail.Size = 5; 
              if (SetupDiGetDeviceInterfaceDetail(hInfoSet, ref oInterface, ref oDetail, nRequiredSize, ref nRequiredSize, IntPtr.Zero))
              {
                  return oDetail.DevicePath;
              }
          }
          return null;
      }
      public void Test(string sendMsg)
      {

          //byte[] data = HexStringToByte(sendMsg);
          //int Num = 0;
          //int totalLength = data.Length;
          //int index = 0;
          //while (sendMsg.Length > index)
          //{
          //    byte[] sData = new byte[61];
          //    int DataLength = 0;
          //    OutputReport oRep = new OutputReport();
          //    if ((index + 61) <= data.Length)
          //    {
          //        Array.Copy(data, index, sData, 0, 61);
          //        DataLength = 61;
          //        index += 61;
          //    }
          //    else
          //    {
          //        DataLength = sendMsg.Length - index;
          //        Array.Copy(data, index, sData, 0, sendMsg.Length - index);
          //        index += sendMsg.Length - index;

          //    }
          //    Num++;
          //    oRep.SetData(sData, Num, totalLength, DataLength); 
          //}
          //RetVal = new byte[sendMsg];
      }

      /** 
   * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序，和和intToBytes（）配套使用
   *  
   * @param src 
   *            byte数组 
   * @param offset 
   *            从数组的第offset位开始 
   * @return int数值 
   */
      public static int bytesToInt(byte[] src, int offset,int length)
      {
          byte[]  data=new byte[4] ;


          Array.Copy(src, 0, data, 0, length);
          int value;
          value = (int)((src[offset] & 0xFF)
                  | ((src[offset + 1] & 0xFF) << 8)
                  | ((src[offset + 2] & 0xFF) << 16)
                  | ((src[offset + 3] & 0xFF) << 24));
          return value;
      }

      
      #endregion
  }
  class AsyncState
  {
      public FileStream FS { get; set; }
      public byte[] Buffer { get; set; }
      public ManualResetEvent EvtHandle { get; set; }
  }


}
