﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M433ConfigTool
{
   /// <summary>
   /// 发送命令对象
   /// </summary>
   public class SendModel
    {
       /// <summary>
       /// 数据
       /// </summary> 
       public byte[] Cmdbyte{ get; set; }
       /// <summary>
       /// 编号
       /// </summary>
       public int No{get;set;}
       /// <summary>
       /// 次数
       /// </summary>
       public int Num { get; set; }
       /// <summary>
       /// 状态
       /// </summary>
       public int State { get; set; }
       /// <summary>
       /// 服务
       /// </summary>
       public int ServerType { get; set; }
       /// <summary>
       /// command(1) or ack(1)
       /// </summary>
       public int packetType { get; set; } 
    }
}
