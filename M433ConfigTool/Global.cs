﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace M433ConfigTool
{
    public static class Global
    {
        /// <summary>
        /// 语言
        /// </summary>
        private static string language = ConfigurationManager.AppSettings["language"].ToString(); 
        public static string Language
        {
            get
            { 
                return language;
            }
            set { language = value; }
        }


        /// <summary>
        /// 语言
        /// </summary>
        public static readonly string UserVersion = ConfigurationManager.AppSettings["UserVersion"].ToString();
    }
}
