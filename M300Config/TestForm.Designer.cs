﻿namespace M300Config
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOneKeyTest = new System.Windows.Forms.Button();
            this.btnModem = new System.Windows.Forms.Button();
            this.btnTestPower = new System.Windows.Forms.Button();
            this.btnTestprobe = new System.Windows.Forms.Button();
            this.btnTestLCD = new System.Windows.Forms.Button();
            this.btnTestKeyPress = new System.Windows.Forms.Button();
            this.LabMsg = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.btnOneKeyTest);
            this.splitContainer1.Panel1.Controls.Add(this.btnModem);
            this.splitContainer1.Panel1.Controls.Add(this.btnTestPower);
            this.splitContainer1.Panel1.Controls.Add(this.btnTestprobe);
            this.splitContainer1.Panel1.Controls.Add(this.btnTestLCD);
            this.splitContainer1.Panel1.Controls.Add(this.btnTestKeyPress);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LabMsg);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(954, 489);
            this.splitContainer1.SplitterDistance = 166;
            this.splitContainer1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 427);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 50);
            this.button2.TabIndex = 1;
            this.button2.Text = "退出测试模式";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 50);
            this.button1.TabIndex = 1;
            this.button1.Text = "进入测试模式";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOneKeyTest
            // 
            this.btnOneKeyTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOneKeyTest.Enabled = false;
            this.btnOneKeyTest.Location = new System.Drawing.Point(3, 369);
            this.btnOneKeyTest.Name = "btnOneKeyTest";
            this.btnOneKeyTest.Size = new System.Drawing.Size(139, 52);
            this.btnOneKeyTest.TabIndex = 0;
            this.btnOneKeyTest.Text = "一键测试";
            this.btnOneKeyTest.UseVisualStyleBackColor = true;
            this.btnOneKeyTest.Click += new System.EventHandler(this.btnOneKeyTest_Click);
            // 
            // btnModem
            // 
            this.btnModem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModem.Enabled = false;
            this.btnModem.Location = new System.Drawing.Point(3, 311);
            this.btnModem.Name = "btnModem";
            this.btnModem.Size = new System.Drawing.Size(139, 52);
            this.btnModem.TabIndex = 0;
            this.btnModem.Text = "测试modem";
            this.btnModem.UseVisualStyleBackColor = true;
            this.btnModem.Click += new System.EventHandler(this.btnModem_Click);
            // 
            // btnTestPower
            // 
            this.btnTestPower.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestPower.Enabled = false;
            this.btnTestPower.Location = new System.Drawing.Point(3, 253);
            this.btnTestPower.Name = "btnTestPower";
            this.btnTestPower.Size = new System.Drawing.Size(139, 52);
            this.btnTestPower.TabIndex = 0;
            this.btnTestPower.Text = "测试电池电量";
            this.btnTestPower.UseVisualStyleBackColor = true;
            this.btnTestPower.Click += new System.EventHandler(this.btnTestPower_Click);
            // 
            // btnTestprobe
            // 
            this.btnTestprobe.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestprobe.Enabled = false;
            this.btnTestprobe.Location = new System.Drawing.Point(3, 195);
            this.btnTestprobe.Name = "btnTestprobe";
            this.btnTestprobe.Size = new System.Drawing.Size(139, 52);
            this.btnTestprobe.TabIndex = 0;
            this.btnTestprobe.Text = "测试探头";
            this.btnTestprobe.UseVisualStyleBackColor = true;
            this.btnTestprobe.Click += new System.EventHandler(this.btnTestprobe_Click);
            // 
            // btnTestLCD
            // 
            this.btnTestLCD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestLCD.Enabled = false;
            this.btnTestLCD.Location = new System.Drawing.Point(3, 137);
            this.btnTestLCD.Name = "btnTestLCD";
            this.btnTestLCD.Size = new System.Drawing.Size(139, 52);
            this.btnTestLCD.TabIndex = 0;
            this.btnTestLCD.Text = "测试LCD显示";
            this.btnTestLCD.UseVisualStyleBackColor = true;
            this.btnTestLCD.Click += new System.EventHandler(this.btnTestLCD_Click);
            // 
            // btnTestKeyPress
            // 
            this.btnTestKeyPress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestKeyPress.Enabled = false;
            this.btnTestKeyPress.Location = new System.Drawing.Point(3, 79);
            this.btnTestKeyPress.Name = "btnTestKeyPress";
            this.btnTestKeyPress.Size = new System.Drawing.Size(139, 52);
            this.btnTestKeyPress.TabIndex = 0;
            this.btnTestKeyPress.Text = "测试按键";
            this.btnTestKeyPress.UseVisualStyleBackColor = true;
            this.btnTestKeyPress.Click += new System.EventHandler(this.btnTestKeyPress_Click);
            // 
            // LabMsg
            // 
            this.LabMsg.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.LabMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LabMsg.Location = new System.Drawing.Point(16, 38);
            this.LabMsg.Multiline = true;
            this.LabMsg.Name = "LabMsg";
            this.LabMsg.ReadOnly = true;
            this.LabMsg.Size = new System.Drawing.Size(766, 439);
            this.LabMsg.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "测试信息：";
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 489);
            this.Controls.Add(this.splitContainer1);
            this.Name = "TestForm";
            this.Text = "测试模式";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestForm_FormClosing);
            this.Load += new System.EventHandler(this.TestForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnOneKeyTest;
        private System.Windows.Forms.Button btnModem;
        private System.Windows.Forms.Button btnTestPower;
        private System.Windows.Forms.Button btnTestprobe;
        private System.Windows.Forms.Button btnTestLCD;
        private System.Windows.Forms.Button btnTestKeyPress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LabMsg;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}