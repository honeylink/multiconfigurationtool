﻿namespace M300Config
{
    partial class M300SetConfig
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M300SetConfig));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labMessage = new System.Windows.Forms.Label();
            this.openbtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.clear = new System.Windows.Forms.Button();
            this.send = new System.Windows.Forms.Button();
            this.sendtext = new System.Windows.Forms.TextBox();
            this.revtext = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSetParameter = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer_Check = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLanguage_nlx = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labMessage);
            this.groupBox1.Controls.Add(this.openbtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(195, 153);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设备连接";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "连接信息：";
            // 
            // labMessage
            // 
            this.labMessage.Location = new System.Drawing.Point(20, 111);
            this.labMessage.Name = "labMessage";
            this.labMessage.Size = new System.Drawing.Size(159, 39);
            this.labMessage.TabIndex = 7;
            this.labMessage.Text = "Message";
            // 
            // openbtn
            // 
            this.openbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openbtn.Location = new System.Drawing.Point(20, 31);
            this.openbtn.Name = "openbtn";
            this.openbtn.Size = new System.Drawing.Size(142, 37);
            this.openbtn.TabIndex = 4;
            this.openbtn.Text = "连接设备";
            this.openbtn.UseVisualStyleBackColor = true;
            this.openbtn.Click += new System.EventHandler(this.openbtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "自动接收";
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(561, 41);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 57);
            this.clear.TabIndex = 11;
            this.clear.Text = "清空显示";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(561, 310);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(75, 60);
            this.send.TabIndex = 10;
            this.send.Text = "发送";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // sendtext
            // 
            this.sendtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sendtext.Location = new System.Drawing.Point(215, 310);
            this.sendtext.Multiline = true;
            this.sendtext.Name = "sendtext";
            this.sendtext.Size = new System.Drawing.Size(323, 61);
            this.sendtext.TabIndex = 8;
            // 
            // revtext
            // 
            this.revtext.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.revtext.Location = new System.Drawing.Point(215, 41);
            this.revtext.Multiline = true;
            this.revtext.Name = "revtext";
            this.revtext.ReadOnly = true;
            this.revtext.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.revtext.Size = new System.Drawing.Size(323, 243);
            this.revtext.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetParameter);
            this.groupBox2.Location = new System.Drawing.Point(12, 171);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(195, 113);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设备参数配置";
            // 
            // btnSetParameter
            // 
            this.btnSetParameter.Location = new System.Drawing.Point(6, 34);
            this.btnSetParameter.Name = "btnSetParameter";
            this.btnSetParameter.Size = new System.Drawing.Size(173, 58);
            this.btnSetParameter.TabIndex = 15;
            this.btnSetParameter.Text = "设备参数配置";
            this.btnSetParameter.UseVisualStyleBackColor = true;
            this.btnSetParameter.Click += new System.EventHandler(this.btnSetParameter_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // timer_Check
            // 
            this.timer_Check.Interval = 1000;
            this.timer_Check.Tick += new System.EventHandler(this.timer_Check_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "发送指令";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "语言：";
            // 
            // cmbLanguage_nlx
            // 
            this.cmbLanguage_nlx.FormattingEnabled = true;
            this.cmbLanguage_nlx.Items.AddRange(new object[] {
            "English",
            "简体中文"});
            this.cmbLanguage_nlx.Location = new System.Drawing.Point(81, 349);
            this.cmbLanguage_nlx.Name = "cmbLanguage_nlx";
            this.cmbLanguage_nlx.Size = new System.Drawing.Size(121, 20);
            this.cmbLanguage_nlx.TabIndex = 17;
            this.cmbLanguage_nlx.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // M300SetConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 383);
            this.Controls.Add(this.cmbLanguage_nlx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.send);
            this.Controls.Add(this.sendtext);
            this.Controls.Add(this.revtext);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "M300SetConfig";
            this.Text = "M364Config_V1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button openbtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.TextBox sendtext;
        private System.Windows.Forms.TextBox revtext;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSetParameter;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Timer timer_Check;
        private System.Windows.Forms.Label labMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLanguage_nlx;
    }
}

