﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TH6100Config
{
    public abstract class Report
    {
        #region Member variables
        /// <summary>报文字节数组</summary>
        private byte[] m_arrBuffer;
        /// <summary>报文 长度</summary>
        private int m_nLength;
        #endregion
        /// <summary>
        /// Sets the raw byte array.
        /// </summary>
        /// <param name="arrBytes">Raw report bytes</param>
        protected void SetBuffer(byte[] arrBytes)
        {
            m_arrBuffer = arrBytes;
           
          
            m_nLength = m_arrBuffer.Length;
        }
        /// <summary>
        /// Sets the raw byte array.
        /// </summary>
        /// <param name="arrBytes">Raw report bytes</param>
        protected void SetBufferData(byte[] arrBytes)
        {
            m_arrBuffer = new byte[arrBytes.Length - 1];
            Array.Copy(arrBytes, 1, m_arrBuffer, 0, arrBytes.Length - 1);

            m_nLength = m_arrBuffer.Length;
        }
        /// <summary>
        ///Buffer读写
        /// </summary>
        public byte[] Buffer
        {
            get { return m_arrBuffer; }
            set { this.m_arrBuffer = value; }
        }
        /// <summary>
        /// 缓冲数据长度读取
        /// </summary>
        public int BufferLength
        {
            get
            {
                return m_nLength;
            }
        } 
    }
   
    /// <summary>
    ///输入报文 
    /// </summary>
    public   class InputReport : Report
    {
        
        public InputReport( ) 
        {
        }
        /// <summary>
        ///把数据放入缓存中
        /// </summary>
        /// <param name="arrData">数组</param>
        public void SetData(byte[] arrData)
        {
            SetBufferData(arrData);
           
        }       

    }
    /// <summary>
    ///输入报文 
    /// </summary>
    public   class OutputReport : Report
    {

        public OutputReport()
        {
        }
        /// <summary>
        ///把数据放入缓存中
        /// </summary>
        /// <param name="arrData">数组</param>
        public void SetData(byte[] data, int Sno, int tatalLength, int length)
        {
            int l = 1;
            byte[] arrBuff = new byte[65];
            byte[] SnoByte = BitConverter.GetBytes(Sno); ;
            byte[] total = BitConverter.GetBytes(tatalLength);
         
            Array.Copy(SnoByte, 0, arrBuff, 1, 1); //序号
            //总长度
            arrBuff[2] = total[1];
            arrBuff[3] = total[0]; 
            for (int i = 0; i < data.Length; i++)
            {
                arrBuff[i + 4] = data[i];
            } 
            SetBuffer(arrBuff); 
        }
        

    }
  
}
