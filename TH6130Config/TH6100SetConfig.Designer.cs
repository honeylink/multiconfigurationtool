﻿namespace TH6100Config
{
    partial class TH6100SetConfig
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TH6100SetConfig));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labMessage = new System.Windows.Forms.Label();
            this.openbtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSetParameter = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLanguage_nlx = new System.Windows.Forms.ComboBox();
            this.cmb_serialport = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labMessage);
            this.groupBox1.Controls.Add(this.openbtn);
            this.groupBox1.Location = new System.Drawing.Point(7, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 153);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设备连接";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "连接信息：";
            // 
            // labMessage
            // 
            this.labMessage.Location = new System.Drawing.Point(72, 111);
            this.labMessage.Name = "labMessage";
            this.labMessage.Size = new System.Drawing.Size(159, 39);
            this.labMessage.TabIndex = 7;
            this.labMessage.Text = "Message";
            // 
            // openbtn
            // 
            this.openbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openbtn.Location = new System.Drawing.Point(74, 31);
            this.openbtn.Name = "openbtn";
            this.openbtn.Size = new System.Drawing.Size(142, 37);
            this.openbtn.TabIndex = 4;
            this.openbtn.Text = "连接设备";
            this.openbtn.UseVisualStyleBackColor = true;
            this.openbtn.Click += new System.EventHandler(this.openbtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetParameter);
            this.groupBox2.Location = new System.Drawing.Point(7, 226);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(283, 113);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设备参数配置";
            // 
            // btnSetParameter
            // 
            this.btnSetParameter.Location = new System.Drawing.Point(58, 31);
            this.btnSetParameter.Name = "btnSetParameter";
            this.btnSetParameter.Size = new System.Drawing.Size(173, 58);
            this.btnSetParameter.TabIndex = 15;
            this.btnSetParameter.Text = "设备参数配置";
            this.btnSetParameter.UseVisualStyleBackColor = true;
            this.btnSetParameter.Click += new System.EventHandler(this.btnSetParameter_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "语言：";
            // 
            // cmbLanguage_nlx
            // 
            this.cmbLanguage_nlx.FormattingEnabled = true;
            this.cmbLanguage_nlx.Items.AddRange(new object[] {
            "简体中文",
            "English"});
            this.cmbLanguage_nlx.Location = new System.Drawing.Point(103, 350);
            this.cmbLanguage_nlx.Name = "cmbLanguage_nlx";
            this.cmbLanguage_nlx.Size = new System.Drawing.Size(174, 20);
            this.cmbLanguage_nlx.TabIndex = 17;
            this.cmbLanguage_nlx.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cmb_serialport
            // 
            this.cmb_serialport.FormattingEnabled = true;
            this.cmb_serialport.Location = new System.Drawing.Point(103, 26);
            this.cmb_serialport.Name = "cmb_serialport";
            this.cmb_serialport.Size = new System.Drawing.Size(174, 20);
            this.cmb_serialport.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "请选择串口：";
            // 
            // TH6100SetConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 383);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmb_serialport);
            this.Controls.Add(this.cmbLanguage_nlx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TH6100SetConfig";
            this.Text = "TH6130配置工具";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button openbtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSetParameter;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label labMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLanguage_nlx;
        private System.Windows.Forms.ComboBox cmb_serialport;
        private System.Windows.Forms.Label label5;
        private System.IO.Ports.SerialPort serialPort1;
    }
}

