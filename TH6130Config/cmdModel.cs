﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TH6100Config
{
    public class cmdModel
    {
        private string cmdName;
        public string CmdName
        {
            get { return cmdName; }
            set { cmdName = value; }
        }
        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        private string val;
        public string Val
        {
            get { return val; }
            set { val = value; }
        }
        private string max;
        public string Max
        {
            get { return max; }
            set { max = value; }
        }
        private string length;
        public string Length
        {
            get { return length; }
            set { length = value; }
        }
        private string cmd;
        public string Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }
        private double  sort;
        public double Sort
        {
            get { return sort; }
            set { sort = value; }
        }
        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
    }
}
