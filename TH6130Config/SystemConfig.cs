﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;

namespace TH6100Config
{
    public partial class SystemConfig : Form
    {
        bool AllSelect = true;//禁止选项卡
        int OngogingReadType = 0;//响应类型
        public ConfigProtocol protocol;
        string DeviceType = "";

        byte[] ConfigurationReadBuffer = null;
        List<SendModel> listSendModelBuffer;
        int sendNum = 0;

        public delegate void GdvAddItemCallback();
        public delegate void SetModelCallBack();
        public delegate bool UpdateUiCallBack();

        bool IsInProgramMode = false;//配置模式

        TestForm test;//测试模式对象

        List<ConfigModel> list = new List<ConfigModel>();
        List<cmdModel> cmdlist = new List<cmdModel>();
        public Hashtable contentHashTable = new Hashtable();
        SelectLanguage Language = new SelectLanguage();

        int readStartAddress;
        int readEndAddress;
        int writeStartAddress;
        int writeEndAddress;

        /// <summary>
        /// 构建
        /// </summary>
        /// <param name="obj">通信接口对象</param>
        public SystemConfig(object obj)
        {
            protocol = new ConfigProtocol(obj); 
            InitializeComponent();

       
            List<cmdModel> listModel = new AppCmd().ReadConfigCmd();

            //根据实际的需求文件初始化地址
            cmdModel readStartModel = listModel.Where(l => l.Type != "5").OrderByDescending(l => l.Sort).LastOrDefault();
            readStartAddress = Convert.ToInt32(readStartModel.Address, 16);

            cmdModel readEndModel = listModel.Where(l => l.Type != "5").OrderByDescending(l => l.Sort).FirstOrDefault();//获取最后一项，其地址值+4即为整个参数缓冲区大小
            readEndAddress = Convert.ToInt32(readEndModel.Address, 16) + 4;

            writeStartAddress = readStartAddress;
            cmdModel writeEndMode = listModel.OrderBy(l => l.Sort).ToList<cmdModel>().LastOrDefault();//获取最后一项，其地址值+4即为整个参数缓冲区大小
            writeEndAddress = Convert.ToInt32(writeEndMode.Address, 16) + 4;

            ConfigurationReadBuffer = new byte[readEndAddress];     //地址确定配置区的数据存储BUFFER大小
        }       


        /// <summary>
        /// UI加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SystemConfig_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            Language.SetLanguage(this);

            protocol.DataResponse += new TH6100Config.ConfigProtocol.DataResponseEventHandler(CmdDataReadCallBack);
            protocol.BDataResponse += new TH6100Config.ConfigProtocol.BDataResponseEventHandler(StoredDataReadCallBack);
            protocol.GetDeviceType += new TH6100Config.ConfigProtocol.DataResponseTypeEventHandler(GetDeviceTypeCallback);

            TitlePanel.TabIndex = 0;
            tabControl1.TabIndex = 10;

            timer1.Start();//定时器启动

            if (Global.Language == "English")
            {
                this.txtAdress_nlx.MaxLength = 100;
                this.txtMonitor1_nlx.MaxLength = 32;
                this.txtMonitor2_nlx.MaxLength = 32;
                this.txtMonitor3_nlx.MaxLength = 32;
                this.txtMonitor4_nlx.MaxLength = 32;
            }
            else
            {
                this.txtAdress_nlx.MaxLength = 25;
                this.txtMonitor1_nlx.MaxLength = 8;
                this.txtMonitor2_nlx.MaxLength = 8;
                this.txtMonitor3_nlx.MaxLength = 8;
                this.txtMonitor4_nlx.MaxLength = 8;
            }
            this.cbContactType_nlx.SelectedIndex = 0;
            this.cbConnectionType_nlx.SelectedIndex = 0;

            dataGridView1.AutoGenerateColumns = false;
            txtMonitor1_nlx.Text = Language.getMsg("T1");
            txtMonitor2_nlx.Text = Language.getMsg("H");
            txtMonitor3_nlx.Text = Language.getMsg("T2");
            txtMonitor4_nlx.Text = Language.getMsg("T3");

            //根据默认设备类型设置界面
            UpdateUiBaseOnDeviceType();

            this.cmbAllowClose_nlx.Items.Add(Language.getMsg("No Permit"));
            this.cmbAllowClose_nlx.Items.Add(Language.getMsg("Permit"));
            this.cmbAllowClose_nlx.SelectedIndex = 0;

            this.comboBoxPrintMode_nlx.Items.Add(Language.getMsg("All"));
            this.comboBoxPrintMode_nlx.Items.Add(Language.getMsg("Recent Specific Count"));
            this.comboBoxPrintMode_nlx.SelectedIndex = 0;
            
            this.cmbUnit_nlx.Items.Add(Language.getMsg("Centigrade"));
            this.cmbUnit_nlx.Items.Add(Language.getMsg("Degrees Fahrenheit"));
            this.cmbUnit_nlx.SelectedIndex = 0;
            
            this.cbUseGPRS_nlx.Items.Add(Language.getMsg("Y"));
            this.cbUseGPRS_nlx.Items.Add(Language.getMsg("N"));         
            this.cbUseGPRS_nlx.SelectedIndex = 0;
            
            this.Show();

            this.txtDId_nlx.Focus();
            AppCmd cmd = new AppCmd();
            cmd.ReadConfig(ref contentHashTable, Application.StartupPath + "\\set.xml");
            ContentTableToUiController(true);
        }

        /// <summary>
        /// 根据设备类型更新UI界面 
        /// </summary>
        private bool UpdateUiBaseOnDeviceType()
        {
            bool ret = true;

            panelMonitor1.Enabled = false;
            panelMonitor2.Enabled = false;
            panelMonitor3.Enabled = false;
            panelMonitor4.Enabled = false;

            this.Text = Language.getMsg("DeviceTypeMsg");

            return ret;
        }

        public void ConfigurationReadBufferAnalyse()
        {
            cmdModel model;
            try
            { 
                AppCmd cmd = new AppCmd();

                //读取配置文件，组成列表
                List<cmdModel> listc= cmd.ReadConfigCmd();
           
                //根据buffer，对列表进行更新
                for (int i = 0; i < listc.Count; i++)
                { 
                   model = listc[i]; 
                   int Address = Convert.ToInt32(model.Address, 16);
                   int length = Convert.ToInt32(model.Max);
                   byte[] valueBuffer = new byte[length];
                   Array.Copy(ConfigurationReadBuffer, Address, valueBuffer, 0, length);
                   string val = "";
                   if (model.CmdName == "allowClose_是否允许本地关机_2.2")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);

                       if (Sel == 4 || Sel == 5 || Sel == 6 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else if (model.CmdName == "contactType_数据协议_42")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 2 || Sel == 3 || Sel == 7)
                       {
                           val = "HRT100";
                       }
                       else
                       {
                           val = "modbus/tcp";
                       }
                   }
                    else if (model.CmdName == "connectionType_网络协议_43")
                   {//!@todo 修改成与操作实现
                        int Sel = 0;
                        int.TryParse(valueBuffer[0].ToString(), out Sel);
                        if (Sel == 1 || Sel == 3 || Sel == 5 || Sel == 7)//这个代码写的稀烂的，直接使用bit与操作即可
                    
                        {
                            val = "UDP";
                        }
                        else
                        {
                            val = "TCP";
                        }
                    }
                   else if (model.CmdName == "isGPRS_启用GPRS_44")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 4 || Sel == 6 || Sel == 7 || Sel == 5)
                       {
                           val = Language.getMsg("Y");
                       }
                       else
                       {
                           val = Language.getMsg("N");
                       }
                   }
                   else if (model.CmdName == "alarmPhone_报警前拨打电话_2.1")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 2 || Sel == 3 || Sel == 6 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else if (model.CmdName == "everyOutTime_每日10时发送日报信息_2")
                   {//!@todo 修改成与操作实现
                       int Sel = 0;
                       int.TryParse(valueBuffer[0].ToString(), out Sel);
                       if (Sel == 1 || Sel == 3 || Sel == 5 || Sel == 7)
                       {
                           val = "1";
                       }
                       else
                       {
                           val = "0";
                       }
                   }
                   else
                   {
                       if (model.Type == "1")
                       {
                           byte[] vals = new byte[4];
                           Array.Copy(valueBuffer, 0, vals, 0, valueBuffer.Length);
                           uint num = 0;
                           num = BitConverter.ToUInt32(vals, 0);
                           val = num.ToString();
                       }
                       else if (model.Type == "2")
                       {
                           float num = 0;
                           num = BitConverter.ToSingle(valueBuffer, 0);
                           val = num.ToString();
                       }
                       else if (model.Type == "3")
                       {
                           val = UCS2Tostring(valueBuffer);                           
                       }
                       else
                       {
                           val = Encoding.UTF8.GetString(valueBuffer);
                       }
                   }
                   contentHashTable[model.CmdName] = val;
                }
            }
            catch (Exception ex)
            {
                Exception s=ex;
            }  
        }

        public void ReadConfigurationUpdateUI()
        {
            TitlePanel.Visible = false;
            ContentTableToUiController(false);
            SetProgressBarPercent(0);
            MessagShow(Language.getMsg("ReadOK"));
        }
        
        /// <summary>
        /// 设置进度条
        /// </summary>
        /// <param name="percent"></param>
        public void SetProgressBarPercent(int percent)
        {
            progressBar1.Value = percent;;
        }
        /// <summary>
        /// 根据数据发送包的数量，更新进度条
        /// </summary>
        public void UpdateProgressBarPercent()
        {
            progressBar1.Value = 100 * sendNum / listSendModelBuffer.Count;
        }
        public void wSetprogressBar()
        {            
            TitlePanel.Visible = false;
            MessagShow(Language.getMsg("DownOK"));
        }
        /// <summary>
        /// 获取到设备类型回调接口
        /// </summary>
        /// <param name="type"></param>
        private void GetDeviceTypeCallback(string type)
        {
            DeviceType = type;

            //更新显示
            this.BeginInvoke(new UpdateUiCallBack(UpdateUiBaseOnDeviceType));
        }

        /// <summary>
        /// 存储的数据的读取回调接口
        /// </summary>
        /// <param name="data"></param>
        private void StoredDataReadCallBack(byte[] data)
        {
            try
            {
                switch (OngogingReadType)
                {
                    case 3:
                        if (sendNum == listSendModelBuffer.Count)
                        {
                            Array.Copy(data, 0, ConfigurationReadBuffer, listSendModelBuffer[sendNum - 1].Sort - data.Length, data.Length);
                            OngogingReadType = 0;

                            //数据读取完成

                            //读取设置文件，根据文件中的地址项解析数据，并更新hash table
                            //解析完成后根据名称赋值控件
                            ConfigurationReadBufferAnalyse();

                            try
                            {
                                if (InvokeRequired)
                                {
                                    this.BeginInvoke(new SetModelCallBack(ReadConfigurationUpdateUI));//异步执行  
                                }
                                else
                                {
                                    ReadConfigurationUpdateUI();
                                }
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }
                        else
                        {
                            //读取到部分数据
                            Array.Copy(data, 0, ConfigurationReadBuffer, listSendModelBuffer[sendNum - 1].Sort - 32, data.Length);
                            protocol.cmdType = 4;
                            if (protocol.IsOpen)
                            {
                                this.BeginInvoke(new SetModelCallBack(UpdateProgressBarPercent));

                                SendData(listSendModelBuffer[sendNum].Cmdbyte);
                            }
                            else
                            {
                                MessagShow(Language.getMsg("CmdExceptionMsg"));
                            }
                            sendNum = sendNum + 1;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("StoredDataReadCallBack exception:" + ex.ToString());
                throw ex;
            }
        }

        /// <summary>
        /// 读到的命令回调
        /// </summary>
        /// <param name="Msg"></param>
        private void CmdDataReadCallBack(string Msg)
        {
            const string ASCII_ACK_VALUE = "6";
            const string ASCII_NAK_VALUE = "E";

            switch (OngogingReadType)
            { 
                case  2:
                    if (Msg == ASCII_ACK_VALUE)
                    {  
                       
                    }
                    break;
                case 1:
                    if (Msg == ASCII_ACK_VALUE)
                    {
                        if (sendNum == listSendModelBuffer.Count)
                        {
                            OngogingReadType=0;
                            this.BeginInvoke(new SetModelCallBack(wSetprogressBar));//异步执行                              
                            SendData("start");//重启
                        }
                        else
                        {
                            protocol.cmdType = 3;
                           
                            if (protocol.IsOpen)
                            {
                                this.BeginInvoke(new SetModelCallBack(UpdateProgressBarPercent));

                                SendData(listSendModelBuffer[sendNum].Cmdbyte);
                                sendNum = sendNum + 1;
                            }
                            else
                            {
                                MessagShow(Language.getMsg("CmdExceptionMsg"));
                            }
                        }
                    }
                    else
                    {
                        MessagShow(Language.getMsg("DownError"));
                    }
                    break;
                default:
                    break;
            }
        }
       
        private void button4_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        /// <summary>
        /// 读取设备参数
        /// 目前无法重复触发，需要思考原因
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                SetDeviceEnterControlMode();

                SetDeviceEnterProgramMode();

                TitlePanel.Visible = true;
                SetProgressBarPercent(0);

                TitleMsg.Text = Language.getMsg("ReadTitle");
                StartReadConfigFromDevice();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            AllSelect = false;
            int sIndex = tabControl1.SelectedIndex;
            if (sIndex < 3) // sIndex < 2
            { 
                tabControl1.SelectedIndex = sIndex + 1;
            }
            if (this.btnNext.Text == Language.getMsg("Down"))
            {              
                TitleMsg.Text = Language.getMsg("DownTitle");
                TitlePanel.Visible = true;
                SetProgressBarPercent(0);
                StartDownloadToDevice();

                Console.WriteLine("download");
            } 
            else if (sIndex + 1 == 4) // sIndex + 1 == 3
            {
                this.btnNext.Text = Language.getMsg("Down");
                btnSave.Visible = true;
                button1.Visible = false;
                btnRead.Visible = false;
                this.tabControl1.Visible = false;
                this.dataGridView1.Visible = true; 
                this.dataGridView1.BeginInvoke(new GdvAddItemCallback(grvList)); //将所有的数据形成data grid
            }           

            this.btnBack.Visible = true;
        }

        /// <summary>
        /// 下载参数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //写入app.config
                UiControllerToContentTable();
                //写入配置文件
                AppCmd cmd = new AppCmd();
                cmd.WriteConfig(contentHashTable);
                this.saveFileDialog1.ShowDialog(); 
            }
            catch (Exception ex)
            {

                MessagShow(Language.getMsg("SaveErrorMsg"));
            }
           
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            AllSelect = false;//禁止选项卡
            AppCmd cmd = new AppCmd(); 
           int sIndex = tabControl1.SelectedIndex;
           sIndex = this.btnNext.Text == Language.getMsg("OK") || this.btnNext.Text == Language.getMsg("Down")  ? 5 : sIndex;// 4:sIndex
           if (sIndex > 0)
           {
              
               tabControl1.SelectedIndex = sIndex - 1;
           }
           if (this.btnNext.Text == Language.getMsg("OK") || this.btnNext.Text == Language.getMsg("Down"))
           {
               UiControllerToContentTable();
               cmd.WriteConfig(contentHashTable);
               btnRead.Visible = true;
               button1.Visible = true;
               btnSave.Visible = false;
               this.dataGridView1.Visible = false;
               this.tabControl1.Visible = true;
               
           }
           this.btnNext.Text =  Language.getMsg("Next");
           if (sIndex - 1 == 0)
           {
               this.btnBack.Visible = false;
           }
             
        }

        /// <summary>
        /// 形成列表样式
        /// </summary>
        public void grvList()
        {
            Console.WriteLine("grvList");
            UiControllerToContentTable();
            Console.WriteLine("##` grvList");
            list.Clear();
            foreach (DictionaryEntry deHB in contentHashTable)
            {
                ConfigModel m = new ConfigModel();

                try
                {
                    m.NameKey = deHB.Key.ToString().Split('_')[0];//关键字
                    //值
                    m.DeviceVal = deHB.Value.ToString() == "False" ? Language.getMsg("N") : deHB.Value.ToString() == "True" ? Language.getMsg("Y") : deHB.Value.ToString();
                    m.ChineseKey = Language.getMsg(deHB.Key.ToString().Split('_')[0] + "Title");//根据关键字加Title获取关键字对应的参数项名
                    m.Sort = double.Parse(deHB.Key.ToString().Split('_')[2]);//排列方式
                    if (string.IsNullOrEmpty(m.DeviceVal))
                    {
                        m.DeviceVal = "";
                    }
                    list.Add(m);
                }
                catch (System.Exception ex)
                {

                }

            }
            dataGridView1.DataSource = list.OrderBy(o => o.Sort).ToList<ConfigModel>();
        }

        /// <summary>
        /// 开始将参数下载到设备
        /// </summary>
        public void StartDownloadToDevice()
        {
            try
            {
                SetDeviceEnterProgramMode();

                AppCmd cmd = new AppCmd();

                //根据所有的控件值更新hash table
                UiControllerToContentTable();

                cmd.WriteConfig(contentHashTable);//将所有的新值写入配置文件

                OngogingReadType = 1;
                protocol.cmdType = 3;//PROGRAM协议有一个随机数的交互流程

                //cmdlist = cmd.ReadConfigCmd();//从配置文件读取配置组成

                packageSendcmd("w");//打包

                if (protocol.IsOpen)
                {
                    //立即启动发送第一个缓冲区
                    SendData(listSendModelBuffer[0].Cmdbyte);
                }
                else
                {
                    TitlePanel.Visible = false;
                    MessagShow(Language.getMsg("connetOff"));
                }

                //初始化发送的缓冲区偏移
                sendNum = 0;
                sendNum = sendNum + 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("###4    public void down() EXCEPTION : " + ex);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                string path = this.openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(path))
                {
                    AppCmd cmd = new AppCmd();
                    cmd.ReadConfig(ref contentHashTable, path);
                    ContentTableToUiController(true);
                    cmd.WriteConfig(contentHashTable);
                    MessagShow(Language.getMsg("ReadFileOk"));
                }
            }
            catch (Exception ex)
            {
                MessagShow(Language.getMsg("ReadFileError"));
            }
        }
        /// <summary>
        /// 打开文件OK回调接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openFileDialog2_FileOk(object sender, CancelEventArgs e) 
        {
            string filepath = openFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            cmd.copy(filepath);            
        }

        /// <summary>
        ///  保存文件OK回调接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string path = this.saveFileDialog1.FileName;
            AppCmd cmd = new AppCmd();
            if (cmd.copy(path))
            {
                MessagShow(Language.getMsg("SaveOK"));
            }
            else
            {
                MessagShow(Language.getMsg("SaveError"));
            }
        }

        /// <summary>
        /// 根据读或者写的命令进行打包
        /// 输出listSendModelBuffer，发送命令缓冲包
        /// </summary>
        /// <param name="type"></param>
        public void packageSendcmd(string type)
        {            
            //读取所有的
            List<cmdModel> list = new List<cmdModel>();
            listSendModelBuffer = new List<SendModel>();
            int starAddress = 0;//起始地址
            AppCmd cmd = new AppCmd();
            int i=0;
            switch (type)
            { 
                case  "R":  

                    //读取所有的配置数据，读取完成后将配置数据进行分解

                    starAddress = readStartAddress;
                    while (starAddress <= readEndAddress)
                    {
                        SendModel sModel = new SendModel();
                        //组装数据从开始->结尾  
                        int byteLength = 0;
                        byte[] address = intTobytenew(starAddress);
                        //每包递减
                        const int singlePacketReadLength = 32;
                        if (readEndAddress - starAddress < singlePacketReadLength)
                        {
                            byteLength = readEndAddress - starAddress;
                            readEndAddress = 0;
                            starAddress = starAddress + byteLength; 
                        }
                        else
                        {
                            byteLength = singlePacketReadLength;
                            starAddress = starAddress + byteLength;
                        } 
                        byte[] bLength = intTobytenew(byteLength); 
                        int totalLength = 4;
                        byte[] data = new byte[totalLength];

                        //打包
                        data[0] =   HusbHelp.HexStringToByte("R")[0]; 
                        data[1] = address[0];
                        data[2] = address[1];
                        data[3] = bLength[1];

                        sModel.Cmdbyte = data;
                        sModel.Sort = starAddress; 
                        listSendModelBuffer.Add(sModel);
                        i++; 
                    } 
                    //组合
                    break;
                case "w":
                    string t = "";
                    int starIndex = 0;
                    string name = "";
                    try
                    {
                        List<cmdModel> cmdList = cmd.ReadConfigCmd().OrderBy(l => l.Sort).ToList<cmdModel>();
                        //cmdModel maxModel = cmdList[cmdList.Count-1];   //获取最后一项，其地址值+4即为整个参数缓冲区大小

                        //int maxDataBufferLength = Convert.ToInt32(maxModel.Address, 16)+4;//计算包总长度

                        ConfigurationReadBuffer = new byte[writeEndAddress];
                        byte[]  writeAllDataBuffer = new byte[writeEndAddress];      //参数待写入缓冲区
                        string sel1 = "";
                        string sel = "";
                        foreach (cmdModel cModel in cmdList)
                        {
                            t = cModel.Type;
                            int Max = Convert.ToInt32(cModel.Max);
                            name = cModel.CmdName;
                            byte[] val;
                        
                            //根据每个设置命令的名称进行分别的处理
                            //先判断是否为特殊名称
                            if (cModel.CmdName == "contactType_数据协议_42")
                            {
                                if (cModel.Val == "HRT100")
                                {
                                    sel = "1" + sel; 
                                }
                                else
                                {
                                    sel = "0" + sel;
                                }                           
                                //待处理bit
                                continue;
                            }
                            else if (cModel.CmdName == "connectionType_网络协议_43")
                            {

                                if (cModel.Val == "UDP")
                                {
                                    sel =sel+ "1";
                                }
                                else
                                {
                                    sel = sel + "0";
                                }
                                //待处理bit//在    cModel.CmdName == "isGPRS_启用GPRS_44"   处进行统一变更处理
                                continue;
                            }
                            else if (cModel.CmdName == "unit_温度单位_34")
                            {
                                byte[] vals = intTobytenew(int.Parse(cModel.Val));
                                val = new byte[1];
                                Array.Copy(vals, 1, val, 0, 1);
                            }
                            else if (cModel.CmdName == "everyOutTime_每日10时发送日报信息_2")
                            {
                                if (cModel.Val == "1")
                                {
                                    sel1 = sel1 + "1";
                                }
                                else
                                {
                                    sel1 = sel1 + "0";
                                }
                                continue;
                            }
                            else if (cModel.CmdName == "alarmPhone_报警前拨打电话_2.1")
                                { 
                                if(cModel.Val=="1")
                                {
                                    sel1 =   "1"+sel1; 
                                }else
                                {
                                    sel1 =   "0"+sel1; 
                                }
                                continue;
                                }
                            else if (cModel.CmdName == "allowClose_是否允许本地关机_2.2")
                                {
                                    if (cModel.Val == "1")
                                    {
                                        sel1 = "1" + sel1;
                                    }
                                    else
                                    {
                                        sel1 = "0" + sel1;
                                    }
                                    byte[] vals = intTobytenew(Convert.ToInt32(sel1, 2));
                                    val = new byte[1];
                                    Array.Copy(vals, 1, val, 0, 1);
                                }  
                            else if (cModel.CmdName == "isGPRS_启用GPRS_44")
                                {
                                    if (cModel.Val ==Language.getMsg("Y"))
                                    {
                                        sel = "1" + sel;
                                    }
                                    else
                                    {
                                        sel = "0" + sel;
                                    }

                                    byte[] vals = intTobytenew(Convert.ToInt32(sel, 2));
                                    val = new byte[1];
                                    Array.Copy(vals, 1, val, 0, 1);
                                }
                            else if (cModel.CmdName == "adress_安装位置_3" )
                            {
                                if (Global.Language == "English")
                                {
                                    val = System.Text.Encoding.ASCII.GetBytes(cModel.Val);
                                }
                                else
                                {
                                    val = stringToUCS2(cModel.Val);
                                }
                            }
                            else
                            {
                                //处理通用种类
                                if (cModel.Type == "1")//将整数字符串转换成byte字节序列
                                {                                
                                    val = new byte[Max];
                                    int num= 0;
                                    int.TryParse(cModel.Val, out num);
                                    byte[] vals = intTobyteold(num);
                                    Array.Copy(vals,0, val, 0, Max);                                   
                                }
                                else if (cModel.Type == "2")//将浮点数字符串转换成byte字节序列
                                {                                
                                    float  num=0;
                                    float.TryParse(cModel.Val, out num);
                                    val = floatTobyte(num); 
                                }
                                else if (cModel.Type == "3") //将字符串按照语言转换成UCS2字节序列
                                {                                                                       
                                    val = stringToUCS2(cModel.Val);                                                 
                                }
                                else if (cModel.Type == "5")//特殊种类
                                {
                                    val = new byte[Max];
                                    byte[] vals = cmd.strToToHexByte(cModel.Val);//cMode.Val必须要有指定的Max长
                                    Array.Copy(vals, 0, val, 0, Max);  
                                }
                                else
                                {
                                    val = HusbHelp.HexStringToByte(cModel.Val);//直接将16进制字符串转换成byte
                                }
                            }
                        
                            if (Max >val.Length)
                            {
                                Max = val.Length;
                            }

                            //将数据值赋值到wData
                            starIndex = Convert.ToInt32(cModel.Address, 16);//将地址字符串转换成数字
                        
                            Array.Copy(val, 0, writeAllDataBuffer, starIndex, Max);//将此单项参数的值复制到缓冲区中的对应的偏移处   
                        }

                        //将整个缓冲区分包进行发送
                        //组装数据从开始->结尾 
                        starIndex = writeStartAddress;
                        while (starIndex < writeEndAddress)
                        {  
                            SendModel sModel = new SendModel();
                            int byteLength = 0;//本包长度
                            int CheckCode = 0;//校验码
                            const int singlePacketWriteLength = 32;
                            if (writeEndAddress - starIndex < singlePacketWriteLength)
                            {
                                byteLength = writeEndAddress - starIndex; 
                            }
                            else
                            {
                                byteLength = singlePacketWriteLength; 
                            }
                            byte[] data = new byte[byteLength];
                            Array.Copy(writeAllDataBuffer, starIndex, data, 0, byteLength); 
                            byte[] address = intTobytenew(starIndex);//得到地址 
                            byte[] bLength = intTobytenew(byteLength); //得到数据长度  
                            foreach (byte v in address)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            foreach (byte v in bLength)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            foreach (byte v in data)
                            {
                                int bv = 0;
                                int.TryParse(v.ToString(), out bv);
                                CheckCode += bv;
                            }
                            byte[] Bcheck =BitConverter.GetBytes(CheckCode);

                            byte[] sendData = new byte[4 + data.Length + 1];
                            sendData[0] = HusbHelp.HexStringToByte("w")[0];
                            sendData[1] = address[0];
                            sendData[2] = address[1];
                            Array.Copy(bLength, 1, sendData, 3, 1);
                            Array.Copy(data, 0, sendData, 4, byteLength);
                            Array.Copy(Bcheck, 0, sendData, 4 + byteLength, 1); 

                            starIndex = starIndex + byteLength;
                            sModel.Cmdbyte = sendData;
                            listSendModelBuffer.Add(sModel);
                        }                  
                    }
                    catch (Exception ex)
                    {                        
                        throw ex;
                    }
                        break;
                default:
                    break;
            }
            //listSendModel = listSendModel;
        }
        
        private void txtPhone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }

        public void MathVilid(KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '\b'))
            {
                e.Handled = true;
                MessagShow(Language.getMsg("EnterNumbersMsg"));

            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtPhone2_KeyPress(object sender, KeyPressEventArgs e)
        {
            MathVilid(e);
        }
        
        private void txtMonitor1min_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;

            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v=0;
            if (!regex.IsMatch(senderText.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                senderText.Text = "0";
            }
            else
            {
                double.TryParse(senderText.Text.Trim(), out v);
                if (v < -1000.00 || v > 1000.99)
                {
                    senderText.Text = "0";
                    MessagShow(Language.getMsg("EnterNumbersrangeMsg"));
                } 
            }
        }

        private void txtMonitor1max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor1min_nlx;
            maxCheck(minTxt, senderText);
        }

        private void txtMonitor7max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor1min_nlx;
            maxCheck(  senderText);
        }

        private void maxCheck( TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                maxTxt.Text = "0";
            }
            
        }

        private void maxCheck(TextBox minTxt, TextBox maxTxt)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]+(\.[0-9]+)?$");
            double v = 0;
            if (!regex.IsMatch(maxTxt.Text))
            {
                MessagShow(Language.getMsg("EnterNumbersMsg"));
                maxTxt.Text = "0";
            }
            else
            {
                double.TryParse(maxTxt.Text.Trim(), out v);
                if (v < -1000.00 || v > 1000.99)
                {
                    maxTxt.Text = "0";
                    MessagShow(Language.getMsg("EnterNumbersrangeMsg"));
                }
                else
                {
                    double minv = 0;
                    double.TryParse(minTxt.Text, out minv);
                    if (v < minv)
                    {
                        maxTxt.Text = "0";
                        MessagShow(Language.getMsg("MaxRMinMsg"));
                    }

                }
            }
        }

        private void txtMonitor2max_Leave(object sender, EventArgs e)
        {

            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor2min_nlx;
            maxCheck(minTxt, senderText);
        }

        private void txtMonitor3max_Leave(object sender, EventArgs e)
        {
            TextBox senderText = (TextBox)sender;
            TextBox minTxt = this.txtMonitor3min_nlx;
            maxCheck(minTxt, senderText);
        }

        /// <summary>
        /// 控制进入配置模式
        /// </summary>
        private void SetDeviceEnterProgramMode()
        {
            if (!IsInProgramMode)
            {
                IsInProgramMode = true;//进入配置模式
                protocol.cmdType = 2;
                OngogingReadType = 2;
                if (protocol.IsOpen)
                {
                    SendData("PROGRAM");//进入计算机模式
                    Thread.Sleep(500);
                }
                else
                {
                    MessagShow(Language.getMsg("CmdExceptionMsg"));
                }
            }
        }

        /// <summary>
        /// 控制进入控制模式
        /// </summary>
        private void SetDeviceEnterControlMode()
        {
            protocol.cmdType = 1;//读取的模式为获取设备类型
            OngogingReadType = 2;
            if (protocol.IsOpen)
            {
                SendData("$$$$$$$");//进入计算机模式
                Thread.Sleep(500);
            }
            else
            {
                MessagShow(Language.getMsg("CmdExceptionMsg"));
            }            
        }


        
        /// <summary>
        /// 读取参数
        /// </summary>
        private void StartReadConfigFromDevice()
        {
            try
            {
                Console.WriteLine("StartReadConfigFromDevice");


                AppCmd cmd = new AppCmd();

                OngogingReadType = 3;

               // cmdlist = cmd.ReadConfigCmd();

                packageSendcmd("R");
                protocol.cmdType = 4;
                if (protocol.IsOpen)
                {
                    SendData(listSendModelBuffer[0].Cmdbyte);//启动发送第一个命令包
                }
                else
                {
                    MessagShow(Language.getMsg("CmdExceptionMsg") );
                }

                sendNum = 0;
                sendNum = sendNum + 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Read exception:" + ex);
                throw ex;
            }
        }
       
        /// <summary>
        /// 根据hash table中的数据更新 UI信息
        /// </summary>
        private void ContentTableToUiController(bool ReadIDFlag)
        {
            this.txtDId_nlx.Text = ReadIDFlag == true ? "" : contentHashTable["deviceID_设备ID_1"].ToString();

            int AllowClose = 0;
            int.TryParse(contentHashTable["allowClose_是否允许本地关机_2.2"].ToString(), out AllowClose);
            this.cmbAllowClose_nlx.SelectedIndex = AllowClose;

            this.txtTakeTime_nlx.Text = contentHashTable["takeTime_数据记录间隔(秒)_30"].ToString();
            this.txtTransfersTime_nlx.Text = contentHashTable["transfersTime_数据上传间隔(秒)_33"].ToString();
            this.txtDelayAlarm_nlx.Text = contentHashTable["delayAlarm_报警确认时间(分)_32"].ToString();
    
            this.txtGPRScontact_nlx.Text = contentHashTable["GPRScontact_GPRS接入点_35"].ToString();
            this.txtGPRSname_nlx.Text = contentHashTable["GPRSname_GPRS用户名_36"].ToString();
            this.txtGPRSpwd_nlx.Text = contentHashTable["GPRSpwd_GPRS密码_37"].ToString();
            this.txtDNS_nlx.Text = contentHashTable["DNS_DNS地址_39"].ToString();
            this.txtIP_nlx.Text = contentHashTable["IP_服务器地址_38"].ToString();
            this.txtPort_nlx.Text = contentHashTable["port_端口_40"].ToString();
            this.txtBeatTime_nlx.Text = contentHashTable["beatTime_GPRS心跳时间(秒)_41"].ToString();
            this.cbContactType_nlx.SelectedItem = contentHashTable["contactType_数据协议_42"].ToString();
            //connectionType_数据协议_42   connectionType_网络协议_43
            this.cbConnectionType_nlx.SelectedItem = contentHashTable["connectionType_网络协议_43"].ToString();
            this.cbUseGPRS_nlx.SelectedItem = contentHashTable["isGPRS_启用GPRS_44"].ToString();
            this.txtOutTime_nlx.Text = contentHashTable["outTime_GPRS空闲超时(秒)_45"].ToString();
            this.txtBackTime_nlx.Text = contentHashTable["backTime_回应超时(秒)_46"].ToString();

            this.textServer2IP_nlx.Text = contentHashTable["IP_服务器2地址_47"].ToString();
            this.textServer2Port_nlx.Text = contentHashTable["port_服务器2端口_48"].ToString();
        }

        /// <summary>
        /// 从UI控件读取值
        /// </summary>
        private void UiControllerToContentTable()
        {
            contentHashTable.Clear();

            contentHashTable.Add("deviceID_设备ID_1", this.txtDId_nlx.Text.Trim());

            contentHashTable.Add("allowClose_是否允许本地关机_2.2", this.cmbAllowClose_nlx.SelectedIndex.ToString());
            contentHashTable.Add("EnableAlarm", this.chkEnableAlarm_nlx.Checked == true ? "1" : "0");
            contentHashTable.Add("EnableOverproofRecordPeriod", this.chkEnableOverproolRecordPeriod_nlx.Checked == true ? "1" : "0");

            contentHashTable.Add("takeTime_数据记录间隔(秒)_30", this.txtTakeTime_nlx.Text.Trim());    
            contentHashTable.Add("transfersTime_数据上传间隔(秒)_33", this.txtTransfersTime_nlx.Text.Trim());
            contentHashTable.Add("delayAlarm_报警确认时间(分)_32", this.txtDelayAlarm_nlx.Text.Trim());

            contentHashTable.Add("GPRScontact_GPRS接入点_35", this.txtGPRScontact_nlx.Text.Trim());
            contentHashTable.Add("GPRSname_GPRS用户名_36", this.txtGPRSname_nlx.Text.Trim());
            contentHashTable.Add("GPRSpwd_GPRS密码_37", this.txtGPRSpwd_nlx.Text.Trim());
            contentHashTable.Add("DNS_DNS地址_39", this.txtDNS_nlx.Text.Trim());
            contentHashTable.Add("IP_服务器地址_38", this.txtIP_nlx.Text.Trim());
            contentHashTable.Add("port_端口_40", this.txtPort_nlx.Text.Trim());
            contentHashTable.Add("beatTime_GPRS心跳时间(秒)_41", this.txtBeatTime_nlx.Text.Trim());
            contentHashTable.Add("contactType_数据协议_42", this.cbContactType_nlx.SelectedItem.ToString());
            contentHashTable.Add("connectionType_网络协议_43", this.cbConnectionType_nlx.SelectedItem.ToString());
            contentHashTable.Add("isGPRS_启用GPRS_44", this.cbUseGPRS_nlx.SelectedItem.ToString());
            contentHashTable.Add("outTime_GPRS空闲超时(秒)_45", this.txtOutTime_nlx.Text.Trim());
            contentHashTable.Add("backTime_回应超时(秒)_46", this.txtBackTime_nlx.Text.Trim());

            contentHashTable.Add("IP_服务器2地址_47", this.textServer2IP_nlx.Text.Trim());
            contentHashTable.Add("port_服务器2端口_48", this.textServer2Port_nlx.Text.Trim());
        }
        
        /// <summary>
        /// 定时触发的接口，按照指定间隔将发送数据缓冲区中发送出去
        /// 目前的实现中采取的是每个发送包的回调接口中直接发送下一个包，故此不应用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendCmdBuffer_Tick(object sender, EventArgs e)
        {
            /*
            if (SendType == 1 && sendNum < listSendModelBuffer.Count - 1)
            {                
                if (ReSendNum == sendNum)
                {
                    RelCount++;
                }
                else
                {
                    ReSendNum = sendNum;
                    RelCount = 1;
                }
                if (RelCount == 2)
                {
                    SendData(listSendModelBuffer[sendNum - 1].Cmdbyte);
                }
                else if (RelCount > 2)
                {
                    SendType = 0;
                    TitlePanel.Visible = false;
                    MessagShow(Language.getMsg("CmdExceptionMsg"));                   
                }             
            }*/
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            e.Cancel = AllSelect;
            AllSelect = true;
        }



        /******************************************
         * 多种数据类型转换方法
         */
        #region 多种数据类型转换方法
        /// <summary>
        /// float转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] floatTobyte(float val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            // List<byte> blist = new List<byte>(); blist.AddRange(aa); blist.RemoveAll(b => b == 0);
            return aa;
        }
        /// <summary>
        /// int转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] intTobyte(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
            List<byte> blist = new List<byte>(); blist.AddRange(aa); blist.RemoveAll(b => b == 0);
            return blist.ToArray();
        }
        /// <summary>
        /// int转byte
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private byte[] intTobyteold(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);

            return aa;
        }
        private byte[] intTobytenew(int val)
        {
            byte[] aa = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(aa);
            byte[] Rebyte = new byte[2];
            Rebyte[0] = aa[2];
            Rebyte[1] = aa[3];
            return Rebyte;
        }

        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] HexStringToBytes(string msg)
        {
            byte[] bstr = new byte[1024];
            bstr = Encoding.BigEndianUnicode.GetBytes(msg);
            return bstr;
        }

       /// <summary>
       /// string To Ucs
       /// </summary>
       /// <param name="str">字符串</param>
       /// <returns></returns>
        private byte[] stringToUCS2(string str)
        {
            byte[] Relval = new byte[str.Length*2];
            string[] strs = new string[str.Length]; 
           char[] arr = str.ToCharArray(); 
            for (int i = 0; i < arr.Length; i++)
            {
                strs[i] = arr[i].ToString();
            }
            for (int i = 0; i < strs.Length; i++)
            {
                string s = strs[i];
                byte[] sUcs = new byte[2];
                sUcs=System.Text.Encoding.Unicode.GetBytes(s);
                sUcs.Reverse();
         
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(sUcs);
                List<byte> blist = new List<byte>(); blist.AddRange(sUcs); 
                Array.Copy(blist.ToArray(), 0, Relval, i * 2, blist.Count);
              
            }
            return Relval;
        }
        
        /// <summary>
        ///  Ucs  To  string
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns></returns>
        private string UCS2Tostring(byte[] by)
        {
            string RelStr = "";
            int star = 0;
            for (int i = 0; i < by.Length/2; i++)
            {
                
                byte[] val = new byte[2];
                Array.Copy(by, star, val,0,2);
                val.Reverse();
                Array.Reverse(val);
                RelStr = RelStr + System.Text.Encoding.Unicode.GetString(val);
                star = star + 2;

            }
            return RelStr;
        }
        #endregion

        /******************************************
        * 其它内部方法
        */
        private void SystemConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            protocol.DataResponse -= new TH6100Config.ConfigProtocol.DataResponseEventHandler(CmdDataReadCallBack);
            protocol.BDataResponse -= new TH6100Config.ConfigProtocol.BDataResponseEventHandler(StoredDataReadCallBack);
            protocol.GetDeviceType -= new TH6100Config.ConfigProtocol.DataResponseTypeEventHandler(GetDeviceTypeCallback);
            this.Hide();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void MessagShow(string Msg)
        {
            UserMessageBox box = new UserMessageBox(Msg);
           
            box.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDId_nlx_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                string EQCodel = txtDId_nlx.Text.Trim().ToUpper();
                int starIndex = 0;
                int endIndex = 0;
                starIndex = EQCodel.IndexOf("ID=");
                if (starIndex > -1)
                {
                    starIndex += 3;
                    endIndex = EQCodel.Length ;
                    this.txtDId_nlx.Text = EQCodel.Substring(starIndex,  endIndex - starIndex  );
                }
                e.Handled = true;
                this.txtAdress_nlx.Focus();
            }
        }

        private void btnTestModel_nlx_Click(object sender, EventArgs e)
        {
            if (test == null)
            {
                test = new TestForm(null);   
            }
            test.Show();
        }



        #region 数据发送接口
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="sendMsg"></param>
        private void SendData(byte[] data)
        {
            protocol.SendData(data);
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="sendMsg"></param>
        private void SendData(string cmd)
        {
            protocol.SendData(cmd);
        }
        #endregion
    }
}
