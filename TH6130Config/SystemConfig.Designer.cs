﻿namespace TH6100Config
{
    partial class SystemConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemConfig));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TitlePanel = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.TitleMsg = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtPrintAmount_nlx = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.comboBoxPrintMode_nlx = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtDBVersion_nlx = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.chkEnableOverproolRecordPeriod_nlx = new System.Windows.Forms.CheckBox();
            this.label62 = new System.Windows.Forms.Label();
            this.chkEnableAlarm_nlx = new System.Windows.Forms.CheckBox();
            this.label61 = new System.Windows.Forms.Label();
            this.cmbAllowClose_nlx = new System.Windows.Forms.ComboBox();
            this.labAllowClose = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtDId_nlx = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkAlarmPhone_nlx = new System.Windows.Forms.CheckBox();
            this.ckeveryOutTime_nlx = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label65 = new System.Windows.Forms.Label();
            this.txtSamplePeriod_nlx = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.cmbUnit_nlx = new System.Windows.Forms.ComboBox();
            this.panelMonitor4 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.txtMonitor4datum_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor4hysteresis_nlx = new System.Windows.Forms.TextBox();
            this.label51_nlx = new System.Windows.Forms.Label();
            this.txtMonitor4_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor4min_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor4max_nlx = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.panelMonitor3 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMonitor3datum_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor3hysteresis_nlx = new System.Windows.Forms.TextBox();
            this.label16_nlx = new System.Windows.Forms.Label();
            this.txtMonitor3_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor3min_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor3max_nlx = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panelMonitor2 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMonitor2datum_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor2_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor2hysteresis_nlx = new System.Windows.Forms.TextBox();
            this.label18_nlx = new System.Windows.Forms.Label();
            this.txtMonitor2min_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor2max_nlx = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panelMonitor1 = new System.Windows.Forms.Panel();
            this.txtMonitor1_nlx = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15_nlx = new System.Windows.Forms.Label();
            this.txtMonitor1min_nlx = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMonitor1datum_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor1hysteresis_nlx = new System.Windows.Forms.TextBox();
            this.txtMonitor1max_nlx = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTakeTime_nlx = new System.Windows.Forms.TextBox();
            this.txtDelayAlarm_nlx = new System.Windows.Forms.TextBox();
            this.txtTransfersTime_nlx = new System.Windows.Forms.TextBox();
            this.txtVisualAlarm_nlx = new System.Windows.Forms.TextBox();
            this.txtAlarmTime_nlx = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textServer2IP_nlx = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textServer2Port_nlx = new System.Windows.Forms.TextBox();
            this.cbUseGPRS_nlx = new System.Windows.Forms.ComboBox();
            this.lab1 = new System.Windows.Forms.Label();
            this.cbContactType_nlx = new System.Windows.Forms.ComboBox();
            this.cbConnectionType_nlx = new System.Windows.Forms.ComboBox();
            this.txtBackTime_nlx = new System.Windows.Forms.TextBox();
            this.txtOutTime_nlx = new System.Windows.Forms.TextBox();
            this.txtBeatTime_nlx = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtIP_nlx = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtGPRSpwd_nlx = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtPort_nlx = new System.Windows.Forms.TextBox();
            this.txtDNS_nlx = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtGPRSname_nlx = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtGPRScontact_nlx = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtAdress_nlx = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPhone10_nlx = new System.Windows.Forms.TextBox();
            this.txtPhone1_nlx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPhone2_nlx = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPhone5_nlx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhone3_nlx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPhone8_nlx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhone4_nlx = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPhone7_nlx = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPhone6_nlx = new System.Windows.Forms.TextBox();
            this.txtPhone9_nlx = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.val = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRead = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnTestModel = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.TitlePanel.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panelMonitor4.SuspendLayout();
            this.panelMonitor3.SuspendLayout();
            this.panelMonitor2.SuspendLayout();
            this.panelMonitor1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(372, 368);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "下一步";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(453, 368);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "保存参数";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(288, 368);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "上一步";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(13, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 344);
            this.tabControl1.TabIndex = 8;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Controls.Add(this.TitlePanel);
            this.tabPage1.Controls.Add(this.txtPrintAmount_nlx);
            this.tabPage1.Controls.Add(this.label67);
            this.tabPage1.Controls.Add(this.comboBoxPrintMode_nlx);
            this.tabPage1.Controls.Add(this.label66);
            this.tabPage1.Controls.Add(this.txtDBVersion_nlx);
            this.tabPage1.Controls.Add(this.label63);
            this.tabPage1.Controls.Add(this.chkEnableOverproolRecordPeriod_nlx);
            this.tabPage1.Controls.Add(this.label62);
            this.tabPage1.Controls.Add(this.chkEnableAlarm_nlx);
            this.tabPage1.Controls.Add(this.label61);
            this.tabPage1.Controls.Add(this.cmbAllowClose_nlx);
            this.tabPage1.Controls.Add(this.labAllowClose);
            this.tabPage1.Controls.Add(this.label60);
            this.tabPage1.Controls.Add(this.txtDId_nlx);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.chkAlarmPhone_nlx);
            this.tabPage1.Controls.Add(this.ckeveryOutTime_nlx);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 318);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "基本参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // TitlePanel
            // 
            this.TitlePanel.BackColor = System.Drawing.SystemColors.Control;
            this.TitlePanel.Controls.Add(this.label59);
            this.TitlePanel.Controls.Add(this.TitleMsg);
            this.TitlePanel.Controls.Add(this.progressBar1);
            this.TitlePanel.Location = new System.Drawing.Point(321, 143);
            this.TitlePanel.Name = "TitlePanel";
            this.TitlePanel.Size = new System.Drawing.Size(200, 93);
            this.TitlePanel.TabIndex = 50;
            this.TitlePanel.Visible = false;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(8, 44);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 12);
            this.label59.TabIndex = 13;
            this.label59.Text = "进度：";
            // 
            // TitleMsg
            // 
            this.TitleMsg.AutoSize = true;
            this.TitleMsg.Location = new System.Drawing.Point(15, 12);
            this.TitleMsg.Name = "TitleMsg";
            this.TitleMsg.Size = new System.Drawing.Size(53, 12);
            this.TitleMsg.TabIndex = 12;
            this.TitleMsg.Text = "进度提示";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(72, 39);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(119, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // txtPrintAmount_nlx
            // 
            this.txtPrintAmount_nlx.Location = new System.Drawing.Point(443, 166);
            this.txtPrintAmount_nlx.MaxLength = 100;
            this.txtPrintAmount_nlx.Name = "txtPrintAmount_nlx";
            this.txtPrintAmount_nlx.Size = new System.Drawing.Size(55, 21);
            this.txtPrintAmount_nlx.TabIndex = 128;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(266, 170);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(65, 12);
            this.label67.TabIndex = 127;
            this.label67.Text = "打印条数：";
            // 
            // comboBoxPrintMode_nlx
            // 
            this.comboBoxPrintMode_nlx.FormattingEnabled = true;
            this.comboBoxPrintMode_nlx.Location = new System.Drawing.Point(443, 140);
            this.comboBoxPrintMode_nlx.Name = "comboBoxPrintMode_nlx";
            this.comboBoxPrintMode_nlx.Size = new System.Drawing.Size(163, 20);
            this.comboBoxPrintMode_nlx.TabIndex = 126;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(266, 142);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(89, 12);
            this.label66.TabIndex = 125;
            this.label66.Text = "本地打印方式：";
            // 
            // txtDBVersion_nlx
            // 
            this.txtDBVersion_nlx.Location = new System.Drawing.Point(93, 82);
            this.txtDBVersion_nlx.MaxLength = 100;
            this.txtDBVersion_nlx.Name = "txtDBVersion_nlx";
            this.txtDBVersion_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtDBVersion_nlx.TabIndex = 123;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(21, 85);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(65, 12);
            this.label63.TabIndex = 124;
            this.label63.Text = "数据版本：";
            // 
            // chkEnableOverproolRecordPeriod_nlx
            // 
            this.chkEnableOverproolRecordPeriod_nlx.AutoSize = true;
            this.chkEnableOverproolRecordPeriod_nlx.Location = new System.Drawing.Point(443, 112);
            this.chkEnableOverproolRecordPeriod_nlx.Name = "chkEnableOverproolRecordPeriod_nlx";
            this.chkEnableOverproolRecordPeriod_nlx.Size = new System.Drawing.Size(15, 14);
            this.chkEnableOverproolRecordPeriod_nlx.TabIndex = 95;
            this.chkEnableOverproolRecordPeriod_nlx.UseVisualStyleBackColor = true;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(266, 113);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(113, 12);
            this.label62.TabIndex = 96;
            this.label62.Text = "超标变更记录间隔：";
            // 
            // chkEnableAlarm_nlx
            // 
            this.chkEnableAlarm_nlx.AutoSize = true;
            this.chkEnableAlarm_nlx.Location = new System.Drawing.Point(443, 83);
            this.chkEnableAlarm_nlx.Name = "chkEnableAlarm_nlx";
            this.chkEnableAlarm_nlx.Size = new System.Drawing.Size(15, 14);
            this.chkEnableAlarm_nlx.TabIndex = 93;
            this.chkEnableAlarm_nlx.UseVisualStyleBackColor = true;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(266, 82);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(113, 12);
            this.label61.TabIndex = 94;
            this.label61.Text = "允许本地超标报警：";
            // 
            // cmbAllowClose_nlx
            // 
            this.cmbAllowClose_nlx.FormattingEnabled = true;
            this.cmbAllowClose_nlx.Location = new System.Drawing.Point(443, 50);
            this.cmbAllowClose_nlx.Name = "cmbAllowClose_nlx";
            this.cmbAllowClose_nlx.Size = new System.Drawing.Size(163, 20);
            this.cmbAllowClose_nlx.TabIndex = 92;
            // 
            // labAllowClose
            // 
            this.labAllowClose.AutoSize = true;
            this.labAllowClose.Location = new System.Drawing.Point(266, 52);
            this.labAllowClose.Name = "labAllowClose";
            this.labAllowClose.Size = new System.Drawing.Size(65, 12);
            this.labAllowClose.TabIndex = 91;
            this.labAllowClose.Text = "本地关机：";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(265, 245);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(101, 12);
            this.label60.TabIndex = 14;
            this.label60.Text = "报警前拨打电话：";
            this.label60.Visible = false;
            // 
            // txtDId_nlx
            // 
            this.txtDId_nlx.Location = new System.Drawing.Point(93, 50);
            this.txtDId_nlx.MaxLength = 100;
            this.txtDId_nlx.Name = "txtDId_nlx";
            this.txtDId_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtDId_nlx.TabIndex = 0;
            this.txtDId_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDId_nlx_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 11;
            this.label13.Text = "设备ID：";
            // 
            // chkAlarmPhone_nlx
            // 
            this.chkAlarmPhone_nlx.AutoSize = true;
            this.chkAlarmPhone_nlx.Location = new System.Drawing.Point(442, 246);
            this.chkAlarmPhone_nlx.Name = "chkAlarmPhone_nlx";
            this.chkAlarmPhone_nlx.Size = new System.Drawing.Size(15, 14);
            this.chkAlarmPhone_nlx.TabIndex = 2;
            this.chkAlarmPhone_nlx.UseVisualStyleBackColor = true;
            this.chkAlarmPhone_nlx.Visible = false;
            // 
            // ckeveryOutTime_nlx
            // 
            this.ckeveryOutTime_nlx.AutoSize = true;
            this.ckeveryOutTime_nlx.Location = new System.Drawing.Point(442, 217);
            this.ckeveryOutTime_nlx.Name = "ckeveryOutTime_nlx";
            this.ckeveryOutTime_nlx.Size = new System.Drawing.Size(15, 14);
            this.ckeveryOutTime_nlx.TabIndex = 2;
            this.ckeveryOutTime_nlx.UseVisualStyleBackColor = true;
            this.ckeveryOutTime_nlx.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(265, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 12);
            this.label12.TabIndex = 9;
            this.label12.Text = "每日10时发送日报信息：";
            this.label12.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label65);
            this.tabPage2.Controls.Add(this.txtSamplePeriod_nlx);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.cmbUnit_nlx);
            this.tabPage2.Controls.Add(this.panelMonitor4);
            this.tabPage2.Controls.Add(this.panelMonitor3);
            this.tabPage2.Controls.Add(this.panelMonitor2);
            this.tabPage2.Controls.Add(this.panelMonitor1);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtTakeTime_nlx);
            this.tabPage2.Controls.Add(this.txtDelayAlarm_nlx);
            this.tabPage2.Controls.Add(this.txtTransfersTime_nlx);
            this.tabPage2.Controls.Add(this.txtVisualAlarm_nlx);
            this.tabPage2.Controls.Add(this.txtAlarmTime_nlx);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(643, 318);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "温湿度参数";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(249, 179);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 12);
            this.label65.TabIndex = 55;
            this.label65.Text = "（秒）";
            // 
            // txtSamplePeriod_nlx
            // 
            this.txtSamplePeriod_nlx.Location = new System.Drawing.Point(180, 175);
            this.txtSamplePeriod_nlx.Name = "txtSamplePeriod_nlx";
            this.txtSamplePeriod_nlx.Size = new System.Drawing.Size(62, 21);
            this.txtSamplePeriod_nlx.TabIndex = 54;
            this.txtSamplePeriod_nlx.Text = "5";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(34, 179);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(65, 12);
            this.label64.TabIndex = 53;
            this.label64.Text = "采样间隔：";
            // 
            // cmbUnit_nlx
            // 
            this.cmbUnit_nlx.AutoCompleteCustomSource.AddRange(new string[] {
            "启用",
            "禁用"});
            this.cmbUnit_nlx.FormattingEnabled = true;
            this.cmbUnit_nlx.Location = new System.Drawing.Point(485, 245);
            this.cmbUnit_nlx.Name = "cmbUnit_nlx";
            this.cmbUnit_nlx.Size = new System.Drawing.Size(109, 20);
            this.cmbUnit_nlx.TabIndex = 52;
            this.cmbUnit_nlx.Visible = false;
            // 
            // panelMonitor4
            // 
            this.panelMonitor4.Controls.Add(this.label40);
            this.panelMonitor4.Controls.Add(this.txtMonitor4datum_nlx);
            this.panelMonitor4.Controls.Add(this.txtMonitor4hysteresis_nlx);
            this.panelMonitor4.Controls.Add(this.label51_nlx);
            this.panelMonitor4.Controls.Add(this.txtMonitor4_nlx);
            this.panelMonitor4.Controls.Add(this.txtMonitor4min_nlx);
            this.panelMonitor4.Controls.Add(this.txtMonitor4max_nlx);
            this.panelMonitor4.Controls.Add(this.label56);
            this.panelMonitor4.Controls.Add(this.label57);
            this.panelMonitor4.Controls.Add(this.label58);
            this.panelMonitor4.Enabled = false;
            this.panelMonitor4.Location = new System.Drawing.Point(6, 136);
            this.panelMonitor4.Name = "panelMonitor4";
            this.panelMonitor4.Size = new System.Drawing.Size(630, 30);
            this.panelMonitor4.TabIndex = 20;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 9);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(47, 12);
            this.label40.TabIndex = 3;
            this.label40.Text = "测点4：";
            // 
            // txtMonitor4datum_nlx
            // 
            this.txtMonitor4datum_nlx.Location = new System.Drawing.Point(447, 4);
            this.txtMonitor4datum_nlx.MaxLength = 8;
            this.txtMonitor4datum_nlx.Name = "txtMonitor4datum_nlx";
            this.txtMonitor4datum_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor4datum_nlx.TabIndex = 32;
            this.txtMonitor4datum_nlx.Text = "0";
            this.txtMonitor4datum_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // txtMonitor4hysteresis_nlx
            // 
            this.txtMonitor4hysteresis_nlx.Location = new System.Drawing.Point(577, 4);
            this.txtMonitor4hysteresis_nlx.MaxLength = 7;
            this.txtMonitor4hysteresis_nlx.Name = "txtMonitor4hysteresis_nlx";
            this.txtMonitor4hysteresis_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor4hysteresis_nlx.TabIndex = 33;
            this.txtMonitor4hysteresis_nlx.Text = "0";
            this.txtMonitor4hysteresis_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // label51_nlx
            // 
            this.label51_nlx.AutoSize = true;
            this.label51_nlx.Location = new System.Drawing.Point(298, 13);
            this.label51_nlx.Name = "label51_nlx";
            this.label51_nlx.Size = new System.Drawing.Size(11, 12);
            this.label51_nlx.TabIndex = 2;
            this.label51_nlx.Text = "~";
            // 
            // txtMonitor4_nlx
            // 
            this.txtMonitor4_nlx.Location = new System.Drawing.Point(67, 4);
            this.txtMonitor4_nlx.MaxLength = 8;
            this.txtMonitor4_nlx.Name = "txtMonitor4_nlx";
            this.txtMonitor4_nlx.Size = new System.Drawing.Size(100, 21);
            this.txtMonitor4_nlx.TabIndex = 29;
            // 
            // txtMonitor4min_nlx
            // 
            this.txtMonitor4min_nlx.Location = new System.Drawing.Point(249, 4);
            this.txtMonitor4min_nlx.MaxLength = 8;
            this.txtMonitor4min_nlx.Name = "txtMonitor4min_nlx";
            this.txtMonitor4min_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor4min_nlx.TabIndex = 30;
            this.txtMonitor4min_nlx.Text = "0";
            this.txtMonitor4min_nlx.Leave += new System.EventHandler(this.txtMonitor1min_Leave);
            // 
            // txtMonitor4max_nlx
            // 
            this.txtMonitor4max_nlx.Location = new System.Drawing.Point(315, 4);
            this.txtMonitor4max_nlx.MaxLength = 8;
            this.txtMonitor4max_nlx.Name = "txtMonitor4max_nlx";
            this.txtMonitor4max_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor4max_nlx.TabIndex = 31;
            this.txtMonitor4max_nlx.Text = "0";
            this.txtMonitor4max_nlx.Leave += new System.EventHandler(this.txtMonitor3max_Leave);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(377, 8);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(53, 12);
            this.label56.TabIndex = 10;
            this.label56.Text = "基准值：";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(502, 6);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(53, 12);
            this.label57.TabIndex = 10;
            this.label57.Text = "迟滞值：";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(185, 10);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(53, 12);
            this.label58.TabIndex = 12;
            this.label58.Text = "上下限：";
            // 
            // panelMonitor3
            // 
            this.panelMonitor3.Controls.Add(this.label17);
            this.panelMonitor3.Controls.Add(this.txtMonitor3datum_nlx);
            this.panelMonitor3.Controls.Add(this.txtMonitor3hysteresis_nlx);
            this.panelMonitor3.Controls.Add(this.label16_nlx);
            this.panelMonitor3.Controls.Add(this.txtMonitor3_nlx);
            this.panelMonitor3.Controls.Add(this.txtMonitor3min_nlx);
            this.panelMonitor3.Controls.Add(this.txtMonitor3max_nlx);
            this.panelMonitor3.Controls.Add(this.label23);
            this.panelMonitor3.Controls.Add(this.label28);
            this.panelMonitor3.Controls.Add(this.label25);
            this.panelMonitor3.Enabled = false;
            this.panelMonitor3.Location = new System.Drawing.Point(7, 95);
            this.panelMonitor3.Name = "panelMonitor3";
            this.panelMonitor3.Size = new System.Drawing.Size(631, 33);
            this.panelMonitor3.TabIndex = 19;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 12);
            this.label17.TabIndex = 3;
            this.label17.Text = "测点3：";
            // 
            // txtMonitor3datum_nlx
            // 
            this.txtMonitor3datum_nlx.Location = new System.Drawing.Point(447, 6);
            this.txtMonitor3datum_nlx.MaxLength = 8;
            this.txtMonitor3datum_nlx.Name = "txtMonitor3datum_nlx";
            this.txtMonitor3datum_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor3datum_nlx.TabIndex = 27;
            this.txtMonitor3datum_nlx.Text = "0";
            this.txtMonitor3datum_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // txtMonitor3hysteresis_nlx
            // 
            this.txtMonitor3hysteresis_nlx.Location = new System.Drawing.Point(577, 6);
            this.txtMonitor3hysteresis_nlx.MaxLength = 7;
            this.txtMonitor3hysteresis_nlx.Name = "txtMonitor3hysteresis_nlx";
            this.txtMonitor3hysteresis_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor3hysteresis_nlx.TabIndex = 28;
            this.txtMonitor3hysteresis_nlx.Text = "0";
            this.txtMonitor3hysteresis_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // label16_nlx
            // 
            this.label16_nlx.AutoSize = true;
            this.label16_nlx.Location = new System.Drawing.Point(298, 18);
            this.label16_nlx.Name = "label16_nlx";
            this.label16_nlx.Size = new System.Drawing.Size(11, 12);
            this.label16_nlx.TabIndex = 2;
            this.label16_nlx.Text = "~";
            // 
            // txtMonitor3_nlx
            // 
            this.txtMonitor3_nlx.Location = new System.Drawing.Point(67, 4);
            this.txtMonitor3_nlx.MaxLength = 8;
            this.txtMonitor3_nlx.Name = "txtMonitor3_nlx";
            this.txtMonitor3_nlx.Size = new System.Drawing.Size(100, 21);
            this.txtMonitor3_nlx.TabIndex = 24;
            // 
            // txtMonitor3min_nlx
            // 
            this.txtMonitor3min_nlx.Location = new System.Drawing.Point(249, 7);
            this.txtMonitor3min_nlx.MaxLength = 8;
            this.txtMonitor3min_nlx.Name = "txtMonitor3min_nlx";
            this.txtMonitor3min_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor3min_nlx.TabIndex = 25;
            this.txtMonitor3min_nlx.Text = "0";
            this.txtMonitor3min_nlx.Leave += new System.EventHandler(this.txtMonitor1min_Leave);
            // 
            // txtMonitor3max_nlx
            // 
            this.txtMonitor3max_nlx.Location = new System.Drawing.Point(315, 7);
            this.txtMonitor3max_nlx.MaxLength = 8;
            this.txtMonitor3max_nlx.Name = "txtMonitor3max_nlx";
            this.txtMonitor3max_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor3max_nlx.TabIndex = 26;
            this.txtMonitor3max_nlx.Text = "0";
            this.txtMonitor3max_nlx.Leave += new System.EventHandler(this.txtMonitor3max_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(377, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 10;
            this.label23.Text = "基准值：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(502, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 10;
            this.label28.Text = "迟滞值：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(185, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 12;
            this.label25.Text = "上下限：";
            // 
            // panelMonitor2
            // 
            this.panelMonitor2.Controls.Add(this.label19);
            this.panelMonitor2.Controls.Add(this.txtMonitor2datum_nlx);
            this.panelMonitor2.Controls.Add(this.txtMonitor2_nlx);
            this.panelMonitor2.Controls.Add(this.txtMonitor2hysteresis_nlx);
            this.panelMonitor2.Controls.Add(this.label18_nlx);
            this.panelMonitor2.Controls.Add(this.txtMonitor2min_nlx);
            this.panelMonitor2.Controls.Add(this.txtMonitor2max_nlx);
            this.panelMonitor2.Controls.Add(this.label20);
            this.panelMonitor2.Controls.Add(this.label27);
            this.panelMonitor2.Controls.Add(this.label24);
            this.panelMonitor2.Enabled = false;
            this.panelMonitor2.Location = new System.Drawing.Point(6, 58);
            this.panelMonitor2.Name = "panelMonitor2";
            this.panelMonitor2.Size = new System.Drawing.Size(631, 30);
            this.panelMonitor2.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 12);
            this.label19.TabIndex = 7;
            this.label19.Text = "测点2：";
            // 
            // txtMonitor2datum_nlx
            // 
            this.txtMonitor2datum_nlx.Location = new System.Drawing.Point(447, 5);
            this.txtMonitor2datum_nlx.MaxLength = 8;
            this.txtMonitor2datum_nlx.Name = "txtMonitor2datum_nlx";
            this.txtMonitor2datum_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor2datum_nlx.TabIndex = 22;
            this.txtMonitor2datum_nlx.Text = "0";
            this.txtMonitor2datum_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // txtMonitor2_nlx
            // 
            this.txtMonitor2_nlx.Location = new System.Drawing.Point(67, 4);
            this.txtMonitor2_nlx.MaxLength = 8;
            this.txtMonitor2_nlx.Name = "txtMonitor2_nlx";
            this.txtMonitor2_nlx.Size = new System.Drawing.Size(100, 21);
            this.txtMonitor2_nlx.TabIndex = 19;
            // 
            // txtMonitor2hysteresis_nlx
            // 
            this.txtMonitor2hysteresis_nlx.Location = new System.Drawing.Point(577, 5);
            this.txtMonitor2hysteresis_nlx.MaxLength = 7;
            this.txtMonitor2hysteresis_nlx.Name = "txtMonitor2hysteresis_nlx";
            this.txtMonitor2hysteresis_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor2hysteresis_nlx.TabIndex = 23;
            this.txtMonitor2hysteresis_nlx.Text = "0";
            this.txtMonitor2hysteresis_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // label18_nlx
            // 
            this.label18_nlx.AutoSize = true;
            this.label18_nlx.Location = new System.Drawing.Point(299, 14);
            this.label18_nlx.Name = "label18_nlx";
            this.label18_nlx.Size = new System.Drawing.Size(11, 12);
            this.label18_nlx.TabIndex = 6;
            this.label18_nlx.Text = "~";
            // 
            // txtMonitor2min_nlx
            // 
            this.txtMonitor2min_nlx.Location = new System.Drawing.Point(249, 6);
            this.txtMonitor2min_nlx.MaxLength = 8;
            this.txtMonitor2min_nlx.Name = "txtMonitor2min_nlx";
            this.txtMonitor2min_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor2min_nlx.TabIndex = 20;
            this.txtMonitor2min_nlx.Text = "0";
            this.txtMonitor2min_nlx.Leave += new System.EventHandler(this.txtMonitor1min_Leave);
            // 
            // txtMonitor2max_nlx
            // 
            this.txtMonitor2max_nlx.Location = new System.Drawing.Point(315, 5);
            this.txtMonitor2max_nlx.MaxLength = 8;
            this.txtMonitor2max_nlx.Name = "txtMonitor2max_nlx";
            this.txtMonitor2max_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor2max_nlx.TabIndex = 21;
            this.txtMonitor2max_nlx.Text = "0";
            this.txtMonitor2max_nlx.Leave += new System.EventHandler(this.txtMonitor1max_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(377, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 10;
            this.label20.Text = "基准值：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(502, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 10;
            this.label27.Text = "迟滞值：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(185, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 11;
            this.label24.Text = "上下限：";
            // 
            // panelMonitor1
            // 
            this.panelMonitor1.Controls.Add(this.txtMonitor1_nlx);
            this.panelMonitor1.Controls.Add(this.label14);
            this.panelMonitor1.Controls.Add(this.label15_nlx);
            this.panelMonitor1.Controls.Add(this.txtMonitor1min_nlx);
            this.panelMonitor1.Controls.Add(this.label22);
            this.panelMonitor1.Controls.Add(this.label21);
            this.panelMonitor1.Controls.Add(this.label26);
            this.panelMonitor1.Controls.Add(this.txtMonitor1datum_nlx);
            this.panelMonitor1.Controls.Add(this.txtMonitor1hysteresis_nlx);
            this.panelMonitor1.Controls.Add(this.txtMonitor1max_nlx);
            this.panelMonitor1.Enabled = false;
            this.panelMonitor1.Location = new System.Drawing.Point(5, 24);
            this.panelMonitor1.Name = "panelMonitor1";
            this.panelMonitor1.Size = new System.Drawing.Size(631, 31);
            this.panelMonitor1.TabIndex = 17;
            // 
            // txtMonitor1_nlx
            // 
            this.txtMonitor1_nlx.Location = new System.Drawing.Point(67, 4);
            this.txtMonitor1_nlx.MaxLength = 8;
            this.txtMonitor1_nlx.Name = "txtMonitor1_nlx";
            this.txtMonitor1_nlx.Size = new System.Drawing.Size(100, 21);
            this.txtMonitor1_nlx.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 12);
            this.label14.TabIndex = 1;
            this.label14.Text = "测点1：";
            // 
            // label15_nlx
            // 
            this.label15_nlx.AutoSize = true;
            this.label15_nlx.Location = new System.Drawing.Point(298, 11);
            this.label15_nlx.Name = "label15_nlx";
            this.label15_nlx.Size = new System.Drawing.Size(11, 12);
            this.label15_nlx.TabIndex = 1;
            this.label15_nlx.Text = "~";
            // 
            // txtMonitor1min_nlx
            // 
            this.txtMonitor1min_nlx.Location = new System.Drawing.Point(249, 4);
            this.txtMonitor1min_nlx.MaxLength = 8;
            this.txtMonitor1min_nlx.Name = "txtMonitor1min_nlx";
            this.txtMonitor1min_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor1min_nlx.TabIndex = 15;
            this.txtMonitor1min_nlx.Text = "0";
            this.txtMonitor1min_nlx.Leave += new System.EventHandler(this.txtMonitor1min_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(185, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 1;
            this.label22.Text = "上下限：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(377, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 1;
            this.label21.Text = "基准值：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(502, 11);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 12);
            this.label26.TabIndex = 1;
            this.label26.Text = "迟滞值：";
            // 
            // txtMonitor1datum_nlx
            // 
            this.txtMonitor1datum_nlx.Location = new System.Drawing.Point(447, 5);
            this.txtMonitor1datum_nlx.MaxLength = 8;
            this.txtMonitor1datum_nlx.Name = "txtMonitor1datum_nlx";
            this.txtMonitor1datum_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor1datum_nlx.TabIndex = 17;
            this.txtMonitor1datum_nlx.Text = "0";
            this.txtMonitor1datum_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // txtMonitor1hysteresis_nlx
            // 
            this.txtMonitor1hysteresis_nlx.Location = new System.Drawing.Point(577, 4);
            this.txtMonitor1hysteresis_nlx.MaxLength = 7;
            this.txtMonitor1hysteresis_nlx.Name = "txtMonitor1hysteresis_nlx";
            this.txtMonitor1hysteresis_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor1hysteresis_nlx.TabIndex = 18;
            this.txtMonitor1hysteresis_nlx.Text = "0";
            this.txtMonitor1hysteresis_nlx.Leave += new System.EventHandler(this.txtMonitor7max_Leave);
            // 
            // txtMonitor1max_nlx
            // 
            this.txtMonitor1max_nlx.Location = new System.Drawing.Point(315, 4);
            this.txtMonitor1max_nlx.MaxLength = 8;
            this.txtMonitor1max_nlx.Name = "txtMonitor1max_nlx";
            this.txtMonitor1max_nlx.Size = new System.Drawing.Size(43, 21);
            this.txtMonitor1max_nlx.TabIndex = 16;
            this.txtMonitor1max_nlx.Text = "0";
            this.txtMonitor1max_nlx.Leave += new System.EventHandler(this.txtMonitor1max_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(249, 211);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 12);
            this.label32.TabIndex = 15;
            this.label32.Text = "（秒）";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(553, 216);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 12);
            this.label36.TabIndex = 15;
            this.label36.Text = "（秒）";
            this.label36.Visible = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(249, 279);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 12);
            this.label39.TabIndex = 15;
            this.label39.Text = "（秒）";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(249, 247);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 12);
            this.label34.TabIndex = 15;
            this.label34.Text = "（秒）";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(553, 183);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 12);
            this.label30.TabIndex = 15;
            this.label30.Text = "（分钟）";
            this.label30.Visible = false;
            // 
            // txtTakeTime_nlx
            // 
            this.txtTakeTime_nlx.Location = new System.Drawing.Point(180, 207);
            this.txtTakeTime_nlx.Name = "txtTakeTime_nlx";
            this.txtTakeTime_nlx.Size = new System.Drawing.Size(62, 21);
            this.txtTakeTime_nlx.TabIndex = 35;
            this.txtTakeTime_nlx.Text = "5";
            this.txtTakeTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtDelayAlarm_nlx
            // 
            this.txtDelayAlarm_nlx.Location = new System.Drawing.Point(485, 212);
            this.txtDelayAlarm_nlx.Name = "txtDelayAlarm_nlx";
            this.txtDelayAlarm_nlx.Size = new System.Drawing.Size(66, 21);
            this.txtDelayAlarm_nlx.TabIndex = 37;
            this.txtDelayAlarm_nlx.Text = "5";
            this.txtDelayAlarm_nlx.Visible = false;
            this.txtDelayAlarm_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtTransfersTime_nlx
            // 
            this.txtTransfersTime_nlx.Location = new System.Drawing.Point(180, 275);
            this.txtTransfersTime_nlx.Name = "txtTransfersTime_nlx";
            this.txtTransfersTime_nlx.Size = new System.Drawing.Size(62, 21);
            this.txtTransfersTime_nlx.TabIndex = 38;
            this.txtTransfersTime_nlx.Text = "5";
            this.txtTransfersTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtVisualAlarm_nlx
            // 
            this.txtVisualAlarm_nlx.Location = new System.Drawing.Point(180, 243);
            this.txtVisualAlarm_nlx.Name = "txtVisualAlarm_nlx";
            this.txtVisualAlarm_nlx.Size = new System.Drawing.Size(62, 21);
            this.txtVisualAlarm_nlx.TabIndex = 36;
            this.txtVisualAlarm_nlx.Text = "5";
            this.txtVisualAlarm_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtAlarmTime_nlx
            // 
            this.txtAlarmTime_nlx.Location = new System.Drawing.Point(485, 179);
            this.txtAlarmTime_nlx.Name = "txtAlarmTime_nlx";
            this.txtAlarmTime_nlx.Size = new System.Drawing.Size(66, 21);
            this.txtAlarmTime_nlx.TabIndex = 34;
            this.txtAlarmTime_nlx.Text = "5";
            this.txtAlarmTime_nlx.Visible = false;
            this.txtAlarmTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(372, 247);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 12);
            this.label38.TabIndex = 13;
            this.label38.Text = "温度单位：";
            this.label38.Visible = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(372, 215);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(89, 12);
            this.label35.TabIndex = 13;
            this.label35.Text = "报警确认时间：";
            this.label35.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(34, 277);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(113, 12);
            this.label37.TabIndex = 13;
            this.label37.Text = "数据上传时间间隔：";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(34, 245);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(89, 12);
            this.label33.TabIndex = 13;
            this.label33.Text = "超标记录间隔：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(34, 210);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 12);
            this.label31.TabIndex = 13;
            this.label31.Text = "数据记录间隔：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(372, 183);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(113, 12);
            this.label29.TabIndex = 13;
            this.label29.Text = "报警短信间隔时间：";
            this.label29.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textServer2IP_nlx);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.textServer2Port_nlx);
            this.tabPage3.Controls.Add(this.cbUseGPRS_nlx);
            this.tabPage3.Controls.Add(this.lab1);
            this.tabPage3.Controls.Add(this.cbContactType_nlx);
            this.tabPage3.Controls.Add(this.cbConnectionType_nlx);
            this.tabPage3.Controls.Add(this.txtBackTime_nlx);
            this.tabPage3.Controls.Add(this.txtOutTime_nlx);
            this.tabPage3.Controls.Add(this.txtBeatTime_nlx);
            this.tabPage3.Controls.Add(this.label55);
            this.tabPage3.Controls.Add(this.label53);
            this.tabPage3.Controls.Add(this.label48);
            this.tabPage3.Controls.Add(this.label50);
            this.tabPage3.Controls.Add(this.label49);
            this.tabPage3.Controls.Add(this.label54);
            this.tabPage3.Controls.Add(this.label52);
            this.tabPage3.Controls.Add(this.label47);
            this.tabPage3.Controls.Add(this.txtIP_nlx);
            this.tabPage3.Controls.Add(this.label46);
            this.tabPage3.Controls.Add(this.txtGPRSpwd_nlx);
            this.tabPage3.Controls.Add(this.label44);
            this.tabPage3.Controls.Add(this.label45);
            this.tabPage3.Controls.Add(this.txtPort_nlx);
            this.tabPage3.Controls.Add(this.txtDNS_nlx);
            this.tabPage3.Controls.Add(this.label43);
            this.tabPage3.Controls.Add(this.txtGPRSname_nlx);
            this.tabPage3.Controls.Add(this.label42);
            this.tabPage3.Controls.Add(this.txtGPRScontact_nlx);
            this.tabPage3.Controls.Add(this.label41);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(643, 318);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "GPRS设置";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // textServer2IP_nlx
            // 
            this.textServer2IP_nlx.Location = new System.Drawing.Point(114, 243);
            this.textServer2IP_nlx.Name = "textServer2IP_nlx";
            this.textServer2IP_nlx.Size = new System.Drawing.Size(127, 21);
            this.textServer2IP_nlx.TabIndex = 55;
            this.textServer2IP_nlx.Text = "192.168.1.1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(32, 247);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 12);
            this.label16.TabIndex = 54;
            this.label16.Text = "服务器2地址：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(30, 274);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 12);
            this.label15.TabIndex = 52;
            this.label15.Text = "服务器2端口：";
            // 
            // textServer2Port_nlx
            // 
            this.textServer2Port_nlx.Location = new System.Drawing.Point(113, 270);
            this.textServer2Port_nlx.MaxLength = 5;
            this.textServer2Port_nlx.Name = "textServer2Port_nlx";
            this.textServer2Port_nlx.Size = new System.Drawing.Size(77, 21);
            this.textServer2Port_nlx.TabIndex = 53;
            this.textServer2Port_nlx.Text = "3000";
            // 
            // cbUseGPRS_nlx
            // 
            this.cbUseGPRS_nlx.AutoCompleteCustomSource.AddRange(new string[] {
            "启用",
            "禁用"});
            this.cbUseGPRS_nlx.CausesValidation = false;
            this.cbUseGPRS_nlx.FormattingEnabled = true;
            this.cbUseGPRS_nlx.Location = new System.Drawing.Point(469, 266);
            this.cbUseGPRS_nlx.Name = "cbUseGPRS_nlx";
            this.cbUseGPRS_nlx.Size = new System.Drawing.Size(77, 20);
            this.cbUseGPRS_nlx.TabIndex = 51;
            this.cbUseGPRS_nlx.Visible = false;
            // 
            // lab1
            // 
            this.lab1.AutoSize = true;
            this.lab1.CausesValidation = false;
            this.lab1.Location = new System.Drawing.Point(373, 268);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(65, 12);
            this.lab1.TabIndex = 5;
            this.lab1.Text = "启用GPRS：";
            this.lab1.Visible = false;
            // 
            // cbContactType_nlx
            // 
            this.cbContactType_nlx.CausesValidation = false;
            this.cbContactType_nlx.FormattingEnabled = true;
            this.cbContactType_nlx.Items.AddRange(new object[] {
            "modbus/tcp",
            "HRT100"});
            this.cbContactType_nlx.Location = new System.Drawing.Point(465, 192);
            this.cbContactType_nlx.Name = "cbContactType_nlx";
            this.cbContactType_nlx.Size = new System.Drawing.Size(107, 20);
            this.cbContactType_nlx.TabIndex = 47;
            this.cbContactType_nlx.Visible = false;
            // 
            // cbConnectionType_nlx
            // 
            this.cbConnectionType_nlx.FormattingEnabled = true;
            this.cbConnectionType_nlx.Items.AddRange(new object[] {
            "TCP",
            "UDP"});
            this.cbConnectionType_nlx.Location = new System.Drawing.Point(91, 180);
            this.cbConnectionType_nlx.Name = "cbConnectionType_nlx";
            this.cbConnectionType_nlx.Size = new System.Drawing.Size(77, 20);
            this.cbConnectionType_nlx.TabIndex = 48;
            // 
            // txtBackTime_nlx
            // 
            this.txtBackTime_nlx.CausesValidation = false;
            this.txtBackTime_nlx.Location = new System.Drawing.Point(465, 226);
            this.txtBackTime_nlx.Name = "txtBackTime_nlx";
            this.txtBackTime_nlx.Size = new System.Drawing.Size(109, 21);
            this.txtBackTime_nlx.TabIndex = 50;
            this.txtBackTime_nlx.Text = "0";
            this.txtBackTime_nlx.Visible = false;
            this.txtBackTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtOutTime_nlx
            // 
            this.txtOutTime_nlx.CausesValidation = false;
            this.txtOutTime_nlx.Location = new System.Drawing.Point(465, 162);
            this.txtOutTime_nlx.Name = "txtOutTime_nlx";
            this.txtOutTime_nlx.Size = new System.Drawing.Size(127, 21);
            this.txtOutTime_nlx.TabIndex = 49;
            this.txtOutTime_nlx.Text = "0";
            this.txtOutTime_nlx.Visible = false;
            this.txtOutTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtBeatTime_nlx
            // 
            this.txtBeatTime_nlx.CausesValidation = false;
            this.txtBeatTime_nlx.Location = new System.Drawing.Point(465, 124);
            this.txtBeatTime_nlx.Name = "txtBeatTime_nlx";
            this.txtBeatTime_nlx.Size = new System.Drawing.Size(127, 21);
            this.txtBeatTime_nlx.TabIndex = 46;
            this.txtBeatTime_nlx.Text = "120";
            this.txtBeatTime_nlx.Visible = false;
            this.txtBeatTime_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.CausesValidation = false;
            this.label55.Location = new System.Drawing.Point(574, 232);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(29, 12);
            this.label55.TabIndex = 2;
            this.label55.Text = "(秒)";
            this.label55.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.CausesValidation = false;
            this.label53.Location = new System.Drawing.Point(592, 170);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(29, 12);
            this.label53.TabIndex = 2;
            this.label53.Text = "(秒)";
            this.label53.Visible = false;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.CausesValidation = false;
            this.label48.Location = new System.Drawing.Point(591, 130);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 2;
            this.label48.Text = "(秒)";
            this.label48.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.CausesValidation = false;
            this.label50.Location = new System.Drawing.Point(373, 195);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 12);
            this.label50.TabIndex = 2;
            this.label50.Text = "数据协议：";
            this.label50.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(12, 183);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(65, 12);
            this.label49.TabIndex = 2;
            this.label49.Text = "网络协议：";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.CausesValidation = false;
            this.label54.Location = new System.Drawing.Point(373, 232);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 12);
            this.label54.TabIndex = 2;
            this.label54.Text = "回应超时：";
            this.label54.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.CausesValidation = false;
            this.label52.Location = new System.Drawing.Point(373, 166);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(89, 12);
            this.label52.TabIndex = 2;
            this.label52.Text = "GPRS空闲超时：";
            this.label52.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.CausesValidation = false;
            this.label47.Location = new System.Drawing.Point(373, 128);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(89, 12);
            this.label47.TabIndex = 2;
            this.label47.Text = "GPRS心跳时间：";
            this.label47.Visible = false;
            // 
            // txtIP_nlx
            // 
            this.txtIP_nlx.Location = new System.Drawing.Point(88, 107);
            this.txtIP_nlx.Name = "txtIP_nlx";
            this.txtIP_nlx.Size = new System.Drawing.Size(127, 21);
            this.txtIP_nlx.TabIndex = 43;
            this.txtIP_nlx.Text = "192.168.1.1";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(12, 111);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 12);
            this.label46.TabIndex = 2;
            this.label46.Text = "服务器地址：";
            // 
            // txtGPRSpwd_nlx
            // 
            this.txtGPRSpwd_nlx.Location = new System.Drawing.Point(544, 35);
            this.txtGPRSpwd_nlx.MaxLength = 16;
            this.txtGPRSpwd_nlx.Name = "txtGPRSpwd_nlx";
            this.txtGPRSpwd_nlx.Size = new System.Drawing.Size(77, 21);
            this.txtGPRSpwd_nlx.TabIndex = 42;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(456, 38);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(65, 12);
            this.label44.TabIndex = 2;
            this.label44.Text = "GPRS密码：";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(12, 149);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 12);
            this.label45.TabIndex = 2;
            this.label45.Text = "端口：";
            // 
            // txtPort_nlx
            // 
            this.txtPort_nlx.Location = new System.Drawing.Point(89, 145);
            this.txtPort_nlx.MaxLength = 5;
            this.txtPort_nlx.Name = "txtPort_nlx";
            this.txtPort_nlx.Size = new System.Drawing.Size(77, 21);
            this.txtPort_nlx.TabIndex = 45;
            this.txtPort_nlx.Text = "3000";
            this.txtPort_nlx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone1_KeyPress);
            // 
            // txtDNS_nlx
            // 
            this.txtDNS_nlx.Location = new System.Drawing.Point(89, 73);
            this.txtDNS_nlx.Name = "txtDNS_nlx";
            this.txtDNS_nlx.Size = new System.Drawing.Size(126, 21);
            this.txtDNS_nlx.TabIndex = 44;
            this.txtDNS_nlx.Text = "192.168.1.1";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(59, 12);
            this.label43.TabIndex = 2;
            this.label43.Text = "DNS地址：";
            // 
            // txtGPRSname_nlx
            // 
            this.txtGPRSname_nlx.Location = new System.Drawing.Point(329, 36);
            this.txtGPRSname_nlx.MaxLength = 16;
            this.txtGPRSname_nlx.Name = "txtGPRSname_nlx";
            this.txtGPRSname_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtGPRSname_nlx.TabIndex = 41;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(241, 36);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(77, 12);
            this.label42.TabIndex = 2;
            this.label42.Text = "GPRS用户名：";
            // 
            // txtGPRScontact_nlx
            // 
            this.txtGPRScontact_nlx.Location = new System.Drawing.Point(88, 36);
            this.txtGPRScontact_nlx.MaxLength = 16;
            this.txtGPRScontact_nlx.Name = "txtGPRScontact_nlx";
            this.txtGPRScontact_nlx.Size = new System.Drawing.Size(127, 21);
            this.txtGPRScontact_nlx.TabIndex = 40;
            this.txtGPRScontact_nlx.Text = "cmnet";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 39);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(77, 12);
            this.label41.TabIndex = 1;
            this.label41.Text = "GPRS接入点：";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtAdress_nlx);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.txtPhone10_nlx);
            this.tabPage4.Controls.Add(this.txtPhone1_nlx);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.txtPhone2_nlx);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.txtPhone5_nlx);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.txtPhone3_nlx);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.txtPhone8_nlx);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.txtPhone4_nlx);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.txtPhone7_nlx);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.txtPhone6_nlx);
            this.tabPage4.Controls.Add(this.txtPhone9_nlx);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(643, 318);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "其它参数";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtAdress_nlx
            // 
            this.txtAdress_nlx.Location = new System.Drawing.Point(113, 53);
            this.txtAdress_nlx.MaxLength = 24;
            this.txtAdress_nlx.Multiline = true;
            this.txtAdress_nlx.Name = "txtAdress_nlx";
            this.txtAdress_nlx.Size = new System.Drawing.Size(504, 40);
            this.txtAdress_nlx.TabIndex = 99;
            this.txtAdress_nlx.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(39, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 113;
            this.label11.Text = "安装位置：";
            this.label11.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 111;
            this.label10.Text = "电话号码10：";
            this.label10.Visible = false;
            // 
            // txtPhone10_nlx
            // 
            this.txtPhone10_nlx.Location = new System.Drawing.Point(113, 245);
            this.txtPhone10_nlx.MaxLength = 16;
            this.txtPhone10_nlx.Name = "txtPhone10_nlx";
            this.txtPhone10_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone10_nlx.TabIndex = 120;
            this.txtPhone10_nlx.Visible = false;
            // 
            // txtPhone1_nlx
            // 
            this.txtPhone1_nlx.Location = new System.Drawing.Point(111, 115);
            this.txtPhone1_nlx.MaxLength = 16;
            this.txtPhone1_nlx.Name = "txtPhone1_nlx";
            this.txtPhone1_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone1_nlx.TabIndex = 108;
            this.txtPhone1_nlx.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(428, 209);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 106;
            this.label9.Text = "电话号码9：";
            this.label9.Visible = false;
            // 
            // txtPhone2_nlx
            // 
            this.txtPhone2_nlx.Location = new System.Drawing.Point(307, 112);
            this.txtPhone2_nlx.MaxLength = 16;
            this.txtPhone2_nlx.Name = "txtPhone2_nlx";
            this.txtPhone2_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone2_nlx.TabIndex = 110;
            this.txtPhone2_nlx.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(426, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 101;
            this.label6.Text = "电话号码6：";
            this.label6.Visible = false;
            // 
            // txtPhone5_nlx
            // 
            this.txtPhone5_nlx.Location = new System.Drawing.Point(307, 158);
            this.txtPhone5_nlx.MaxLength = 16;
            this.txtPhone5_nlx.Name = "txtPhone5_nlx";
            this.txtPhone5_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone5_nlx.TabIndex = 115;
            this.txtPhone5_nlx.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 109;
            this.label4.Text = "电话号码3：";
            this.label4.Visible = false;
            // 
            // txtPhone3_nlx
            // 
            this.txtPhone3_nlx.Location = new System.Drawing.Point(508, 111);
            this.txtPhone3_nlx.MaxLength = 16;
            this.txtPhone3_nlx.Name = "txtPhone3_nlx";
            this.txtPhone3_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone3_nlx.TabIndex = 112;
            this.txtPhone3_nlx.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 100;
            this.label8.Text = "电话号码7：";
            this.label8.Visible = false;
            // 
            // txtPhone8_nlx
            // 
            this.txtPhone8_nlx.Location = new System.Drawing.Point(307, 206);
            this.txtPhone8_nlx.MaxLength = 16;
            this.txtPhone8_nlx.Name = "txtPhone8_nlx";
            this.txtPhone8_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone8_nlx.TabIndex = 118;
            this.txtPhone8_nlx.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 105;
            this.label5.Text = "电话号码4：";
            this.label5.Visible = false;
            // 
            // txtPhone4_nlx
            // 
            this.txtPhone4_nlx.Location = new System.Drawing.Point(112, 157);
            this.txtPhone4_nlx.MaxLength = 16;
            this.txtPhone4_nlx.Name = "txtPhone4_nlx";
            this.txtPhone4_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone4_nlx.TabIndex = 114;
            this.txtPhone4_nlx.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(230, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 12);
            this.label7.TabIndex = 104;
            this.label7.Text = "电话号码8：";
            this.label7.Visible = false;
            // 
            // txtPhone7_nlx
            // 
            this.txtPhone7_nlx.Location = new System.Drawing.Point(112, 203);
            this.txtPhone7_nlx.MaxLength = 16;
            this.txtPhone7_nlx.Name = "txtPhone7_nlx";
            this.txtPhone7_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone7_nlx.TabIndex = 117;
            this.txtPhone7_nlx.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(230, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 103;
            this.label3.Text = "电话号码5：";
            this.label3.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(229, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 102;
            this.label1.Text = "电话号码2：";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 107;
            this.label2.Text = "电话号码1：";
            this.label2.Visible = false;
            // 
            // txtPhone6_nlx
            // 
            this.txtPhone6_nlx.Location = new System.Drawing.Point(508, 162);
            this.txtPhone6_nlx.MaxLength = 16;
            this.txtPhone6_nlx.Name = "txtPhone6_nlx";
            this.txtPhone6_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone6_nlx.TabIndex = 116;
            this.txtPhone6_nlx.Visible = false;
            // 
            // txtPhone9_nlx
            // 
            this.txtPhone9_nlx.Location = new System.Drawing.Point(510, 206);
            this.txtPhone9_nlx.MaxLength = 16;
            this.txtPhone9_nlx.Name = "txtPhone9_nlx";
            this.txtPhone9_nlx.Size = new System.Drawing.Size(107, 21);
            this.txtPhone9_nlx.TabIndex = 119;
            this.txtPhone9_nlx.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.val});
            this.dataGridView1.Location = new System.Drawing.Point(10, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(654, 357);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // name
            // 
            this.name.DataPropertyName = "ChineseKey";
            this.name.HeaderText = "名称";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 200;
            // 
            // val
            // 
            this.val.DataPropertyName = "DeviceVal";
            this.val.HeaderText = "数据";
            this.val.Name = "val";
            this.val.ReadOnly = true;
            this.val.Width = 400;
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(11, 368);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(93, 23);
            this.btnRead.TabIndex = 6;
            this.btnRead.Text = "读取参数文件";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.button4_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "1.xml";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.CheckFileExists = false;
            this.openFileDialog2.CheckPathExists = false;
            this.openFileDialog2.FileName = "save.xml";
            this.openFileDialog2.ShowReadOnly = true;
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "配置文件|*.xml";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(113, 368);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "读取设备参数";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.sendCmdBuffer_Tick);
            // 
            // btnTestModel
            // 
            this.btnTestModel.Location = new System.Drawing.Point(584, 368);
            this.btnTestModel.Name = "btnTestModel";
            this.btnTestModel.Size = new System.Drawing.Size(75, 23);
            this.btnTestModel.TabIndex = 1;
            this.btnTestModel.Text = "测试模式";
            this.btnTestModel.UseVisualStyleBackColor = true;
            this.btnTestModel.Click += new System.EventHandler(this.btnTestModel_nlx_Click);
            // 
            // SystemConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(676, 397);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnTestModel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SystemConfig";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemConfig_FormClosing);
            this.Load += new System.EventHandler(this.SystemConfig_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.TitlePanel.ResumeLayout(false);
            this.TitlePanel.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panelMonitor4.ResumeLayout(false);
            this.panelMonitor4.PerformLayout();
            this.panelMonitor3.ResumeLayout(false);
            this.panelMonitor3.PerformLayout();
            this.panelMonitor2.ResumeLayout(false);
            this.panelMonitor2.PerformLayout();
            this.panelMonitor1.ResumeLayout(false);
            this.panelMonitor1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtTakeTime_nlx;
        private System.Windows.Forms.TextBox txtDelayAlarm_nlx;
        private System.Windows.Forms.TextBox txtTransfersTime_nlx;
        private System.Windows.Forms.TextBox txtVisualAlarm_nlx;
        private System.Windows.Forms.TextBox txtAlarmTime_nlx;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMonitor2max_nlx;
        private System.Windows.Forms.TextBox txtMonitor2min_nlx;
        private System.Windows.Forms.Label label18_nlx;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtMonitor3max_nlx;
        private System.Windows.Forms.TextBox txtMonitor3min_nlx;
        private System.Windows.Forms.Label label16_nlx;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMonitor1max_nlx;
        private System.Windows.Forms.TextBox txtMonitor3hysteresis_nlx;
        private System.Windows.Forms.TextBox txtMonitor3datum_nlx;
        private System.Windows.Forms.TextBox txtMonitor1hysteresis_nlx;
        private System.Windows.Forms.TextBox txtMonitor1datum_nlx;
        private System.Windows.Forms.TextBox txtMonitor2hysteresis_nlx;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMonitor2datum_nlx;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtMonitor1min_nlx;
        private System.Windows.Forms.Label label15_nlx;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtIP_nlx;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtGPRSpwd_nlx;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtPort_nlx;
        private System.Windows.Forms.TextBox txtDNS_nlx;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtGPRSname_nlx;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtGPRScontact_nlx;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtBeatTime_nlx;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cbContactType_nlx;
        private System.Windows.Forms.ComboBox cbConnectionType_nlx;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtBackTime_nlx;
        private System.Windows.Forms.TextBox txtOutTime_nlx;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtMonitor2_nlx;
        private System.Windows.Forms.TextBox txtMonitor3_nlx;
        private System.Windows.Forms.TextBox txtMonitor1_nlx;
        private System.Windows.Forms.ComboBox cbUseGPRS_nlx;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn val;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox txtMonitor4_nlx;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtMonitor4max_nlx;
        private System.Windows.Forms.TextBox txtMonitor4min_nlx;
        private System.Windows.Forms.Label label51_nlx;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtMonitor4hysteresis_nlx;
        private System.Windows.Forms.TextBox txtMonitor4datum_nlx;
        private System.Windows.Forms.Panel panelMonitor4;
        private System.Windows.Forms.Panel panelMonitor3;
        private System.Windows.Forms.Panel panelMonitor2;
        private System.Windows.Forms.Panel panelMonitor1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label TitleMsg;
        private System.Windows.Forms.Panel TitlePanel;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox cmbUnit_nlx;
        private System.Windows.Forms.Button btnTestModel;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtSamplePeriod_nlx;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtDBVersion_nlx;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.CheckBox chkEnableOverproolRecordPeriod_nlx;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.CheckBox chkEnableAlarm_nlx;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox cmbAllowClose_nlx;
        private System.Windows.Forms.Label labAllowClose;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtDId_nlx;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkAlarmPhone_nlx;
        private System.Windows.Forms.CheckBox ckeveryOutTime_nlx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAdress_nlx;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPhone10_nlx;
        private System.Windows.Forms.TextBox txtPhone1_nlx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPhone2_nlx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPhone5_nlx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPhone3_nlx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPhone8_nlx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhone4_nlx;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPhone7_nlx;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPhone6_nlx;
        private System.Windows.Forms.TextBox txtPhone9_nlx;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox comboBoxPrintMode_nlx;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtPrintAmount_nlx;
        private System.Windows.Forms.TextBox textServer2IP_nlx;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textServer2Port_nlx;
    }
}