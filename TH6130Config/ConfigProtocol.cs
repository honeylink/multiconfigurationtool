﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using System.IO.Ports;
using Microsoft.Win32.SafeHandles;


namespace TH6100Config
{
    public class ConfigProtocol
    {
        public delegate void DataRecievedEventHandler(byte[] data);
        public event DataRecievedEventHandler DataRecieved;
        public delegate void DataResponseEventHandler(string Msg);
        public event DataResponseEventHandler DataResponse;
        public delegate void DataResponseTypeEventHandler(string Msg);
        public event DataResponseTypeEventHandler GetDeviceType;
        public delegate void BDataResponseEventHandler(byte[] data);
        public event BDataResponseEventHandler BDataResponse;
        public delegate void TestEventHandler(string Msg, bool state);
        public event TestEventHandler TestMsg;

        public int TLength = 0;
        public string deviceType = "";// "TH6100_T_T";//设备类型
        public int cmdType = 0;
        #region  常量
        /// <summary>
        /// 是否已打开
        /// </summary>
        public bool IsOpen = false;

        private SerialPortData serialPort;

        #endregion

        public ConfigProtocol(object comobj)
        {
            serialPort = (SerialPortData)comobj;
            serialPort.DataReceived += new SerialPortData.SerialPortDataReceiveEventArgs(ReadCallback);
            IsOpen = true;
        }

        public void Dispost()
        {
        }

        /// <summary>
        /// 关闭打开的设备
        /// </summary>
        public void CloseDevice()
        {
        }

        #region   发送数据
        public void SendData(string sendMsg)
        {
            serialPort.SendData(sendMsg);
        }
        /// <summary>
        /// 发送命令
        /// </summary>
        /// <param name="sendMsg"></param>
        public void SendData(byte[] data)
        {
            serialPort.SendData(data, 0, data.Length);
        }
        #endregion
        #region   接收数据
        public void ReadCallback(object sender, SerialDataReceivedEventArgs e, byte[] bytes)
        {
            ReadCompleted(bytes);
        }


        /// <summary>
        /// 回调，数据读完后的方法
        /// </summary>
        /// <param name="iResult">结果</param>
        protected void ReadCompleted(byte[] databuffer)
        {
            bool packetIsFull = false;
            Console.WriteLine("ReadCompleted:" + cmdType);
            try
            {
                string State = Encoding.UTF8.GetString(databuffer).Replace("\0", null);
                if (string.IsNullOrEmpty(State))
                {
                    IsOpen = false;
                }
                else
                {
                    //!@todo 需要确定是否一个完整的包已经接收完成，也许可以通过校验和来判断
                    packetIsFull = true;//临时测试
                    if (packetIsFull)
                    {
                        string ReturnData = "";
                        
                        switch (cmdType)
                        {
                            case 1://不知道意义，可能是获取设备类型
                                analyze(databuffer, ref ReturnData);
                                if(GetDeviceType != null)
                                {
                                    GetDeviceType(SelectType(ReturnData));
                                }                                   
                                break;
                            case 2://PROGRAM   模式
                                analyze(databuffer, ref ReturnData);
                                int i = 0;
                                int.TryParse(ReturnData, out i);
                                byte[] da = BitConverter.GetBytes(i);
                                byte[] Retda = new byte[1];
                                Array.Copy(da, 0, Retda, 0, 1);
                                cmdType = 3;
                                SendData(Retda);
                                break;
                            case 3:  // 随机数或者纯命令模式
                                analyze(databuffer, ref ReturnData);
                                if(DataResponse != null)
                                {
                                    DataResponse(ReturnData);
                                }                                   
                                break;
                            case 4:  //普通数据读取模式 
                                if(BDataResponse != null)
                                {
                                    BDataResponse(analyze(databuffer, ref ReturnData));
                                }                                   
                                break;
                            case 7:  //命令响应
                                DataRecieved(databuffer);
                                break;
                            case 8://进入测试模式 
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                                byte[] Retdas = new byte[TLength];
                                Array.Copy(databuffer, 4, Retdas, 0, TLength);
                                if (Retdas.Length == 1 && Retdas[0] == 6)
                                {
                                    ReturnData = "6";
                                }
                                else
                                {
                                    ReturnData = Encoding.UTF8.GetString(Retdas);
                                }

                                AnalyzeTest(ReturnData);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SelectType(string type)
        {
            switch (type)
            {
                case "6401":
                    deviceType = "TH6100-TH_0";
                    break;
                case "6402":
                    deviceType = "TH6100-T_0";
                    break;
                case "6404":
                    deviceType = "TH6100-T_T";
                    break;
                case "6403":
                    deviceType = "TH6100-TH_T";
                    break;
                default:
                    break;
            }
            return deviceType;
        }

        public byte[] analyze(byte[] data, ref string RetrData)
        {
            try
            {
                Console.WriteLine("analyze:" + cmdType);

                byte[] UsefulResultData = null;

                //命令解析
                if (data.Length > 0)
                {
                    switch (cmdType)
                    {
                        case 1:
                            if (data[0].ToString() == "37" && data[data.Length - 1].ToString() == "37")//获取设备类型
                            {
                                RetrData = data[1].ToString("X") + data[2].ToString("X").PadLeft(2, '0');
                            }
                            break;
                        case 2:
                            RetrData = data[0].ToString();
                            break;
                        case 3:
                            RetrData = data[0].ToString();
                            break;
                        case 4:
                            int address = Convert.ToInt32(data[1].ToString()) * 256 + Convert.ToInt32(data[2].ToString());
                            int Length = Convert.ToInt32(data[3].ToString());
                            byte[] bVal = new byte[Length];
                            Array.Copy(data, 4, bVal, 0, Length);
                            RetrData = Encoding.UTF8.GetString(bVal).Replace("\0", null);
                            UsefulResultData = bVal;
                            break;
                    }
                }
                //返回有用数据
                return UsefulResultData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AnalyzeTest(string RetrData)
        {
            try
            {
                switch (cmdType)
                {
                    case 8://进入测试模式
                        if (RetrData.IndexOf("Enter test mode!") >= 0)
                        {
                            TestMsg("进入测试模式成功", false);
                        }
                        break;
                    case 9://测试按键
                        if (RetrData.IndexOf("Please press key!") >= 0)
                        {
                            TestMsg("请按按键... ", true);
                        }
                        else if (RetrData == "6")//按键成功
                        {
                            TestMsg(" 按键OK ", false);
                        }
                        else
                        {
                            TestMsg("请按按键... ", true);
                        }

                        break;
                    case 10://测试LCD显示
                        if (RetrData == "6")//按键成功
                        {
                            TestMsg("LCD测试OK ", false);
                        }
                        break;
                    case 11://测试探头
                        if (RetrData == "6")//按键成功
                        {
                            TestMsg("探头测试完成 ", false);
                        }
                        else
                        {
                            TestMsg("数据：" + RetrData, true);
                        }
                        break;
                    case 12://测试 电池电量 
                        if (RetrData == "6")
                        {
                            TestMsg(" 电池电量完成  ", false);
                        }
                        else
                        {
                            TestMsg("数据：" + RetrData, true);
                        }
                        break;
                    case 13://测试modem 
                        if (RetrData == "6")
                        {
                            TestMsg("Modem测试完成 ", false);
                        }
                        else
                        {
                            TestMsg(RetrData, true);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }




        #endregion
        #region 数据转换

        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToHexString(byte[] bytes)
        {
            string str = string.Empty;
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    str += bytes[i].ToString("X2");
                }
            }
            return str;
        }

        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] HexStringToByte(string msg)
        {
            byte[] bstr = new byte[1024];
            bstr = Encoding.UTF8.GetBytes(msg);
            return bstr;
        }
        #endregion

        #region  操作方法


        public void Test(string sendMsg)
        {

            //byte[] data = HexStringToByte(sendMsg);
            //int Num = 0;
            //int totalLength = data.Length;
            //int index = 0;
            //while (sendMsg.Length > index)
            //{
            //    byte[] sData = new byte[61];
            //    int DataLength = 0;
            //    OutputReport oRep = new OutputReport();
            //    if ((index + 61) <= data.Length)
            //    {
            //        Array.Copy(data, index, sData, 0, 61);
            //        DataLength = 61;
            //        index += 61;
            //    }
            //    else
            //    {
            //        DataLength = sendMsg.Length - index;
            //        Array.Copy(data, index, sData, 0, sendMsg.Length - index);
            //        index += sendMsg.Length - index;

            //    }
            //    Num++;
            //    oRep.SetData(sData, Num, totalLength, DataLength); 
            //}
            //RetVal = new byte[sendMsg];
        }

        /** 
        * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序，和和intToBytes（）配套使用
        *  
        * @param src 
        *            byte数组 
        * @param offset 
        *            从数组的第offset位开始 
        * @return int数值 
        */
        public static int bytesToInt(byte[] src, int offset, int length)
        {
            byte[] data = new byte[4];


            Array.Copy(src, 0, data, 0, length);
            int value;
            value = (int)((src[offset] & 0xFF)
                    | ((src[offset + 1] & 0xFF) << 8)
                    | ((src[offset + 2] & 0xFF) << 16)
                    | ((src[offset + 3] & 0xFF) << 24));
            return value;
        }

        #endregion
    }


}
