﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;
using System.IO.Ports;

namespace TH6100Config
{
    public partial class TH6100SetConfig : Form
    {
        private delegate void SetModelCallBack();    
      
        SelectLanguage Language = new SelectLanguage();
        public bool isCon = false;
        SystemConfig setconfig = null;
        SerialPortData serialPortData;

        public TH6100SetConfig()
        {
            InitializeComponent();
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
         
        }

        private void openbtn_Click(object sender, EventArgs e)
        {
            serialPortData = new SerialPortData(cmb_serialport.SelectedItem.ToString());
            if (serialPortData.openPort() == true)
            {
                this.openbtn.Enabled = false;
                cmb_serialport.Enabled = false;
                cmbLanguage_nlx.Enabled = false;
                labMessage.Text = Language.getMsg("DeviceConnetOKMsg");

                this.btnSetParameter.Enabled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        { 
            this.StartPosition = FormStartPosition.CenterScreen;
            Language.SetLanguage(this);
            cmbLanguage_nlx.SelectedItem = Global.Language;
            btnSetParameter.Enabled = false;

            //portsArray数组获得所有串口列表  
            string[] portsArray = SerialPort.GetPortNames();

            //把获得的串口名添加到列表框  
            foreach (string portnumber in portsArray)
            {
                cmb_serialport.Items.Add(portnumber);
            }  
            if(cmb_serialport.Items.Count>0)
            {
                cmb_serialport.SelectedIndex = 0;
            }
        }


        private void shows()
        {
            if (setconfig == null)
            {
                setconfig = new SystemConfig(serialPortData);
                setconfig.Show();
            }
            else
            {
                setconfig.Close();
                setconfig = null;
                setconfig = new SystemConfig(serialPortData);
                setconfig.Show();
            }                   
        }
        public void GetType(string type)
        {
            this.BeginInvoke(new SetModelCallBack(shows));//异步执行    
        }

        public void Dmsg(byte[] data )
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new TH6100Config.HusbHelp.DataRecievedEventHandler(Dmsg), new object[] { data });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            else
            { 
               string rec_data = BitConverter.ToString(data);
               //this.revtext.AppendText(rec_data + "\r\n ");
            }
        }

        private void btnSetParameter_Click(object sender, EventArgs e)
        {
            try
            {
                this.BeginInvoke(new SetModelCallBack(shows));//异步执行    
            }
            catch (Exception ex )
            {
                MessagShow(Language.getMsg("CmdExceptionMsg"));
                MessageBox.Show(Language.getMsg("CmdExceptionMsg") );
            }
         
          
        } 
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
         
        /// <summary>
        /// btye数组转换为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] HexStringToBytes(string msg)
        {
            byte[] bstr = new byte[1024];
            bstr = Encoding.BigEndianUnicode.GetBytes(msg);
            return bstr;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            AppCmd app = new AppCmd();
            app.SaveConfig("language",this.cmbLanguage_nlx.SelectedItem.ToString());
            string language = ConfigurationManager.AppSettings[this.cmbLanguage_nlx.SelectedItem.ToString()].ToString();
            Global.Language = this.cmbLanguage_nlx.SelectedItem.ToString();
            Language.SetLanguage(this);
        }
        private void MessagShow(string Msg)
        {
            UserMessageBox box = new UserMessageBox(Msg);
 
            box.Show();
        }
    }
}
