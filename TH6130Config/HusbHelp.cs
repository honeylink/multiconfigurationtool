﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Threading;

namespace TH6100Config
{
  public   class HusbHelp :MsUsbAPI
  {
      public delegate void DataRecievedEventHandler(byte[] data); 
      public event DataRecievedEventHandler DataRecieved;
      public delegate void DataResponseEventHandler(string Msg);
      public event DataResponseEventHandler DataResponse;
      public delegate void DataResponseTypeEventHandler(string Msg);
      public event DataResponseTypeEventHandler GetType;
      public delegate void BDataResponseEventHandler(byte[] data);
      public event BDataResponseEventHandler BDataResponse;
      public delegate void TestEventHandler(string Msg,bool state );
      public event TestEventHandler TestMsg;
      public static List<InputReport> Relist=new List<InputReport>();
      public int TNum = 0;
      public int TLength = 0;
      public string  dType = "";//设备类型
      public int cmdType = 0;
      #region  常量

      /// <summary>
      /// usb是否已存在了
      /// </summary>
      public bool history = false;
      /// <summary>
      /// usb是否已打开
      /// </summary>
      public bool IsOpen = false; 
      /// <summary>
      /// 设备访问地址
      /// </summary>
      private  string devicePath = string.Empty; 
      /// <summary>设备句柄</summary>
      private IntPtr m_hHandle;
      /// <summary>文件流用于读写</summary>
      private FileStream m_oFile;
      /// <summary>输入长度</summary>
      private int m_nInputReportLength;
      /// <summary>输出长度</summary>
      private int m_nOutputReportLength;
      private IntPtr hDevInfo;
      /// <summary>
      /// 包序号
      /// </summary>
      private int SNO;
      
      #endregion

      #region  获取HID 设备
      /// <summary>
      /// 检验是否有HID设备
      /// </summary>
      /// <returns>成功true 失败false</returns>
      public bool ck_device()
      {
           
            FindDevice("0483", "5710" );	 
            return history;
      }
      /// <summary>
      /// 获取可用的HID的对象集合
      /// </summary>
      /// <returns></returns>
      public List<string> getGetHusbList()
      {
          List<string> list = new List<string>();
          
       //   GetHidDeviceList(ref list);//获取HID的USB设备列表
          return list;
      }
   
      #endregion

      #region   建立连接
      /// <summary>
      /// 打开指定信息的设备
      /// </summary> 
      /// <returns>成功：true  失败：false</returns>
      public bool OpenDevice( )
      {
          try
          {            
              Initialise();//初始化设备
          }
          catch (Exception ex)
          { 
              throw ex;
          }
          return IsOpen;
      }
      /// <summary>
      /// 初始化设备
      /// </summary>
      /// <param name="strPath">设备访问地址</param>
      private void Initialise( )
      { 
          m_hHandle = CreateFile(devicePath, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
          if (m_hHandle != InvalidHandleValue || m_hHandle == null)	 
          {
              IntPtr lpData;
              if (HidD_GetPreparsedData(m_hHandle, out lpData))	 
              {
                  try
                  {  
                      HidCaps oCaps;
                      HidP_GetCaps(lpData, out oCaps);	 //获取设备输入输出上限
                      m_nInputReportLength = oCaps.InputReportByteLength;	 //得到输入报文长度
                      m_nOutputReportLength = oCaps.OutputReportByteLength;  //得到输出报文长度    

                      m_oFile = new FileStream(new SafeFileHandle(m_hHandle, false), FileAccess.ReadWrite, m_nInputReportLength, true);
                     // m_oFile = new FileStream(new SafeFileHandle(m_hHandle, false), FileAccess.ReadWrite, inputReportLength, true);
                      IsOpen = true;
                      Thread th = new Thread(BeginAsyncRead);
                      th.Start();
                      
                  }
                  catch (Exception ex)
                  {
                      throw ex;
                  }
                  finally
                  {
                      HidD_FreePreparsedData(ref lpData);	// before we quit the funtion, we must free the internal buffer reserved in GetPreparsedData
                  }
              } 
          }
          else	 
          {
              m_hHandle = IntPtr.Zero; 
          }
      }


      public void Dispost()
      {
          //释放设备资源(hDevInfo是SetupDiGetClassDevs获取的)
          SetupDiDestroyDeviceInfoList(hDevInfo);
          //关闭连接(HidHandle是Create的时候获取的)
         // CloseHandle(HidHandle);
      }


  

      /// <summary>
      /// 关闭打开的设备
      /// </summary>
      public void CloseDevice()
      {
          if (history)
          {
           
              m_oFile.Close();
              IsOpen = false;
              history = false;
          }
      }
      #endregion
      #region   发送数据
      public void SendData(string sendMsg)
      {
        
             byte[] data = HexStringToByte(sendMsg);
              int Num=0;
              int totalLength=data.Length;
             int index = 0;
             while (sendMsg.Length > index)
             {
                 byte[] sData = new byte[61];
                 int DataLength = 0;
                 OutputReport oRep = new OutputReport();
                 if ((index + 61) <= data.Length)
                 { 
                     Array.Copy(data, index, sData,0, 61);
                     DataLength = 61;
                     index += 61;
                 }
                 else
                 {
                     DataLength = sendMsg.Length - index;
                     Array.Copy(data, index, sData, 0, sendMsg.Length - index); 
                     index += sendMsg.Length - index; 
                 } 
                 Num++;
                 oRep.SetData(sData, Num, totalLength, DataLength ); 
                 m_oFile.Write(oRep.Buffer, 0, oRep.BufferLength); 
             } 
           
          
      }
      /// <summary>
      /// 发送命令
      /// </summary>
      /// <param name="sendMsg"></param>
      public void SendData(byte[] data)
      {
          try
          { 
              int Num = 0;
              int totalLength = data.Length;
              int index = 0;
              while (data.Length > index)
              { 
                  byte[] sData = new byte[61];
                  int DataLength = 0;
                  OutputReport oRep = new OutputReport();
                  if ((index + 61) <= data.Length)
                  {
                      Array.Copy(data, index, sData, 0, 61);
                      DataLength = 61;
                      index += 61;
                  }
                  else
                  {
                      DataLength = data.Length - index;
                      Array.Copy(data, index, sData, 0, data.Length - index);
                      index += data.Length - index; 
                  }
                  Num++;
                 
                  oRep.SetData(sData, Num, totalLength  , DataLength); 
                  m_oFile.Write(oRep.Buffer, 0, oRep.BufferLength);
              } 
          }
          catch (IOException ex)
          {
              throw new Exception("设备通讯异常");
          }

      }
      #endregion
      #region   接收数据
      /// <summary> 
      ///异步读取
      /// </summary>
      private void BeginAsyncRead()
      {

          byte[] inputBuff = new byte[m_nInputReportLength];
          m_oFile.BeginRead(inputBuff, 0, m_nInputReportLength, new AsyncCallback(ReadCompleted),inputBuff); 
      }
      /// <summary>
      /// 回调，数据读完后的方法
      /// </summary>
      /// <param name="iResult">结果</param>
      protected void ReadCompleted(IAsyncResult iResult)
      {          
          byte[] arrBuff = (byte[])iResult.AsyncState;  // 检索读缓冲  
          //可以认为每个HID包，数据有包数量（1字节）+有效数据长度（2字节）+有效数据（61字节）
          try
          {
              string State = Encoding.UTF8.GetString(arrBuff).Replace("\0", null);
              if (string.IsNullOrEmpty(State))
              {
                  IsOpen = false;
              }
              else
              {
                  InputReport oInRep = new InputReport();	//创建设备输入报文
                  oInRep.SetData(arrBuff);	// 数据设置 
                  int SNum = (int)oInRep.Buffer[0];
                  if (SNum == 1)
                  {
                      TLength = Convert.ToInt32(oInRep.Buffer[1].ToString("X2") + oInRep.Buffer[2].ToString("X2"), 16);
                      TNum = TLength / 61;
                      if (TLength % 61 != 0)
                      {
                          TNum += 1;
                      }
                      Relist.Clear();
                      SNO = SNum;
                  }

                  Relist.Add(oInRep);
                  
                  if (Relist.Count == TNum)
                  {
                      string RetrData = "";

                      switch (cmdType)
                      {
                          case 1:
                              analyze(ref RetrData); 
                              GetType( SelectType(RetrData));                              
                              break;
                          case 2://PROGRAM   模式
                              analyze(ref RetrData);
                              int i = 0;
                              int.TryParse(RetrData, out i);
                              byte[] da = BitConverter.GetBytes(i);
                              byte[] Retda = new byte[1];
                              Array.Copy(da, 0, Retda, 0, 1);
                              cmdType = 3;
                              SendData(Retda);
                              break;
                          case 3:  // 随机数
                              analyze(ref RetrData);
                              DataResponse(RetrData);
                              break;
                          case 4:  //命令模式 
                              BDataResponse(analyze(ref RetrData));
                              break;
                          case 7:  //命令相应 
                              DataRecieved(oInRep.Buffer);
                              break;
                          case 8://进入测试模式 
                          case 9: 
                          case 10: 
                          case 11: 
                          case 12: 
                          case 13:
                              byte[] Retdas = new byte[TLength];
                              Array.Copy(arrBuff,4, Retdas, 0, TLength);
                              if (Retdas.Length == 1 && Retdas[0] == 6)
                              {
                                  RetrData = "6";
                              }
                              else
                              {
                                  RetrData = Encoding.UTF8.GetString(Retdas);
                              }
                             
                              AnalyzeTest(RetrData);
                              break;
                          default  :
                              break;
                        }
                  }          
                  BeginAsyncRead();
              }
            
          } 
          catch (IOException ex)
          {
             throw ex;

          }
          //finally
          //{
          //    BeginAsyncRead();	// 继续读取
          //}
      }

      private string  SelectType(string type)
      {
          switch (type)
          {
              case "3001":
                  dType = "M300-TH_0";
                  break;
              case "3002":
                  dType = "M300-T_0";
                  break;
              case "3003":
                  dType = "M300-T_T";
                  break;
              case "3004":
                  dType = "M300-TH_T";
                  break;
              default:
                  break;


          }
          return dType;
      }

      public byte[] analyze(ref  string RetrData )
      {
          byte[] ReData = null;
        //去除长度 
         byte[]  data=new byte[TLength];
         // 去除序号
         int i = 0;
         foreach (InputReport  model in Relist)
         {

             byte[] xdata = model.Buffer;
             if (TNum == 1)
             {
                 Array.Copy(xdata, 3, data, 0, TLength);
             }
             else
             {
                 Array.Copy(xdata, 3, data, 0, 61);
             }  
              TNum--;  
         }
         //命令解析
         if (data.Length > 0)
          {
              switch (cmdType)
              {
                  case 1:
                      if (data[0].ToString() == "37" && data[data.Length - 1].ToString() == "37")//获取设备类型
                      { 
                          RetrData = data[1].ToString("X") + data[2].ToString("X").PadLeft(2, '0');
                      }
                      break;
                  case 2:
                      RetrData = data[0].ToString();
                      break;
                  case 3:
                      RetrData = data[0].ToString();
                      break;
                  case 4:


                      int address = Convert.ToInt32(data[1].ToString()) * 256 + Convert.ToInt32(data[2].ToString()) ;
                      int Length = Convert.ToInt32(data[3].ToString());
                      byte[]  bVal=new    byte[Length] ; 
                      Array.Copy(data,4,bVal,0,Length);
                     
                         RetrData = Encoding.UTF8.GetString(bVal).Replace("\0", null);
                     
                    
                      ReData = bVal;
                      break;              

              }          
         }          
          //返回有用数据
         return ReData;
      }
      public void AnalyzeTest(   string RetrData)
      {
          try
          {        
              switch (cmdType)
              {
                  case 8://进入测试模式
                      if (RetrData.IndexOf("Enter test mode!") >= 0)
                      {
                          TestMsg("进入测试模式成功", false);
                      }
                      break;
                  case 9://测试按键
                      if (RetrData.IndexOf("Please press key!")>=0 )
                      {
                          TestMsg("请按按键... ", true);
                      }
                      else if (RetrData == "6")//按键成功
                      {
                          TestMsg(" 按键OK ", false);
                      }
                      else
                      {
                          TestMsg("请按按键... ", true);
                      }

                      break;
                  case 10://测试LCD显示
                      if (RetrData == "6")//按键成功
                      {
                          TestMsg("LCD测试OK ", false);
                      }
                      break;
                  case 11://测试探头
                      if (RetrData == "6")//按键成功
                      {
                          TestMsg("探头测试完成 ", false);
                      }
                      else
                      {
                          TestMsg("数据：" + RetrData, true);
                      }
                      break;
                  case 12://测试 电池电量 
                      if (RetrData == "6")
                      {
                          TestMsg(" 电池电量完成  ", false);
                      }
                      else
                      {
                          TestMsg("数据：" + RetrData, true);
                      }
                      break;
                  case 13://测试modem 
                      if (RetrData == "6")
                      {
                          TestMsg("Modem测试完成 ", false);
                      }
                      else
                      {
                          TestMsg(RetrData, true);
                      }
                      break;             
              }
          }
          catch (Exception ex)
          {

          }
      }
      



      #endregion 
      #region 数据转换

      /// <summary>
      /// btye数组转换为字符串
      /// </summary>
      /// <param name="bytes"></param>
      /// <returns></returns>
      public static string ByteToHexString(byte[] bytes)
      {
          string str = string.Empty;
          if (bytes != null)
          {
              for (int i = 0; i < bytes.Length; i++)
              {
                  str += bytes[i].ToString("X2");
              }
          }
          return str;
      }

      /// <summary>
      /// btye数组转换为字符串
      /// </summary>
      /// <param name="bytes"></param>
      /// <returns></returns>
      public static byte[] HexStringToByte(string msg)
      {
          byte[] bstr = new byte[1024];
             bstr = Encoding.UTF8.GetBytes(msg);
          return bstr;
      }
      #endregion

      #region  操作方法
      /// <summary>
      ///搜索USB设备通过vid和pid
      /// </summary>
      /// <param name="nVid">供应商ID</param>
      /// <param name="nPid">产品ID</param>
      public   void FindDevice(string nVid, string nPid )
      {
          bool ckflag = false;
          string strPath = string.Empty; 
          string strSearch = string.Format("vid_{0}&pid_{1}", nVid, nPid); // first, build the path search string
          Guid gHid = HIDGuid;
          IntPtr hInfoSet = SetupDiGetClassDevs(ref gHid, null, IntPtr.Zero, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);	//得到当前的hid设备
          try
          {
              DeviceInterfaceData oInterface = new DeviceInterfaceData();	// 建立一个设备接口的数据块
              oInterface.Size = Marshal.SizeOf(oInterface);
              //遍历所有的hid接口得到所需的hid接口
              int nIndex = 0;
             
              while (SetupDiEnumDeviceInterfaces(hInfoSet, 0, ref gHid, (uint)nIndex, ref oInterface))	// 获取hid设备描述
              {
                  string strDevicePath = GetDevicePath(hInfoSet, ref oInterface);	//获取hid设备信息集合中访问路径
                  if (strDevicePath.IndexOf(strSearch) >= 0)
                  {
                      devicePath = strDevicePath;
                      ckflag = true;
                      break;
                  }
                  nIndex++;
              }
              history = ckflag;
          }
          catch (Exception ex)
          {
              devicePath = "";
              history = false;
          }
         
           
      }
      /// <summary>
      
      /// 用于返回在hid设备信息集合中返回访问路径
      /// </summary>
      /// <param name="hInfoSet">处理的信息集</param>
      /// <param name="oInterface">设备接口结构/参数</param>
      /// <returns>设备的访问路径</returns>
      private static string GetDevicePath(IntPtr hInfoSet, ref DeviceInterfaceData oInterface)
      {
          uint nRequiredSize = 0;
          // 获取设备接口的描述
          if (!SetupDiGetDeviceInterfaceDetail(hInfoSet, ref oInterface, IntPtr.Zero, 0, ref nRequiredSize, IntPtr.Zero))
          {
              DeviceInterfaceDetailData oDetail = new DeviceInterfaceDetailData();
              oDetail.Size = 5; 
              if (SetupDiGetDeviceInterfaceDetail(hInfoSet, ref oInterface, ref oDetail, nRequiredSize, ref nRequiredSize, IntPtr.Zero))
              {
                  return oDetail.DevicePath;
              }
          }
          return null;
      }
      public void Test(string sendMsg)
      {

          //byte[] data = HexStringToByte(sendMsg);
          //int Num = 0;
          //int totalLength = data.Length;
          //int index = 0;
          //while (sendMsg.Length > index)
          //{
          //    byte[] sData = new byte[61];
          //    int DataLength = 0;
          //    OutputReport oRep = new OutputReport();
          //    if ((index + 61) <= data.Length)
          //    {
          //        Array.Copy(data, index, sData, 0, 61);
          //        DataLength = 61;
          //        index += 61;
          //    }
          //    else
          //    {
          //        DataLength = sendMsg.Length - index;
          //        Array.Copy(data, index, sData, 0, sendMsg.Length - index);
          //        index += sendMsg.Length - index;

          //    }
          //    Num++;
          //    oRep.SetData(sData, Num, totalLength, DataLength); 
          //}
          //RetVal = new byte[sendMsg];
      }

      /** 
   * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序，和和intToBytes（）配套使用
   *  
   * @param src 
   *            byte数组 
   * @param offset 
   *            从数组的第offset位开始 
   * @return int数值 
   */
      public static int bytesToInt(byte[] src, int offset,int length)
      {
          byte[]  data=new byte[4] ;


          Array.Copy(src, 0, data, 0, length);
          int value;
          value = (int)((src[offset] & 0xFF)
                  | ((src[offset + 1] & 0xFF) << 8)
                  | ((src[offset + 2] & 0xFF) << 16)
                  | ((src[offset + 3] & 0xFF) << 24));
          return value;
      }

      
      #endregion
  }
 

}
