﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data; 
namespace InputData
{
    public class MssqlHelper
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public static readonly string connectionString =  ConfigurationManager.AppSettings["MSsqlSystem"].ToString(); 

        /// <summary>
        /// 执行超时
        /// </summary>
        public static int cmdTimeout = 60;

        /// <summary>
        /// 执行sql语句返回影响的行
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="cmdType"></param>
        /// <param name="cmdText"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
             SqlCommand cmd = null; 
             SqlConnection conn=null ;
            try
            { 
                cmd = new SqlCommand();
                using (  conn = new SqlConnection(connectionString))
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                    int val = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    return val;
                }
            }
            catch (Exception ex)
            { 
                throw ex ;
            }
            finally
            {
                
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                } 
                
            }
        }

        /// <summary>
        /// 内部方法
        /// </summary>
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = cmdText;
                cmd.CommandTimeout = cmdTimeout;
                if (trans != null)
                    cmd.Transaction = trans;

                cmd.CommandType = cmdType;

                if (cmdParms != null)
                {
                    foreach (SqlParameter parm in cmdParms)
                        cmd.Parameters.Add(parm);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
             


               
            
        }

        /// <summary>
        /// 返回datatable
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="cmdText"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static DataTable ExecuteTable(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = null;
            SqlConnection conn  = null;
            try
            {
                  cmd = new SqlCommand();
                using (conn = new SqlConnection(connectionString))
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet, "Result");
                    cmd.Parameters.Clear();
                    if (dataSet.Tables.Count > 0)
                    {
                        return dataSet.Tables[0];
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            { 
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
          
        }

        /// <summary>
        /// 执行sql语句，返回sqldatareader
        /// </summary>
        public static SqlDataReader ExecuteReader(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {

            SqlCommand cmd = null;
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(connectionString);
                cmd = new SqlCommand();
                PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear(); 
                return rdr;
            }
            catch(Exception ex)
            {
                 
                throw ex;
            }
            finally
            {
               
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        /// <summary>
        /// 执行sql语句返回首行首列
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlConnection conn=null;
            SqlCommand cmd=null;
            try
            {
                  cmd = new SqlCommand();
                using (conn = new SqlConnection(connectionString))
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                    object val = cmd.ExecuteScalar();
                    cmd.Parameters.Clear();
                    return val;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
               
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
          
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static int ExecuteSqlTran(List<String> SQLStringList)
        {
            SqlCommand cmd = null;
            SqlConnection conn = null;
            try
            {
                using ( conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                     cmd = new SqlCommand();
                     cmd.Connection = conn;
                    SqlTransaction tx = conn.BeginTransaction();
                    cmd.Transaction = tx;
                    try
                    {
                        int count = 0;
                        for (int n = 0; n < SQLStringList.Count; n++)
                        {
                            string strsql = SQLStringList[n];
                            if (strsql.Trim().Length > 1)
                            {
                                cmd.CommandText = strsql;
                                count += cmd.ExecuteNonQuery();
                                tx.Save("SaveUcard");
                            }
                        }
                        tx.Rollback("SaveUcard");
                        tx.Commit();
                        return count;
                    }
                    catch (Exception e)
                    {
                        tx.Rollback();
                        return 0;
                        throw e;
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
               
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

           
        }

        public static DataSet RunProcedurePage(string storedProcName, IDataParameter[] parameters)
        {
            SqlConnection conn = null;

            try
            {
                using (  conn  = new SqlConnection(connectionString))
                {
                    DataSet dataSet = new DataSet();
                    conn.Open();
                    SqlDataAdapter sqlDA = new SqlDataAdapter();
                    sqlDA.SelectCommand = BuildQueryCommand(conn, storedProcName, parameters);
                    sqlDA.Fill(dataSet, "puntable"); 
                    DataTable table = new DataTable();
                    DataColumn dc = null;
                    DataRow dr = table.NewRow(); 
                    foreach (SqlParameter parameter in parameters)
                    {
                        if (parameter != null)
                        {

                            if (parameter.Direction == ParameterDirection.Output)
                            {
                                dc = table.Columns.Add(parameter.ParameterName.Substring(1), Type.GetType("System.String"));
                                dr[parameter.ParameterName.Substring(1)] = parameter.Value;
                            } 
                        }
                    }
                    table.Rows.Add(dr);
                    dataSet.Tables.Add(table);
                    return dataSet;
                }
            }
            catch (Exception ex)
            { 
                throw ex; 
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            
        }


        /// <summary>
        /// 构建 SqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>SqlCommand</returns>
        private static SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in parameters)
            {
                if (parameter != null)
                {
                    // 检查未分配值的输出参数,将其分配以DBNull.Value.
                    if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                        (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    command.Parameters.Add(parameter);
                    
                }
            }

            return command;
        }

        
    }
}
