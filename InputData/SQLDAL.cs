﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace InputData
{
    public class SQLDAL
    {
        public int InputData(string ID, string rID, DateTime begintime, DateTime endtime, int max, int min, int In, int bd)
        {
            int rs = 0;
            double oldVle = 0;

            for (; begintime < endtime; begintime = begintime.AddMinutes(In))
            {
                double newVle = 0;
                do
                {
                    Random r = new Random();
                    int i = r.Next(min, max);
                    newVle = (double)i / 100;
                } while (oldVle != 0 && Math.Abs(newVle - oldVle) > bd);
                newVle = Math.Round(newVle, 1);
                oldVle = Math.Round(newVle, 1);
                string sql = "INSERT INTO  TerminalRecord ([Address],[Value],[Intime],[TermanilID],[nAlarm],[ValType])  VALUES (" + rID + "," + newVle + ",'" + begintime + "'," + ID + ",0,0 )";
                MssqlHelper.ExecuteNonQuery(System.Data.CommandType.Text, sql, null);
            }
            return rs;
        }
        public int deletData(string ID, string rID, DateTime begintime, DateTime endtime )
        {
            int rs = 0;

            string sql = "delete  TerminalRecord where TermanilID=" + ID + " and Address=" + rID + " and Intime  between '" + begintime + "' and '" + endtime+"'";
            MssqlHelper.ExecuteNonQuery(System.Data.CommandType.Text, sql, null); 
            return rs;
        }
        public int deletData( )
        {
            int rs = 0; 
            string sql = "delete  TerminalRecord  ";
            MssqlHelper.ExecuteNonQuery(System.Data.CommandType.Text, sql, null);

            return rs;
        }
        public int deletData(string ID)
        {
            int rs = 0;
            string sql = "delete  TerminalRecord where TermanilID=" + ID;
            MssqlHelper.ExecuteNonQuery(System.Data.CommandType.Text, sql, null);

            return rs;
        }
        public int deletData(DateTime begintime, DateTime endtime)
        {
            int rs = 0;
            string sql = "delete  TerminalRecord   where  Intime  between '" + begintime + "' and '" + endtime+"'";
            MssqlHelper.ExecuteNonQuery(System.Data.CommandType.Text, sql, null);

            return rs;
        }
     
    }
}
