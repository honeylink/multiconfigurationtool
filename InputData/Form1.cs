﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InputData
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        SQLDAL sqlDal = new SQLDAL();
        private void button1_Click(object sender, EventArgs e)
        {
            string ID = txtDeviceID.Text.Trim();
            string rID = cmbType.Text.Trim() == "温度" ? "1" : cmbType.Text.Trim() == "湿度" ? "2" : cmbType.Text.Trim() == "湿度"?"3":"1";
            string beginTime = beginDate.Text.Trim();
            string endTime = EndDate.Text.Trim();
            string Max = txtMax.Text.Trim() == "" ? "0" : txtMax.Text.Trim();
            string Min = txtMin.Text.Trim() == "" ? "0" : txtMin.Text.Trim();
            string BDT=txtBD.Text.Trim();
            string interval = txtInterval.Text.Trim();
        
            DateTime begin=DateTime.Parse(beginTime);
            DateTime end =DateTime.Parse(endTime);
            double mx=0;
            double mn=0;
            double.TryParse(Max,out mx);
            double.TryParse(Min,out mn);
            int MaxVal=(int)mx*100;
            int MinVal=(int)mn*100;
            int ins  = int.Parse(interval);
            int bd  = int.Parse(BDT);
            int i=  sqlDal.InputData(ID, rID, begin, end, MaxVal, MinVal, ins, bd);
           MessageBox.Show("影响行数"+i);
        }

        private void button2_Click(object sender, EventArgs e)
        {
           string ID = txtDeviceID.Text.Trim();
           int N = sqlDal.deletData(ID);
           if (N >-1)
           {
               MessageBox.Show("删除成功,影响行数" + N);
           }
           else
           {
               MessageBox.Show("删除失败");
           }
          
        }

        private void button3_Click(object sender, EventArgs e)
        {

            string TID=this.txtDeviceID.Text.Trim();
            string AddID = this.cmbType.SelectedText.Trim();
            string beginTime = beginDate.Text.Trim();
            string endTime = EndDate.Text.Trim();
            DateTime begin = DateTime.Parse(beginTime);
            DateTime end = DateTime.Parse(endTime);
            int N = sqlDal.deletData(begin, end, TID, AddID);
            if (N > -1)
            {
                MessageBox.Show("删除成功,影响行数" + N);
            }
            else
            {
                MessageBox.Show("删除失败");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            string ID = txtDeviceID.Text.Trim();
            string beginTime = beginDate.Text.Trim();
            string endTime = EndDate.Text.Trim();
            DateTime begin = DateTime.Parse(beginTime);
            DateTime end = DateTime.Parse(endTime);
            string rID = cmbType.Text.Trim() == "温度" ? "1" : cmbType.Text.Trim() == "湿度" ? "2" : cmbType.Text.Trim() == "湿度" ? "3" : "1";
            int N = sqlDal.deletData(ID, rID,begin, end);
            if (N > -1)
            {
                MessageBox.Show("删除成功,影响行数" + N);
            }
            else
            {
                MessageBox.Show("删除失败");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string ID = txtDeviceID.Text.Trim();
            int N= sqlDal.deletData();
            if (N > -1)
            {
                MessageBox.Show("删除成功,影响行数" + N);
            }
            else
            {
                MessageBox.Show("删除失败");
            }
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
